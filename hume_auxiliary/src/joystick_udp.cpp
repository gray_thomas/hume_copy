#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "joystick_udp.hpp"

#define BUFLEN 1024
namespace uta{
namespace ui{

struct sockaddr_in si_other;
int slen;

int s = 0;
int indexOfMessage = 0;
void send_udp(JoystickState *pMsg, const char * sendAddress)
{
	if (s == 0)
	{
		if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			return;
		si_other.sin_addr.s_addr = inet_addr(sendAddress);

	}

	struct sockaddr_in si_to;
	memset((char *) &si_to, 0, sizeof(si_to));
	si_to.sin_family = AF_INET;
	si_to.sin_port = htons(JOYSTICK_PORT);
	si_to.sin_addr.s_addr = inet_addr(sendAddress); // this is the meka ip address

	slen = sizeof(si_to);

	int ret = sendto(s, pMsg, sizeof(JoystickState), 0, (const struct sockaddr*) &si_to, sizeof(si_to));
	if (indexOfMessage % 100 == 0)
		fprintf(stderr, "Size of message is %d, ADDR: %08x(%d:%d %d)\n", (int) sizeof(JoystickState),si_to.sin_addr.s_addr, slen, (int) sizeof(si_to), ret);
	++indexOfMessage;
}

}
}
