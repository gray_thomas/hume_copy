/*
 * JoystickReporter.cpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


//#include <arpa/inet.h>
//#include <fcntl.h>
//#include <netinet/in.h>
#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <sys/socket.h>
//#include <sys/stat.h>
//#include <sys/types.h>
#include <unistd.h>
#include <iostream>
//#include <stdexcept>

//#include "Daemon.hpp"
//#include "joystick_udp.hpp"
#include "JoystickReceiver.hpp"

using namespace std;
using namespace uta::ui;
using namespace uta::threads;
int main(void)
{
	JoystickReader reader;
	reader.start();
	cout << "Hello World Joystick Reporter!!!" << endl; /* prints Hello World!!! */
	pause();

	puts("Terminated.");
	return 0;
}
