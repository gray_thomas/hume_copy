/*
 * JoystickReceiver.cpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "JoystickReceiver.hpp"
#include <iostream>
#include <stdexcept>
using namespace std;
namespace uta
{
namespace ui
{
ButtonLatch::ButtonLatch() :
			wasPressed(false),
			isClick(false),
			isRelease(false)
{
}
void ButtonLatch::update(bool isPressed)
{
	isClick = (isPressed && (!wasPressed));
	isRelease = ((!isPressed) && wasPressed);
	wasPressed=isPressed;
}
bool ButtonLatch::isClicked()
{
	return isClick;
}
bool ButtonLatch::isReleased()
{
	return isRelease;
}
void JoystickReader::latchButtons()
{
	for (int i=0;i<14;i++)
		latches[i].update(mostRecentMessage.buttons[i]);
	latches[14].update(mostRecentMessage.axes[5]<0);
	latches[15].update(mostRecentMessage.axes[6]<0);
	latches[16].update(mostRecentMessage.axes[5]>0);
	latches[17].update(mostRecentMessage.axes[6]>0);
}
bool JoystickReader::GetButtonClick(int i)
{
	if (i<0||i>17)
		throw runtime_error("button index does not exist");
	return latches[i].isClicked();
}
void JoystickReader::handleUpdate()
{
//	cout << "received state ";
//	for (int i = 0; i < 14; i++)
//		cout << (mostRecentMessage.buttons[i] ? "x" : "o");
//	for (int i = 0; i < 7; i++)
//		cout << " " << ((double) mostRecentMessage.axes[i]) / 32767.0;
//	cout << endl;
}
const JoystickState& JoystickReader::getJoystick()
{
	return mostRecentMessage;
}
void JoystickReader::loop()
{
	if (isFirstLoop())
	{
		socketAddressStructureLength = sizeof(remoteSocketAddress);
		if ((socketInteger = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		{
			printf("failed to create socket\n");
			return;
		}

		memset((char *) &mySocketAddress, 0, sizeof(mySocketAddress));
		mySocketAddress.sin_family = AF_INET;
		mySocketAddress.sin_port = htons(JOYSTICK_PORT);

		mySocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
		bind(socketInteger, (const sockaddr*) &mySocketAddress, (socklen_t) sizeof(mySocketAddress));

	}

	int flags = 0;
	receivedDataSize = recvfrom(socketInteger, (void*) &mostRecentMessage, sizeof(JoystickState), flags,
			(sockaddr*) &remoteSocketAddress, (socklen_t*) &socketAddressStructureLength);
	handleUpdate();
}

}
}
