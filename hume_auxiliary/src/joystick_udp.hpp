#ifndef COMMON_UDP_H
#define COMMON_UDP_H
//#ifdef __cplusplus
//extern "C"
//{
//#endif

#define CMD_CALIBRATE				0x01000000
#define CMD_SET_TORQUE				0x02000000
#define CMD_SET_MODE				0x03000000
#define CMD_SET_MODE_RAW_TORQUE		0x03000001
#define CMD_SET_MODE_COMP_TORQUE	0x03000002

#define CMD_DATA_LEN 29

#define NOTIFY_ADDR             "192.168.1.139"
#define NOTIFY_PORT             51123
#define CMD_PORT                51124
#define STT_PORT                51125



#include <stdint.h>
#include "JoystickState.hpp"

namespace uta{
namespace ui{

extern void send_udp(JoystickState *pBuf, const char* ipAddress);

}
}
//#ifdef __cplusplus
//}
//#endif
#endif
