/*
 * JoystickReceiver.hpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef JOYSTICKRECEIVER_HPP_
#define JOYSTICKRECEIVER_HPP_
#include <JoystickState.hpp>
#include <Daemon.hpp>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
namespace uta
{
namespace ui
{

class ButtonLatch
{
	bool wasPressed;
	bool isClick;
	bool isRelease;
public:
	ButtonLatch();
	void update(bool isPressed);
	bool isClicked();
	bool isReleased();
};

class JoystickReader: public uta::threads::Daemon
{
	struct sockaddr_in mySocketAddress, remoteSocketAddress;
	int count;
	int receivedDataSize;
	int socketInteger;
	int socketAddressStructureLength;
protected:
	ButtonLatch latches[18];
	JoystickState mostRecentMessage;
	virtual void handleUpdate();
	void loop();
public:
	void latchButtons();
	bool GetButtonClick(int i);
	const JoystickState& getJoystick();
	~JoystickReader()
	{
	}
};
}
}

#endif /* JOYSTICKRECEIVER_HPP_ */
