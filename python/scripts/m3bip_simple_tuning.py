#! /usr/bin/python

# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.


import time
import math
import glob 

import m3.gui as m3g
import m3.rt_proxy as m3p
import m3.toolbox as m3t
import m3.component_factory as m3f

import m3bip.bip_ctrl_simple_pb2 as mec
import m3bip.bip_ctrl_simple as m3s

import m3bip.bip_actuator_ec as m3aec
import m3bip.bip_actuator as m3a

class M3Proc:
	def __init__(self):
		self.proxy = m3p.M3RtProxy()
		self.gui = m3g.M3Gui(stride_ms=125)
		self.cnt=0
		self.bias=[]
		
	def stop(self):
		self.ctrl.set_mode_off()
		self.proxy.step()
		self.proxy.stop()
		
	def start(self):
		self.proxy.start()
		
		cnames = self.proxy.get_available_components('m3bip_ctrl_simple')
		self.names=m3t.user_select_components_interactive(cnames)
		if len(self.names) == 0:
			return

		if len(self.names) > 1:
			print "Pick only one"
			return
		
		self.comp_name = self.names[0]
		self.ctrl		= m3s.M3BipCtrlSimple(self.comp_name)
		
		self.proxy.subscribe_status(self.ctrl)
		self.proxy.publish_command(self.ctrl) 
		self.proxy.publish_param(self.ctrl) 

		# get the corresponding ec comp.
		self.act_name	= 'm3bip_actuator_'+self.comp_name.split('_')[3]
		self.ec_name	= 'm3bip_actuator_ec_'+self.comp_name.split('_')[3]
		self.act		= m3a.M3BipActuator(self.act_name)
		self.ec			= m3aec.M3BipActuatorEc(self.ec_name)

		self.proxy.subscribe_status(self.ec)
		self.proxy.publish_param(self.ec)

		self.proxy.subscribe_status(self.act)
		self.proxy.publish_param(self.act)


		self.proxy.make_operational(self.comp_name)
		self.proxy.make_operational(self.act_name)
		self.proxy.make_operational(self.ec_name)

		
		pwr_rt='m3pwr_pwr016'
		pwr_ec='m3uta_pwr_ec_016'
		
		self.pwr=m3f.create_component(pwr_rt)
		self.proxy.publish_command(self.pwr)
		self.proxy.make_operational(pwr_rt)
		self.proxy.make_operational(pwr_ec)

		self.proxy.make_operational_all()
		self.pwr.set_motor_power_on()

		
		self.proxy.step()

		#Create gui
		self.mode = [0]
		self.traj = [0]
		
		self.motor_torque_desired	= [0]
		self.joint_torque_desired	= [0]
		
		self.motor_theta_desired	= [0]
		self.joint_theta_desired	= [0]
		self.joint_stiffness		= [0]
		
		
		self.save		= False
		self.save_last	= False
		self.do_scope	= False
		self.scope		= None
		
		# extract status fields
		self.status_dict = self.proxy.get_status_dict()
		
		self.scope_keys=m3t.get_msg_fields(self.ctrl.status)
		self.scope_keys.sort()
		self.scope_keys		= ['None']+self.scope_keys
		self.scope_field1	= [0]
		self.scope_field2	= [0]
		

		self.zero_motor_theta_last	= False
		self.zero_motor_theta_flag	= False
		
		self.zero_spring_theta_last	= False
		self.zero_spring_theta_flag	= False
		
		self.zero_joint_theta_last	= False
		self.zero_joint_theta_flag	= False


		motor_torque_max = 0.3
		joint_torque_max = 100.0

		motor_theta_max	= 700.0
		joint_theta_max	= 2.0

		self.param_dict=self.proxy.get_param_dict()

		self.zero_motor_theta	= self.param_dict[self.act_name]['calibration']['zero_motor_theta']
		self.zero_spring_theta	= self.param_dict[self.act_name]['calibration']['zero_spring_theta']
		self.zero_joint_theta	= self.param_dict[self.act_name]['calibration']['zero_joint_theta']


		self.gui.add('M3GuiTree',   'Status',    (self,'status_dict'),[],[],m3g.M3GuiRead,column=2)
		self.gui.add('M3GuiTree',   'Param',   (self,'param_dict'),[],[],m3g.M3GuiWrite,column=3)
		self.gui.add('M3GuiModes',  'Mode',      (self,'mode'),range(1),[[	'Off','Motor Torque','Motor Theta',
																			'Joint Torque','Joint Theta'],1],m3g.M3GuiWrite)
		self.gui.add('M3GuiModes',  'Traj',      (self,'traj'),range(1),[['Off','Motor Torque Square','Motor Torque Sine',
																			'Motor Theta Square','Motor Theta Sine',
																			'Joint Torque Square','Joint Torque Sine',
																			'Joint Theta Square','Joint Theta Sine'],1],m3g.M3GuiWrite)

		self.gui.add('M3GuiSliders','Motor Torque (Nm)',	(self,'motor_torque_desired'),range(1),[-motor_torque_max,motor_torque_max],m3g.M3GuiWrite)
		self.gui.add('M3GuiSliders','Joint Torque (Nm)',	(self,'joint_torque_desired'),range(1),[-joint_torque_max,joint_torque_max],m3g.M3GuiWrite)
		self.gui.add('M3GuiSliders','Motor Theta (rad)',	(self,'motor_theta_desired'),range(1),[-motor_theta_max,motor_theta_max],m3g.M3GuiWrite) 
		self.gui.add('M3GuiSliders','Joint Theta (rad)',	(self,'joint_theta_desired'),range(1),[-joint_theta_max,joint_theta_max],m3g.M3GuiWrite) 
		self.gui.add('M3GuiSliders','Joint Stiffness (%)',	(self,'joint_stiffness'),range(1),[0,100],m3g.M3GuiWrite) 
		self.gui.add('M3GuiToggle', 'Save',					(self,'save'),[],[['On','Off']],m3g.M3GuiWrite)

		self.gui.add('M3GuiToggle', 'Zero Motor Theta',		(self,'zero_motor_theta_flag'),	[],[['On','Off']],m3g.M3GuiWrite)
		self.gui.add('M3GuiToggle', 'Zero Spring Theta',	(self,'zero_spring_theta_flag'),	[],[['On','Off']],m3g.M3GuiWrite)
		self.gui.add('M3GuiToggle', 'Zero Joint Theta',		(self,'zero_joint_theta_flag'),	[],[['On','Off']],m3g.M3GuiWrite)

		self.gui.add('M3GuiModes',  'Scope1',				(self,'scope_field1'),range(1),[self.scope_keys,1],m3g.M3GuiWrite)
		self.gui.add('M3GuiModes',  'Scope2',				(self,'scope_field2'),range(1),[self.scope_keys,1],m3g.M3GuiWrite)
		self.gui.add('M3GuiToggle', 'Scope',				(self,'do_scope'),[],[['On','Off']],m3g.M3GuiWrite)
		self.gui.start(self.step)

			
		
	def step(self):

		if self.do_scope and self.scope is None:
			self.scope=m3t.M3Scope2(xwidth=100,yrange=None)
			
		self.proxy.step()
		self.cnt=self.cnt+1
		self.status_dict=self.proxy.get_status_dict()


		if self.zero_motor_theta_flag and not self.zero_motor_theta_last:
			self.zero_motor_theta = -self.act.get_motor_theta_raw()
			print 'New motor theta zero',self.zero_motor_theta
			
#		if self.zero_motor_current_flag and not self.zero_motor_current_last:
#			self.zero_motor_current -= self.act.get_motor_current_raw()
#			print 'New motor current zero',self.zero_motor_current
		
		if self.zero_spring_theta_flag and not self.zero_spring_theta_last:
			self.zero_spring_theta = -self.act.get_spring_raw()
			print 'New spring zero',self.zero_spring_theta
		
		if self.zero_joint_theta_flag and not self.zero_joint_theta_last:
			self.zero_joint_theta = -self.act.get_joint_raw()
			print 'New joint zero',self.zero_joint_theta	


		self.zero_motor_theta_last		= self.zero_motor_theta_flag
		self.zero_spring_theta_last		= self.zero_spring_theta_flag
		self.zero_joint_theta_last		= self.zero_joint_theta_flag


		self.param_dict[self.act_name]['calibration']['zero_motor_theta']		= self.zero_motor_theta
		self.param_dict[self.act_name]['calibration']['zero_spring_theta']		= self.zero_spring_theta
		self.param_dict[self.act_name]['calibration']['zero_joint_theta']		= self.zero_joint_theta



		self.proxy.set_param_from_dict(self.param_dict)

		
		if self.do_scope and self.scope is not None:
			f1=self.scope_keys[self.scope_field1[0]]
			f2=self.scope_keys[self.scope_field2[0]]
			x1=x2=None
			if f1!='None' and f1!='base':
				x1=m3t.get_msg_field_value(self.ctrl.status,f1)
				print f1,':',x1
			if f2!='None' and f2!='base':
				x2=m3t.get_msg_field_value(self.ctrl.status,f2)   
				print f2,':',x2
			if x1==None:
				x1=x2
			if x2==None:
				x2=x1
			if x1!=None and x2!=None: #Handle only one value or two
				self.scope.plot(x1,x2)
				print'-----------------'



		motor_torque	=self.motor_torque_desired[0]		
		motor_theta		=self.motor_theta_desired[0]
		joint_torque	=self.joint_torque_desired[0]
		joint_theta		=self.joint_theta_desired[0]
		

		
		self.ctrl.set_traj_mode(self.traj[0])
			

		if self.mode[0]==0:
			self.ctrl.set_mode_off()
			
		# motor related modes
		elif self.mode[0]==1:
			self.ctrl.set_mode_motor_torque()
			self.ctrl.set_motor_torque(motor_torque)
			
		elif self.mode[0]==2:
			self.ctrl.set_mode_motor_theta()
			self.ctrl.set_motor_theta(motor_theta)
			
		# joint related modes
		elif self.mode[0]==3:
			self.ctrl.set_mode_joint_torque()
			self.ctrl.set_joint_torque(joint_torque)
			
		elif self.mode[0]==4:
			self.ctrl.set_mode_joint_theta()
			self.ctrl.set_joint_theta(joint_theta)
			self.ctrl.set_joint_stiffness(self.joint_stiffness[0]/100.0)
		
		else:
			self.ctrl.set_mode_off()
		
		
		
		if (self.save and not self.save_last):
			self.ctrl.write_config()
			self.act.write_config()
			self.ec.write_config()

		self.save_last=self.save


if __name__ == '__main__':
	t=M3Proc()
	try:
		t.start()
	except (KeyboardInterrupt,EOFError):
		pass
	t.stop()



