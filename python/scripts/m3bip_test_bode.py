#!/usr/bin/python

# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.

import time
import sys
from m3bip_testunit import M3BipTest
import m3bip.lcj_signal_gen_pb2

class M3BipTestBode(M3BipTest):
    #def __init__(self):
    #    print "I am a bode test"
    #    M3LcjTest.__init()

    def extrainit(self):
        self.parser.add_option('-b','--beginning_frequency',dest='begin_freq',
                               type=float,default=.1)
        self.parser.add_option('-e','--end_frequency',dest='end_freq',type=float,default=10)
        self.parser.add_option('-a','--amplitude',dest='amplitude',type=float,default=.5)
        self.parser.add_option('-m','--mode',dest='mode',type=str,default='motor_torque')
        self.parser.add_option('-t','--traj_mode',dest='traj_mode',default=1,type=int)

    def extraparse(self):
        # arguments for trajectory
        control_mode_map = {'motor_torque': 1, 'motor_velocity':5, 'joint_torque':3, 'joint_theta':4}
        self.comps['ctrl'].command.ctrl_mode = control_mode_map[self.opts.mode]
        self.comps['ctrl'].command.traj_mode = self.opts.traj_mode
        pass

    def print_fun(self):
        return [time.time()-self.tstart]


if __name__ == "__main__":
    t = M3BipTestBode()
    t.run()

#    sys.exit()

# try:
#     for i in range(10):
#         proxy.step()
# 	time.sleep(.1)
#     pzero = signal_gen.param.trajectory_param.zero
#     zero = comp.get_motor_theta_rad()
#     joint_angle_zero = comp.get_joint_theta_rad()
#     if opts.joint_angle_mode:
#         signal_gen.param.trajectory_param.zero = 0.0 #joint_angle_zero + pzero
#         comp.param.joint_theta_traj.zero = joint_angle_zero
#         comp.param.motor_theta_traj.zero = zero
#     elif opts.velocity_mode:
# 	zero = 0.0
	
#     else:
#         signal_gen.param.trajectory_param.zero = zero + pzero
    


    
#     time.sleep(.1)

#     proxy.start_log_service(logname,sample_freq_hz=opts.log_frequency
#                         ,samples_per_file=samples_per_file)
    
#     tstart = time.time()

#     ctrl_dev.set_enable_on()	
#     comp.set_mode_motor_torque()
#     while (time.time()-tstart < opts.duration):
#         proxy.step()
        
#         if opts.joint_angle_mode:
#             printlist = [signal_gen.status.time,
#                          signal_gen.status.trajectory.x-joint_angle_zero,
#                          comp.get_joint_theta_rad()- joint_angle_zero]
#         else:
#             printlist = [ signal_gen.status.time,
#                           signal_gen.status.trajectory.x-zero,
#                           comp.get_motor_theta_rad()-zero,
#                           comp.status.cmd.cmd_motor_torque]
                     
#         print_format = "\t".join("{0: 8.5f}".format(x) for x in printlist)
#         print print_format
#         if opts.do_plot:
#             scope.plot(printlist)
#         time.sleep(0.1)

# except (KeyboardInterrupt):
#     pass
# ctrl_dev.set_enable_off()
# proxy.stop_log_service()
# proxy.stop() 

# if opts.do_csv:
#     log_names = [c.name for c in log_components]
#     m3lcj_log2csv.log2csv(logname,log_names)

# if opts.do_diff_time_plot:
#     m3lcj_log_plotter.log_plot(logname,diff_time_plot=True)

# #if opts.do_motor_theta_plot:
# #    m3lcj_log_plotter.log_plot(logname,
    
