#! /usr/bin/python
import time
import m3.rt_proxy as m3p
import m3.toolbox as m3t
import m3.component_factory as mcf
import m3bip.bip_logger as m3s
import yaml
import os.path



class M3Log:
	def __init__(self):
		self.proxy = m3p.M3RtProxy()

	def stop(self):
		self.log.set_enable(0)
		self.proxy.stop()
		
	def start(self):
		self.proxy.start()
		self.comp_name='m3bip_logger_lg0'
		self.log=m3s.M3BipLogger(self.comp_name)
		#self.ctrl_dev=m3w.M3LcjCtrlDev('m3lcj_ctrl_dev_j0')
		

		self.proxy.make_operational_all()
#		self.proxy.make_operational('m3bip_actuator_j0')
#		self.proxy.make_operational('m3bip_actuator_ec_j0')
		

		self.proxy.subscribe_status(self.log)
		self.proxy.publish_command(self.log)

		
		self.proxy.step()

		
	def do_log(self):

		self.duration = 20.0


		
		self.path = self.log.get_path_name()
		self.logname = self.log.get_log_name()
		
		print 'bla ', m3t.get_msg_field_value(self.log.status,'path')
		print 'path: ',self.path
		print 'logname: ',self.logname


		print
		print 'Hit enter to begin log...'
		raw_input()

		print 'Starting log...'

		self.log.set_enable(1)
		self.proxy.step()
	
		#proxy.start_log_service(logname,sample_freq_hz=freq,samples_per_file=samples)
		ts=time.time()
		while time.time()-ts<self.duration:
			time.sleep(0.25)
			print ' Time: ',time.time()-ts

		self.proxy.step()
		self.log.set_enable(0)
	

	
		print '--------------------------------------'
		print 'Starting dump of ' + self.logname
	
	
		self.ns = self.proxy.get_log_num_samples(self.logname)
		
		if self.ns==0:
			print 'get ns failed'
			self.stop()
			return
		print 'Found ',self.ns,' samples'

		self.start_t = 0
		self.end_t = self.ns-1
		print 'start :'+str(self.start_t)
		print 'end   :'+str(self.end_t)
	
		comp=mcf.create_component(self.comp_name)
		self.proxy.register_log_component(comp)
		fields=[]
	
	#	'''print 'Fetching data...'
	#	fn=m3t.get_m3_log_path()+logname+'/'+logname+'_dump.yaml'
	#	f=file(fn,'w')
	#	print 'Saving...',fn
	#	f.write(yaml.safe_dump(comp.status, default_flow_style=False,width=200))
	#	f.close()'''
		
		print comp.status.DESCRIPTOR.fields_by_name.keys()


		fields.append('act_status.motor.theta')
		fields.append('act_status.motor.thetadot')
		fields.append('act_status.motor.current')
		
		fields.append('act_status.raw.raw_dac_cmd')
		
		fields.append('act_status.command.motor_theta')
		
		fields.append('act_status.joint.torque')
		fields.append('act_status.joint.theta')
		
		fields.append('act_status.raw.timestamp')
		fields.append('act_status.joint.torque')

		
		print 'Fetching data...'
		data={}
		for k in fields:
			data[k]=[]
#			print '---------------------------'
#			print k
#			print m3t.get_msg_field_value(comp.status,k)
#			print dir(m3t.get_msg_field_value(comp.status,k))
			  
			if hasattr(m3t.get_msg_field_value(comp.status,k),'__len__'):
				for j in range(len(m3t.get_msg_field_value(comp.status,k))):
					data[k].append([])
				     
				     
		for i in range(self.start_t,self.end_t):
			self.proxy.load_log_sample(self.logname,i)
			for k in fields:
				repeated = False
				# skip protobuf subclasses (like base) for now.  note we'll want this for humanoid
				if hasattr(m3t.get_msg_field_value(comp.status,k),'__metaclass__'):
					pass
				elif hasattr(m3t.get_msg_field_value(comp.status,k),'__len__'):       
					for j in range(len(m3t.get_msg_field_value(comp.status,k))):
						data[k][j].append(m3t.get_msg_field_value(comp.status,k)[j])
				else:
					data[k].append(m3t.get_msg_field_value(comp.status,k))

		fn=self.path+'/'+self.logname+'.csv'

		f=file(fn,'w')
		print 'Saving...',fn
#		print data
	
		keys = data.keys()
		f.write(','.join(keys) + '\n')
#		print ','.join(keys)
	
		for i in range(0,self.end_t):
			vals = [str(data[k][i]) for k in keys]
			f.write(','.join(vals)+'\n')
#			print ','.join(vals)

		f.close()


if __name__ == '__main__':
	t = M3Log()
	try:
		t.start()
		t.do_log()
	except (KeyboardInterrupt,EOFError):
		pass
	t.stop()
