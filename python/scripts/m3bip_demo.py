#! /usr/bin/python

#Copyright  2008, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.

import time
import m3.toolbox as m3t
import m3.rt_proxy as m3p
import m3bip.bip_act_array
import m3.pwr
import m3.gui as m3g
import m3.trajectory as m3jt
import m3bip.bip_actuator_mode_pb2 as ba
import numpy as nu
import yaml

class M3Proc:
	def __init__(self):
		self.proxy = m3p.M3RtProxy()
		self.gui = m3g.M3Gui(stride_ms=50)
	def stop(self):
		self.proxy.stop()

	def start(self):
		# ######## Setup Proxy and Components #########################
		self.proxy.start()
		
		right_leg_name='m3bip_act_array_aa0'
		left_leg_name='m3bip_act_array_aa1'
		pwr_name='m3pwr_pwr016'
		
		self.left_leg = m3bip.bip_act_array.M3BipActArray(left_leg_name)
		self.right_leg = m3bip.bip_act_array.M3BipActArray(right_leg_name)
		self.pwr = m3.pwr.M3Pwr(pwr_name)
		
				# ####### Setup Proxy #############
		self.proxy.subscribe_status(self.left_leg)
		self.proxy.publish_command(self.left_leg)
		self.proxy.subscribe_status(self.right_leg)
		self.proxy.publish_command(self.right_leg)
		self.proxy.subscribe_status(self.pwr)
		self.proxy.publish_command(self.pwr)
		
		self.proxy.make_operational_all()
		self.pwr.set_motor_power_on()
		self.ndof=3
		
		self.via_traj={}
		self.via_traj_first=True

		self.jt_left = m3jt.JointTrajectory(3)
		self.jt_right = m3jt.JointTrajectory(3)
		
		self.via_traj_run_left = []

		self.via_traj_run_left.append([0, -15, -80])
		self.via_traj_run_left.append([-5, -10, -40])
		self.via_traj_run_left.append([0, 40,  -10])
		self.via_traj_run_left.append([5, -10,-40])
		self.via_traj_run_left.append([0, -15,  -80])
		
		self.via_traj_run_right = []
		self.via_traj_run_right.append([0,  40,  -10])
		self.via_traj_run_right.append([5, -10,-40])
		self.via_traj_run_right.append([0, -15, -80])
		self.via_traj_run_right.append([-5, -10, -40])
		self.via_traj_run_right.append([0,  40,   -10])
		

		# ######## Demo and GUI #########################
		#self.off=False
		
		self.leg_mode_names=['Off','ZeroTheta','ZeroForce','Run']
		
		self.leg_mode_methods=[self.step_off, self.step_zero_theta, self.step_zero_force, self.step_via_traj]
		
		self.leg_mode=[0]
		
		#self.poses={'zero':  {'right_arm':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],'left_arm':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]}}		
		
		self.velocity=[5]
		self.stiffness=[90]
		
		
		self.gui.add('M3GuiModes',  'Leg Mode',      (self,'leg_mode'),range(1),[self.leg_mode_names,1],m3g.M3GuiWrite,column=1)
		#self.gui.add('M3GuiToggle', 'ESTOP', (self,'off'),[],[['Legs Enabled','Legs Disabled']],m3g.M3GuiWrite,column=1)
		self.gui.add('M3GuiSliders','Velocity ', (self,'velocity'),[0],[0,40],m3g.M3GuiWrite,column=1)
		self.gui.add('M3GuiSliders','Stiffness', (self,'stiffness'),[0],[0,100],m3g.M3GuiWrite,column=1) 
		self.gui.start(self.step)

	def get_leg_mode_name(self):
		return self.leg_mode_names[self.leg_mode[0]]
	
	

	def step(self):
		self.proxy.step()

		#print 'Legs: ',self.get_leg_mode_name()
		
		apply(self.leg_mode_methods[self.leg_mode[0]])
		
		if self.get_leg_mode_name()!='Run':
			self.via_traj_first=True
	
	def step_via_traj(self):
		
	
		if self.via_traj_first:
			self.via_traj_first=False
			
			self.jt_left = m3jt.JointTrajectory(3)
			self.jt_right = m3jt.JointTrajectory(3)
			
			for v in self.via_traj_run_right:
				#print 'Adding via',v            
				self.jt_right.add_via_deg(v, [self.velocity[0]]*self.ndof)
			for v in self.via_traj_run_left:
				#print 'Adding via',v            
				self.jt_left.add_via_deg(v, [self.velocity[0]]*self.ndof)
				
			self.proxy.step()
			
			
			
			theta_0_left = nu.array(self.left_leg.get_joint_theta_deg())
			theta_0_right = nu.array(self.right_leg.get_joint_theta_deg())
			
			print '-------------'
			print 'left', theta_0_left
			print 'right', theta_0_right
			
			self.jt_left.start(theta_0_left, [0]*self.ndof)
			self.jt_right.start(theta_0_right, [0]*self.ndof)
			
			print
			print '--------- Right Splines (', len(self.jt_right.splines), ')--------'
			print '------------q_0, q_f, qdot_0, qdot_f, tf--------------'					
			for s in self.jt_right.splines:
				print s.q_0, s.q_f, s.qdot_0, s.qdot_f, s.tf
				
			print
			print '--------- Left Splines (', len(self.jt_left.splines), ')--------'
			print '------------q_0, q_f, qdot_0, qdot_f, tf--------------'					
			for s in self.jt_left.splines:
				print s.q_0, s.q_f, s.qdot_0, s.qdot_f, s.tf
		
		else:	
			#print
			#print 'Hit any key to start or (q) to quit execution'
			
		
			
			#p=m3t.get_keystroke()
			
			#if p != 'q':						
			self.left_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_THETA]*3)
			self.right_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_THETA]*3)
		
			
		
			if self.jt_left.is_splined_traj_complete() and self.jt_right.is_splined_traj_complete():
				self.via_traj_first=True
				
			if not self.jt_left.is_splined_traj_complete():
				q = self.jt_left.step()
				self.left_leg.set_joint_theta_deg(q)
				self.left_leg.set_joint_stiffness([self.stiffness[0]]*self.ndof)								
				self.proxy.step()
				
				
			if not self.jt_right.is_splined_traj_complete():
				q = self.jt_right.step()
				self.right_leg.set_joint_theta_deg(q)
				self.right_leg.set_joint_stiffness([self.stiffness[0]]*self.ndof)								
				self.proxy.step()

			
			'''self.bot.set_mode_splined_traj_gc(self.arm_name)
			self.bot.set_stiffness(self.arm_name, self.get_stiffness())
			self.bot.set_slew_rate_proportion(self.arm_name,[1.0]*self.ndof)
			if self.via_traj_first and len(self.via_traj[self.get_arm_mode_name()]):
				self.via_traj_first=False
				theta_0 = self.bot.get_theta_deg(self.arm_name)[:]	
				vias=[theta_0]+self.via_traj[self.get_arm_mode_name()]+[theta_0] #start and stop at current pos	
				for v in vias:
					self.bot.add_splined_traj_via_deg(self.arm_name, v,[self.velocity[0]]*self.ndof)				
			if  self.bot.is_splined_traj_complete(self.arm_name):
				self.via_traj_first=True'''

	def step_zero_theta(self):
		self.left_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_THETA]*3)
		self.right_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_THETA]*3)
		
		self.left_leg.set_joint_theta_deg([0]*3)
		self.right_leg.set_joint_theta_deg([0]*3)
		self.left_leg.set_joint_stiffness([1.0]*3)	
		self.right_leg.set_joint_stiffness([1.0]*3)	

	def step_zero_force(self):
		self.left_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_TORQUE]*3)
		self.right_leg.set_control_mode([ba.BIP_ACT_MODE_JOINT_TORQUE]*3)
		
		self.left_leg.set_joint_torque_Nm([0]*3)
		self.right_leg.set_joint_torque_Nm([0]*3)

	
	def step_off(self):
		self.left_leg.set_mode_off()
		self.right_leg.set_mode_off()



if __name__ == '__main__':
	t=M3Proc()
	try:
		t.start()
	except (KeyboardInterrupt,EOFError):
		pass
	t.stop()





