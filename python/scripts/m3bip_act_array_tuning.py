#! /usr/bin/python

# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.

#import matplotlib
#matplotlib.use('TkAgg')
import time
import m3.gui as m3g
import m3.rt_proxy as m3p
import m3.toolbox as m3t
import m3.component_factory as m3f


import m3bip.bip_actuator_pb2 as mec
import m3bip.bip_actuator as m3s


import math
import glob 


class M3Proc:
	def __init__(self):
		self.proxy = m3p.M3RtProxy()
		self.gui = m3g.M3Gui(stride_ms=125)
		self.cnt=0
		self.bias=[]
		
	def stop(self):
		
		self.proxy.stop()
		
	def start(self):
#		self.proxy.start(start_data_svc=True,start_ros_svc=True)
		self.proxy.start()
		

                chain_names=self.proxy.get_available_components('m3bip_act_array')
		self.chain=[]
		if len(chain_names)>0:
			print 'Select kinematic chain'
			self.chain_names=m3t.user_select_components_interactive(chain_names,single=True)
		self.chain.append(m3f.create_component(self.chain_names[0]))
		self.proxy.subscribe_status(self.chain[-1])
		self.proxy.publish_command(self.chain[-1])

		pwr_rt='m3pwr_pwr016'		
		self.pwr=m3f.create_component(pwr_rt)
		#self.proxy.publish_command(self.pwr)

                self.proxy.make_operational_all()
                
		self.proxy.subscribe_status(self.pwr)
		self.proxy.publish_command(self.pwr)

		
		self.pwr.set_motor_power_on()
		
		self.proxy.step()

		#Create gui
		self.mode = [0]
		
		self.motor_torque_desired = [0]*self.chain[-1].ndof
                self.motor_velocity_desired = [0]*self.chain[-1].ndof
                self.joint_torque_desired = [0]*self.chain[-1].ndof
                self.joint_theta_desired = [0]*self.chain[-1].ndof
		
		self.status_dict	= self.proxy.get_status_dict()
		
		#self.theta_slew	= [50.0]
		#self.stiffness	= [50.0]

		motor_torque_max=4
                motor_velocity_max=125
                joint_torque_max=4
                joint_theta_max=125

		self.gui.add('M3GuiTree',   'Status',	(self,'status_dict'),[],[],m3g.M3GuiRead,column=2)

		self.gui.add('M3GuiModes',  'Mode',		(self,'mode'),range(1),[['Off','MotorTorque','JointTorque','JointTheta','MotorVel'],1],m3g.M3GuiWrite)

		self.gui.add('M3GuiSliders','motorTorque(Nm)',	(self,'motor_torque_desired'),range(len(self.motor_torque_desired)),[-motor_torque_max,motor_torque_max],m3g.M3GuiWrite)
                self.gui.add('M3GuiSliders','jointTorque(Nm)',	(self,'joint_torque_desired'),range(len(self.motor_torque_desired)),[-joint_torque_max,joint_torque_max],m3g.M3GuiWrite)
                self.gui.add('M3GuiSliders','jointTheta(deg)',	(self,'joint_theta_desired'),range(len(self.motor_torque_desired)),[-joint_theta_max,joint_theta_max],m3g.M3GuiWrite)
                self.gui.add('M3GuiSliders','motorVelocity(deg/s)',	(self,'motor_velocity_desired'),range(len(self.motor_torque_desired)),[-motor_velocity_max,motor_velocity_max],m3g.M3GuiWrite)

		self.gui.start(self.step)

	def step(self):
		
		self.proxy.step()
		self.cnt=self.cnt+1
		self.status_dict=self.proxy.get_status_dict()

                self.chain[-1].set_control_mode(self.mode[0])
                self.chain[-1].set_motor_torque_Nm(self.motor_torque_desired)
                self.chain[-1].set_joint_torque_Nm(self.joint_torque_desired)
                self.chain[-1].set_joint_theta_deg(self.joint_theta_desired)
                self.chain[-1].set_motor_vel_deg(self.motor_velocity_desired)
		self.chain[-1].set_joint_stiffness([1.0]*3)
		
		self.proxy.step()
		
		'''if self.mode[0]==ba.BIP_ACT_MODE_OFF:
			self.chain[-1].set_mode_off()

		elif self.mode[0]==ba.BIP_ACT_MODE_MOTOR_TORQUE:
			self.chain[-1].set_mode_motor_torque()
			self.act.set_torque_mNm(self.motor_torque_desired[0])
		else:
			self.act.set_mode_off()'''
		
		


if __name__ == '__main__':
	t=M3Proc()
	try:
		t.start()
	except (KeyboardInterrupt,EOFError):
		pass
	t.stop()



