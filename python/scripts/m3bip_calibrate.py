#! /usr/bin/python

#Copyright  2008, Meka Robotics
#All rights reserved.
#http://mekabot.com

#Redistribution and use in source and binary forms, with or without
#modification, are permitted. 


#THIS SOFTWARE IS PROVIDED BY THE Copyright HOLDERS AND CONTRIBUTORS
#"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#Copyright OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES INCLUDING,
#BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.


from m3bip.bip_calibrate_act_array import *

# ###############################################################################################################
if __name__ == '__main__':
	ctypes = {'act_array':{'comp_ec':'m3bip_actuator_ec','comp_rt':'m3bip_actuator','inst':M3BipCalibrate_ActArray()}		  
		  }
	
	kk=ctypes.keys()
	print 'Select calibration type'
	for i in range(len(kk)):
		print i,':',kk[i]
	ctype=kk[m3t.get_int()]
	calib=ctypes[ctype]['inst']

	try:
		if calib.start(ctypes[ctype]): 
			calib.get_task()
	except (KeyboardInterrupt,EOFError):
		pass
	except m3t.M3Exception,e:
		print 'M3Exception',e
	finally:
		print 'Shutting down system'
		calib.stop()
		
