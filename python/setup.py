from distutils.core import setup
setup(
    name = "m3bip",
    packages = ["m3bip"],
    version = "1.0.2",
    description = "UTA Biped Components",
    author = "Mark Pilgrim",
    author_email = "mark@diveintomark.org",
    url = "http://chardet.feedparser.org/",
    download_url = "http://chardet.feedparser.org/download/python3-chardet-1.0.1.tgz"
)
