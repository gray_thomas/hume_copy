# -*- coding: utf-8 -*-
#M3 -- Meka Robotics Robot Components
#Copyright (c) 2010 Meka Robotics
#Author: edsinger@mekabot.com (Aaron Edsinger)

#M3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#M3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Lesser General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with M3.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os 
from m3.toolbox import *
import m3bip.bip_ctrl_simple_pb2
import m3bip.bip_actuator_pb2  as a
from m3.component import M3Component
from m3.unit_conversion import *

	
class M3BipCtrlSimple(M3Component):
	"""Calibrated interface for the M3 ctrl simple"""
	def __init__(self,name,type='m3bip_ctrl_simple'):
		M3Component.__init__(self,name,type=type)
		self.status=m3bip.bip_ctrl_simple_pb2.M3BipCtrlSimpleStatus()
		self.command=m3bip.bip_ctrl_simple_pb2.M3BipCtrlSimpleCommand()
		self.param=m3bip.bip_ctrl_simple_pb2.M3BipCtrlSimpleParam()
		self.read_config()

# SETTERS
	# modes
	def set_control_mode(self,m):
		self.command.ctrl_mode=m
		
	def set_traj_mode(self,m):
		self.command.traj_mode=m
	
	def set_mode_off(self):
		self.command.ctrl_mode=m3bip.bip_ctrl_simple_pb2.BIP_CTRL_MODE_OFF

	def set_mode_motor_torque(self):
		self.command.ctrl_mode=m3bip.bip_ctrl_simple_pb2.BIP_CTRL_MODE_MOTOR_TORQUE
		
	def set_mode_motor_theta(self):
		self.command.ctrl_mode=m3bip.bip_ctrl_simple_pb2.BIP_CTRL_MODE_MOTOR_THETA

	def set_mode_joint_torque(self):
		self.command.ctrl_mode=m3bip.bip_ctrl_simple_pb2.BIP_CTRL_MODE_JOINT_TORQUE

	def set_mode_joint_theta(self):
		self.command.ctrl_mode=m3bip.bip_ctrl_simple_pb2.BIP_CTRL_MODE_JOINT_THETA

	# commands
	def set_motor_torque(self, t):
		self.command.desired_motor_torque = t

	def set_motor_theta(self, t):
		self.command.desired_motor_theta = t

	def set_joint_torque(self, t):
		self.command.desired_joint_torque = t

	def set_joint_theta(self, t):
		self.command.desired_joint_theta = t

	def set_joint_stiffness(self, t):
		self.command.desired_joint_stiffness = t
	

# GETTERS
	# Motor related
	def get_motor_torque(self):
		return self.status.motor.torque

	def get_motor_theta_rad(self):
		return self.status.motor.theta

	def get_motor_thetadot_rad(self):
		return self.status.motor.thetadot
	
	# joint related
	def get_joint_torque(self):
		return self.status.joint.torque

	def get_joint_theta_rad(self):
		return self.status.joint.theta

	def get_joint_thetadot_rad(self):
		return self.status.joint.thetadot

	
	# misc	
	def get_timestamp_uS(self):
		return self.status.base.timestamp
	
	def get_flags(self):
		return self.status.flags
	
