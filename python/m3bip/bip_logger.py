#M3 -- Meka Robotics Robot Components
#Copyright (c) 2010 Meka Robotics
#Author: edsinger@mekabot.com (Aaron Edsinger)

#M3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#M3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Lesser General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with M3.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os 
import m3.toolbox as m3t
import m3bip.bip_logger_pb2 as mec
from m3.component import M3Component


class M3BipLogger(M3Component):
	"""EtherCAT interface for the M3Bip Logger"""
	def __init__(self,name,type='m3bip_logger'):
		M3Component.__init__(self,name,type=type)
		self.status		= mec.M3BipLoggerStatus()
		self.command	= mec.M3BipLoggerCommand()
		self.param		= mec.M3BipLoggerParam()
		self.read_config()

		

	def get_path_name(self):
		return self.status.path

	def get_log_name(self):
		return self.status.log_name

	def set_enable(self,val):
		self.command.enable=val

