# -*- coding: utf-8 -*-
#M3 -- Meka Robotics Robot Components
#Copyright (c) 2010 Meka Robotics
#Author: edsinger@mekabot.com (Aaron Edsinger)

#M3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#M3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Lesser General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with M3.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os 
from m3.toolbox import *
import m3bip.bip_act_array_pb2
import m3bip.bip_actuator_mode_pb2 as ba
from m3.component import M3Component
from m3.unit_conversion import *

	
class M3BipActArray(M3Component):	
	def __init__(self,name,type='m3bip_act_array'):
		M3Component.__init__(self,name,type=type)
		self.status		= m3bip.bip_act_array_pb2.M3BipActArrayStatus()
		self.command	= m3bip.bip_act_array_pb2.M3BipActArrayCommand()
		self.param		= m3bip.bip_act_array_pb2.M3BipActArrayParam()
		self.read_config()
                self.ndof=self.config['ndof']
                for i in range(self.ndof):
                    self.command.torque.append(0)
                    self.command.joint_theta.append(0)
                    self.command.joint_torque.append(0)
                    self.command.motor_velocity.append(0)
		    self.command.joint_stiffness.append(0)            
                    self.command.ctrl_mode.append(ba.BIP_ACT_MODE_OFF)

# SETTERS
	# modes
	def set_control_mode(self,m,ind=None):
                #self.command.ctrl_mode=m
                M3Component.set_int_array(self,self.command.ctrl_mode,m,ind)

	def set_mode_off(self,ind=None):
                M3Component.set_int_array(self,self.command.ctrl_mode,ba.BIP_ACT_MODE_OFF,ind)
		#self.command.ctrl_mode=ba.BIP_ACT_MODE_OFF

	def set_mode_motor_torque(self,ind=None):
                M3Component.set_int_array(self,self.command.ctrl_mode,ba.BIP_ACT_MODE_MOTOR_TORQUE,ind)
		#self.command.ctrl_mode=ba.BIP_ACT_MODE_MOTOR_TORQUE

	# commands
	def set_motor_torque_Nm(self,t,ind=None):		
                M3Component.set_float_array(self,self.command.torque,t,ind)
		#self.command.torque = t
	def set_joint_torque_Nm(self,t,ind=None):
                M3Component.set_float_array(self,self.command.joint_torque,t,ind)
		#self.command.torque = t
	def set_joint_theta_deg(self,t,ind=None):
                M3Component.set_float_array(self,self.command.joint_theta,deg2rad(nu.array(t)),ind)
		#self.command.torque = t
	def set_motor_vel_deg(self,t,ind=None):
                M3Component.set_float_array(self,self.command.motor_velocity,deg2rad(nu.array(t)),ind)
		#self.command.torque = t
	def set_joint_stiffness(self,t,ind=None):
                M3Component.set_float_array(self,self.command.joint_stiffness,t,ind)
		#self.command.torque = t
	
	

# GETTERS
	# Motor/amp related
	
	def get_motor_current(self):
		return self.status.current
	def get_motor_torque(self):
		return self.status.motor_torque
	def get_motor_theta(self):
		return self.status.motor_theta
	def get_motor_thetadot(self):
		return self.status.motor_thetadot
	
	
	# spring related
	
	def get_spring_theta(self):
		return self.status.spring_theta
	def get_spring_force(self):
		return self.status.spring_force
	
	
	# joint related
	
	def get_joint_theta_deg(self):
		return rad2deg(nu.array(self.status.joint_theta))
	def get_joint_thetadot_deg(self):		
		return rad2deg(nu.array(self.status.joint_thetadot))
	def get_joint_torque(self):
		return self.status.joint_torque

		# misc
	def get_timestamp_uS(self):
		return self.status.base.timestamp
	def get_flags(self):
		return self.status.flags
	
