#M3 -- Meka Robotics Robot Components
#Copyright (c) 2010 Meka Robotics
#Author: edsinger@mekabot.com (Aaron Edsinger)

#M3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#M3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Lesser General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with M3.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os 
import m3.toolbox as m3t
import m3bip.bip_actuator_ec_pb2 as mec
from m3.component import M3Component


class M3BipActuatorEc(M3Component):
	"""EtherCAT interface for the M3Bip Actuator"""
	def __init__(self,name,type='m3bip_actuator_ec'):
		M3Component.__init__(self,name,type=type)
		self.status=mec.M3BipActuatorEcStatus()
		self.command=mec.M3BipActuatorEcCommand()
		self.param=mec.M3BipActuatorEcParam()
		self.read_config()

	def set_dac_desire(self,val):
		self.command.dac_desired=val
	def set_mode(self,val):
		self.command.mode=val
		
#	def set_mode_off(self):
#		self.command.mode=m3.actuator_pb2.ACTUATOR_MODE_OFF
#	def set_mode_dac(self):
#		self.command.mode=m3.actuator_pb2.ACTUATOR_MODE_DAC
	
	def get_timestamp(self):
		return self.status.timestamp
	def get_raw_ext_temp(self):
		return self.status.raw_ext_temp
	def get_raw_motor_temp(self):
		return self.status.raw_motor_temp
	def get_raw_motor_current(self):
		return self.status.raw_motor_current
	def get_raw_motor_angle(self):
		return self.status.raw_motor_angle
	def get_raw_act_torque(self):
		return self.status.raw_act_torque
	def get_raw_joint_angle(self):
		return self.status.raw_joint_angle
	def get_raw_dac_cmd(self):
		return self.status.raw_dac_cmd
	def get_raw_joint_enc_err(self):
		return self.status.raw_joint_enc_err
	def get_raw_force_enc_err(self):
		return self.status.raw_force_enc_err
	def get_raw_motor_enc_err(self):
		return self.status.raw_motor_enc_err

