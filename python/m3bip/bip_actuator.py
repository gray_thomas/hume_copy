# -*- coding: utf-8 -*-
#M3 -- Meka Robotics Robot Components
#Copyright (c) 2010 Meka Robotics
#Author: edsinger@mekabot.com (Aaron Edsinger)

#M3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#M3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Lesser General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with M3.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os 
from m3.toolbox import *
import m3bip.bip_actuator_pb2
import m3bip.bip_actuator_mode_pb2 as act_modes
from m3.component import M3Component
from m3.unit_conversion import *

	
class M3BipActuator(M3Component):
	"""Calibrated interface for the M3 Actuator"""
	def __init__(self,name,type='m3bip_actuator'):
		M3Component.__init__(self,name,type=type)
		self.status		= m3bip.bip_actuator_pb2.M3BipActuatorStatus()
		self.command	= m3bip.bip_actuator_pb2.M3BipActuatorCommand()
		self.param		= m3bip.bip_actuator_pb2.M3BipActuatorParam()
		self.read_config()

# SETTERS
	# modes
	def set_control_mode(self,m):
		self.command.ctrl_mode = m
		
	def set_mode_off(self):
		self.command.ctrl_mode = act_modes.BIP_ACT_MODE_OFF

	def set_mode_motor_torque(self):
		self.command.ctrl_mode = act_modes.BIP_ACT_MODE_MOTOR_TORQUE

	def set_mode_motor_theta(self):
		self.command.ctrl_mode = act_modes.BIP_ACT_MODE_MOTOR_THETA
		
	def set_mode_joint_torque(self):
		self.command.ctrl_mode = act_modes.BIP_ACT_MODE_JOINT_TORQUE
		
	def set_mode_joint_theta(self):
		self.command.ctrl_mode = act_modes.BIP_ACT_MODE_JOINT_THETA
		

	# commands
	def set_torque_mNm(self, t):
		self.command.torque = t
	
	

# GETTERS
	# Motor/amp related
	def get_motor_current_raw(self):
		return self.status.raw.raw_motor_current
	def get_motor_current(self):
		return self.status.motor.current
	def get_motor_torque(self):
		return self.status.motor.torque
	def get_motor_theta_raw(self):
		return self.status.raw.raw_motor_angle
	def get_motor_theta(self):
		return self.status.motor.theta
	def get_motor_thetadot(self):
		return self.status.motor.thetadot
	
	
	# spring related
	def get_spring_raw(self):
		return self.status.raw.raw_act_torque
	def get_spring_theta(self):
		return self.status.spring.theta
	def get_spring_force(self):
		return self.status.spring.force
	
	
	# joint related
	def get_joint_raw(self):
		return self.status.raw.raw_joint_angle
	def get_joint_theta(self):
		return self.status.joint.theta
	def get_joint_thetadot(self):
		return self.status.joint.thetadot
	def get_joint_torque(self):
		return self.status.joint.torque

	
	# misc
	def get_timestamp_uS(self):
		return self.status.base.timestamp
	def get_flags(self):
		return self.status.flags
	
