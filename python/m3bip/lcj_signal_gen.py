# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.

import yaml
import os 
from m3.toolbox import *
import m3bip.lcj_signal_gen_pb2
from m3.component import M3Component
from m3.unit_conversion import *

class M3LcjSignalGen(M3Component):
    def __init__(self,name,type='m3lcj_signal_gen'):
        M3Component.__init__(self,name,type=type)
        self.status=m3bip.lcj_signal_gen_pb2.M3LcjSignalGenStatus()
        self.command=m3bip.lcj_signal_gen_pb2.M3LcjSignalGenCommand()
        self.param=m3bip.lcj_signal_gen_pb2.M3LcjSignalGenParam()
        self.read_config()
