# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import component_base_pb2
import bip_actuator_pb2
import actuator_ec_pb2

DESCRIPTOR = descriptor.FileDescriptor(
  name='bip_logger.proto',
  package='',
  serialized_pb='\n\x10\x62ip_logger.proto\x1a\x14\x63omponent_base.proto\x1a\x12\x62ip_actuator.proto\x1a\x11\x61\x63tuator_ec.proto\"\xa6\x01\n\x11M3BipLoggerStatus\x12\x1b\n\x04\x62\x61se\x18\x01 \x01(\x0b\x32\r.M3BaseStatus\x12(\n\nact_status\x18\x02 \x01(\x0b\x32\x14.M3BipActuatorStatus\x12*\n\x0b\x61\x63t_command\x18\x03 \x01(\x0b\x32\x15.M3BipActuatorCommand\x12\x0c\n\x04path\x18\x04 \x01(\t\x12\x10\n\x08log_name\x18\x05 \x01(\t\"!\n\x10M3BipLoggerParam\x12\r\n\x05\x64\x65\x62ug\x18\x01 \x01(\x01\"3\n\x12M3BipLoggerCommand\x12\r\n\x05\x64\x65\x62ug\x18\x01 \x01(\x01\x12\x0e\n\x06\x65nable\x18\x02 \x01(\x05\x42\x02H\x01')




_M3BIPLOGGERSTATUS = descriptor.Descriptor(
  name='M3BipLoggerStatus',
  full_name='M3BipLoggerStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='base', full_name='M3BipLoggerStatus.base', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='act_status', full_name='M3BipLoggerStatus.act_status', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='act_command', full_name='M3BipLoggerStatus.act_command', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='path', full_name='M3BipLoggerStatus.path', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='log_name', full_name='M3BipLoggerStatus.log_name', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=82,
  serialized_end=248,
)


_M3BIPLOGGERPARAM = descriptor.Descriptor(
  name='M3BipLoggerParam',
  full_name='M3BipLoggerParam',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='debug', full_name='M3BipLoggerParam.debug', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=250,
  serialized_end=283,
)


_M3BIPLOGGERCOMMAND = descriptor.Descriptor(
  name='M3BipLoggerCommand',
  full_name='M3BipLoggerCommand',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='debug', full_name='M3BipLoggerCommand.debug', index=0,
      number=1, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='enable', full_name='M3BipLoggerCommand.enable', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=285,
  serialized_end=336,
)

_M3BIPLOGGERSTATUS.fields_by_name['base'].message_type = component_base_pb2._M3BASESTATUS
_M3BIPLOGGERSTATUS.fields_by_name['act_status'].message_type = bip_actuator_pb2._M3BIPACTUATORSTATUS
_M3BIPLOGGERSTATUS.fields_by_name['act_command'].message_type = bip_actuator_pb2._M3BIPACTUATORCOMMAND
DESCRIPTOR.message_types_by_name['M3BipLoggerStatus'] = _M3BIPLOGGERSTATUS
DESCRIPTOR.message_types_by_name['M3BipLoggerParam'] = _M3BIPLOGGERPARAM
DESCRIPTOR.message_types_by_name['M3BipLoggerCommand'] = _M3BIPLOGGERCOMMAND

class M3BipLoggerStatus(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3BIPLOGGERSTATUS
  
  # @@protoc_insertion_point(class_scope:M3BipLoggerStatus)

class M3BipLoggerParam(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3BIPLOGGERPARAM
  
  # @@protoc_insertion_point(class_scope:M3BipLoggerParam)

class M3BipLoggerCommand(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _M3BIPLOGGERCOMMAND
  
  # @@protoc_insertion_point(class_scope:M3BipLoggerCommand)

# @@protoc_insertion_point(module_scope)
