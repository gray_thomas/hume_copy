#! /usr/bin/python

# MEKA CONFIDENTIAL
# 
# Copyright 2011 
# Meka Robotics LLC
# All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Meka Robotics LLC. The intellectual and 
# technical concepts contained herein are proprietary to 
# Meka Robotics LLC and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Meka Robotics LLC.



import m3.toolbox as m3t

# local components
import m3bip.bip_actuator_ec
import m3bip.bip_actuator
import m3bip.bip_logger
import m3bip.bip_act_array

import m3bip.bip_ctrl_simple



component_map['m3bip_logger'] = m3bip.bip_logger.M3BipLogger
component_map['m3bip_actuator'] = m3bip.bip_actuator.M3BipActuator
component_map['m3bip_act_array'] = m3bip.bip_act_array.M3BipActArray
component_map['m3bip_actuator_ec'] = m3bip.bip_actuator_ec.M3BipActuatorEc

component_map['m3bip_ctrl_simple'] = m3bip.bip_ctrl_simple.M3BipCtrlSimple
