#ifndef HUME_THREAD
#define HUME_THREAD

#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
class Hume_Thread{
protected:
    pthread_t hume_thread;
    bool firstLoopFlag;
    bool isRunning;

protected:
    void terminate();
    bool isFirstLoop();
public:
    Hume_Thread();
    virtual ~Hume_Thread(void);
    virtual void run(void) = 0;

    void start();
};
void *runData(void * arg);
void sigint(int signo);



#endif
