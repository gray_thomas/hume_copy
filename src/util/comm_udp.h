#ifndef COMM_UDP
#define COMM_UDP

#include <string>
// #define CMD_CALIBRATE				0x01000000
// #define CMD_SET_TORQUE				0x02000000
// #define CMD_SET_MODE				0x03000000
// #define CMD_SET_MODE_RAW_TORQUE		0x03000001
// #define CMD_SET_MODE_COMP_TORQUE	0x03000002

#define IP_ADDR_MYSELF "127.0.0.1"
#define IP_ADDR_OLD_PC "127.0.0.1"

namespace COMM{
    void recieve_data(int &socket, int port, void* data, int data_size, const char* ip_addr);
    void send_data(int &socket, int port, void * data, int data_size, const char* ip_addr);
//    void check_send_socket(int* socket, int* ret_socket);

    void recieve_data_unix(const char* server_name, void* data, int data_size);
    void send_data_unix   (const char* server_name, void* data, int data_size);
}

#endif
