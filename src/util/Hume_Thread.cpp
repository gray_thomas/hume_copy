#include "Hume_Thread.h"

Hume_Thread::Hume_Thread() :
    hume_thread(),
    firstLoopFlag(false),
    isRunning(false)
{}

Hume_Thread::~Hume_Thread()
{
    pthread_cancel(hume_thread);
    pthread_join(hume_thread, NULL);
}
void Hume_Thread::terminate()
{
    printf("terminating thread\n");
    isRunning = false;
}
bool Hume_Thread::isFirstLoop()
{
    return firstLoopFlag;
}

void *runThread(void * arg)
{
    ((Hume_Thread*) arg)->run();
    return NULL;
}

void sigint(int signo)
{
    (void) signo;
}
void Hume_Thread::start()
{
    sigset_t sigset, oldset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGINT);
    pthread_sigmask(SIG_BLOCK, &sigset, &oldset);
    pthread_create(&hume_thread, NULL, runThread, this);
    struct sigaction s;
    s.sa_handler = sigint;
    sigemptyset(&s.sa_mask);
    s.sa_flags = 0;
    sigaction(SIGINT, &s, NULL);
    pthread_sigmask(SIG_SETMASK, &oldset, NULL);
    
}

