/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CONTROL_H__
#define __CONTROL_H__ 


void setup_control();
void step_control();
int16_t get_motor_velocity();
void velocity_control(int16_t vel_desired);

int16_t get_velocity_cmd();
int32_t get_joint_pos_error();
int16_t get_joint_velocity();

int16_t	get_ctrl_tmp();
int16_t get_ctrl_state();
int16_t get_des();



// Modes - as defined in "bip_actuator_ec.proto"
#define MODE_ESTOP			0
#define MODE_OFF			1
#define MODE_DAC			2
#define MODE_VEL			3
#define MODE_TORQUE			4
#define MODE_JOINT_THETA	5
#define MODE_MOTOR_THETA	6

// States
#define CTRL_STATE_OFF		0
#define CTRL_STATE_DANGER	1
#define CTRL_STATE_ERROR	2
#define CTRL_STATE_RUN		3

#endif
