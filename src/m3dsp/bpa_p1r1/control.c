/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "setup.h"
#include "control.h"
#include "dio.h"

static int16_t state;
static int16_t ctrl_cnt;
static int16_t wd_cnt;
static int16_t last_status;
static int32_t last_pos;

static int32_t des;

static int16_t velocity;
static int16_t velocity_cmd;
static int16_t last_mode;
static int16_t ctrl_tmp;

static int16_t kp_vel;

int16_t	get_ctrl_tmp()
{
	return ctrl_tmp;
}

int16_t get_des()
{
	return (int16_t) des;
}

int16_t get_ctrl_state()
{
	return state;
}

void setup_control() 
{
	last_mode		= -1;
	velocity_cmd	= 0;
	state			= CTRL_STATE_OFF;
	wd_cnt			= 0;
	last_status		= 0xFFFF;
}

void step_control()
{
	static int32_t p_term;
	static int32_t i_term;
	static int32_t error;
	static int32_t error_sum;

	static int16_t des_vel;
	
	static int16_t mode;
	

	des		= ec_cmd.desired;
	mode	= ec_cmd.mode;
        ec_stat.foot_contact_and_latency_bytes = 0x00;
        ec_stat.foot_contact_and_latency_bytes |= ec_cmd.latency_bytes & 0x01;
	// ERROR CHECKING STUFF:
	wd_cnt++;
	if (ec_cmd.status != last_status)		// if the status changes, everything is cool
	{
		wd_cnt = 0;
		last_status = ec_cmd.status;
	}
	else if (wd_cnt > 10)					// if the status doesn't change for some cycles, we're hosed
	{
		state = CTRL_STATE_ERROR;
	}

        // FOOT SENSOR STUFF:
        if (DIGITAL_RA0==1)
        {
            PORTAbits.RA4=PORTAbits.RA0;
            ec_stat.foot_contact_and_latency_bytes |= PORTAbits.RA0 ? 0xFE:0x00;
        }
	
	// STATE MACHINE STUFF:
	switch (state)
	{
	case CTRL_STATE_OFF :
		set_dac(0);
		ClrEnableAmp;
		
		ctrl_cnt	= 0;
		last_pos	= 0;
		error_sum	= 0;
		last_mode	= mode;
		
		if ((mode != MODE_OFF) && (mode != MODE_ESTOP))
			state = CTRL_STATE_RUN;
			
		break;
	case CTRL_STATE_RUN :

		if (mode != last_mode)	// the mode changed - reset the controller vars
		{
			state = CTRL_STATE_OFF;
		}

		if (vertx_pos(1) > ec_cmd.joint_theta_max)
			state = CTRL_STATE_DANGER;
		
		else if (vertx_pos(1) < ec_cmd.joint_theta_min)
			state = CTRL_STATE_DANGER;

		kp_vel = ec_cmd.kp_velocity;

		switch (mode)
		{
		case MODE_DAC:
			// everything is cool and we're in dac mode...
			set_dac(des);
			SetEnableAmp;
			break;

		case MODE_VEL:
			velocity_control(des);
			break;
		
		case MODE_TORQUE:
			error		= (int32_t)des - vertx_pos(0);
			error_sum	+= error;
			error_sum	= CLAMP(error_sum,-ec_cmd.ki_torque_limit,ec_cmd.ki_torque_limit);

			p_term	= (int32_t)ec_cmd.kp_torque * error;
			i_term	= (int32_t)ec_cmd.ki_torque * error_sum;

			des_vel	= (int16_t)(p_term >> 8) + (int16_t)(i_term >> 8);
		
			velocity_control(des_vel);

			break;
		
		case MODE_JOINT_THETA:

			error		= (int32_t)des - vertx_pos(1);
			error_sum	+= error;
			error_sum	= CLAMP(error_sum,-ec_cmd.ki_joint_limit,ec_cmd.ki_joint_limit);
		
			p_term	= (int32_t)ec_cmd.kp_joint * error;
			i_term	= (int32_t)ec_cmd.ki_joint * error_sum;

			des_vel	= (int16_t)(p_term >> 12) + (int16_t)(i_term >> 12);
		
			velocity_control(des_vel);
			break;

		case MODE_MOTOR_THETA:
	
			error		= (int32_t)des - qei_position();
			error_sum	+= error;
			error_sum	= CLAMP(error_sum,-ec_cmd.ki_motor_limit,ec_cmd.ki_motor_limit);
		
		
			p_term	= (int32_t)ec_cmd.kp_motor * error;
			i_term	= (int32_t)ec_cmd.ki_motor * error_sum;

			ctrl_tmp = (int16_t)(p_term>>12);
		
			des_vel	= (int16_t)(p_term >> 12) + (int16_t)(i_term >> 12);
		
			velocity_control(des_vel);
			break;
	
		default:
			// bad mode or off mode...
			set_dac(0);
			ClrEnableAmp;
			state = CTRL_STATE_OFF;
		}
		
		break;

	case CTRL_STATE_DANGER :

		kp_vel = ec_cmd.kp_danger;
		velocity_control(0);

//		if ( (qei_position() > (last_pos-10)) && (qei_position() < (last_pos+10)) )
		if (ABS(last_pos - qei_position()) < 2)
			ctrl_cnt++;
		else
			ctrl_cnt = 0;
			
		last_pos = qei_position();
		
		if (ctrl_cnt > 1000)
			state = CTRL_STATE_ERROR;

		if (mode == MODE_ESTOP)
			state = CTRL_STATE_OFF;

		break;
		
	case CTRL_STATE_ERROR :
	default:
		
		if ((mode == MODE_OFF) || (mode == MODE_ESTOP))
			state = CTRL_STATE_OFF;
		
		set_dac(0);
		ClrEnableAmp;
	}		


}

void velocity_control(int16_t vel_desired)
{
	static int32_t vel_error;
	static int32_t velocity;
	static int32_t result;
	
	velocity = 0x7FFF / get_period_16();
	
	vel_error = vel_desired - velocity;
	
	velocity_cmd = vel_desired;
	
	result = (int32_t)kp_vel * (int32_t)vel_error;
	
//	if (result > 0x7FFF)
//		set_dac(0x7FFF);
//	else if (result < -0x7FFF)
//		set_dac(-0x7FFF);
//	else
		set_dac((int16_t)result);
	
	SetEnableAmp;
}

int16_t get_velocity_cmd()
{
	return velocity_cmd;
//	return velocity;
}


// note, only call this once
int16_t get_joint_velocity()
{
	static int16_t	joint_last = 0;
	int16_t vel;
	
	vel = vertx_pos(1) - joint_last;
	joint_last = vertx_pos(1);
	return vel;
}


