/* 
M3 -- Meka Robotics Real-Time Control System
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "p33FJ32MC204.h"
#include "setup.h"



//---------------------------------------------------------------
//                  CONFIG
//---------------------------------------------------------------
//New config set
//Refer to p33FJ32MC204.h for the details
_FWDT(FWDTEN_OFF & WINDIS_OFF & WDTPRE_PR128 & WDTPOST_PS32768);					/* Turn off the Watch-Dog Timer.  */
_FPOR(FPWRT_PWR1 & ALTI2C_ON & HPOL_ON & LPOL_ON & PWMPIN_ON);			// Turn off the power-up timers, ALTI2C = 0: I2C mapped to ASDA1/ASCL1
_FOSCSEL(FNOSC_PRIPLL & IESO_ON);	// Auto switch to the EC+PLL clock
//Clock switching+monitor disabled, OSC2 is clock O/P, External clock, allow lock/unlock of peripheral-pin-select
_FOSC(FCKSM_CSDCMD & OSCIOFNC_OFF  & POSCMD_EC & IOL1WAY_OFF); 
_FGS(GCP_OFF & GSS_OFF & GWRP_OFF);            							// Disable Code Protection
_FICD(ICS_PGD2 & JTAGEN_OFF);

//---------------------------------------------------------------------------------
//                     MAIN
//---------------------------------------------------------------------------------

extern ready_for_sending_flag; 	//This variable comes from m3serial.c (for FB_DEV board), and allows the data sending to be done in the main, but only after a reception of data from the slave
									//It helps symchronizing the communication, while freeing the UART interruption while sending data (since the sending process is done in the main

int main (void)
{
	int dummy;
	volatile int i,j;
	//Setup oscillator/ports/pins first
	setup_oscillator();
	setup_ports();
	setup_peripheral_pin_select();

	setup_dio();
	setup_control();
	setup_qei();
	setup_adc();
	setup_timer3();
	setup_vertx();
	setup_dac();

	while (!eeprom_loaded())		//Wait until ESC is ready
		SetHeartbeatLED;
	setup_ethercat();
	ClrHeartbeatLED;


	setup_interrupt_priorities();

	while(1)
	{
		if (get_ctrl_state() == CTRL_STATE_OFF)
			j = 20000;
		else if (get_ctrl_state() == CTRL_STATE_DANGER)
			j = 5000;
		else if (get_ctrl_state() == CTRL_STATE_ERROR)
			j = 5000;
		else if (get_ctrl_state() == CTRL_STATE_RUN)
			j = 10000;
		
		if (i++%j==0 && 0== DIGITAL_RA0)
		{
			ToggleHeartbeatLED();
		}
		
		
		step_ethercat();
	} 
}
