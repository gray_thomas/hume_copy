#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../adc.c ../control.c ../dio.c ../ethercat.c ../ethercat_appl.c ../ethercat_hw.c ../ethercat_slave_fsm.c ../main.c ../setup.c ../timer3.c ../dac.c ../encoder_qei.c ../encoder_vertx.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/adc.o ${OBJECTDIR}/_ext/1472/control.o ${OBJECTDIR}/_ext/1472/dio.o ${OBJECTDIR}/_ext/1472/ethercat.o ${OBJECTDIR}/_ext/1472/ethercat_appl.o ${OBJECTDIR}/_ext/1472/ethercat_hw.o ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/setup.o ${OBJECTDIR}/_ext/1472/timer3.o ${OBJECTDIR}/_ext/1472/dac.o ${OBJECTDIR}/_ext/1472/encoder_qei.o ${OBJECTDIR}/_ext/1472/encoder_vertx.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/adc.o.d ${OBJECTDIR}/_ext/1472/control.o.d ${OBJECTDIR}/_ext/1472/dio.o.d ${OBJECTDIR}/_ext/1472/ethercat.o.d ${OBJECTDIR}/_ext/1472/ethercat_appl.o.d ${OBJECTDIR}/_ext/1472/ethercat_hw.o.d ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d ${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1472/setup.o.d ${OBJECTDIR}/_ext/1472/timer3.o.d ${OBJECTDIR}/_ext/1472/dac.o.d ${OBJECTDIR}/_ext/1472/encoder_qei.o.d ${OBJECTDIR}/_ext/1472/encoder_vertx.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/adc.o ${OBJECTDIR}/_ext/1472/control.o ${OBJECTDIR}/_ext/1472/dio.o ${OBJECTDIR}/_ext/1472/ethercat.o ${OBJECTDIR}/_ext/1472/ethercat_appl.o ${OBJECTDIR}/_ext/1472/ethercat_hw.o ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/setup.o ${OBJECTDIR}/_ext/1472/timer3.o ${OBJECTDIR}/_ext/1472/dac.o ${OBJECTDIR}/_ext/1472/encoder_qei.o ${OBJECTDIR}/_ext/1472/encoder_vertx.o

# Source Files
SOURCEFILES=../adc.c ../control.c ../dio.c ../ethercat.c ../ethercat_appl.c ../ethercat_hw.c ../ethercat_slave_fsm.c ../main.c ../setup.c ../timer3.c ../dac.c ../encoder_qei.c ../encoder_vertx.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33FJ32MC204
MP_LINKER_FILE_OPTION=,--script="../p33FJ32MC204_APP.gld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/adc.o: ../adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/adc.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../adc.c  -o ${OBJECTDIR}/_ext/1472/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/adc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/control.o: ../control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/control.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../control.c  -o ${OBJECTDIR}/_ext/1472/control.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/control.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/control.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/dio.o: ../dio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/dio.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../dio.c  -o ${OBJECTDIR}/_ext/1472/dio.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/dio.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/dio.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat.o: ../ethercat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat.c  -o ${OBJECTDIR}/_ext/1472/ethercat.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_appl.o: ../ethercat_appl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_appl.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_appl.c  -o ${OBJECTDIR}/_ext/1472/ethercat_appl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_appl.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_appl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_hw.o: ../ethercat_hw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_hw.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_hw.c  -o ${OBJECTDIR}/_ext/1472/ethercat_hw.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_hw.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_hw.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o: ../ethercat_slave_fsm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_slave_fsm.c  -o ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../main.c  -o ${OBJECTDIR}/_ext/1472/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/setup.o: ../setup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/setup.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../setup.c  -o ${OBJECTDIR}/_ext/1472/setup.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/setup.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/setup.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/timer3.o: ../timer3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/timer3.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../timer3.c  -o ${OBJECTDIR}/_ext/1472/timer3.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/timer3.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/timer3.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/dac.o: ../dac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/dac.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../dac.c  -o ${OBJECTDIR}/_ext/1472/dac.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/dac.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DM3_DAC -DUSE_DAC -DUSE_ENCODER_MA3 -DUSE_ADC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DM3_DAC_0_1 -DUSE_TIMESTAMP_DC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/dac.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/encoder_qei.o: ../encoder_qei.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/encoder_qei.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../encoder_qei.c  -o ${OBJECTDIR}/_ext/1472/encoder_qei.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/encoder_qei.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/encoder_qei.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/encoder_vertx.o: ../encoder_vertx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/encoder_vertx.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../encoder_vertx.c  -o ${OBJECTDIR}/_ext/1472/encoder_vertx.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/encoder_vertx.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/encoder_vertx.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1472/adc.o: ../adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/adc.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../adc.c  -o ${OBJECTDIR}/_ext/1472/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/adc.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/control.o: ../control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/control.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../control.c  -o ${OBJECTDIR}/_ext/1472/control.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/control.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/control.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/dio.o: ../dio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/dio.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../dio.c  -o ${OBJECTDIR}/_ext/1472/dio.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/dio.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/dio.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat.o: ../ethercat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat.c  -o ${OBJECTDIR}/_ext/1472/ethercat.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_appl.o: ../ethercat_appl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_appl.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_appl.c  -o ${OBJECTDIR}/_ext/1472/ethercat_appl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_appl.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_appl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_hw.o: ../ethercat_hw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_hw.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_hw.c  -o ${OBJECTDIR}/_ext/1472/ethercat_hw.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_hw.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_hw.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o: ../ethercat_slave_fsm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../ethercat_slave_fsm.c  -o ${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ethercat_slave_fsm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../main.c  -o ${OBJECTDIR}/_ext/1472/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/setup.o: ../setup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/setup.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../setup.c  -o ${OBJECTDIR}/_ext/1472/setup.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/setup.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/setup.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/timer3.o: ../timer3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/timer3.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../timer3.c  -o ${OBJECTDIR}/_ext/1472/timer3.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/timer3.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/timer3.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/dac.o: ../dac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/dac.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../dac.c  -o ${OBJECTDIR}/_ext/1472/dac.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/dac.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DM3_DAC -DUSE_DAC -DUSE_ENCODER_MA3 -DUSE_ADC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DM3_DAC_0_1 -DUSE_TIMESTAMP_DC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/dac.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/encoder_qei.o: ../encoder_qei.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/encoder_qei.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../encoder_qei.c  -o ${OBJECTDIR}/_ext/1472/encoder_qei.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/encoder_qei.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/encoder_qei.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1472/encoder_vertx.o: ../encoder_vertx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/encoder_vertx.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../encoder_vertx.c  -o ${OBJECTDIR}/_ext/1472/encoder_vertx.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/encoder_vertx.o.d"      -g -omf=elf -O0 -I".." -DEMBEDDED -DUSE_DAC -DUSE_CONTROL -DUSE_ETHERCAT -DUSE_TIMER3 -DUSE_DIO -DUSE_TIMESTAMP_DC -DUSE_ENCODER_QEI -DM3_ELMO_B1R1 -DUSE_BIP_ACT_PDO_V0 -DUSE_ENCODER_VERTX -DUSE_ADC -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/encoder_vertx.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../p33FJ32MC204_APP.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -Wl,--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--library-path="..",--no-force-link,--smart-io,-Map="${DISTDIR}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../p33FJ32MC204_APP.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -Wl,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--library-path="..",--no-force-link,--smart-io,-Map="${DISTDIR}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/m3_controller_bip_p1r1.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf 
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
