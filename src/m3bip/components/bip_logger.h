/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3BIP_LOG_H
#define M3BIP_LOG_H

#include <google/protobuf/message.h>

#include <m3/toolbox/toolbox.h>

#include <m3rt/base/m3ec_def.h>
#include <m3rt/base/component.h>
#include <m3rt/base/component_ec.h>

#include <m3/hardware/pwr.h>
#include <m3/hardware/actuator.h>

#include "bip_actuator.pb.h"
#include "actuator_ec.pb.h"
#include "m3bip_pdo_v0_def.h"


#include "bip_logger.pb.h"
#include "bip_actuator.h"



#define MAX_PAGE_QUEUE 300 //In case log service not stopped properly, force shutdown

namespace m3bip
{
	using namespace std;
	using namespace m3;
	using namespace m3rt;
	//class M3ComponentFactory;

	
class M3BipLogger : public m3rt::M3Component
{
	public:
		M3BipLogger(): m3rt::M3Component(CALIB_PRIORITY),	act(NULL),
															start_idx(0),
															num_page_write(0),
															num_kbyte_write(0),
															num_kbytes_in_buffer(0),
															entry_idx(0),
															page_idx_read(0),
															page_idx_write(0),
															pages_written(0)
		{
			
			RegisterVersion("default",DEFAULT);		//RBL
			RegisterVersion("iss",ISS);			//ISS. No change from DEFAULT
			RegisterVersion("esp",ESP);			//ESP. Moved torque feedforward from DSP to Component 
		}
		google::protobuf::Message * GetCommand(){return &command;}
		google::protobuf::Message * GetStatus(){return &status;}
		google::protobuf::Message * GetParam(){return &param;}
		
		bool WritePagesToDisk();
		bool WriteEntry(bool final);
		bool enable;
		
	protected:
		enum {DEFAULT,ISS, ESP};
		bool ReadConfig(const char * filename);
		void Startup();
		void Shutdown();
		void StepStatus();
		void StepCommand();
		
		M3BipLoggerStatus	status;
		M3BipLoggerCommand	command;
		M3BipLoggerParam	param;
		
		M3BaseStatus * GetBaseStatus(){return status.mutable_base();}
		
		bool LinkDependentComponents();		
		
		M3StatusLogPage * GetNextPageToRead();
		M3StatusLogPage * GetNextPageToWrite();
		
		void MarkPageEmpty();
		void MarkPageFull();
		
		string act_name;
		string load_name;
		M3BipActuator *	act;
		M3Actuator *	load;
		
		string GetNextFilename(int num_entry);
		
		string log_name;
		string path;
		
		M3StatusAll * entry;
		vector<M3StatusLogPage*> pages;
		vector<bool> is_page_full;
		vector<M3Component *> components;
		int start_idx;
		int downsample_cnt;
		int downsample_rate;
		M3StatusLogPage * page;
		M3RtSystem * sys;
		int page_size;
		int hlt;
		int verbose;
		int num_page_write;
		int num_kbyte_write;
		int num_kbytes_in_buffer;
		int entry_idx;
		int page_idx_write;
		int page_idx_read;
		int pages_written;
		mReal freq;
};
}

#endif
