/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2011 Meka Robotics
Author: lee@mekabot.com (Lee Magnusson)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

//#include "m3lcj/components/lcj_signal_gen.h"
#include "lcj_signal_gen.h"
#include "lcj_signal_gen.pb.h"
#include "m3rt/base/m3rt_def.h"
#include "m3rt/base/component_factory.h"
#include <cmath>

namespace m3bip {
	
using namespace m3rt;
using namespace std;
using namespace m3;

M3LcjSignalGenerator::M3LcjSignalGenerator() :
  m3rt::M3Component(ROBOT_CTRL_PRIORITY)
{
	printf("Creating signal generator\n");
    zero_modifier_value = 0;
    ramp_mult = 1;
  //  tp = param.motor_position_trajectory_param();
    RegisterVersion("default",DEFAULT);
}

bool M3LcjSignalGenerator::LinkDependentComponents()
{
    return true;
}


bool M3LcjSignalGenerator::ReadConfig ( const char* filename )
{
    mReal val;
    if (!M3Component::ReadConfig(filename))
	return false;
    YAML::Node doc;
    GetYamlDoc(filename, doc);

    doc["param"]["trajectory_param"]["freq"]>>val;
    param.mutable_trajectory_param()->set_freq(val);
    doc["param"]["trajectory_param"]["amp"]>>val;
    param.mutable_trajectory_param()->set_amp(val);
    doc["param"]["trajectory_param"]["zero"]>>val;
    param.mutable_trajectory_param()->set_zero(val);
    doc["param"]["trajectory_param"]["zero_ramp"]>>val;
    param.mutable_trajectory_param()->set_zero_ramp(val);
    doc["param"]["trajectory_param"]["max_ramp_excursion"]>>val;
    param.mutable_trajectory_param()->set_max_ramp_excursion(val); 
    doc["param"]["trajectory_param"]["chirp_freq_start"]>>val;
    param.mutable_trajectory_param()->set_chirp_freq_start(val); 
    doc["param"]["trajectory_param"]["chirp_freq_end"]>>val;
    param.mutable_trajectory_param()->set_chirp_freq_end(val); 
    doc["param"]["trajectory_param"]["chirp_duration"]>>val;
    param.mutable_trajectory_param()->set_chirp_duration(val); 

    
    return true;
}

void M3LcjSignalGenerator::Startup()
{		
    command.set_start(0);
    t_start =  GetBaseStatus()->timestamp()/1000000.0;
    SetStateSafeOp();
}

void M3LcjSignalGenerator::Shutdown()
{

}

void M3LcjSignalGenerator::StepCommand()
{

}

void M3LcjSignalGenerator::StepStatus()
{
    mReal t, x, x_dot, x_ddot;
    const M3LcjSignalGenParam::M3LcjTrajectoryParam * tp = &param.trajectory_param();
    if (command.start() == 1) {
	t_start = GetBaseStatus()->timestamp()/1000000.0;
	zero_modifier_value = 0.0;
    }
    
    t = GetBaseStatus()->timestamp()/1000000.0 - t_start;			// start from zero time
    status.set_time(t);
    
    mReal freq_rad, beta;
    int p;
    
    switch (command.mode()) {
	default:
	case M3LcjSignalGenCommand::ZERO:
	    x = x_dot = x_ddot = 0;
	    break;
	case M3LcjSignalGenCommand::SIN:
	    freq_rad = 2*M_PI*tp->freq();
	    x = tp->amp()*sin(freq_rad*t);
	    x_dot = tp->amp()*freq_rad*cos(freq_rad*t);
	    x_ddot = -tp->amp()*freq_rad*freq_rad*sin(freq_rad*t);
	    break;
	case M3LcjSignalGenCommand::CHIRP:
	    p = 1;
	    goto chirp_calc;
	case M3LcjSignalGenCommand::QUAD_CHIRP:
	    p = 2;
	    chirp_calc:
	    beta = (tp->chirp_freq_end() - tp->chirp_freq_start()) / powint(tp->chirp_duration(),p);
	    x = tp->amp()*cos(2*M_PI*(beta/(1+p)*powint(t,1+p) + tp->chirp_freq_start()*t - .25));
	    if (t > tp->chirp_duration())
		x = x_dot = x_ddot = 0;
	    break;
    }
    
    switch (command.modifier()) {
	default:
	case M3LcjSignalGenCommand::NONE:
	    zero_modifier_value = 0.;
	    break;
	case M3LcjSignalGenCommand::RAMP:
	    if (abs(zero_modifier_value) > tp->max_ramp_excursion())
			ramp_mult *= -1;
	    zero_modifier_value += ramp_mult*tp->zero_ramp()/RT_TASK_FREQUENCY;
	    break;
	case M3LcjSignalGenCommand::SQUARE:
	    if (x>0)
			x = tp->amp();
	    else
			x = -tp->amp();
	    break;
    }
    
    x += tp->zero() + zero_modifier_value;		// ignore effect on x_dot and x_ddot for now

    status.mutable_trajectory()->set_x(x); 
    status.mutable_trajectory()->set_x_dot(x_dot);
    status.mutable_trajectory()->set_x_ddot(x_ddot); 
}

mReal powint(mReal x, int e)
{
    mReal x_st = x;
    for(int i=1;i<e;i++)
	x *= x_st;
    return(x);
}

} // namespace m3lcj