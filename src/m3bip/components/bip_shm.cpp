/* 
   M3 -- Meka Robotics Robot Components
   Copyright (c) 2010 Meka Robotics
   Author: edsinger@mekabot.com (Aaron Edsinger)

   M3 is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   M3 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <bip_shm.h>
#include <iostream>
#include "m3rt/base/component_factory.h"
#include <vector>
#include <stdio.h>

namespace m3bip
{

    using namespace m3rt;
    using namespace std;
    using namespace m3;
    using namespace m3uta;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    M3BaseStatus * M3BipShm::GetBaseStatus()
    {
	return status.mutable_base();
    }

    void M3BipShm::Startup()
    {
        sds_status_size = sizeof(M3BipShmSdsStatus);
        sds_cmd_size = sizeof(M3BipShmSdsCommand);
        hume_system_ = new HUME_System();
        memset(&statusStruct, 0, sds_status_size);
        imu_mode_ = IMU_MODE_ORIENTATION;
    }

    void M3BipShm::ResetCommandSds(unsigned char * sds)
    {
	memset(sds, 0, sizeof(M3BipShmSdsCommand));
    }

    size_t M3BipShm::GetStatusSdsSize()
    {
	return sds_status_size;
    }

    size_t M3BipShm::GetCommandSdsSize()
    {
	return sds_cmd_size;
    }

    void operator>>(M3BipLegShmCommand& sdsCmd, M3BipActArray* leg)
    {
        M3BipActArrayCommand* actArrayCommand = (M3BipActArrayCommand*) leg->GetCommand();

        
        
        for (int i = 0; i < leg->GetNumDof(); i++)
        {
            actArrayCommand->set_dsp_latency_bit_cmd(i, sdsCmd.dsp_latency_bit_cmd[i]);
        
            actArrayCommand->set_ctrl_mode(i, (ACT_MODE) sdsCmd.ctrl_mode[i]);
            actArrayCommand->set_torque(i, sdsCmd.motor_torque[i]);

            actArrayCommand->set_joint_theta(i, sdsCmd.joint_theta[i]);

            //Command_Joint_ThetaDot
            actArrayCommand->set_joint_thetadot(i, sdsCmd.joint_thetadot[i]);
        
            actArrayCommand->set_joint_torque(i, sdsCmd.joint_torque[i]);
            actArrayCommand->set_motor_velocity(i, sdsCmd.motor_velocity[i]);
            actArrayCommand->set_joint_stiffness(i, sdsCmd.joint_stiffness[i]);
        
            M3BipActuator* actuator = ((M3BipActuator*) leg->GetActuator(i));
            
//        M3BipParamPID* pidParamJointTheta = ((M3BipParamPID*) actuator->ParamPidJointTheta());

            // pidParamJointTheta->set_k_p(sdsCmd.k_p_jnt[i]);
            // pidParamJointTheta->set_k_i(sdsCmd.k_i_jnt[i]);
            // pidParamJointTheta->set_k_d(sdsCmd.k_d_jnt[i]);

            M3BipActuatorEcParam* actuatorEmbeddedControlParameters =
                ((M3BipActuatorEcParam*) ((M3BipActuatorEc*) actuator->GetActuatorEc())->GetParam());

            // actuatorEmbeddedControlParameters->set_kp_motor(sdsCmd.kp_motor[i]);
            // actuatorEmbeddedControlParameters->set_ki_motor(sdsCmd.ki_motor[i]);
            // actuatorEmbeddedControlParameters->set_ki_motor_limit(sdsCmd.ki_motor_limit[i]);
            actuatorEmbeddedControlParameters->set_kp_torque(sdsCmd.kp_torque[i]);
            actuatorEmbeddedControlParameters->set_ki_torque(sdsCmd.ki_torque[i]);

            // actuatorEmbeddedControlParameters->set_ki_torque_limit(sdsCmd.ki_torque_limit[i]);
            // actuatorembeddedcontrolparameters->set_kp_velocity(sdsCmd.kp_motor_velocity[i]);
            ///DH...
            // actuatorEmbeddedControlParameters->set_kp_joint(sdsCmd.k_p_jnt[i]);
            // actuatorEmbeddedControlParameters->set_ki_joint(sdsCmd.k_i_jnt[i]);
            // printf("kp torque of %i joint in sds cmd: %i\n", i, sdsCmd.kp_torque[i]);
            // printf("ki torque of %i joint in sds cmd: %i\n", i, sdsCmd.ki_torque[i]);
                                
	}
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void operator<<(M3BipLegShmStatus& sdsStatus, M3BipActArray* leg)
    {
        M3BipActArrayStatus* legStatus = (M3BipActArrayStatus*) (leg->GetStatus());
        for (int i = 0; i < leg->GetNumDof(); i++)
        {
            sdsStatus.motor_temp_winding[i] = legStatus->motor_temp_winding(i);
            sdsStatus.motor_temp_ambient[i] = legStatus->motor_temp_ambient(i);
            sdsStatus.motor_temp_housing[i] = legStatus->motor_temp_housing(i);
            sdsStatus.current_cmd[i] = legStatus->current_cmd(i);
            sdsStatus.motor_torque_cmd[i] = legStatus->motor_torque_cmd(i);
            sdsStatus.joint_torque_cmd[i] = legStatus->joint_torque_cmd(i);
            sdsStatus.joint_theta_cmd[i] = legStatus->joint_theta_cmd(i);
            sdsStatus.current[i] = legStatus->current(i);
            sdsStatus.motor_torque[i] = legStatus->motor_torque(i);
            sdsStatus.motor_torquedot[i] = legStatus->motor_torquedot(i);
            sdsStatus.motor_theta[i] = legStatus->motor_theta(i);
            sdsStatus.motor_torque_cmd[i] = legStatus->motor_torque_cmd(i);
            sdsStatus.motor_thetadot[i] = legStatus->motor_thetadot(i);
            sdsStatus.motor_thetadotdot[i] = legStatus->motor_thetadotdot(i);
            sdsStatus.joint_theta[i] = legStatus->joint_theta(i);
            sdsStatus.joint_thetadot[i] = legStatus->joint_thetadot(i);
            sdsStatus.joint_thetadotdot[i] = legStatus->joint_thetadotdot(i);
            sdsStatus.joint_torque[i] = legStatus->joint_torque(i);
            sdsStatus.joint_torquedot[i] = legStatus->joint_torquedot(i);
            sdsStatus.spring_theta[i] = legStatus->spring_theta(i);
            sdsStatus.spring_force[i] = legStatus->spring_force(i);
            sdsStatus.spring_forcedot[i] = legStatus->spring_forcedot(i);
            sdsStatus.foot_contact[i] = legStatus->foot_contact(i);
            // sdsStatus.foot_contact[i] = false;
            sdsStatus.dsp_latency_bit_status[i] = legStatus->dsp_latency_bit_status(i);
	}
    }

    void M3BipShm::StepStatus(){
// populate statusStruct from inputs
        statusStruct.timestamp = GetBaseStatus()->timestamp();
    
        if (left_leg != NULL)
            statusStruct.left << left_leg;
    
        if (right_leg != NULL)
            statusStruct.right << right_leg;
    
        statusStruct.right.dsp_latency_bit_status[0] = latencyBit;
        imu->setIMU_Mode(IMU_MODE_ORIENTATION);
        if (imu)
        {
            for (int i = 0; i < 3; i++)
            {
                statusStruct.imu.accelerometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().accelerometer(i);
                statusStruct.imu.magnetometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().magnetometer(i);
                statusStruct.imu.ang_vel[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().ang_vel(i);
            }
            for (int i = 0; i < 9; i++)
                statusStruct.imu.orientation_mtx[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().orientation_mtx(i);
            // printf("orietation matrix: %f %f %f \n", statusStruct.imu.orientation_mtx[0], statusStruct.imu.orientation_mtx[1], statusStruct.imu.orientation_mtx[2]);
        }
// statusStruct populated now
        doControl();
// populate commandStruct in doControl
    }
    void M3BipShm::ChangeSO3toEulerXYZ(double * ori_mtx, std::vector<double> & Euler_Ang)
    {
        Euler_Ang.resize(3);
//    printf("ori_mtx: %d, %d, %d, \n", ori_mtx[0], ori_mtx[1], ori_mtx[3]);
        //M11,M12,M13,M21,M22,M23,M31,M32,M33
        //Y
        Euler_Ang[1] = atan2(-ori_mtx[5], ori_mtx[8]);
        //X
        Euler_Ang[0] = atan2(ori_mtx[2], sqrt(ori_mtx[8] * ori_mtx[8] + ori_mtx[5] * ori_mtx[5]));
        //Z
        Euler_Ang[2] = atan2(-ori_mtx[1], ori_mtx[0]);

    }
    void M3BipShm::doControl()
    {
        std::vector<double> euler_ang;
        ChangeSO3toEulerXYZ(statusStruct.imu.orientation_mtx, euler_ang);
        //Make state
        std::vector<double> jpos(6);
        std::vector<double> jvel(6);
        std::vector<double> torque(6);
        std::vector<double> ang_vel(3);

        for (int i(0); i<3; ++i){
            // Right
            jpos[i]   = statusStruct.right.joint_theta[i];
            jvel[i]   = statusStruct.right.joint_thetadot[i];
            torque[i] = statusStruct.right.joint_torque[i];
            // Left
            jpos  [3+i] = statusStruct.left.joint_theta[i];
            jvel  [3+i] = statusStruct.left.joint_thetadot[i];
            torque[3+i] = statusStruct.left.joint_torque[i];
        }
        ang_vel[1] = -statusStruct.imu.ang_vel[0];
        ang_vel[0] = -statusStruct.imu.ang_vel[1];
        ang_vel[2] = -statusStruct.imu.ang_vel[2];
        

        // ang_vel[1] = 0.0;
        // ang_vel[0] = 0.0;
        // ang_vel[2] = 0.0;

        // if(sdsStatus.foot_contact[1]){ statusStruct.right.foot_contact[0] = true;}
        // else { statusStruct.right.foot_contact[0] = false;}

        // if(sdsStatus.foot_contact[2]){ statusStruct.left.foot_contact[0] = true;}
        // else { statusStruct.left.foot_contact[0] = false;}

        
        std::vector<double> vec_command;
        std::vector<int32_t> torque_gain_kp(6);
        std::vector<int32_t> torque_gain_ki(6);
        bool ori_mode_change;
        hume_system_->getTorqueInput(jpos,  jvel, torque, euler_ang, ang_vel,
                                     statusStruct.right.foot_contact[2],
                                     statusStruct.right.foot_contact[1],
                                     torque_gain_kp,
                                     torque_gain_ki,
                                     vec_command, ori_mode_change);
        if(ori_mode_change){
            imu_mode_acc_ang_vel();
        }
        else{
            imu_mode_orientation();
        }
        /// ACT_TORQUE GAIN
        
        for (int i=0;i<3;i++)
        {
            // commandStruct.right.ctrl_mode[i]=BIP_ACT_MODE_OFF;
            // commandStruct.left.ctrl_mode[i]=BIP_ACT_MODE_OFF;
            commandStruct.right.kp_motor_velocity[i]=0;
            commandStruct.left.kp_motor_velocity[i]=0;
        }

        for (int i(0); i< 3; ++i){
            commandStruct.right.ctrl_mode[i] = BIP_ACT_MODE_JOINT_TORQUE;
            commandStruct.left.ctrl_mode[i] = BIP_ACT_MODE_JOINT_TORQUE;
            commandStruct.right.joint_torque[i] = vec_command[i];
            commandStruct.left.joint_torque[i] = vec_command[3 + i];
        }

        // Turn off Abduction joint torque control
        commandStruct.right.ctrl_mode[0] = BIP_ACT_MODE_JOINT_THETA;
        commandStruct.left.ctrl_mode[0] = BIP_ACT_MODE_JOINT_THETA;

        commandStruct.right.joint_theta[0] = vec_command[0];
        commandStruct.left.joint_theta[0] = vec_command[3];
        
        commandStruct.right.joint_thetadot[0] = 0.0;
        commandStruct.left.joint_thetadot[0] = 0.0;

        
        for (int i = 0; i<3; ++i){
            commandStruct.right.kp_torque[i] = torque_gain_kp[i];
            commandStruct.left.kp_torque[i] = torque_gain_kp[i +3];
            
            commandStruct.right.ki_torque[i] = torque_gain_ki[i];
            commandStruct.left.ki_torque[i] = torque_gain_ki[i + 3];
            // printf("gain: %d\n", commandStruct.right.kp_torque[i]);

            commandStruct.right.joint_stiffness[i] = 1.0;
            commandStruct.left.joint_stiffness[i] = 1.0;
        }

    }
    void M3BipShm::StepCommand(){


	int64_t dt = GetBaseStatus()->timestamp() - commandStruct.timestamp; // microseconds
	bool shm_timeout = ABS(dt) > (timeout * 1000);

	if (left_leg != NULL)
	{
            commandStruct.left >> left_leg;
	}

	if (right_leg != NULL)
	{
            commandStruct.right >> right_leg;
	}
	latencyBit = commandStruct.left.dsp_latency_bit_cmd[0];

	if (pwr != NULL)
	{
            if (startup_motor_pwr_on)
                pwr->SetMotorEnable(true);
	}
	if (imu)
	{
            if (imu_mode == 0)
            {

                ((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ACCEL_ANG_RATE_MAG);
            }
            else if (imu_mode == 1)
            {
                ((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ORIENTATION);

            }
	}
        imu->setIMU_Mode(imu_mode_);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bool M3BipShm::LinkDependentComponents()
    {
	tmp_cnt = 0;

	if (left_leg_name.size() != 0)
	{
            left_leg = (M3BipActArray*) factory->GetComponent(left_leg_name);

            if (left_leg == NULL)
            {
                M3_WARN("M3BipActArray component %s declared for M3BipShm but could not be linked\n", left_leg_name.c_str());
                //return false;
            }
	}

	if (right_leg_name.size() != 0)
	{
            right_leg = (M3BipActArray*) factory->GetComponent(right_leg_name);

            if (right_leg == NULL)
            {
                M3_WARN("M3BipActArray component %s declared for M3BipShm but could not be linked\n", right_leg_name.c_str());
                //return false;
            }
	}

	if (pwr_name.size() != 0)
	{

            pwr = (M3Pwr*) factory->GetComponent(pwr_name);

            if (pwr == NULL)
            {
                M3_WARN("M3Pwr component %s declared for M3TorqueShm but could not be linked\n", pwr_name.c_str());
                //return false;
            }
	}

	if (imu_name.size() != 0)
	{

            imu = (M3UTAImu*) factory->GetComponent(imu_name);

            if (pwr == NULL)
            {
                M3_WARN("M3UTAImu component %s declared for M3TorqueShm but could not be linked\n", imu_name.c_str());
                //return false;
            }
	}

	if (pwr || left_leg || right_leg || imu)
	{
            return true;
	}
	else
	{
            M3_ERR("Could not link any components for BIP shared memory.\n");
            return false;
	}
    }

    bool M3BipShm::ReadConfig(const char * filename)
    {
	if (!M3Component::ReadConfig(filename))
            return false;

	YAML::Node doc;
	GetYamlDoc(filename, doc);

	try
	{
            doc["left_leg_component"] >> left_leg_name;
	} catch (YAML::KeyNotFound& e)
	{
            left_leg_name = "";
	}

	try
	{
            doc["right_leg_component"] >> right_leg_name;
	} catch (YAML::KeyNotFound& e)
	{
            right_leg_name = "";
	}

	try
	{
            doc["pwr_component"] >> pwr_name;
	} catch (YAML::KeyNotFound& e)
	{
            pwr_name = "";
	}

	try
	{
            doc["imu_component"] >> imu_name;
	} catch (YAML::KeyNotFound& e)
	{
            imu_name = "";
	}

	try
	{
            doc["startup_motor_pwr_on"] >> startup_motor_pwr_on;
	} catch (YAML::KeyNotFound& e)
	{
            startup_motor_pwr_on = false;
	}

	try
	{
            doc["imu_mode"] >> imu_mode;
	} catch (YAML::KeyNotFound& e)
	{
            imu_mode = 1;
	}

	doc["timeout"] >> timeout;

	return true;
    }
}

