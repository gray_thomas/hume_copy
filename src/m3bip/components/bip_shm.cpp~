/* 
 M3 -- Meka Robotics Robot Components
 Copyright (c) 2010 Meka Robotics
 Author: edsinger@mekabot.com (Aaron Edsinger)

 M3 is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 M3 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with M3.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bip_shm.h>
#include <iostream>
#include "m3rt/base/component_factory.h"

namespace m3bip
{

using namespace m3rt;
using namespace std;
using namespace m3;
using namespace m3uta;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
M3BaseStatus * M3BipShm::GetBaseStatus()
{
	return status.mutable_base();
}

void M3BipShm::Startup()
{
    printf("M3bip shm start\n");
	M3Component::Startup();

	sds_status_size = sizeof(M3BipShmSdsStatus);
	sds_cmd_size = sizeof(M3BipShmSdsCommand);

	memset(&statusStruct, 0, sds_status_size);
        
}

void M3BipShm::ResetCommandSds(unsigned char * sds)
{

	memset(sds, 0, sizeof(M3BipShmSdsCommand));

}

size_t M3BipShm::GetStatusSdsSize()
{
	return sds_status_size;
}

size_t M3BipShm::GetCommandSdsSize()
{
	return sds_cmd_size;
}

void operator>>(M3BipLegShmCommand& sdsCmd, M3BipActArray* leg)
{
    //DH TEST
//    printf("M3bip shm command >> \n");

	M3BipActArrayCommand* actArrayCommand = (M3BipActArrayCommand*) leg->GetCommand();

	for (int i = 0; i < leg->GetNumDof(); i++)
	{
		actArrayCommand->set_dsp_latency_bit_cmd(i, sdsCmd.dsp_latency_bit_cmd[i]);

		actArrayCommand->set_ctrl_mode(i, (ACT_MODE) sdsCmd.ctrl_mode[i]);
		actArrayCommand->set_torque(i, sdsCmd.motor_torque[i]);
		actArrayCommand->set_joint_theta(i, sdsCmd.joint_theta[i]);

                //Command_Joint_ThetaDot
                actArrayCommand->set_joint_thetadot(i, sdsCmd.joint_thetadot[i]);
                
                actArrayCommand->set_joint_torque(i, sdsCmd.joint_torque[i]);
		actArrayCommand->set_motor_velocity(i, sdsCmd.motor_velocity[i]);
		actArrayCommand->set_joint_stiffness(i, sdsCmd.joint_stiffness[i]);

		M3BipActuator* actuator = ((M3BipActuator*) leg->GetActuator(i));

		M3BipParamPID* pidParamJointTheta = ((M3BipParamPID*) actuator->ParamPidJointTheta());

		pidParamJointTheta->set_k_p(sdsCmd.k_p_jnt[i]);
		pidParamJointTheta->set_k_i(sdsCmd.k_i_jnt[i]);
		pidParamJointTheta->set_k_d(sdsCmd.k_d_jnt[i]);

		M3BipActuatorEcParam* actuatorEmbeddedControlParameters =
				((M3BipActuatorEcParam*) ((M3BipActuatorEc*) actuator->GetActuatorEc())->GetParam());

		actuatorEmbeddedControlParameters->set_kp_motor(sdsCmd.kp_motor[i]);
		actuatorEmbeddedControlParameters->set_ki_motor(sdsCmd.ki_motor[i]);
		actuatorEmbeddedControlParameters->set_ki_motor_limit(sdsCmd.ki_motor_limit[i]);
		actuatorEmbeddedControlParameters->set_kp_torque(sdsCmd.kp_torque[i]);
		actuatorEmbeddedControlParameters->set_ki_torque(sdsCmd.ki_torque[i]);
		actuatorEmbeddedControlParameters->set_ki_torque_limit(sdsCmd.ki_torque_limit[i]);
		actuatorEmbeddedControlParameters->set_kp_velocity(sdsCmd.kp_motor_velocity[i]);
                ///DH...
                actuatorEmbeddedControlParameters->set_kp_joint(sdsCmd.k_p_jnt[i]);
                actuatorEmbeddedControlParameters->set_ki_joint(sdsCmd.k_i_jnt[i]);

	}
}
/*
// kill
void M3BipShm::SetCommandFromSds(unsigned char * data)
{
    //DH TEST
    // printf("M3bip shm set command from sds \n");

	M3BipShmSdsCommand * sds = (M3BipShmSdsCommand *) data;
	request_command();
	memcpy(&commandStruct, sds, GetCommandSdsSize());
	release_command();

	int64_t dt = GetBaseStatus()->timestamp() - commandStruct.timestamp; // microseconds
	bool shm_timeout = ABS(dt) > (timeout * 1000);

	if (left_leg != NULL)
	{
		commandStruct.left >> left_leg;
	}

	if (right_leg != NULL)
	{
		commandStruct.right >> right_leg;
	}
	latencyBit = commandStruct.left.dsp_latency_bit_cmd[0];

	if (pwr != NULL)
	{
		if (startup_motor_pwr_on)
			pwr->SetMotorEnable(true);
	}

	if (imu)
	{

		if (imu_mode == 0)
		{

			((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ACCEL_ANG_RATE_MAG);
		}
		else if (imu_mode == 1)
		{
			((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ORIENTATION);

		}
	}

}
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void operator<<(M3BipLegShmStatus& sdsStatus, M3BipActArray* leg)
{
    //DH TEST
//    printf("M3bip shm status << sds \n");

	M3BipActArrayStatus* legStatus = (M3BipActArrayStatus*) (leg->GetStatus());
	for (int i = 0; i < leg->GetNumDof(); i++)
	{
		sdsStatus.motor_temp_winding[i] = legStatus->motor_temp_winding(i);
		sdsStatus.motor_temp_ambient[i] = legStatus->motor_temp_ambient(i);
		sdsStatus.motor_temp_housing[i] = legStatus->motor_temp_housing(i);
		sdsStatus.current_cmd[i] = legStatus->current_cmd(i);
		sdsStatus.motor_torque_cmd[i] = legStatus->motor_torque_cmd(i);
		sdsStatus.joint_torque_cmd[i] = legStatus->joint_torque_cmd(i);
		sdsStatus.joint_theta_cmd[i] = legStatus->joint_theta_cmd(i);
		sdsStatus.current[i] = legStatus->current(i);
		sdsStatus.motor_torque[i] = legStatus->motor_torque(i);
		sdsStatus.motor_torquedot[i] = legStatus->motor_torquedot(i);
		sdsStatus.motor_theta[i] = legStatus->motor_theta(i);
		sdsStatus.motor_torque_cmd[i] = legStatus->motor_torque_cmd(i);
		sdsStatus.motor_thetadot[i] = legStatus->motor_thetadot(i);
		sdsStatus.motor_thetadotdot[i] = legStatus->motor_thetadotdot(i);
		sdsStatus.joint_theta[i] = legStatus->joint_theta(i);
		sdsStatus.joint_thetadot[i] = legStatus->joint_thetadot(i);
		sdsStatus.joint_thetadotdot[i] = legStatus->joint_thetadotdot(i);
		sdsStatus.joint_torque[i] = legStatus->joint_torque(i);
		sdsStatus.joint_torquedot[i] = legStatus->joint_torquedot(i);
		sdsStatus.spring_theta[i] = legStatus->spring_theta(i);
		sdsStatus.spring_force[i] = legStatus->spring_force(i);
		sdsStatus.spring_forcedot[i] = legStatus->spring_forcedot(i);
		sdsStatus.foot_contact[i] = legStatus->foot_contact(i);
		// sdsStatus.foot_contact[i] = false;
		sdsStatus.dsp_latency_bit_status[i] = legStatus->dsp_latency_bit_status(i);

	}
}
/*
//kill
void M3BipShm::SetSdsFromStatus(unsigned char * data)
{
    //DH TEST
//    printf("M3bip shm set sds from status \n");

	statusStruct.timestamp = GetBaseStatus()->timestamp();

	if (left_leg != NULL)
	{
		statusStruct.left << left_leg;
	}

	if (right_leg != NULL)
	{
		statusStruct.right << right_leg;
	}
//	printf("bip_shm: left_leg_joint_2 latency bit from status is %s",
//			((M3BipActArrayStatus*) left_leg->GetStatus())->dsp_latency_bit_status(2) ? "True" : "False");
	statusStruct.right.dsp_latency_bit_status[0] = latencyBit;

	if (imu)
	{
		for (int i = 0; i < 3; i++)
		{
			statusStruct.imu.accelerometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().accelerometer(i);
			statusStruct.imu.magnetometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().magnetometer(i);
			statusStruct.imu.ang_vel[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().ang_vel(i);
		}
		for (int i = 0; i < 9; i++)
			statusStruct.imu.orientation_mtx[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().orientation_mtx(i);
	}

	M3BipShmSdsStatus * sds = (M3BipShmSdsStatus *) data;
	request_status();
	memcpy(sds, &statusStruct, GetStatusSdsSize());
	release_status();

}*/

void M3BipShm::StepStatus(){
// populate statusStruct from inputs
	statusStruct.timestamp = GetBaseStatus()->timestamp();

	if (left_leg != NULL)
		statusStruct.left << left_leg;

	if (right_leg != NULL)
		statusStruct.right << right_leg;

	statusStruct.right.dsp_latency_bit_status[0] = latencyBit;

	if (imu)
	{
		for (int i = 0; i < 3; i++)
		{
			statusStruct.imu.accelerometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().accelerometer(i);
			statusStruct.imu.magnetometer[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().magnetometer(i);
			statusStruct.imu.ang_vel[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().ang_vel(i);
		}
		for (int i = 0; i < 9; i++)
			statusStruct.imu.orientation_mtx[i] = ((M3UTAImuStatus*) imu->GetStatus())->imu().orientation_mtx(i);
	}
// statusStruct populated now
// doControl
	doControl();
// populate commandStruct in doControl
}
void M3BipShm::doControl()
{
	for (int i=0;i<3;i++)
	{
		commandStruct.right.ctrl_mode[i]=BIP_ACT_MODE_OFF;
		commandStruct.left.ctrl_mode[i]=BIP_ACT_MODE_OFF;
		commandStruct.right.kp_motor_velocity[i]=0;
		commandStruct.left.kp_motor_velocity[i]=0;
	}
}
void M3BipShm::StepCommand(){
	
	int64_t dt = GetBaseStatus()->timestamp() - commandStruct.timestamp; // microseconds
	bool shm_timeout = ABS(dt) > (timeout * 1000);

	if (left_leg != NULL)
	{
		commandStruct.left >> left_leg;
	}

	if (right_leg != NULL)
	{
		commandStruct.right >> right_leg;
	}
	latencyBit = commandStruct.left.dsp_latency_bit_cmd[0];

	if (pwr != NULL)
	{
		if (startup_motor_pwr_on)
			pwr->SetMotorEnable(true);
	}

	if (imu)
	{

		if (imu_mode == 0)
		{

			((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ACCEL_ANG_RATE_MAG);
		}
		else if (imu_mode == 1)
		{
			((M3UTAImuParam*) imu->GetParam())->set_mode(IMU_MODE_ORIENTATION);

		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool M3BipShm::LinkDependentComponents()
{
	tmp_cnt = 0;

	if (left_leg_name.size() != 0)
	{
		left_leg = (M3BipActArray*) factory->GetComponent(left_leg_name);

		if (left_leg == NULL)
		{
			M3_WARN("M3BipActArray component %s declared for M3BipShm but could not be linked\n", left_leg_name.c_str());
			//return false;
		}
	}

	if (right_leg_name.size() != 0)
	{
		right_leg = (M3BipActArray*) factory->GetComponent(right_leg_name);

		if (right_leg == NULL)
		{
			M3_WARN("M3BipActArray component %s declared for M3BipShm but could not be linked\n", right_leg_name.c_str());
			//return false;
		}
	}

	if (pwr_name.size() != 0)
	{

		pwr = (M3Pwr*) factory->GetComponent(pwr_name);

		if (pwr == NULL)
		{
			M3_WARN("M3Pwr component %s declared for M3TorqueShm but could not be linked\n", pwr_name.c_str());
			//return false;
		}
	}

	if (imu_name.size() != 0)
	{

		imu = (M3UTAImu*) factory->GetComponent(imu_name);

		if (pwr == NULL)
		{
			M3_WARN("M3UTAImu component %s declared for M3TorqueShm but could not be linked\n", imu_name.c_str());
			//return false;
		}
	}

	if (pwr || left_leg || right_leg || imu)
	{
		return true;
	}
	else
	{
		M3_ERR("Could not link any components for BIP shared memory.\n");
		return false;
	}
}

bool M3BipShm::ReadConfig(const char * filename)
{
	if (!M3Component::ReadConfig(filename))
		return false;

	YAML::Node doc;
	GetYamlDoc(filename, doc);

	try
	{
		doc["left_leg_component"] >> left_leg_name;
	} catch (YAML::KeyNotFound& e)
	{
		left_leg_name = "";
	}

	try
	{
		doc["right_leg_component"] >> right_leg_name;
	} catch (YAML::KeyNotFound& e)
	{
		right_leg_name = "";
	}

	try
	{
		doc["pwr_component"] >> pwr_name;
	} catch (YAML::KeyNotFound& e)
	{
		pwr_name = "";
	}

	try
	{
		doc["imu_component"] >> imu_name;
	} catch (YAML::KeyNotFound& e)
	{
		imu_name = "";
	}

	try
	{
		doc["startup_motor_pwr_on"] >> startup_motor_pwr_on;
	} catch (YAML::KeyNotFound& e)
	{
		startup_motor_pwr_on = false;
	}

	try
	{
		doc["imu_mode"] >> imu_mode;
	} catch (YAML::KeyNotFound& e)
	{
		imu_mode = 1;
	}

	doc["timeout"] >> timeout;

	return true;
}
}

