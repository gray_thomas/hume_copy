/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <m3bip/components/bip_actuator_ec.h>
#include "m3rt/base/component_factory.h"

namespace m3bip{
	
using namespace m3rt;
using namespace std;

#define PWR_SLEW_TIME 4.0 //Seconds to ramp setpoint in after power up

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int M3BipActuatorEc::panic	= 0;		// start up in panic mode
int M3BipActuatorEc::n_id	= 0;

void M3BipActuatorEc::ResetCommandPdo(unsigned char * pdo)
{
	if (IsPdoVersion(BIP_ACT_PDO_V0) ) 
	{
		M3BipActPdoV0Cmd * p = (M3BipActPdoV0Cmd *) pdo;
		memset(p,0,sizeof(M3BipActPdoV0Cmd));	
	}
}

void M3BipActuatorEc::SetStatusFromPdo(unsigned char * data)
{

    if (data == NULL)
      return;
  
    if (IsPdoVersion(BIP_ACT_PDO_V0))
    {
		M3BipActPdoV0Status * actuatorStatus = (M3BipActPdoV0Status *) data;

		status.set_timestamp(			actuatorStatus->timestamp			);
		status.set_debug(				actuatorStatus->debug				);
		status.set_raw_period(			actuatorStatus->raw_period			);
		status.set_raw_temp(			actuatorStatus->raw_temp			);
		status.set_raw_motor_current(	actuatorStatus->raw_motor_current	);
		status.set_raw_motor_angle(		actuatorStatus->raw_motor_angle		);
		status.set_raw_act_torque(		actuatorStatus->raw_act_torque		);
		status.set_raw_joint_angle(		actuatorStatus->raw_joint_angle		);
		status.set_raw_dac_cmd(			actuatorStatus->raw_dac_cmd			);
		status.set_flags(				actuatorStatus->flags				);
		status.set_raw_velocity_cmd(	actuatorStatus->raw_velocity_cmd	);
		status.set_ctrl_state(			actuatorStatus->ctrl_state			);
		status.set_raw_cmd(				actuatorStatus->raw_cmd				);
		status.set_foot_contact_and_latency_bytes(actuatorStatus->foot_contact_and_latency_bytes);

		// States: (as defined in control.h of the dsp code)
		// CTRL_STATE_OFF		0
		// CTRL_STATE_DANGER	1
		// CTRL_STATE_ERROR		2
		// CTRL_STATE_RUN		3
		
		if (status.ctrl_state() == 1)
		{
//			SetStateError();
			M3BipActuatorEc::panic |= (1<<id);
			if (ec_cnt%100==0)
				M3_WARN("DSP DANGER FOR %s \n",GetName().c_str());
		}
		else if (status.ctrl_state() == 2)
		{
//			SetStateError();
			M3BipActuatorEc::panic |= (1<<id);
			if (ec_cnt%100==0)
				M3_WARN("DSP ERROR FOR %s \n",GetName().c_str());
		}
		
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void M3BipActuatorEc::SetPdoFromCommand(unsigned char * data)
{

	M3BipActPdoV0Cmd * actuatorCommand;
    mReal pwr_scale; 		// Smooth ramp in of current on power-on
	ec_cnt++;
	
	if (data == NULL)
		return;
	
	if (IsPdoVersion(BIP_ACT_PDO_V0))
	{
	    M3BipActPdoV0Cmd * actuatorCommand = (M3BipActPdoV0Cmd *) data;


		if ( !IsEstopOn() && (last_mode == command.mode()) )
		{
			pwr_scale = pwr_slew.Step(1.0,1.0/PWR_SLEW_TIME);
		}
		else
		{
			pwr_slew.Reset(0);
			pwr_scale = 0;
			
			
			start_motor_theta = status.raw_motor_angle();
			start_joint_theta = status.raw_joint_angle();
			start_joint_torque = status.raw_act_torque();
                        //DH ...
                        // printf("start_joint theta: %x \n", start_joint_theta);
                        // printf("start_joint theta: %p \n", start_joint_theta);
                        // printf("start_joint theta double: %d \n", start_joint_theta);
		}
		
		last_mode = command.mode();


		actuatorCommand->dac_max			= (int16_t)param.dac_max();
		actuatorCommand->joint_theta_max	= (int16_t)param.joint_theta_max();
		actuatorCommand->joint_theta_min	= (int16_t)param.joint_theta_min();
		
		actuatorCommand->kp_danger		= (int16_t)param.kp_danger();
		actuatorCommand->kp_velocity		= (int16_t)param.kp_velocity();
		
		actuatorCommand->kp_torque		= (int16_t)param.kp_torque();
		actuatorCommand->ki_torque		= (int16_t)param.ki_torque();
		actuatorCommand->ki_torque_limit	= (int16_t)param.ki_torque_limit();
		// printf("actuator_ec kp: %i \n", actuatorCommand->kp_torque);
                // printf("actuator_ec ki: %i \n", actuatorCommand->ki_torque);
		// actuatorCommand->kp_joint		= (int16_t)0;
		// actuatorCommand->ki_joint		= (int16_t)0;
		// actuatorCommand->ki_joint_limit	= (int16_t)0;

		actuatorCommand->kp_joint		= (int16_t)param.kp_joint();
		actuatorCommand->ki_joint		= (int16_t)param.ki_joint();
		actuatorCommand->ki_joint_limit	= (int16_t)param.ki_joint_limit();

                
		actuatorCommand->kp_motor		= (int16_t)param.kp_motor();
		actuatorCommand->ki_motor		= (int16_t)param.ki_motor();
		actuatorCommand->ki_motor_limit	= (int16_t)param.ki_motor_limit();
		
		actuatorCommand->latency_bytes  = (int16_t)command.latency_bytes();
		this->status.set_foot_contact_and_latency_bytes((int16_t)command.latency_bytes());
                
		actuatorCommand->kp_embeddamp = (int16_t) param.kp_embeddamp();

		if (IsEstopOn())
		{
			actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_ESTOP;
			actuatorCommand->status		= (int16_t)ec_cnt;
			actuatorCommand->desired		= 0;
			// clear our panic bit
			M3BipActuatorEc::panic &= ~(1<<id);
		}
		else if (M3BipActuatorEc::panic != 0) // if any of the panic bits are set...
		{
			actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_OFF;
			actuatorCommand->status		= (int16_t)ec_cnt;
			actuatorCommand->desired		= 0;
			
			// if mode is set to off, clear our panic bit
			if (command.mode() == BIP_ACT_EC_MODE_OFF)
				M3BipActuatorEc::panic &= ~(1<<id);
		}
		else
		{
			switch (command.mode())
			{
				case BIP_ACT_EC_MODE_DAC:
						
					actuatorCommand->desired		= (int32_t)(command.dac_desired()*pwr_scale);
					actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_DAC;
					actuatorCommand->status		= (int16_t)ec_cnt;
					break;
					
				case BIP_ACT_EC_MODE_MOTOR_VELOCITY:
					actuatorCommand->desired		= (int32_t)(command.motor_velocity_desired()*pwr_scale);
					actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_MOTOR_VELOCITY;
					actuatorCommand->status		= (int16_t)ec_cnt;
					
					break;
				
                        case BIP_ACT_EC_MODE_JOINT_TORQUE:

                            actuatorCommand->desired = (int32_t)( (1.0-pwr_scale)*start_joint_torque + command.joint_torque_desired()*pwr_scale );

                            actuatorCommand->mode = (int16_t)BIP_ACT_EC_MODE_JOINT_TORQUE;
                            actuatorCommand->status = (int16_t)ec_cnt;
                            //DH...
                            // printf("in Joint torque desired\n, %i", actuatorCommand->desired);					
                            break;
				
                        case BIP_ACT_EC_MODE_JOINT_THETA:
					actuatorCommand->desired		= (int32_t)( (1.0-pwr_scale)*start_joint_theta + command.joint_theta_desired()*pwr_scale );
					actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_JOINT_THETA;
					actuatorCommand->status		= (int16_t)ec_cnt;
                                        //DH...
                                        // printf("in Joint theta desired\n, %x", actuatorCommand->desired);
                                        // printf("in Joint theta desired\n, %p", actuatorCommand->desired);   
					break;
				
				case BIP_ACT_EC_MODE_MOTOR_THETA:
					actuatorCommand->desired		= (int32_t)( (1.0-pwr_scale)*start_motor_theta + command.motor_theta_desired()*pwr_scale );
					actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_MOTOR_THETA;
					actuatorCommand->status		= (int16_t)ec_cnt;
					
					break;
				
				default:
					actuatorCommand->mode		= (int16_t)BIP_ACT_EC_MODE_OFF;
					actuatorCommand->status		= (int16_t)ec_cnt;
					actuatorCommand->desired		= 0;
			}
		}


	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool M3BipActuatorEc::LinkDependentComponents()
{
	pwr=(M3Pwr*) factory->GetComponent(pwr_name);
	if (pwr==NULL)
	{
		M3_INFO("M3Pwr component %s not found for component %s..\n",pwr_name.c_str(),GetName().c_str());
		return false;
	}

	return true;
}

bool M3BipActuatorEc::ReadConfig(const char * filename)
{
	//M3_INFO("M3Bip Actuator config loading from %s\n\n",filename);
	
	YAML::Node doc;
	if (!M3ComponentEc::ReadConfig(filename))
		return false;
	GetYamlDoc(filename, doc);	
	doc["pwr_component"] >> pwr_name;
	const YAML::Node& ymlparam = doc["param"];
	int val;
	
	ymlparam["dac_max"] >> val;
	param.set_dac_max(val);
	
	ymlparam["joint_theta_max"] >> val;
	param.set_joint_theta_max(val);
	ymlparam["joint_theta_min"] >> val;
	param.set_joint_theta_min(val);
	
	ymlparam["config"] >> val;
	param.set_config(val);
	
	ymlparam["kp_velocity"] >> val;
	param.set_kp_velocity(val);
	
	ymlparam["kp_danger"] >> val;
	param.set_kp_danger(val);
	
	ymlparam["kp_torque"] >> val;
	param.set_kp_torque(val);
	ymlparam["ki_torque"] >> val;
	param.set_ki_torque(val);
	ymlparam["ki_torque_limit"] >> val;
	param.set_ki_torque_limit(val);

	
	ymlparam["kp_joint"] >> val;
	param.set_kp_joint(val);
	ymlparam["ki_joint"] >> val;
	param.set_ki_joint(val);
	ymlparam["ki_joint_limit"] >> val;
	param.set_ki_joint_limit(val);
	
	ymlparam["kp_motor"] >> val;
	param.set_kp_motor(val);
	ymlparam["ki_motor"] >> val;
	param.set_ki_motor(val);
	ymlparam["ki_motor_limit"] >> val;
	param.set_ki_motor_limit(val);

        ymlparam["kp_embeddamp" ] >> val;
        param.set_kp_embeddamp(val);
        
	return true;
}

}
   
