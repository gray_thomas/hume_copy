/* 
 M3 -- Meka Robotics Robot Components
 Copyright (c) 2010 Meka Robotics
 Author: edsinger@mekabot.com (Aaron Edsinger)

 M3 is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 M3 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with M3.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef M3_BIP_ACTUATOR_H
#define M3_BIP_ACTUATOR_H

#define VERTX_14BIT_MAX		16383.0
#define DSP_FREQUENCY		2000.0

#include <google/protobuf/message.h>

#include <m3rt/base/component.h>

#include <m3/toolbox/toolbox.h>
#include <m3/toolbox/dfilter.h>

#include "bip_actuator.pb.h"
#include "bip_actuator_ec.h"
#include "bip_actuator_ec.pb.h"

namespace m3bip
{
using namespace std;
using namespace m3;
using namespace m3rt;
//using namespace ros;
/////////////////////////////////////////////////////////////////////////

struct M3BipMotorCalib
{
	mReal alpha_cu;
	mReal nominal_voltage;						// V
	mReal no_load_current;						// mA
	mReal max_efficiency;						// %
	mReal winding_resistance;					// Ohm
	mReal torque_constant;						// Nm/A
	mReal thermal_resistance_housing_ambient;	// K/W
	mReal thermal_resistance_rotor_housing;		// K/W
	mReal thermal_time_constant_winding;		// s (Seconds to achieve 63% of steady-state temp)
	mReal thermal_time_constant_motor;			// s
	mReal max_winding_temp;						// C
	mReal max_motor_temp;

	mReal A_per_dac;							// A/dac
	mReal A_per_adc;							// adc/A
	mReal adc_zero;								// ticks

	mReal sec_per_cnt;							// sec/cnt

	mReal cb_scale;
	mReal cb_ticks_per_rev;

};

struct M3BipSpringCalib
{
	mReal spring_const;				// N/m
};

struct M3BipJointCalib
{
	mReal hub_r;
	mReal scale;
};

class M3BipActuator: public m3rt::M3Component
{
public:
	M3BipActuator() :
				m3rt::M3Component(CALIB_PRIORITY),
				pnt_cnt(0),
				start_flag(0),
				motor_theta_offset(0),
				last_raw_motor_angle(0),
				power_heat(0),
				first_wt_step(5),
				act_ec(NULL)
	{
		RegisterVersion("default", DEFAULT);
	}

	void SetCommandMode(ACT_MODE mode)
	{
		command.set_ctrl_mode(mode);
	}

	void SetCommandMotorTorque(mReal torque)
	{
		command.set_motor_torque(torque);
	}
	void SetCommandMotorTheta(mReal theta)
	{
		command.set_motor_theta(theta);
	}
	void SetCommandMotorVelocity(mReal v)
	{
		command.set_motor_velocity(v);
	}

	void SetCommandJointTorque(mReal t)
	{
		command.set_joint_torque(t);
	}
	void SetCommandJointTheta(mReal t)
	{
		command.set_joint_theta(t);
	}
    	void SetCommandJointThetadot(mReal t)
	{
		command.set_joint_thetadot(t);
	}

	void SetCommandJointStiffness(mReal s)
	{
		command.set_joint_stiffness(s);
	}

	void SetCommandDSPLatencyBit(bool bit)
	{
		command.set_dsp_latency_bit_cmd(bit);
	}

	M3BipActuatorCommand* MutableCommand()
	{
		return (&command);
	}

	mReal JointTorqueNm2Dsp(mReal t)
	{
		// Nm to Dsp coordinates
		mReal force;
		mReal rad;
		mReal a;

		force = t / joint_calib.hub_r;
		rad = force / spring_calib.spring_const;
		a = rad * VERTX_14BIT_MAX / 2 / M_PI;

		return a - param.calibration().zero_spring_theta();
	}

	mReal JointThetaRad2Dsp(mReal rad)
	{
		mReal a;

//		rad = rad;
		a = rad * VERTX_14BIT_MAX / 2 / M_PI;

		return a - param.calibration().zero_joint_theta();
	}

	mReal MotorThetaRad2Dsp(mReal rad)
	{
		mReal a;

//		rad = rad;
		a = rad * motor_calib.cb_ticks_per_rev / (2.0 * M_PI);

		return a - param.calibration().zero_motor_theta();
	}

	mReal MotorVelocityRads2Dsp(mReal v)
	{
		mReal tic_per_rad;
		mReal s_per_cnt;
		mReal a;

		tic_per_rad = (motor_calib.cb_ticks_per_rev / 4) / (2 * M_PI);// 4 because the input caputure on the dsp on acts on the rising edge of channel A

		a = v * tic_per_rad * motor_calib.sec_per_cnt * 0x7FFF;
		return (a);
	}

	//Access to Status Messages
	M3BipActuatorStatusRaw * StatusRaw()
	{
		return status.mutable_raw();
	}
	M3BipActuatorStatusCommand * StatusCommand()
	{
		return status.mutable_command();
	}
	M3BipActuatorStatusMotor * StatusMotor()
	{
		return status.mutable_motor();
	}
	M3BipActuatorStatusSpring * StatusSpring()
	{
		return status.mutable_spring();
	}
	M3BipActuatorStatusJoint * StatusJoint()
	{
		return status.mutable_joint();
	}
	M3BipActuatorStatusTemperature * StatusTemp()
	{
		return status.mutable_temp();
	}

	//Access to Param Messages
	M3BipParamCalibration * ParamCalibration()
	{
		return param.mutable_calibration();
	}
	M3BipParamSoftlimits * ParamSoftLimits()
	{
		return param.mutable_softlimits();
	}

	M3BipParamPID * ParamPidJointTheta()
	{
		return param.mutable_pid_joint_theta();
	}

	//Conversions
	mReal MotorTorqueToMotorCurrent(mReal Nm)
	{
		return (Nm / motor_calib.torque_constant);
	}
	mReal MotorCurrentToMotorTorque(mReal A)
	{
		return (A * motor_calib.torque_constant);
	}

	mReal MotorCurrentToDacTicks(mReal a)
	{
		return a / motor_calib.A_per_dac;
	}
	mReal AdcTicksToMotorCurrent(mReal ticks)
	{
		return ticks * motor_calib.A_per_adc;
	}

	virtual bool IsEstopOn()
	{
		if (!GetActuatorEc())
			return false;
		return act_ec->IsEstopOn();
	}
	M3BipActuatorEc *GetActuatorEc()
	{
		return act_ec;
	}

	// motor getters
	mReal GetMotorTorque()
	{
		return StatusMotor()->torque();
	}
	mReal GetMotorTorqueDot()
	{
		return StatusMotor()->torquedot();
	}
	mReal GetMotorCurrent()
	{
		return StatusMotor()->current();
	}
	mReal GetMotorTheta()
	{
		return StatusMotor()->theta();
	}
	mReal GetMotorThetaDot()
	{
		return StatusMotor()->thetadot();
	}
	mReal GetMotorThetaDotDot()
	{
		return StatusMotor()->thetadotdot();
	}

	// spring getters		
	mReal GetSpringForce()
	{
		return StatusSpring()->force();
	}
	mReal GetSpringForceDot()
	{
		return StatusSpring()->forcedot();
	}

	// joint getters
	mReal GetJointTorque()
	{
		return StatusJoint()->torque();
	}
	mReal GetJointTorqueDot()
	{
		return StatusJoint()->torquedot();
	}
	mReal GetJointTheta()
	{
		return StatusJoint()->theta();
	}
	mReal GetJointThetaDot()
	{
		return StatusJoint()->thetadot();
	}

	// misc getters
	int64_t GetTimestamp()
	{
		return GetBaseStatus()->timestamp();
	}
	bool GetFootContact()
	{
		return StatusRaw()->foot_contact();
	}
	bool GetDSPLatencyBit()
	{
		return StatusRaw()->dsp_latency_bit_status();
	}

	google::protobuf::Message * GetCommand()
	{
		return &command;
	}
	google::protobuf::Message * GetStatus()
	{
		return &status;
	}
	google::protobuf::Message * GetParam()
	{
		return &param;
	}

protected:

	// required:
	enum
	{
		DEFAULT
	};
	void Startup();
	void Shutdown();
	void StepStatus();
	void StepCommand();
	bool LinkDependentComponents();
	bool ReadConfig(const char * filename);
	M3BaseStatus * GetBaseStatus()
	{
		return status.mutable_base();
	}

	M3BipActuatorStatus status;
	M3BipActuatorCommand command;
	M3BipActuatorParam param;

	//////////////////////
	void StepRaw();
	void StepMotor();
	void StepSpring();
	void StepJoint();
	void StepThermal();

	void StepOverloadDetect();

	// vars:
	M3BipActuatorEc * act_ec;
	string act_ec_name;
	string my_name;

	int pnt_cnt;
	int start_flag;
	int chid;
	int overload_cnt;

	int motor_theta_offset;
	int last_raw_motor_angle;

	// Calib structs -> hold configuration constants for sensor offsets and whatnot...
	M3BipMotorCalib motor_calib;
	M3BipSpringCalib spring_calib;
	M3BipJointCalib joint_calib;

	M3TempSensor ambient_temp_sense;
	M3TimeAvg ambient_temp_avg;
	M3TimeAvg curr_avg_rms;

	mReal i_rms;
	mReal power_heat;

	int first_wt_step;

	MatrixXf eA1;
	VectorXf eA2, Tprev;

	mReal joint_theta_last;
	M3PID pid_joint_theta;

	// Filters
	M3SensorFilter motor_theta_df;
	M3SensorFilter motor_torque_df;

	M3SensorFilter spring_force_df;

	M3SensorFilter joint_theta_df;

};

}

#endif

