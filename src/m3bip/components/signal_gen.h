
#ifndef SIGNAL_GEN_H
#define SIGNAL_GEN_H

#include "m3rt/base/component.h"
#include "m3/toolbox/toolbox.h"

namespace signalgen
{
//	using namespace std;
//	using namespace m3;
//	using namespace ros;

typedef struct {
	mReal x;
	mReal x_dot;
	mReal x_ddot;
} traj;

enum signal_command { 
	ZERO,
	SQUARE,
	SIN,
	CHIRP,
	QUAD_CHIRP };

typedef struct {
	mReal zero;
	mReal amp;
	mReal freq_start;
	mReal freq_end;
	mReal chirp_duration;
	signal_command command;
} param;
	
class SignalGenerator
{
public:
	SignalGenerator(){};
	void Init(param pin);
	void Start(mReal t);
	traj Step(mReal t);
    
protected:
	
private:
	traj chirp_calc(mReal pval, mReal t);
	param p;
	mReal tstart;
};

mReal powint(mReal x, int e);

}

#endif

