/* 
 M3 -- Meka Robotics Robot Components
 Copyright (c) 2010 Meka Robotics
 Author: edsinger@mekabot.com (Aaron Edsinger)

 M3 is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 M3 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with M3.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef M3BIP_ACTUATOR_EC_H
#define M3BIP_ACTUATOR_EC_H

#include <google/protobuf/message.h>

#include <m3/toolbox/toolbox.h>

#include <m3rt/base/m3ec_def.h>
#include <m3rt/base/component.h>
#include <m3rt/base/component_ec.h>

#include <m3/hardware/pwr.h>

#include "bip_actuator_ec.pb.h"
#include "m3bip_pdo_v0_def.h"

namespace m3bip
{

using namespace std;
using namespace m3rt;
using namespace m3;

class M3BipActuatorEc: public m3rt::M3ComponentEc
{
public:
	M3BipActuatorEc() :
				pwr(NULL),
				pdo_status_size(0),
				pdo_cmd_size(0),
				m3rt::M3ComponentEc()
	{
		RegisterVersion("default", DEFAULT);
		RegisterPdo("m3bip_act_pdo_v0", BIP_ACT_PDO_V0);
		id = n_id;
		n_id++;
	}
	google::protobuf::Message * GetCommand()
	{
		return &command;
	}
	google::protobuf::Message * GetStatus()
	{
		return &status;
	}
	google::protobuf::Message * GetParam()
	{
		return &param;
	}

	bool IsEstopOn()
	{
		return !(pwr->IsMotorPowerOn());
	}

protected:
	bool ReadConfig(const char * filename);

	M3EtherCATStatus * GetEcStatus()
	{
		return status.mutable_ethercat();
	}
	M3BaseStatus * GetBaseStatus()
	{
		return status.mutable_base();
	}

	void SetStatusFromPdo(unsigned char * data);
	void SetPdoFromCommand(unsigned char * data);
	bool LinkDependentComponents();
	void ResetCommandPdo(unsigned char * pdo);
    
    enum
    {
        DEFAULT
    };
    enum
    {
        BIP_ACT_PDO_V0
    };
    
    M3BipActuatorEcStatus status;
    M3BipActuatorEcCommand command;
    M3BipActuatorEcParam param;

    static int panic;
    static int n_id;
    int id;
    
    M3Pwr * pwr;
    string pwr_name;
    
    M3TimeSlew pwr_slew;
    
    int last_mode;
    
    mReal start_motor_theta;
    mReal start_joint_theta;
    mReal start_joint_torque;
    
    int pdo_status_size;
    int pdo_cmd_size;
    int ec_cnt;
    bool latency_bit;
};
    
} // end namespace
#endif

