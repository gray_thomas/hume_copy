/* 
 M3 -- Meka Robotics Robot Components
 Copyright (c) 2010 Meka Robotics
 Author: edsinger@mekabot.com (Aaron Edsinger)

 M3 is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 M3 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with M3.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>

#include "m3rt/base/m3rt_def.h"
#include "m3rt/base/component_factory.h"
#include <iostream>

#include "bip_actuator.h"

namespace m3bip
{

using namespace m3rt;
using namespace std;
using namespace m3;
//using namespace ros;

////////////////////////////////////////////////////////////////////////////////

bool M3BipActuator::ReadConfig(const char * filename)
{
	YAML::Node doc;
	mReal val;

	if (!M3Component::ReadConfig(filename))
		return false;

	GetYamlDoc(filename, doc);

	doc["act_ec_component"] >> act_ec_name;

	doc["param"]["pid_joint_theta"]["k_p"] >> val;
	ParamPidJointTheta()->set_k_p(val);
	doc["param"]["pid_joint_theta"]["k_i"] >> val;
	ParamPidJointTheta()->set_k_i(val);
	doc["param"]["pid_joint_theta"]["k_d"] >> val;
	ParamPidJointTheta()->set_k_d(val);
	doc["param"]["pid_joint_theta"]["k_i_limit"] >> val;
	ParamPidJointTheta()->set_k_i_limit(val);
	doc["param"]["pid_joint_theta"]["k_i_range"] >> val;
	ParamPidJointTheta()->set_k_i_range(val);
	doc["param"]["pid_joint_theta"]["k_slew_max"] >> val;
	ParamPidJointTheta()->set_k_slew_max(val);
	doc["param"]["pid_joint_theta"]["k_des_min"] >> val;
	ParamPidJointTheta()->set_k_des_min(val);
	doc["param"]["pid_joint_theta"]["k_des_max"] >> val;
	ParamPidJointTheta()->set_k_des_max(val);

//Calibration
	doc["param"]["calibration"]["zero_motor_theta"] >> val;
	ParamCalibration()->set_zero_motor_theta(val);
	doc["param"]["calibration"]["zero_motor_current"] >> val;
	ParamCalibration()->set_zero_motor_current(val);

	doc["param"]["calibration"]["zero_spring_theta"] >> val;
	ParamCalibration()->set_zero_spring_theta(val);

	doc["param"]["calibration"]["zero_joint_theta"] >> val;
	ParamCalibration()->set_zero_joint_theta(val);

// Temprature Sensors
	ambient_temp_sense.ReadConfig(doc["calib"]["ambient_temp"]);

// Sensor Filters
	motor_theta_df.ReadConfig(doc["calib"]["motor_theta_df"]);
	motor_torque_df.ReadConfig(doc["calib"]["motor_torque_df"]);
	spring_force_df.ReadConfig(doc["calib"]["spring_force_df"]);
	joint_theta_df.ReadConfig(doc["calib"]["joint_theta_df"]);

//Limits params
	doc["param"]["softlimits"]["max_overload_time"] >> val;
	ParamSoftLimits()->set_max_overload_time(val);

// Motor spec
	try
	{
		doc["calib"]["motor_calib"]["alpha_cu"] >> motor_calib.alpha_cu;
	} catch (YAML::TypedKeyNotFound<string> e)
	{
		motor_calib.alpha_cu = 0.00393; //copper constant
	}
	doc["calib"]["motor_calib"]["winding_resistance"] >> motor_calib.winding_resistance;
	doc["calib"]["motor_calib"]["torque_constant"] >> motor_calib.torque_constant;
	doc["calib"]["motor_calib"]["thermal_resistance_housing_ambient"] >> motor_calib.thermal_resistance_housing_ambient;
	doc["calib"]["motor_calib"]["thermal_resistance_rotor_housing"] >> motor_calib.thermal_resistance_rotor_housing;
	doc["calib"]["motor_calib"]["thermal_time_constant_winding"] >> motor_calib.thermal_time_constant_winding;
	doc["calib"]["motor_calib"]["thermal_time_constant_motor"] >> motor_calib.thermal_time_constant_motor;
	doc["calib"]["motor_calib"]["max_winding_temp"] >> motor_calib.max_winding_temp;
	doc["calib"]["motor_calib"]["max_motor_temp"] >> motor_calib.max_motor_temp;

	doc["calib"]["motor_calib"]["A_per_dac"] >> motor_calib.A_per_dac;
	doc["calib"]["motor_calib"]["A_per_adc"] >> motor_calib.A_per_adc;
	doc["calib"]["motor_calib"]["adc_zero"] >> motor_calib.adc_zero;

	doc["calib"]["motor_calib"]["cb_scale"] >> motor_calib.cb_scale;
	doc["calib"]["motor_calib"]["cb_ticks_per_rev"] >> motor_calib.cb_ticks_per_rev;

	doc["calib"]["motor_calib"]["sec_per_cnt"] >> motor_calib.sec_per_cnt;

// Spring related
	doc["calib"]["spring_calib"]["spring_const"] >> spring_calib.spring_const;

// Joint related
	doc["calib"]["joint_calib"]["hub_r"] >> joint_calib.hub_r;
	doc["calib"]["joint_calib"]["scale"] >> joint_calib.scale;

	return true;
} // end ReadConfig

bool M3BipActuator::LinkDependentComponents()
{
	act_ec = (M3BipActuatorEc*) factory->GetComponent(act_ec_name);
	if (act_ec == NULL)
	{
		M3_INFO("M3BipActuatorEc component %s not found for component %s\n", act_ec_name.c_str(), GetName().c_str());
		return false;
	}
	return true;
}

#define BOUNDS_AVERAGE_TIME 2.0 //Seconds to average temp values for out-of-bounds errors
void M3BipActuator::Startup()
{

// temprature init
	int downsample = 20;
	ambient_temp_avg.Resize((int) (BOUNDS_AVERAGE_TIME * 1000000), downsample);
	curr_avg_rms.Resize((int) (BOUNDS_AVERAGE_TIME * 1000000), downsample);

	mReal C1, C2, tau1, tau2, tau3;

	MatrixXf I = MatrixXf::Zero(2, 2);
	MatrixXf ser = MatrixXf::Zero(2, 2);
	MatrixXf A = MatrixXf::Zero(2, 2);
	VectorXf B = VectorXf::Zero(2);

	//Initialize matrices
	eA2 = VectorXf::Zero(2);
	Tprev = VectorXf::Zero(2);
	eA1 = MatrixXf::Zero(2, 2);

	tau1 = motor_calib.thermal_time_constant_winding;
	tau2 = motor_calib.thermal_time_constant_motor;
	C1 = tau1 / motor_calib.thermal_resistance_rotor_housing;
	C2 = tau2 / motor_calib.thermal_resistance_housing_ambient;
	tau3 = motor_calib.thermal_resistance_rotor_housing * C2;

	mReal dt = 1. / RT_TASK_FREQUENCY;
	I << 1, 0, 0, 1;

	A << -1 / tau1, 1 / tau1, 1 / tau3, -1 / tau3 - 1 / tau2;

	B << 1 / C1, 0;

	ser = I;
	eA1 = I;
	for (int i = 1; i < 5; i++)
	{
		ser = ser * A * dt / i;
		eA1 = eA1 + ser;
	}
	eA2 = A.inverse() * (eA1 - I) * B;

	Tprev << 0.0, 0.0;

////////////////////

	if (act_ec != NULL)
		SetStateSafeOp();
	else
		SetStateError();
}

void M3BipActuator::Shutdown()
{
}

////////////////////////////////////////////////////////////////////////////////
//			STATUS

// update the "raw" status field
void M3BipActuator::StepRaw()
{
	M3BipActuatorEcStatus * raw_status;

	raw_status = (M3BipActuatorEcStatus*) (act_ec->GetStatus());

	StatusRaw()->set_timestamp(raw_status->timestamp());
	StatusRaw()->set_debug(raw_status->debug());
	StatusRaw()->set_raw_temp(raw_status->raw_temp());
	StatusRaw()->set_raw_motor_current(raw_status->raw_motor_current());
	StatusRaw()->set_raw_motor_angle(raw_status->raw_motor_angle());
	StatusRaw()->set_raw_act_torque(raw_status->raw_act_torque());
	StatusRaw()->set_raw_joint_angle(raw_status->raw_joint_angle());
	StatusRaw()->set_raw_dac_cmd(raw_status->raw_dac_cmd());
	StatusRaw()->set_raw_period(raw_status->raw_period());
	StatusRaw()->set_flags(raw_status->flags());
	// StatusRaw()->set_foot_contact(raw_status->foot_contact_and_latency_bytes() & 0xFE > 0);
        unsigned tmp = (unsigned int)raw_status->foot_contact_and_latency_bytes() & 0x0000ffff;
        StatusRaw()->set_foot_contact(tmp>9);
        // printf("foot_contact unsigned: %u \n", tmp );
        // printf("foot_contact: %s \n", tmp>9 ? "true":"false");
        // StatusRaw()->set_foot_contact(false);
//	printf("bip_actuator latency bit is %s", raw_status->foot_contact_and_latency_bytes() & 0x01 > 0?"1":"0");
	StatusRaw()->set_dsp_latency_bit_status(raw_status->foot_contact_and_latency_bytes() & 0x01 > 0);

}

#define MAX_ENC				0x7FFF
#define ROLLOVER_THRESHOLD	(MAX_ENC/2)

void M3BipActuator::StepMotor()
{
	mReal motor_cnt;
	mReal motor_rad;
	mReal motor_raddot;
	mReal secs;
	mReal motor_adc;
	mReal motor_current;
	mReal motor_torque;
	M3BipActuatorEcStatus *raw_status = (M3BipActuatorEcStatus*) (act_ec->GetStatus());

// current/torque
	// current sensors arn't working so we'll just feedback the commanded value

	motor_adc = raw_status->raw_dac_cmd();

	motor_current = (motor_adc * motor_calib.A_per_dac);

	motor_torque = MotorCurrentToMotorTorque(motor_current);

	motor_torque_df.Step(motor_torque, raw_status->timestamp());

	StatusMotor()->set_current(motor_current);
	StatusMotor()->set_torque(motor_torque_df.GetX());
	StatusMotor()->set_torquedot(motor_torque_df.GetXDot());

// theta

	// roll over the bottom (-32xxx -> 32xxx)
	if ((start_flag == 1) && (raw_status->raw_motor_angle() > last_raw_motor_angle)
			&& (raw_status->raw_motor_angle() - last_raw_motor_angle > ROLLOVER_THRESHOLD))
	{
		motor_theta_offset -= 2 * MAX_ENC;
	}

	// roll over the top (32xxx -> -32xxx)
	else if ((start_flag == 1) && (last_raw_motor_angle > raw_status->raw_motor_angle())
			&& (last_raw_motor_angle - raw_status->raw_motor_angle() > ROLLOVER_THRESHOLD))
	{
		motor_theta_offset += 2 * MAX_ENC;
	}

	motor_cnt = raw_status->raw_motor_angle() + motor_theta_offset;
	start_flag = 1;

	// Keep track of the last counts for sensors that could rollover.
	last_raw_motor_angle = raw_status->raw_motor_angle();

	motor_cnt += ParamCalibration()->zero_motor_theta();
	motor_rad = 2.0 * M_PI * (motor_cnt / motor_calib.cb_ticks_per_rev);

	secs = raw_status->raw_period() * motor_calib.sec_per_cnt;
	motor_raddot = (2.0 * M_PI / (motor_calib.cb_ticks_per_rev / 4)) / secs;

	motor_theta_df.Step(motor_rad, raw_status->timestamp());

	StatusMotor()->set_theta(motor_rad);
	StatusMotor()->set_thetadot(motor_raddot);
	StatusMotor()->set_thetadotdot(motor_theta_df.GetXDotDot());

} // end StepMotor

void M3BipActuator::StepSpring()
{
	int spring_cnt;
	mReal spring_rad;
	mReal spring_force;
	M3BipActuatorEcStatus *raw_status = (M3BipActuatorEcStatus*) (act_ec->GetStatus());

	spring_cnt = raw_status->raw_act_torque() + ParamCalibration()->zero_spring_theta();
	spring_rad = 2.0 * M_PI * (spring_cnt) / (mReal) VERTX_14BIT_MAX;
	spring_force = spring_calib.spring_const * spring_rad;

	spring_force_df.Step(spring_force, raw_status->timestamp());

	StatusSpring()->set_theta(spring_rad);
	StatusSpring()->set_force(spring_force_df.GetX());
	StatusSpring()->set_forcedot(spring_force_df.GetXDot());

} // end StepSpring

void M3BipActuator::StepJoint()
{
	mReal joint_cnt;
	mReal joint_rad;

	M3BipActuatorEcStatus *raw_status = (M3BipActuatorEcStatus*) (act_ec->GetStatus());

	joint_cnt = raw_status->raw_joint_angle() + ParamCalibration()->zero_joint_theta();
	joint_rad = (2.0 * M_PI * (joint_cnt) / (mReal) VERTX_14BIT_MAX) * joint_calib.scale;

	joint_theta_df.Step(joint_rad, raw_status->timestamp());

	StatusJoint()->set_theta(joint_theta_df.GetX());
	StatusJoint()->set_thetadot(joint_theta_df.GetXDot());
	StatusJoint()->set_thetadotdot(joint_theta_df.GetXDotDot());

	StatusJoint()->set_torque(joint_calib.hub_r * StatusSpring()->force());
	StatusJoint()->set_torquedot(joint_calib.hub_r * StatusSpring()->forcedot());

} // end StepJoint

void M3BipActuator::StepOverloadDetect()
{

	int overload_cnt_orig = overload_cnt;

	//This is now redundant, leave in for now. Allows to set a more conservative max winding temp.
	if (StatusTemp()->housing() > motor_calib.max_motor_temp)
		overload_cnt++;

	if (StatusTemp()->winding() > motor_calib.max_winding_temp)
		overload_cnt++;

	if (overload_cnt == overload_cnt_orig) //no overload, reset cntr
	{
		overload_cnt = 0;
	}
	else if (overload_cnt >= (int) (ParamSoftLimits()->max_overload_time() * (mReal) RT_TASK_FREQUENCY))
	{
//		SetStateError();
		if (pnt_cnt % 200 == 0)
		{
			M3_ERR("------ OVER-THRESHOLD EVENT FOR %s ---------------\n", GetName().c_str());
//			M3_ERR("Motor current %f | Threshold of %f \n",GetCurrent(),max_current);
			M3_ERR("Motor temp %f (C) | Threshold of %f (C) \n", StatusTemp()->housing(), motor_calib.max_motor_temp);
			M3_ERR("Motor winding temp %f (C) | Threshold of %f \n", StatusTemp()->winding(), motor_calib.max_winding_temp);
			M3_ERR("------------------------------------------------------\n");
		}
	}

} // end StepOverloadDetect

void M3BipActuator::StepThermal()
{
	mReal i;
	mReal hot_resistance;
	mReal winding_temp, case_temp, ambient_temp;

	ambient_temp_sense.Step(StatusRaw()->raw_temp());

	i = StatusMotor()->current(); // mA
//	i_rms		= min(motor_calib.starting_current*1000.0,sqrt(ABS(curr_avg_rms.Step(i*i))))/1000.0;
//	i_rms 		= i_rms/1000.0; // mA -> A

	hot_resistance = motor_calib.winding_resistance * (1.0 + motor_calib.alpha_cu * (winding_temp - 25.0));

//	power_heat	= (i_rms)*(i_rms)*hot_resistance;
	power_heat = i * i * hot_resistance;

	if (first_wt_step)
		ambient_temp_avg.Reset(25.0);

	ambient_temp = ambient_temp_avg.Step(ambient_temp_sense.GetTempC());

	Tprev = eA1 * Tprev + eA2 * power_heat;

	winding_temp = ambient_temp + Tprev(0);
	case_temp = ambient_temp + Tprev(1);

	first_wt_step = MAX(0,first_wt_step-1);

	StatusTemp()->set_ambient(ambient_temp);
	StatusTemp()->set_winding(winding_temp);
	StatusTemp()->set_housing(case_temp);
}

void M3BipActuator::StepStatus()
{
	pnt_cnt++;

	if (IsStateError())
		return;

	StepRaw();
	StepMotor();
	StepSpring();
	StepJoint();

	StepThermal();	// this must happen after StepMotor()

	StepOverloadDetect();

} // end StepStatus

////////////////////////////////////////////////////////////////////////////////
//			COMMAND

#define HW_DAC_LIMIT	2047

void M3BipActuator::StepCommand()
{

	M3BipActuatorEcCommand * ec_command = (M3BipActuatorEcCommand *) act_ec->GetCommand();

	mReal desired_joint_torque;
	mReal stiffness;

	mReal dac_desired;
	mReal current_desired;
	mReal torque_desired;

	if (IsStateError())
	{
		ec_command->set_mode(BIP_ACT_EC_MODE_OFF);
		return;
	}

	if (!act_ec || IsStateSafeOp())
		return;

	ec_command->set_latency_bytes(command.dsp_latency_bit_cmd() ? 0xFF : 00);

	switch (command.ctrl_mode())
	{
	case BIP_ACT_MODE_MOTOR_TORQUE:

		torque_desired = command.motor_torque();

		current_desired = MotorTorqueToMotorCurrent(torque_desired);
		dac_desired = (int) MotorCurrentToDacTicks(current_desired);

		// one final clamp to make really sure we don't send an extreme value
		dac_desired = CLAMP(dac_desired, -HW_DAC_LIMIT, HW_DAC_LIMIT);

		// set ec command fields
		ec_command->set_mode(BIP_ACT_EC_MODE_DAC);
		ec_command->set_dac_desired(dac_desired);

		// update personal status
		StatusCommand()->set_torque(torque_desired);
		StatusCommand()->set_current(current_desired);
		StatusCommand()->set_dac(dac_desired);

		break;

	case BIP_ACT_MODE_JOINT_TORQUE:
		ec_command->set_mode(BIP_ACT_EC_MODE_JOINT_TORQUE);
		ec_command->set_joint_torque_desired(JointTorqueNm2Dsp(command.joint_torque()));

		StatusCommand()->set_joint_torque(command.joint_torque());
		break;

	case BIP_ACT_MODE_JOINT_THETA:

		stiffness = CLAMP(command.joint_stiffness(),0.0,1.0);

		desired_joint_torque = pid_joint_theta.Step(
                    StatusJoint()->theta(),
                    StatusJoint()->thetadot(),
                    command.joint_theta(),
                    ParamPidJointTheta()->k_p() * stiffness,
                    ParamPidJointTheta()->k_i() * stiffness, ParamPidJointTheta()->k_d() * stiffness,
                    ParamPidJointTheta()->k_i_limit(),
                    ParamPidJointTheta()->k_i_range());
                
                // printf("In Joint theta control: desired %f\n", StatusJoint()->theta());
                // printf("theta dot, command joint theta, kp, ki, kd, stiffness, i limit, i_range, %f, %f, %f, %f, %f, %f, %f, %f \n",
                //        StatusJoint()->thetadot(),
                //        command.joint_theta(),
                //        ParamPidJointTheta()->k_p(),
                //        ParamPidJointTheta()->k_i(),
                //        ParamPidJointTheta()->k_d(),
                //        stiffness,
                //        ParamPidJointTheta()->k_i_limit(),
                //        ParamPidJointTheta()->k_i_range());

		// desired_joint_torque = pid_joint_theta.Step(StatusJoint()->theta(), StatusJoint()->thetadot(), command.joint_theta(), command.joint_thetadot(),
		// 		ParamPidJointTheta()->k_p() * stiffness, ParamPidJointTheta()->k_i() * stiffness, ParamPidJointTheta()->k_d() * stiffness,
		// 		ParamPidJointTheta()->k_i_limit(), ParamPidJointTheta()->k_i_range());

                
                // desired_joint_torque = pid_joint_theta.Step(0.0, StatusJoint()->thetadot(), 0.0, command.joint_thetadot(),
		// 		ParamPidJointTheta()->k_p() * stiffness, ParamPidJointTheta()->k_i() * stiffness, ParamPidJointTheta()->k_d() * stiffness,
		// 		ParamPidJointTheta()->k_i_limit(), ParamPidJointTheta()->k_i_range());

                
		joint_theta_last = status.joint().theta();

		ec_command->set_mode(BIP_ACT_EC_MODE_JOINT_TORQUE);
		ec_command->set_joint_torque_desired(JointTorqueNm2Dsp(desired_joint_torque));

                // ec_command->set_mode(BIP_ACT_EC_MODE_JOINT_THETA);
		// ec_command->set_joint_theta_desired(JointThetaRad2Dsp(command.joint_theta()));
                // printf("desired theta : %f \n", command.joint_theta());

		StatusCommand()->set_joint_torque(desired_joint_torque);
		StatusCommand()->set_joint_theta(command.joint_theta());

		break;

	case BIP_ACT_MODE_MOTOR_VELOCITY:
		ec_command->set_mode(BIP_ACT_EC_MODE_MOTOR_VELOCITY);
		ec_command->set_motor_velocity_desired(MotorVelocityRads2Dsp(command.motor_velocity()));

		StatusCommand()->set_motor_velocity(command.motor_velocity());
		break;

	case BIP_ACT_MODE_MOTOR_THETA:
		ec_command->set_mode(BIP_ACT_EC_MODE_MOTOR_THETA);
		ec_command->set_motor_theta_desired(MotorThetaRad2Dsp(command.motor_theta()));

		StatusCommand()->set_motor_theta(command.motor_theta());
		break;

	case BIP_ACT_MODE_OFF:
	default:

		// set ec command fields
		ec_command->set_mode(BIP_ACT_EC_MODE_OFF);
		ec_command->set_dac_desired(0);

		// update status
		StatusCommand()->set_torque(0);
		StatusCommand()->set_current(0);
		StatusCommand()->set_dac(0);
		break;
	}

}

} // end StepCommand
