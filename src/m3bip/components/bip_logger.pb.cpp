// Generated by the protocol buffer compiler.  DO NOT EDIT!

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "bip_logger.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace {

const ::google::protobuf::Descriptor* M3BipLoggerStatus_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3BipLoggerStatus_reflection_ = NULL;
const ::google::protobuf::Descriptor* M3BipLoggerParam_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3BipLoggerParam_reflection_ = NULL;
const ::google::protobuf::Descriptor* M3BipLoggerCommand_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  M3BipLoggerCommand_reflection_ = NULL;

}  // namespace


void protobuf_AssignDesc_bip_5flogger_2eproto() {
  protobuf_AddDesc_bip_5flogger_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "bip_logger.proto");
  GOOGLE_CHECK(file != NULL);
  M3BipLoggerStatus_descriptor_ = file->message_type(0);
  static const int M3BipLoggerStatus_offsets_[5] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, base_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, act_status_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, act_command_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, path_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, log_name_),
  };
  M3BipLoggerStatus_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3BipLoggerStatus_descriptor_,
      M3BipLoggerStatus::default_instance_,
      M3BipLoggerStatus_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerStatus, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3BipLoggerStatus));
  M3BipLoggerParam_descriptor_ = file->message_type(1);
  static const int M3BipLoggerParam_offsets_[1] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerParam, debug_),
  };
  M3BipLoggerParam_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3BipLoggerParam_descriptor_,
      M3BipLoggerParam::default_instance_,
      M3BipLoggerParam_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerParam, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerParam, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3BipLoggerParam));
  M3BipLoggerCommand_descriptor_ = file->message_type(2);
  static const int M3BipLoggerCommand_offsets_[2] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerCommand, debug_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerCommand, enable_),
  };
  M3BipLoggerCommand_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      M3BipLoggerCommand_descriptor_,
      M3BipLoggerCommand::default_instance_,
      M3BipLoggerCommand_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerCommand, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(M3BipLoggerCommand, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(M3BipLoggerCommand));
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_bip_5flogger_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3BipLoggerStatus_descriptor_, &M3BipLoggerStatus::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3BipLoggerParam_descriptor_, &M3BipLoggerParam::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    M3BipLoggerCommand_descriptor_, &M3BipLoggerCommand::default_instance());
}

}  // namespace

void protobuf_ShutdownFile_bip_5flogger_2eproto() {
  delete M3BipLoggerStatus::default_instance_;
  delete M3BipLoggerStatus_reflection_;
  delete M3BipLoggerParam::default_instance_;
  delete M3BipLoggerParam_reflection_;
  delete M3BipLoggerCommand::default_instance_;
  delete M3BipLoggerCommand_reflection_;
}

void protobuf_AddDesc_bip_5flogger_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::protobuf_AddDesc_component_5fbase_2eproto();
  ::protobuf_AddDesc_bip_5factuator_2eproto();
  ::protobuf_AddDesc_actuator_5fec_2eproto();
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n\020bip_logger.proto\032\024component_base.proto"
    "\032\022bip_actuator.proto\032\021actuator_ec.proto\""
    "\246\001\n\021M3BipLoggerStatus\022\033\n\004base\030\001 \001(\0132\r.M3"
    "BaseStatus\022(\n\nact_status\030\002 \001(\0132\024.M3BipAc"
    "tuatorStatus\022*\n\013act_command\030\003 \001(\0132\025.M3Bi"
    "pActuatorCommand\022\014\n\004path\030\004 \001(\t\022\020\n\010log_na"
    "me\030\005 \001(\t\"!\n\020M3BipLoggerParam\022\r\n\005debug\030\001 "
    "\001(\001\"3\n\022M3BipLoggerCommand\022\r\n\005debug\030\001 \001(\001"
    "\022\016\n\006enable\030\002 \001(\005B\002H\001", 340);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "bip_logger.proto", &protobuf_RegisterTypes);
  M3BipLoggerStatus::default_instance_ = new M3BipLoggerStatus();
  M3BipLoggerParam::default_instance_ = new M3BipLoggerParam();
  M3BipLoggerCommand::default_instance_ = new M3BipLoggerCommand();
  M3BipLoggerStatus::default_instance_->InitAsDefaultInstance();
  M3BipLoggerParam::default_instance_->InitAsDefaultInstance();
  M3BipLoggerCommand::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_bip_5flogger_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_bip_5flogger_2eproto {
  StaticDescriptorInitializer_bip_5flogger_2eproto() {
    protobuf_AddDesc_bip_5flogger_2eproto();
  }
} static_descriptor_initializer_bip_5flogger_2eproto_;


// ===================================================================

#ifndef _MSC_VER
const int M3BipLoggerStatus::kBaseFieldNumber;
const int M3BipLoggerStatus::kActStatusFieldNumber;
const int M3BipLoggerStatus::kActCommandFieldNumber;
const int M3BipLoggerStatus::kPathFieldNumber;
const int M3BipLoggerStatus::kLogNameFieldNumber;
#endif  // !_MSC_VER

M3BipLoggerStatus::M3BipLoggerStatus()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3BipLoggerStatus::InitAsDefaultInstance() {
  base_ = const_cast< ::M3BaseStatus*>(&::M3BaseStatus::default_instance());
  act_status_ = const_cast< ::M3BipActuatorStatus*>(&::M3BipActuatorStatus::default_instance());
  act_command_ = const_cast< ::M3BipActuatorCommand*>(&::M3BipActuatorCommand::default_instance());
}

M3BipLoggerStatus::M3BipLoggerStatus(const M3BipLoggerStatus& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3BipLoggerStatus::SharedCtor() {
  _cached_size_ = 0;
  base_ = NULL;
  act_status_ = NULL;
  act_command_ = NULL;
  path_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  log_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3BipLoggerStatus::~M3BipLoggerStatus() {
  SharedDtor();
}

void M3BipLoggerStatus::SharedDtor() {
  if (path_ != &::google::protobuf::internal::kEmptyString) {
    delete path_;
  }
  if (log_name_ != &::google::protobuf::internal::kEmptyString) {
    delete log_name_;
  }
  if (this != default_instance_) {
    delete base_;
    delete act_status_;
    delete act_command_;
  }
}

void M3BipLoggerStatus::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3BipLoggerStatus::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3BipLoggerStatus_descriptor_;
}

const M3BipLoggerStatus& M3BipLoggerStatus::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_bip_5flogger_2eproto();  return *default_instance_;
}

M3BipLoggerStatus* M3BipLoggerStatus::default_instance_ = NULL;

M3BipLoggerStatus* M3BipLoggerStatus::New() const {
  return new M3BipLoggerStatus;
}

void M3BipLoggerStatus::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (has_base()) {
      if (base_ != NULL) base_->::M3BaseStatus::Clear();
    }
    if (has_act_status()) {
      if (act_status_ != NULL) act_status_->::M3BipActuatorStatus::Clear();
    }
    if (has_act_command()) {
      if (act_command_ != NULL) act_command_->::M3BipActuatorCommand::Clear();
    }
    if (has_path()) {
      if (path_ != &::google::protobuf::internal::kEmptyString) {
        path_->clear();
      }
    }
    if (has_log_name()) {
      if (log_name_ != &::google::protobuf::internal::kEmptyString) {
        log_name_->clear();
      }
    }
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3BipLoggerStatus::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional .M3BaseStatus base = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_base()));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(18)) goto parse_act_status;
        break;
      }
      
      // optional .M3BipActuatorStatus act_status = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
         parse_act_status:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_act_status()));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(26)) goto parse_act_command;
        break;
      }
      
      // optional .M3BipActuatorCommand act_command = 3;
      case 3: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
         parse_act_command:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_act_command()));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(34)) goto parse_path;
        break;
      }
      
      // optional string path = 4;
      case 4: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
         parse_path:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_path()));
          ::google::protobuf::internal::WireFormat::VerifyUTF8String(
            this->path().data(), this->path().length(),
            ::google::protobuf::internal::WireFormat::PARSE);
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(42)) goto parse_log_name;
        break;
      }
      
      // optional string log_name = 5;
      case 5: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
         parse_log_name:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_log_name()));
          ::google::protobuf::internal::WireFormat::VerifyUTF8String(
            this->log_name().data(), this->log_name().length(),
            ::google::protobuf::internal::WireFormat::PARSE);
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3BipLoggerStatus::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional .M3BaseStatus base = 1;
  if (has_base()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessageMaybeToArray(
      1, this->base(), output);
  }
  
  // optional .M3BipActuatorStatus act_status = 2;
  if (has_act_status()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessageMaybeToArray(
      2, this->act_status(), output);
  }
  
  // optional .M3BipActuatorCommand act_command = 3;
  if (has_act_command()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessageMaybeToArray(
      3, this->act_command(), output);
  }
  
  // optional string path = 4;
  if (has_path()) {
    ::google::protobuf::internal::WireFormat::VerifyUTF8String(
      this->path().data(), this->path().length(),
      ::google::protobuf::internal::WireFormat::SERIALIZE);
    ::google::protobuf::internal::WireFormatLite::WriteString(
      4, this->path(), output);
  }
  
  // optional string log_name = 5;
  if (has_log_name()) {
    ::google::protobuf::internal::WireFormat::VerifyUTF8String(
      this->log_name().data(), this->log_name().length(),
      ::google::protobuf::internal::WireFormat::SERIALIZE);
    ::google::protobuf::internal::WireFormatLite::WriteString(
      5, this->log_name(), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3BipLoggerStatus::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional .M3BaseStatus base = 1;
  if (has_base()) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteMessageNoVirtualToArray(
        1, this->base(), target);
  }
  
  // optional .M3BipActuatorStatus act_status = 2;
  if (has_act_status()) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteMessageNoVirtualToArray(
        2, this->act_status(), target);
  }
  
  // optional .M3BipActuatorCommand act_command = 3;
  if (has_act_command()) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteMessageNoVirtualToArray(
        3, this->act_command(), target);
  }
  
  // optional string path = 4;
  if (has_path()) {
    ::google::protobuf::internal::WireFormat::VerifyUTF8String(
      this->path().data(), this->path().length(),
      ::google::protobuf::internal::WireFormat::SERIALIZE);
    target =
      ::google::protobuf::internal::WireFormatLite::WriteStringToArray(
        4, this->path(), target);
  }
  
  // optional string log_name = 5;
  if (has_log_name()) {
    ::google::protobuf::internal::WireFormat::VerifyUTF8String(
      this->log_name().data(), this->log_name().length(),
      ::google::protobuf::internal::WireFormat::SERIALIZE);
    target =
      ::google::protobuf::internal::WireFormatLite::WriteStringToArray(
        5, this->log_name(), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3BipLoggerStatus::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional .M3BaseStatus base = 1;
    if (has_base()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          this->base());
    }
    
    // optional .M3BipActuatorStatus act_status = 2;
    if (has_act_status()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          this->act_status());
    }
    
    // optional .M3BipActuatorCommand act_command = 3;
    if (has_act_command()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          this->act_command());
    }
    
    // optional string path = 4;
    if (has_path()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->path());
    }
    
    // optional string log_name = 5;
    if (has_log_name()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->log_name());
    }
    
  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3BipLoggerStatus::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3BipLoggerStatus* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3BipLoggerStatus*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3BipLoggerStatus::MergeFrom(const M3BipLoggerStatus& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_base()) {
      mutable_base()->::M3BaseStatus::MergeFrom(from.base());
    }
    if (from.has_act_status()) {
      mutable_act_status()->::M3BipActuatorStatus::MergeFrom(from.act_status());
    }
    if (from.has_act_command()) {
      mutable_act_command()->::M3BipActuatorCommand::MergeFrom(from.act_command());
    }
    if (from.has_path()) {
      set_path(from.path());
    }
    if (from.has_log_name()) {
      set_log_name(from.log_name());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3BipLoggerStatus::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3BipLoggerStatus::CopyFrom(const M3BipLoggerStatus& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3BipLoggerStatus::IsInitialized() const {
  
  return true;
}

void M3BipLoggerStatus::Swap(M3BipLoggerStatus* other) {
  if (other != this) {
    std::swap(base_, other->base_);
    std::swap(act_status_, other->act_status_);
    std::swap(act_command_, other->act_command_);
    std::swap(path_, other->path_);
    std::swap(log_name_, other->log_name_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3BipLoggerStatus::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3BipLoggerStatus_descriptor_;
  metadata.reflection = M3BipLoggerStatus_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int M3BipLoggerParam::kDebugFieldNumber;
#endif  // !_MSC_VER

M3BipLoggerParam::M3BipLoggerParam()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3BipLoggerParam::InitAsDefaultInstance() {
}

M3BipLoggerParam::M3BipLoggerParam(const M3BipLoggerParam& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3BipLoggerParam::SharedCtor() {
  _cached_size_ = 0;
  debug_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3BipLoggerParam::~M3BipLoggerParam() {
  SharedDtor();
}

void M3BipLoggerParam::SharedDtor() {
  if (this != default_instance_) {
  }
}

void M3BipLoggerParam::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3BipLoggerParam::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3BipLoggerParam_descriptor_;
}

const M3BipLoggerParam& M3BipLoggerParam::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_bip_5flogger_2eproto();  return *default_instance_;
}

M3BipLoggerParam* M3BipLoggerParam::default_instance_ = NULL;

M3BipLoggerParam* M3BipLoggerParam::New() const {
  return new M3BipLoggerParam;
}

void M3BipLoggerParam::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    debug_ = 0;
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3BipLoggerParam::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional double debug = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &debug_)));
          set_has_debug();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3BipLoggerParam::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional double debug = 1;
  if (has_debug()) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(1, this->debug(), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3BipLoggerParam::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional double debug = 1;
  if (has_debug()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(1, this->debug(), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3BipLoggerParam::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional double debug = 1;
    if (has_debug()) {
      total_size += 1 + 8;
    }
    
  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3BipLoggerParam::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3BipLoggerParam* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3BipLoggerParam*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3BipLoggerParam::MergeFrom(const M3BipLoggerParam& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_debug()) {
      set_debug(from.debug());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3BipLoggerParam::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3BipLoggerParam::CopyFrom(const M3BipLoggerParam& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3BipLoggerParam::IsInitialized() const {
  
  return true;
}

void M3BipLoggerParam::Swap(M3BipLoggerParam* other) {
  if (other != this) {
    std::swap(debug_, other->debug_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3BipLoggerParam::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3BipLoggerParam_descriptor_;
  metadata.reflection = M3BipLoggerParam_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int M3BipLoggerCommand::kDebugFieldNumber;
const int M3BipLoggerCommand::kEnableFieldNumber;
#endif  // !_MSC_VER

M3BipLoggerCommand::M3BipLoggerCommand()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void M3BipLoggerCommand::InitAsDefaultInstance() {
}

M3BipLoggerCommand::M3BipLoggerCommand(const M3BipLoggerCommand& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void M3BipLoggerCommand::SharedCtor() {
  _cached_size_ = 0;
  debug_ = 0;
  enable_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

M3BipLoggerCommand::~M3BipLoggerCommand() {
  SharedDtor();
}

void M3BipLoggerCommand::SharedDtor() {
  if (this != default_instance_) {
  }
}

void M3BipLoggerCommand::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* M3BipLoggerCommand::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return M3BipLoggerCommand_descriptor_;
}

const M3BipLoggerCommand& M3BipLoggerCommand::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_bip_5flogger_2eproto();  return *default_instance_;
}

M3BipLoggerCommand* M3BipLoggerCommand::default_instance_ = NULL;

M3BipLoggerCommand* M3BipLoggerCommand::New() const {
  return new M3BipLoggerCommand;
}

void M3BipLoggerCommand::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    debug_ = 0;
    enable_ = 0;
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool M3BipLoggerCommand::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional double debug = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(
                 input, &debug_)));
          set_has_debug();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(16)) goto parse_enable;
        break;
      }
      
      // optional int32 enable = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
         parse_enable:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &enable_)));
          set_has_enable();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }
      
      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void M3BipLoggerCommand::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional double debug = 1;
  if (has_debug()) {
    ::google::protobuf::internal::WireFormatLite::WriteDouble(1, this->debug(), output);
  }
  
  // optional int32 enable = 2;
  if (has_enable()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->enable(), output);
  }
  
  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* M3BipLoggerCommand::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional double debug = 1;
  if (has_debug()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteDoubleToArray(1, this->debug(), target);
  }
  
  // optional int32 enable = 2;
  if (has_enable()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(2, this->enable(), target);
  }
  
  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int M3BipLoggerCommand::ByteSize() const {
  int total_size = 0;
  
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional double debug = 1;
    if (has_debug()) {
      total_size += 1 + 8;
    }
    
    // optional int32 enable = 2;
    if (has_enable()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->enable());
    }
    
  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void M3BipLoggerCommand::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const M3BipLoggerCommand* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const M3BipLoggerCommand*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void M3BipLoggerCommand::MergeFrom(const M3BipLoggerCommand& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_debug()) {
      set_debug(from.debug());
    }
    if (from.has_enable()) {
      set_enable(from.enable());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void M3BipLoggerCommand::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void M3BipLoggerCommand::CopyFrom(const M3BipLoggerCommand& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool M3BipLoggerCommand::IsInitialized() const {
  
  return true;
}

void M3BipLoggerCommand::Swap(M3BipLoggerCommand* other) {
  if (other != this) {
    std::swap(debug_, other->debug_);
    std::swap(enable_, other->enable_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata M3BipLoggerCommand::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = M3BipLoggerCommand_descriptor_;
  metadata.reflection = M3BipLoggerCommand_reflection_;
  return metadata;
}


// @@protoc_insertion_point(namespace_scope)

// @@protoc_insertion_point(global_scope)
