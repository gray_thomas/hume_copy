/* 
M3 -- Meka Robotics Robot Components
Copyright (c) 2010 Meka Robotics
Author: edsinger@mekabot.com (Aaron Edsinger)

M3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with M3.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef M3BIP_SHM_SDS_H
#define M3BIP_SHM_SDS_H

#include <m3rt/base/m3rt_def.h>
#include <m3rt/base/m3ec_def.h>
#include "m3bip/components/bip_actuator_mode.pb.h"

#define MAX_NDOF 3  // per limb

typedef struct 
{
	mReal			accelerometer[3];
	mReal			magnetometer[3];
	mReal			ang_vel[3];
	mReal			orientation_mtx[9]; //M11,M12,M13,M21,M22,M23,M31,M32,M33
}M3BipImuStatus;

typedef struct 
{	
  mReal motor_temp_winding[MAX_NDOF];
  mReal motor_temp_ambient[MAX_NDOF];
  mReal motor_temp_housing[MAX_NDOF];
  mReal  current_cmd[MAX_NDOF];
  mReal  motor_torque_cmd[MAX_NDOF];
  mReal  joint_torque_cmd[MAX_NDOF];
  mReal  joint_theta_cmd[MAX_NDOF];
  mReal  current[MAX_NDOF];
  mReal  motor_torque[MAX_NDOF];
  mReal  motor_torquedot[MAX_NDOF] ;
  mReal  motor_theta[MAX_NDOF];
  mReal  motor_thetadot[MAX_NDOF];
  mReal  motor_thetadotdot[MAX_NDOF];
  mReal  joint_theta[MAX_NDOF];
  mReal  joint_thetadot[MAX_NDOF];
   mReal joint_thetadotdot[MAX_NDOF];
  mReal  joint_torque[MAX_NDOF];
   mReal joint_torquedot[MAX_NDOF];
   mReal spring_theta[MAX_NDOF];
   mReal spring_force[MAX_NDOF];
   mReal spring_forcedot[MAX_NDOF];
   bool foot_contact[MAX_NDOF];
   bool dsp_latency_bit_status[MAX_NDOF];
}M3BipLegShmStatus;

typedef struct 
{
  mReal motor_torque[MAX_NDOF];
  ACT_MODE ctrl_mode[MAX_NDOF];
    mReal joint_theta[MAX_NDOF];

    mReal joint_thetadot[MAX_NDOF];
    
  mReal joint_torque[MAX_NDOF];
  mReal motor_velocity[MAX_NDOF];	
  
  mReal k_p_jnt[MAX_NDOF];	
  mReal k_i_jnt[MAX_NDOF];	
  mReal k_d_jnt[MAX_NDOF];	
  mReal joint_stiffness[MAX_NDOF];	
  
  int32_t kp_motor[MAX_NDOF];	
  int32_t ki_motor[MAX_NDOF];	
  int32_t ki_motor_limit[MAX_NDOF];	
  int32_t kp_torque[MAX_NDOF];	
  int32_t ki_torque[MAX_NDOF];	
  int32_t ki_torque_limit[MAX_NDOF];	
  int32_t kp_motor_velocity[MAX_NDOF];
  bool dsp_latency_bit_cmd[MAX_NDOF];
}M3BipLegShmCommand;


typedef struct
{
    int64_t	timestamp;
    M3BipLegShmStatus right;
    M3BipLegShmStatus left;
    M3BipImuStatus	imu;
}M3BipShmSdsStatus;

typedef struct
{
  int64_t	timestamp;  
    M3BipLegShmCommand right;
    M3BipLegShmCommand left;
}M3BipShmSdsCommand;

#endif
