/**
 * A realtime robotics component for Meka Robotics M3 System.
 * Copyright (C) 2011 Aaron Edsinger
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include <stdio.h>
#include <m3rt/base/component.h>

#include <m3bip/components/bip_ctrl_simple.h>
#include <m3bip/components/bip_shm.h>
#include <m3bip/components/bip_logger.h>
#include <m3bip/components/bip_act_array.h>
#include <m3bip/components/bip_actuator_virtual.h>
#include <m3bip/components/bip_actuator.h>
#include <m3bip/components/bip_actuator_ec.h>

#include <m3/hardware/pwr_ec.h>
#include <m3/hardware/pwr.h>

extern "C" 
{	
	#define M3BIP_CTRL_SIMPLE_NAME			"m3bip_ctrl_simple"
	#define M3BIP_ACT_ARRAY_NAME			"m3bip_act_array"
	#define M3BIP_LOGGER_NAME				"m3bip_logger"
	#define M3BIP_SHM_NAME					"m3bip_shm"
	#define M3BIP_ACTUATOR_VIRTUAL_NAME		"m3bip_actuator_virtual"
	#define M3BIP_ACTUATOR_NAME				"m3bip_actuator"
	#define M3BIP_ACTUATOR_EC_NAME			"m3bip_actuator_ec"
	
	#define M3PWR_EC_NAME					"m3pwr_ec"
	#define M3PWR_NAME						"m3pwr"

//       std::cout<<"adding constructors"<<std::endl;
	// internal components
	m3rt::M3Component* create_bip_shm(){return new m3bip::M3BipShm;}
	void destroy_bip_shm(m3rt::M3Component* c){delete c;}
	
	m3rt::M3Component* create_bip_logger(){return new m3bip::M3BipLogger;}
	void destroy_bip_logger(m3rt::M3Component* c){delete c;}

	m3rt::M3Component* create_bip_act_array(){return new m3bip::M3BipActArray;}
 	void destroy_bip_act_array(m3rt::M3Component* c){delete c;}

	m3rt::M3Component* create_bip_actuator(){return new m3bip::M3BipActuator;}
	void destroy_bip_actuator(m3rt::M3Component* c){delete c;}

	m3rt::M3Component* create_bip_actuator_virtual(){return new m3bip::M3BipActuatorVirtual;}
	void destroy_bip_actuator_virtual(m3rt::M3Component* c){delete c;}


	m3rt::M3Component* create_bip_actuator_ec(){return new m3bip::M3BipActuatorEc;}
	void destroy_bip_actuator_ec(m3rt::M3Component* c){delete c;}
	
	m3rt::M3Component* create_bip_ctrl_simple(){return new m3bip::M3BipCtrlSimple;}
	void destroy_bip_ctrl_simple(m3rt::M3Component* c){delete c;}
	
	// external components
	m3rt::M3Component* create_pwr_ec(){return new m3::M3PwrEc;}
	void destroy_pwr_ec(m3rt::M3Component* c){delete c;}

	m3rt::M3Component* create_pwr(){return new m3::M3Pwr;}
	void destroy_pwr(m3rt::M3Component* c){delete c;}



	class M3FactoryProxy 
	{ 
		public:
		M3FactoryProxy()
		{
			
			// internal components
			m3rt::creator_factory[M3BIP_SHM_NAME] = create_bip_shm;
			m3rt::destroyer_factory[M3BIP_SHM_NAME] = destroy_bip_shm;
			
			m3rt::creator_factory[M3BIP_LOGGER_NAME] = create_bip_logger;
			m3rt::destroyer_factory[M3BIP_LOGGER_NAME] = destroy_bip_logger;

			m3rt::creator_factory[M3BIP_ACT_ARRAY_NAME] = create_bip_act_array;
			m3rt::destroyer_factory[M3BIP_ACT_ARRAY_NAME] = destroy_bip_act_array;
			
			m3rt::creator_factory[M3BIP_ACTUATOR_VIRTUAL_NAME] = create_bip_actuator_virtual;
			m3rt::destroyer_factory[M3BIP_ACTUATOR_VIRTUAL_NAME] = destroy_bip_actuator_virtual;

			m3rt::creator_factory[M3BIP_ACTUATOR_NAME] = create_bip_actuator;
			m3rt::destroyer_factory[M3BIP_ACTUATOR_NAME] = destroy_bip_actuator;

			m3rt::creator_factory[M3BIP_ACTUATOR_EC_NAME] = create_bip_actuator_ec;
			m3rt::destroyer_factory[M3BIP_ACTUATOR_EC_NAME] = destroy_bip_actuator_ec;


			m3rt::creator_factory[M3BIP_CTRL_SIMPLE_NAME] = create_bip_ctrl_simple;
			m3rt::destroyer_factory[M3BIP_CTRL_SIMPLE_NAME] = destroy_bip_ctrl_simple;
			

			// external components
			m3rt::creator_factory[M3PWR_EC_NAME] = create_pwr_ec;
			m3rt::destroyer_factory[M3PWR_EC_NAME] = destroy_pwr_ec;

			m3rt::creator_factory[M3PWR_NAME] = create_pwr;
			m3rt::destroyer_factory[M3PWR_NAME] = destroy_pwr;

		}
	};
	///////////////////////////////////////////////////////
	// The library's one instance of the proxy
	M3FactoryProxy proxy;
}
