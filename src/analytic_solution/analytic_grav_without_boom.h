#ifndef ANALYTIC_GRAVITY
#define ANALYTIC_GRAVITY

#include "util/wrap_eigen.hpp"
#include <vector>
#define NUM_JOINT 12

bool getGrav(const std::vector<double> & q, jspace::Vector & grav);

#endif
