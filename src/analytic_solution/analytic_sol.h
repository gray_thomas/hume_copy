#ifndef ANALYTIC_SOL
#define ANALYTIC_SOL

#include "util/wrap_eigen.hpp"
#include <vector>
#define NUM_JOINT 12

bool getMassMatrix(const std::vector<double> & q, jspace::Matrix & a);


#endif
