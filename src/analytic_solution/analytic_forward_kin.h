#ifndef ANALYTIC_FORWARD_KIN
#define ANALYTIC_FORWARD_KIN

#include "util/wrap_eigen.hpp"
#include <vector>
#include "Hume_Controller/HumeSystem.h"
#include <stdio.h>

void getForwardKinematics(const std::vector<double> & q, LinkID link_id, jspace::Vector & pos);

#endif
