#ifndef HUME_PROTOCOL
#define HUME_PROTOCOL


////////////////////////////////////////////////////////////////////
#define PORT_JOINT_ACT_GAIN_FROM_SYSTEM 51119
#define PORT_BODY_XZ_GAIN_FROM_TASK 51120
#define PORT_TILTING_GAIN_FROM_TASK 51121
#define PORT_HEIGHT_GAIN_FROM_TASK 51122
//FOOT
#define PORT_LEFT_FOOT_GAIN_FROM_TASK 51123
#define PORT_RIGHT_FOOT_GAIN_FROM_TASK 51124
// Height Tilting
#define PORT_HEIGHT_TILTING_GAIN_FROM_TASK 51125
// Height Tilting Foot
#define PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN_FROM_TASK 51126
#define PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN_FROM_TASK 51127
// Height Tilting Roll Foot
#define PORT_HEIGHT_TILTING_ROLL_LEFT_FOOT_GAIN_FROM_TASK 51128
#define PORT_HEIGHT_TILTING_ROLL_RIGHT_FOOT_GAIN_FROM_TASK 51129
// Height Tilting Roll
#define PORT_HEIGHT_TILTING_ROLL_GAIN_FROM_TASK 51130
// Height Tilting Foot XZ
#define PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN_FROM_TASK 51131
#define PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN_FROM_TASK 51132
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

// Body Task
#define PORT_BODY_TASK  61119
#define PORT_ORI_TASK 61120
// Body XZ Task
#define PORT_BODY_XZ_GAIN       61121
#define PORT_BODY_XZ_TASK       61122
// Hume System
#define PORT_HUME_SYSTEM        61123
#define PORT_BODY_CONTROL       61124
// Tilting Task
#define PORT_TILTING_TASK       61125
#define PORT_TILTING_GAIN       61126
//Joint
#define PORT_JOINT_ACT_DATA 61127
#define PORT_JOINT_ACT_GAIN 61128
//Height Task
#define PORT_HEIGHT_TASK 61129
#define PORT_HEIGHT_GAIN 61130
//Foot Task
#define PORT_FOOT_TASK 61131

#define PORT_LEFT_FOOT_GAIN 61132
#define PORT_RIGHT_FOOT_GAIN 61133
// Hume Data for Display
#define PORT_HUME_SYSTEM_GRAPH 61134
// Height & Tilting
#define PORT_HEIGHT_TILTING_DATA 61135
#define PORT_HEIGHT_TILTING_GAIN 61136
// Height & Tilting & Foot
#define PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN 61137
#define PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN 61138
// Height & Tilting & Roll & Foot
#define PORT_HEIGHT_TILTING_ROLL_LEFT_FOOT_GAIN 61139
#define PORT_HEIGHT_TILTING_ROLL_RIGHT_FOOT_GAIN 61140
// Height & Tilting & Roll
#define PORT_HEIGHT_TILTING_ROLL_GAIN 61141
// Height & Tilting & Foot
#define PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN 61142
#define PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN 61143




#define STR_HUME_SYSTEM "hume_system"

#include "Hume_Controller/HumeSystem.h"

namespace HumeProtocol{

    typedef struct{
        int count;

        double tot_conf[NUM_JOINT];
        double tot_vel[NUM_JOINT];

        double torque_input[NUM_ACT_JOINT];
        double curr_torque[NUM_ACT_JOINT];
        double curr_time;

        double ori[3];
        double ori_vel[3];
    
    }hume_system_data;

    typedef struct
    {
        double body_pos[3];
        double body_vel[3];

        double lfoot_pos[3];
        double rfoot_pos[3];

        double body_des[3];
    }body_ctrl_data;

    typedef struct {
        double body_des[2];
        double body_vel_des[2];
        double body_pos[2];
        double body_vel[2];
        double input[2];
    }body_xz_task;

    typedef struct{  // Yaw, Pitch, Roll
        double ori_des[3];
        double ori_vel_des[3];
        double ori[3];
        double ori_vel[3];
    }ori_task_data;
    
    typedef struct {
        double body_des[3];
        double body_vel_des[3];
        double body_pos[3];
        double body_vel[3];
    }body_task_data;

    typedef struct {
        double foot_des[3];
        double foot_vel_des[3];
        double foot_pos[3];
        double foot_vel[3];
    }foot_task_data;
    
    typedef struct{
        double kp;
        double kd;
    }pd_1_gain;
    
    typedef struct{
        double kp[2];
        double kd[2];
    }pd_2_gain;

    typedef struct{
        double kp[3];
        double kd[3];
    }pd_3_gain;


    typedef struct{
        double kp[4];
        double kd[4];
    }pd_4_gain;

    
    typedef struct{
        double kp[5];
        double kd[5];
    }pd_5_gain;

    typedef struct{
        double kp[6];
        double kd[6];
    }pd_6_gain;
    
    typedef struct {
        double height_des;
        double height_vel_des;
        double height;
        double height_vel;
    }height_task;

    typedef struct{
        double foot_des[3];
        double foot_vel_des[3];
        double foot_pos[3];
        double foot_vel[3];
    }foot_task;
    
    typedef struct {
        double ang_des;
        double ang;
        double ang_vel_des;
        double ang_vel;
        double input;
    }tilting_task;

    typedef struct {
        int16_t kp[NUM_ACT_JOINT];
        int16_t ki[NUM_ACT_JOINT];
    }pi_joint_gain;
}

#endif
