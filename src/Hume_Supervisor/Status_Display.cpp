#include <stdio.h>
#include <iostream>
#include "Text_Display.h"
#include "Graph_Display.h"


int main (int argc, char ** argv){
    Text_Display text_display;
    Graph_Display_Z_Pitch graph_display_z_pitch;
    // Graph_Display_Foot graph_display_foot;
    // Graph_Display_Gamma graph_display_gamma;
    text_display.start();
    graph_display_z_pitch.start();
    // graph_display_foot. start();
    // graph_display_gamma. start();

    return Fl::run();
}
