#include "Graph_Display.h"
#include <iostream>

#include "util/comm_udp.h"
#include "util/wrap_eigen.hpp"
#include "Hume_Controller/HumeSystem.h"

#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <math.h>
#include <time.h>


#define RGB(r,g,b) (((r)<<24)|((g)<<16)|((b)<<8))
#define IN_Y_CANVAS(x) ( ((x)>0) && ((x)<height) )

static int torso_info[3] = {0};

static bool dirty = false;
static bool bPause = false;

double gScale = 2000.0;
double gScroll = 100.;
double gTScale = 1.;
pthread_mutex_t data_mutex;

using namespace jspace;

int colors[] = {
    RGB(255,0,0),
    RGB(0,255,0),
    RGB(0,255,255),
    RGB(255,255,0),
    RGB(255,255,255),
    RGB(255,0,255),
    RGB(255,100,100),
    RGB(100,255,100),
    RGB(100,100,255),
    RGB(255,255,100),
    RGB(100,255,255),
    RGB(255,100,255)
};

TrackWindow::TrackWindow(int xx, int yy, int width, int height)
    : Fl_Widget(xx, yy, width, height, ""), cb_scale_value_(0.0), cb_tscale_value_(0.0)
{
    Fl::add_timeout(0.1, timer_cb, this);
}

TrackWindow::~TrackWindow()  {
    Fl::remove_timeout(timer_cb, this);
}

void TrackWindow::timer_cb(void * param)
{
    if ( dirty )
    {
        reinterpret_cast<TrackWindow*>(param)->redraw();
//        dirty = false;
    }
    Fl::repeat_timeout(0.3, // gets initialized within tick()
                       timer_cb,
                       param);
}

void TrackWindow::setPlotParam(double unit, double grid, double y_middle){
    unit_ = unit;
    grid_ = grid;
    y_middle_ = y_middle;
}

void TrackWindow::draw()
{
#if 0
    double scale;
    if (w() > h()) {
        scale = h() / 11.0;
    }
    else {
        scale = w() / 11.0;
    }
#endif
    int width  = w();
    int height = h();
    double const x0(width / 2.0);
    double const y0(height / 2.0);

    if ( bPause )
        fl_color(RGB(80,80,80));
    else
        fl_color(FL_BLACK);

    fl_rectf(x(), y(), width, height);
    
    int number = data_vec_.size();
    double tscale = gTScale;
    double step = 1./tscale;
    int skip = 0;
    int sample_number = width/step + 1;

    double y_center = y_middle_ + cb_scale_value_;
    double local_unit = unit_ + cb_tscale_value_;
    
    skip = number - sample_number;
    skip = (int)((double)skip * gScroll / 100.);
    if ( skip < 0 )        skip = 0;
    
    Vector curr, prev;
    fl_line_style(FL_SOLID, 1, 0);
    int i = 0;
    double j = 0;

    // unit_ = 0.1;
    // grid  = 0.01;
    // y_max =1.0);
    // y_min(0.9);
    double scale = height/local_unit;
    
    fl_color(RGB(40,40,40));
    for ( i = 0 ; i < y0 ; i += grid_ * scale )
    {
        fl_line(x(), y0 + i, width, y0 + i);
        fl_line(x(), y0 - i, width, y0 - i);
    }

    fl_color(RGB(255,255,255));


    char buf[20];
    sprintf(buf, "%+5.2lf", y_center);
    fl_draw(buf, x(), y0);
    
    for ( i = grid_ * scale, j = grid_ ; i < y0 ; i += grid_ * scale )
    {
        char buf[20];
        if ( y0 - i < 0 )
            continue;
        if ( y0 + i > height )
            continue;
        sprintf(buf, "%+5.2lf", y_center + j);
        fl_draw(buf, x(), y0 - i);
        sprintf(buf, "%-5.2lf", y_center - j);
        fl_draw(buf, x(), y0 + i);
        j += grid_;
    }
    
    fl_color(RGB(80,80,80));
    fl_line(x(), y0, width, y0);

    // Draw Graph
    int num_disp;
    if( data_vec_.size()>0){
        num_disp = data_vec_[0].rows();
    }
    std::vector<Vector>::iterator it = this->data_vec_.begin();
    std::vector<Vector>::iterator end = this->data_vec_.end();
    it += skip;

    for ( i = 0 ; it < end ; ++i )
    {
        curr = *it;
        int x_p, x_c;
        int j;
        
        if ( step >= 1. )
        {
            x_p = (i-1)*step;
            x_c = x_p + step;
            for ( j = 0 ; j < num_disp ; j++ )
            {
                if ( x_p < 0 )
                    break;

                // printf("pre: %f\n", prev[j]);
                // printf("cur: %f\n", curr[j]);
                double y_p = y0-(prev[j] - y_center)*scale;
                double y_c = y0-(curr[j] - y_center)*scale;
                
                if (!IN_Y_CANVAS(y_p))
                    continue;
                if (!IN_Y_CANVAS(y_c))
                    continue;
                
                fl_color(colors[j]);
                fl_line(x_p, y_p, x_c, y_c);
            }
            prev = curr;
            ++it;
        }
        else
        {
            int num;
            x_p = i-1;
            x_c = i;

            num = (tscale >= end-it)?(end-it):(tscale);
            for ( j = 0 ; j < num_disp ; j++ )
            {
                if ( x_p < 0 )
                    break;
                int k;
                double y_max = curr[j];
                double y_min = curr[j];
                for ( k = 0 ; k < num ; k++ )
                {
                    double y = (*(it+k))[j];
					
                    if ( y > y_max )
                        y_max = y;
                    else if ( y < y_min )
                        y_min = y;
                }

                y_min = y0-(y_min * scale);
                y_max = y0-(y_max * scale);
                double y_p = y0-(prev[j] - y_center) * scale;
                double y_c = y0-(curr[j] - y_center) * scale;

                fl_color(colors[j]);
                fl_line(x_p, y_p, x_c, y_c);
                fl_line(x_c, y_min, x_c, y_max);

            }
            prev = *(it+tscale-1);
            it += tscale;
        }
    }	

    // {
    //     static int count = 0;

    //     if ( (count % 100) == 0 )
    //         fprintf(stderr, "Step %f\n", step);
    //     ++count;
    // }	

//	pthread_mutex_unlock(&data_mutex);

#if 0
    fl_color(FL_YELLOW);
    vector<double>::iterator itx = traceX.begin();
    vector<double>::iterator itz = traceZ.begin();
    while ( itx != traceX.end() )
    {
		
        fl_point(x0+(*itx)*60, y0-(*itz)*60);
        itx++;
        itz++;
    }

    fl_color(FL_WHITE);
//	fl_point(x0+myModel.x*20, y0-myModel.z*20);
    fl_arc(x0+myModel.x*60, y0-myModel.z*60, 5, 5, 0.0, 360.0);
    printf("x    : %5f, z    : %5f\n", myModel.x, myModel.z);
/*
  fl_color(FL_WHITE);
  fl_line_style(FL_SOLID, 4, 0);
  raw_draw_tree(*sim_tree, x0, y0, scale);

  fl_color(FL_GREEN);
  fl_line_style(FL_SOLID, 10, 0);
  raw_draw_com(*sim_tree, x0, y0, scale);


  fl_line_style(0);       // back to default
*/
#endif
}





MyWindow:: 
MyWindow(int width, int height, const char * title) 
    : Fl_Double_Window(width, height, title) 
{
    printf("[My Window] Construct\n");
    Fl::visual(FL_DOUBLE|FL_INDEX); 
//    simulator = new Simulator(0, 0, width, height - 40); 
//    toggle = new Fl_Button(5, height - 35, 100, 30, "&Toggle"); 
//    toggle->callback(cb_toggle); 
//    pause = new Fl_Button(width / 2 - 50, height - 35, 100, 30, "&Pause"); 
//    pause->callback(cb_pause); 
    graph = new TrackWindow(10, 10, width - 120, height - 20 );

    quit = new Fl_Button(width - 75, height - 35, 70, 30, "&Quit"); 
    quit->callback(cb_quit, this); 
	
    save = new Fl_Button(width - 150, height - 35, 70, 30, "&Save");  
    save->callback(cb_save, this); 
	
    reset = new Fl_Button(width - 225, height - 35, 70, 30, "&Reset"); 
    reset->callback(cb_reset, this); 
	
    pause = new Fl_Button(width - 300, height - 35, 70, 30, "&Pause"); 
    pause->callback(cb_pause, this); 
	

    mScale = new Fl_Value_Slider( 5, height - 50, (width - 350)/2 - 2, 20, "");
    mScale->type(FL_HORIZONTAL);
    mScale->bounds(-5., 5.); 
    mScale->callback(cb_scale, this);
    
    mScroll = new Fl_Value_Slider( 5, height - 25, width - 350, 20, "");
    mScroll->type(FL_HORIZONTAL);
    mScroll->bounds(0., 100.); 
    mScroll->value(100.);
    mScroll->callback(cb_scroll, this);
    
    mTScale = new Fl_Value_Slider( 5 + (width - 350)/2 - 2 + 5, height - 50, (width - 350)/2 - 3, 20, "");
    mTScale->type(FL_HORIZONTAL);
    mTScale->bounds(-2., 2.); 
    mTScale->value(0.);
    mTScale->callback(cb_tscale, this);
    
    this->end(); 
    resizable(this); 
    this->show(); 
}



void MyWindow::
resize(int x, int y, int w, int h)
{
    Fl_Double_Window::resize(x, y, w, h);
    quit->resize(w-75, h-35, 70, 30);
    save->resize(w-150, h-35, 70, 30);
    reset->resize(w-225, h-35, 70, 30);
    pause->resize(w-300, h-35, 70, 30);
    graph->resize(10, 10, w-60, h-65);
    mScale->resize( 5, h - 50, (w - 380)/2-2, 20);
    mScroll->resize( 5, h - 25, w - 380, 20);
    mTScale->resize( 5 + (w-380)/2-2 + 5, h - 50, (w - 380)/2-3, 20);
}

void MyWindow::
cb_reset(Fl_Widget *widget, void *param)
{
    dirty = true;
    // _value.clear();
}
  
void MyWindow::
cb_pause(Fl_Widget *widget, void *param)
{
    dirty = true;
    bPause = !bPause;	

}
  
void MyWindow::
cb_save(Fl_Widget *widget, void *param)
{
    time_t now;
    struct tm *local;
    char buff[255];

    time(&now);

    local = localtime(&now);
	
    sprintf(buff, "GRAPH_%04d-%02d-%02d_%02d:%02d:%02d.log", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
    fprintf(stderr, "File: %s\n", buff);

    FILE *pOut;

    pOut = fopen(buff, "w");

    if ( pOut == NULL )
        return;

    // for (vector<values>::iterator it = _value.begin(); it != _value.end() ; ++it )
    // {
    // 	values curr = *it;
    // 	int j;
    // 	fprintf(pOut, "%lld ", curr.timeStamp);
    // 	for ( int j = 0 ; j < num_input ; j++ )
    // 	{
    // 		fprintf(pOut, "%f ", curr.value[j]);
    // 	}
    // 	fprintf(pOut, "\n");
    // }	
    fclose(pOut);
}

void MyWindow::
cb_quit(Fl_Widget *widget, void *param)
{
    reinterpret_cast<MyWindow*>(param)->hide();
}

void MyWindow::
cb_scale(Fl_Widget *widget, void *param)
{
    int i;
    dirty = true;
    Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
    MyWindow *pWin = (MyWindow *)param;
    pWin->graph->cb_scale_value_ = pSlider->value();
    printf("[CB scale] Value: %f\n", pSlider->value());
}

void MyWindow::
cb_tscale(Fl_Widget *widget, void *param)
{
    int i;
    dirty = true;
    Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
    MyWindow *pWin = (MyWindow *)param;
    pWin->graph->cb_tscale_value_ = pSlider->value();
}

void MyWindow::
cb_scroll(Fl_Widget *widget, void *param)
{
    int i;
    dirty = true;
    Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
    MyWindow *pWin = (MyWindow *)param;
    double value = pSlider->value();

    gScroll = value;
}


Graph_Display::Graph_Display(): Hume_Thread(),
                                win_height(400), win_width(500)
{
    win_vec_.clear();
}
 
Graph_Display::~Graph_Display(){
    for (int i(0); i< win_vec_.size(); ++i){
        delete win_vec_[i];
    }
}

void Graph_Display::add_window(char* title, double unit, double grid, double y_middle){
    MyWindow* win = new MyWindow(win_width, win_height, title);
    win->graph->setPlotParam(unit, grid, y_middle);
    win_vec_.push_back(win);
}

Graph_Display_Gamma::Graph_Display_Gamma(): Graph_Display(),
                                            socket_gamma_(0){
    add_window("J1 gamma", 100.0, 10.0, 0.0);
    add_window("J2 gamma", 100.0, 10.0, 0.0);
    add_window("J4 gamma", 100.0, 10.0, 0.0);
    add_window("J5 gamma", 100.0, 10.0, 0.0);
    
    printf("[Graph Display Gamma] Construction \n");
}
void Graph_Display_Gamma::run(void){
    HumeProtocol::hume_system_data hume_data;
    while (true){
        COMM::recieve_data(socket_gamma_, PORT_HUME_SYSTEM_GRAPH, & hume_data, sizeof(HumeProtocol::hume_system_data), IP_ADDR_MYSELF);

        Vector tmp(2);
        // J1 & J2
        for (int i(0); i<2 ; ++i){
            tmp[0] = hume_data.torque_input[1 + i];
            tmp[1] = hume_data.curr_torque[1 + i];
            win_vec_[i]->graph->data_vec_.push_back(tmp);
        }
        // J4 & J5
        for (int i(0); i<2 ; ++i){
            tmp[0] = hume_data.torque_input[4 + i];
            tmp[1] = hume_data.curr_torque [4 + i];
            win_vec_[2 + i]->graph->data_vec_.push_back(tmp);
        }

        dirty = true;
    }
}

Graph_Display_Z_Pitch::Graph_Display_Z_Pitch(): Graph_Display(),
                                                socket_tilting_task_(0),
                                                socket_body_task_(0){
    add_window("Body Z", 0.08, 0.01, 1.04);
    add_window("Pitch", 0.1, 0.01, 0.0);
    printf("[Graph Display Z & Pitch] Construction \n");
}

void Graph_Display_Z_Pitch::run(void ){
    HumeProtocol::body_task_data body_data;
    HumeProtocol::ori_task_data   ori_data;
    
    while(true){
        // Body XZ
        COMM::recieve_data(socket_body_task_, PORT_BODY_TASK, &body_data, sizeof(HumeProtocol::body_task_data), IP_ADDR_MYSELF);
        //Tilting 
        COMM::recieve_data(socket_tilting_task_, PORT_ORI_TASK, & ori_data, sizeof(HumeProtocol::ori_task_data), IP_ADDR_MYSELF);

        Vector tmp(2);
        // Z
        tmp[0] = body_data.body_des[2];
        tmp[1] = body_data.body_pos[2];
        win_vec_[0]->graph->data_vec_.push_back(tmp);

        // Tilting
        tmp[0] = ori_data.ori_des[1];
        tmp[1] = ori_data.ori[1];
        win_vec_[1]->graph->data_vec_.push_back(tmp);
        
        dirty = true;
    }
}

Graph_Display_Foot::Graph_Display_Foot(): Graph_Display(),
                                          socket_foot_task_(0){
    add_window("Foot X", 0.7, 0.2, 0.0);
    add_window("Foot Y", 0.7, 0.1, 0.0);
    add_window("Foot Z", 0.25, 0.05, 0.05);
}

void Graph_Display_Foot::run(){
    HumeProtocol::foot_task_data foot_data;

    while(true){
        //Foot
        COMM::recieve_data(socket_foot_task_, PORT_FOOT_TASK, & foot_data, sizeof(HumeProtocol::foot_task_data), IP_ADDR_MYSELF);

        Vector tmp(2);
        // Foot X, Y, Z
        for (int i(0); i<3 ; ++i){
            tmp[0] = foot_data.foot_des[i];
            tmp[1] = foot_data.foot_pos[i];
            win_vec_[i] -> graph->data_vec_.push_back(tmp);
        }
        dirty = true;
    }
}
