#include "Gain_Manager.h"

int main (int argc, char ** argv){
    // Gain_Manager_Body_XZ body_xz_gain_manager;
    // Gain_Manager_Height  height_gain_manager;
    // Gain_Manager_Foot_Left    left_foot_gain_manager;
    // Gain_Manager_Foot_Right   right_foot_gain_manager;
    // Gain_Manager_Tilting tilting_gain_manager;
    Gain_Manager_Actuator actuator_manager;
    Gain_Manager_Height_Tilting_Left_Foot_XZ height_tilting_left_foot_xz;
    Gain_Manager_Height_Tilting_Right_Foot_XZ height_tilting_right_foot_xz;

    actuator_manager.start();
    height_tilting_left_foot_xz.start();
    height_tilting_right_foot_xz.start();
    // height_gain_manager .start();
    // left_foot_gain_manager   .start();
    // right_foot_gain_manager   .start();
    // tilting_gain_manager.start();
    // body_xz_gain_manager.start();
    
    return Fl::run();
}
