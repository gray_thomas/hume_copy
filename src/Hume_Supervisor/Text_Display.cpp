#include "Text_Display.h"
#include <iostream>
#include "hume_protocol.h"
#include "util/comm_udp.h"
#include "util/wrap_eigen.hpp"
#include "Hume_Controller/HumeSystem.h"

using namespace jspace;

Text_Display::Text_Display(): Hume_Thread(), socket_(NULL){}

void Text_Display::run(void ){
    HumeProtocol::hume_system_data data;
    while(true){
        COMM::recieve_data(socket_, PORT_HUME_SYSTEM, &data, sizeof(HumeProtocol::hume_system_data), IP_ADDR_MYSELF);

        // COMM::recieve_data_unix(STR_HUME_SYSTEM, &data, sizeof(HumeProtocol::hume_system_data));
        printf("\n Count: %i\n", data.count);
        pretty_print(data.tot_conf, "Total Configuration", NUM_JOINT);
        pretty_print(data.tot_vel, "Total Velocity", NUM_JOINT);
        pretty_print(data.torque_input, "COMMAND", NUM_ACT_JOINT);
        pretty_print(data.curr_torque, "Curr Torque", NUM_ACT_JOINT);

    }
}
