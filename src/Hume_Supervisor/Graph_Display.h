#ifndef GRAPH_DISPLAY
#define GRAPH_DISPLAY

#include "util/Hume_Thread.h"
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Value_Slider.H>
#include <vector>
#include "hume_protocol.h"
#include "util/wrap_eigen.hpp"

using namespace jspace;

class TrackWindow : public Fl_Widget
{
public:
    TrackWindow(int xx, int yy, int width, int height);
    virtual ~TrackWindow();
    virtual void draw();
    void setPlotParam(double unit, double grid, double y_middle);
    void tick();
    static void timer_cb(void *param);

public:
    vector<Vector> data_vec_;
    double unit_;
    double grid_;
    double y_middle_;
    double cb_scale_value_;
    double cb_tscale_value_;
};

class MyWindow : public Fl_Double_Window 
{
public:
    MyWindow(int width, int height, const char *title);
    
    virtual void resize(int x, int y, int w, int h);	
    
    Fl_Button *quit;
    Fl_Button *save;
    Fl_Button *reset;
    Fl_Button *pause;

    Fl_Value_Slider *mScale;
    Fl_Value_Slider *mScroll;
    Fl_Value_Slider *mTScale;

    TrackWindow *graph;
    
    static void cb_quit(Fl_Widget *widget, void *param);
    static void cb_save(Fl_Widget *widget, void *param);
    static void cb_reset(Fl_Widget *widget, void *param);
    static void cb_pause(Fl_Widget *widget, void *param);
    static void cb_scale(Fl_Widget *widget, void *param);
    static void cb_tscale(Fl_Widget *widget, void *param);
    static void cb_scroll(Fl_Widget *widget, void *param);

public:

};

class Graph_Display : public Hume_Thread{
public:    
    Graph_Display();
    virtual ~Graph_Display();

protected:
    void add_window(char* title, double grid, double unit, double y_middle);
    std::vector<MyWindow*> win_vec_;
    
    int win_width;
    int win_height;
};

class Graph_Display_Gamma : public Graph_Display{
public:
    Graph_Display_Gamma();
    virtual ~Graph_Display_Gamma(){}
    virtual void run(void);

protected:
    int socket_gamma_;
};

class Graph_Display_Z_Pitch : public Graph_Display{
public:
    Graph_Display_Z_Pitch();
    virtual ~Graph_Display_Z_Pitch(){}
    virtual void run();

protected:    
    int socket_tilting_task_;
    int socket_body_task_;
};


class Graph_Display_Foot : public Graph_Display{
public:
    Graph_Display_Foot();
    virtual ~Graph_Display_Foot(){}
    virtual void run(void );

protected:
    int socket_foot_task_;
};

#endif
