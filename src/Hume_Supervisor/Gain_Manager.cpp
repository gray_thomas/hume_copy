#include "Gain_Manager.h"
#include "util/comm_udp.h"

ManageWindow::ManageWindow(int width, int _height, int num_slot, const char * title, char** slider_name) :
    height_for_one_slot_(30),
    num_slot_(num_slot), button_width_(100),
    boundary_(30),
    upper_limit_(2000),
    lower_limit_(0),
    b_set_(false),
    Fl_Double_Window(width, _height, title){
    
    mSlider_.resize(num_slot_);
    mInput_.resize(num_slot_);
    mReset_.resize(num_slot_);
    base_info_.resize(num_slot_);
    default_value_.resize(num_slot_);
    set_value_.resize(num_slot_);
    
    int i;
    
    Fl::visual(FL_DOUBLE|FL_INDEX); 
    begin(); 
    
    double height = 2*boundary_ + (2*height_for_one_slot_ + 5)*num_slot_;
    
    quit = new Fl_Button(width - 105, height - 35, 100, 30, "&Quit"); 
    quit->callback(cb_quit, this); 
    get = new Fl_Button(width - 210, height - 35, 100, 30, "&Get"); 
    get->callback(cb_get, this);
    set = new Fl_Button(width - 315, height - 35, 100, 30, "&Set"); 
    set->callback(cb_set, this); 

    double slider_width = width - button_width_ - 2*boundary_;

    for ( i = 0 ; i < num_slot_ ; i++ )
    {
        mInput_[i] = new Fl_Float_Input(250, boundary_ + (2*height_for_one_slot_ + 5)*i, button_width_, height_for_one_slot_, ""); 
        mInput_[i]->callback(cb_head, this); 
    }
    for ( i = 0 ; i < num_slot_ ; i++ )
    {
        mSlider_[i] = new Fl_Value_Slider( boundary_, boundary_ + (2*height_for_one_slot_ + 5)*i,
                                           slider_width, height_for_one_slot_,
                                           slider_name[i]);
        mSlider_[i]->type(FL_HORIZONTAL);
        mSlider_[i]->bounds(lower_limit_, upper_limit_);
        mSlider_[i]->callback(cb_head, this);
        
        mReset_[i] = new Fl_Button(boundary_ +slider_width + 20, boundary_ + (2*height_for_one_slot_ + 5)*i,
                                   button_width_, height_for_one_slot_, "&Reset"); 
        mReset_[i]->callback(cb_reset, this); 
    }

    end(); 
    resizable(this); 
    show(); 
}

void ManageWindow::set_default_info(int idx, double value){
    default_value_[idx] = value;
}

void ManageWindow::cb_get(Fl_Widget * widget, void * param){
    ManageWindow* pWin = (ManageWindow*) param;

    int num_slot(pWin->num_slot_);

    for(int i(0); i< num_slot; ++i){
        char buf[20];
        pWin->mSlider_[i]->value( pWin->default_value_[i] );
        pWin->base_info_[i] = pWin->default_value_[i];
        sprintf(buf, "%f", pWin->default_value_[i]);
        pWin->mInput_[i]->value(buf );
    }
}

void ManageWindow::cb_set(Fl_Widget * widget, void * param){
    ManageWindow* pWin = (ManageWindow*) param;

    int num_slot(pWin->num_slot_);

    for(int i(0); i< num_slot; ++i){
        pWin->set_value_[i] = pWin->mSlider_[i] -> value();
    }
    pWin->b_set_ = true;
}

void ManageWindow::cb_reset(Fl_Widget *widget, void *param)
{
    int i, idx;
    bool dirty = false;
    Fl_Button *mReset = (Fl_Button *)widget;
    ManageWindow *pWin = (ManageWindow *)param;
    int num_slot(pWin->num_slot_);
    
    for ( i = 0 ; i < num_slot ; i++ )
    {
        if ( (widget == pWin->mReset_[i]) && (pWin->base_info_[i] != pWin->default_value_[i]) )
        {
            pWin->base_info_[i] = pWin->default_value_[i];
            dirty = true;
            idx = i;
        }
    }
    if ( dirty )
    {
        pWin->mSlider_[idx]->value(pWin->base_info_[idx]);
        char buf[20];
        sprintf(buf, "%f", (double)pWin->base_info_[idx]);
        pWin->mInput_[idx]->value(buf);
    }
}

void ManageWindow::
cb_head(Fl_Widget *widget, void *param)
{
    int i, idx;
    bool dirty = false;
    Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
    Fl_Float_Input  *pInput = (Fl_Float_Input *)widget; 
    ManageWindow *pWin = (ManageWindow *)param;
    int num_slot(pWin->num_slot_);
    
    float value;
    for ( i = 0 ; i < num_slot ; i++ )
    {
        if ( (pSlider == pWin->mSlider_[i]) && (pWin->base_info_[i] != value) )
        {
            value = pSlider->value();
            pWin->base_info_[i] = value;
            dirty = true;
            idx = i;
        }
        else if ( (pInput == pWin->mInput_[i]) && (pWin->base_info_[i] != value) )
        {
            value = atof(pInput->value());
            pWin->base_info_[i] = value;
            dirty = true;
            idx = i;
        }
    }

    if ( dirty )
    {
        pWin->mSlider_[idx]->value(pWin->base_info_[idx]);
        char buf[20];
        sprintf(buf, "%f", (double)(pWin->base_info_[idx]));
        pWin->mInput_[idx]->value(buf);
    }
}

void ManageWindow::resize(int x, int y, int w, int h)
{
    Fl_Double_Window::resize(x, y, w, h);
    quit->resize(w-105, h-35, 100, 30);
}

void ManageWindow::cb_quit(Fl_Widget *widget, void *param)
{
    reinterpret_cast<ManageWindow*>(param)->hide();
}


Gain_Manager::Gain_Manager(): Hume_Thread(),
                              win_width_(500){}

Gain_Manager_Body_XZ::Gain_Manager_Body_XZ():Gain_Manager(),
                                             socket_body_xz_recieve_(0),
                                             socket_body_xz_send_(0){
    // XZ Task
    char* slot_name[4]{
            (char*)"[Kp] X",
            (char*)"[Kp] Z",
            (char*)"[Kd] X",
            (char*)"[Kd] Z" };
    
    add_manager("Body xz task", 4, slot_name);
}

Gain_Manager_Tilting::Gain_Manager_Tilting():Gain_Manager(),
                                             socket_tilting_recieve_(0),
                                             socket_tilting_send_(0){

    // Tilting Task
    char* slot_name_tilting[2]{ (char*)"[Kp]",
            (char*)"[Kd]"};
    add_manager("Tilting task", 2, slot_name_tilting);
}

Gain_Manager_Actuator::Gain_Manager_Actuator(): Gain_Manager(),
                                                socket_actuator_recieve_(0),
                                                socket_actuator_send_(0){
    // Actuator
    char* slot_name_actuator[2*NUM_ACT_JOINT]{
        (char*) "[Kp] Joint 0",
            (char*) "[Kp] Joint 1",
            (char*) "[Kp] Joint 2",
            (char*) "[Kp] Joint 3",
            (char*) "[Kp] Joint 4",
            (char*) "[Kp] Joint 5",
            (char*) "[Ki] Joint 0",
            (char*) "[Ki] Joint 1",
            (char*) "[Ki] Joint 2",
            (char*) "[Ki] Joint 3",
            (char*) "[Ki] Joint 4",
            (char*) "[Ki] Joint 5" };
    add_manager("Actuator", 2*NUM_ACT_JOINT, slot_name_actuator);
}
Gain_Manager_Height_Tilting_Left_Foot:: Gain_Manager_Height_Tilting_Left_Foot():
    Gain_Manager(),
    socket_height_tilting_left_foot_recieve_(0),
    socket_height_tilting_left_foot_send_(0){
    
    char * slot_name [ 10] {
        (char*)"[Kp] Height",
            (char*)"[Kp] Tilting",
            (char*)"[Kp] Foot X",
            (char*)"[Kp] Foot Y",
            (char*)"[Kp] Foot Z",
            (char*)"[Kd] Height",
            (char*)"[Kd] Tilting",
            (char*)"[Kd] Foot X",
            (char*)"[Kd] Foot Y",
            (char*)"[Kd] Foot Z"};
    add_manager("Height & Tilting & Left Foot", 10, slot_name);
}

void Gain_Manager_Height_Tilting_Left_Foot::run(){
    while(true){
        recieve_send_pd_gain(0, 5,
                             socket_height_tilting_left_foot_recieve_,
                             socket_height_tilting_left_foot_send_,
                             PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN_FROM_TASK,
                             PORT_HEIGHT_TILTING_LEFT_FOOT_GAIN);
    }
}

void Gain_Manager_Height_Tilting_Right_Foot::run(){
    while(true){
        recieve_send_pd_gain(0, 5,
                             socket_height_tilting_right_foot_recieve_,
                             socket_height_tilting_right_foot_send_,
                             PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN_FROM_TASK,
                             PORT_HEIGHT_TILTING_RIGHT_FOOT_GAIN);
    }
}

Gain_Manager_Height_Tilting_Right_Foot:: Gain_Manager_Height_Tilting_Right_Foot():
    Gain_Manager(),
    socket_height_tilting_right_foot_recieve_(0),
    socket_height_tilting_right_foot_send_(0){
    
    char * slot_name [ 10] {
        (char*)"[Kp] Height",
            (char*)"[Kp] Tilting",
            (char*)"[Kp] Foot X",
            (char*)"[Kp] Foot Y",
            (char*)"[Kp] Foot Z",
            (char*)"[Kd] Height",
            (char*)"[Kd] Tilting",
            (char*)"[Kd] Foot X",
            (char*)"[Kd] Foot Y",
            (char*)"[Kd] Foot Z"};
    add_manager("Height & Tilting & Right Foot", 10, slot_name);
}


Gain_Manager_Height::Gain_Manager_Height(): Gain_Manager(),
                                            socket_height_recieve_(0),
                                            socket_height_send_(0){
    // Hieght Task
    char * slot_name_height[2]{ (char*) "[Kp]",
            (char*) "[Kd]"};
    add_manager("Height Task", 2, slot_name_height);
}

Gain_Manager_Foot_Left::Gain_Manager_Foot_Left():Gain_Manager(),
                                                 socket_foot_recieve_(0),
                                                 socket_foot_send_(0){
    // Foot Task
    char * slot_name_foot[6]{
        (char*) "[Kp] Foot X",
            (char *) "[Kp] Foot Y",
            (char *) "[Kp] Foot Z",
            (char* ) "[Kd] Foot X",
            (char *) "[Kd] Foot Y",
            (char *) "[Kd] Foot Z"        };
    add_manager("Left Foot Task", 6, slot_name_foot);
}

Gain_Manager_Foot_Right::Gain_Manager_Foot_Right():Gain_Manager(),
                                                 socket_foot_recieve_(0),
                                                 socket_foot_send_(0){
    // Foot Task
    char * slot_name_foot[6]{
        (char*) "[Kp] Foot X",
            (char *) "[Kp] Foot Y",
            (char *) "[Kp] Foot Z",
            (char* ) "[Kd] Foot X",
            (char *) "[Kd] Foot Y",
            (char *) "[Kd] Foot Z"        };
    add_manager("Right Foot Task", 6, slot_name_foot);
}


Gain_Manager::~Gain_Manager(){
    for( int i(0); i< win_vec_.size(); ++i){
        delete win_vec_[i];
    }
}

void Gain_Manager::add_manager(char* title, int num_slot, char** name_list){
    ManageWindow* win = new ManageWindow(win_width_, num_slot*70 +50, num_slot, title, name_list);
    win_vec_.push_back(win);
}

void Gain_Manager_Body_XZ::run(){
    while(true){
        recieve_send_pd_gain(0, 2, socket_body_xz_recieve_, socket_body_xz_send_, PORT_BODY_XZ_GAIN_FROM_TASK, PORT_BODY_XZ_GAIN);
    }
}

void Gain_Manager_Tilting::run(){
    while(true){
        recieve_send_pd_gain(0, 1, socket_tilting_recieve_, socket_tilting_send_, PORT_TILTING_GAIN_FROM_TASK, PORT_TILTING_GAIN);
    }
}

void Gain_Manager_Actuator::run(){
    while(true){
        recieve_send_pi_gain(0, socket_actuator_recieve_, socket_actuator_send_, PORT_JOINT_ACT_GAIN_FROM_SYSTEM, PORT_JOINT_ACT_GAIN);
    }
}

void Gain_Manager_Height::run(){
    while(true){
        recieve_send_pd_gain(0, 1, socket_height_recieve_, socket_height_send_, PORT_HEIGHT_GAIN_FROM_TASK, PORT_HEIGHT_GAIN);
    }
}

void Gain_Manager_Foot_Left::run(){
    while(true){
        recieve_send_pd_gain(0, 3, socket_foot_recieve_, socket_foot_send_, PORT_LEFT_FOOT_GAIN_FROM_TASK, PORT_LEFT_FOOT_GAIN);
    }
}

void Gain_Manager_Foot_Right::run(){
    while(true){
        recieve_send_pd_gain(0, 3, socket_foot_recieve_, socket_foot_send_, PORT_RIGHT_FOOT_GAIN_FROM_TASK, PORT_RIGHT_FOOT_GAIN);
    }
}

void Gain_Manager::recieve_send_pi_gain(int idx, int & recieve_socket, int & send_socket, int recieve_port, int send_port){
    int data_size = sizeof(HumeProtocol::pi_joint_gain);
    HumeProtocol::pi_joint_gain recieve_gain_set;
    HumeProtocol::pi_joint_gain send_gain_set;
    
    COMM::recieve_data(recieve_socket, recieve_port, & recieve_gain_set, data_size, IP_ADDR_MYSELF);
    for( int i(0); i< NUM_ACT_JOINT; ++i){
        win_vec_[idx]->set_default_info(i, (double)recieve_gain_set.kp[i]);
        win_vec_[idx]->set_default_info(i + NUM_ACT_JOINT, (double)recieve_gain_set.ki[i]);
    }

    if(win_vec_[idx]->b_set_){
        for (int i(0); i< NUM_ACT_JOINT; ++i){
            send_gain_set.kp[i] = (int32_t)win_vec_[idx]->set_value_[i];
            send_gain_set.ki[i] = (int32_t)win_vec_[idx]->set_value_[i + NUM_ACT_JOINT];
        }
        COMM::send_data(send_socket, send_port, & send_gain_set, data_size, IP_ADDR_MYSELF);
        win_vec_[idx]->b_set_ = false;
    }
}


void Gain_Manager::recieve_send_pd_gain(int win_idx, int num_gain, int & recieve_socket, int & send_socket, int recieve_port, int send_port){
    int data_size;
    int * recieve_gain_set;
    int * send_gain_set;

    switch (num_gain) {
    case 1:
        data_size = sizeof(HumeProtocol::pd_1_gain);
        HumeProtocol::pd_1_gain recieve_gain_set_1;
        HumeProtocol::pd_1_gain send_gain_set_1;

        COMM::recieve_data(recieve_socket, recieve_port, &recieve_gain_set_1, data_size, IP_ADDR_MYSELF);
        set_default_value(win_idx, num_gain, &recieve_gain_set_1);
        if(win_vec_[win_idx]->b_set_){
            set_gain_set(win_idx, num_gain, &send_gain_set_1);        
            COMM::send_data(send_socket, send_port, &send_gain_set_1, data_size, IP_ADDR_MYSELF);
            win_vec_[win_idx]->b_set_ = false;
        }
        break;
    case 2:
        data_size = sizeof(HumeProtocol::pd_2_gain);
        HumeProtocol::pd_2_gain recieve_gain_set_2;
        HumeProtocol::pd_2_gain send_gain_set_2;

        COMM::recieve_data(recieve_socket, recieve_port, &recieve_gain_set_2, data_size, IP_ADDR_MYSELF);
        set_default_value(win_idx, num_gain, &recieve_gain_set_2);

        if(win_vec_[win_idx]->b_set_){
            set_gain_set(win_idx, num_gain, &send_gain_set_2);        
            COMM::send_data(send_socket, send_port, &send_gain_set_2, data_size, IP_ADDR_MYSELF);
            win_vec_[win_idx]->b_set_ = false;
        }
        break;
    case 3:
        data_size = sizeof(HumeProtocol::pd_3_gain);
        HumeProtocol::pd_3_gain recieve_gain_set_3;
        HumeProtocol::pd_3_gain send_gain_set_3;

        COMM::recieve_data(recieve_socket, recieve_port, &recieve_gain_set_3, data_size, IP_ADDR_MYSELF);
        set_default_value(win_idx, num_gain, &recieve_gain_set_3);

        if(win_vec_[win_idx]->b_set_){
            set_gain_set(win_idx, num_gain, &send_gain_set_3);        
            COMM::send_data(send_socket, send_port, &send_gain_set_3, data_size, IP_ADDR_MYSELF);
            win_vec_[win_idx]->b_set_ = false;
        }
        break;
    case 4:
        data_size = sizeof(HumeProtocol::pd_4_gain);
        HumeProtocol::pd_3_gain recieve_gain_set_4;
        HumeProtocol::pd_3_gain send_gain_set_4;

        COMM::recieve_data(recieve_socket, recieve_port, &recieve_gain_set_4, data_size, IP_ADDR_MYSELF);
        set_default_value(win_idx, num_gain, &recieve_gain_set_4);

        if(win_vec_[win_idx]->b_set_){
            set_gain_set(win_idx, num_gain, &send_gain_set_4);        
            COMM::send_data(send_socket, send_port, &send_gain_set_4, data_size, IP_ADDR_MYSELF);
            win_vec_[win_idx]->b_set_ = false;
        }
        break;
    case 5:
        data_size = sizeof(HumeProtocol::pd_5_gain);
        HumeProtocol::pd_5_gain recieve_gain_set_5;
        HumeProtocol::pd_5_gain send_gain_set_5;

        COMM::recieve_data(recieve_socket, recieve_port, &recieve_gain_set_5, data_size, IP_ADDR_MYSELF);
        set_default_value(win_idx, num_gain, &recieve_gain_set_5);

        if(win_vec_[win_idx]->b_set_){
            set_gain_set(win_idx, num_gain, &send_gain_set_5);        
            COMM::send_data(send_socket, send_port, &send_gain_set_5, data_size, IP_ADDR_MYSELF);
            win_vec_[win_idx]->b_set_ = false;
        }
        break;
    }
}

void Gain_Manager::set_default_value(int win_idx, int num_gain, void * recieve_gain_set){

    switch (num_gain){
    case 1:
        win_vec_[win_idx]->set_default_info(0, ((HumeProtocol::pd_1_gain *)recieve_gain_set)->kp);
        win_vec_[win_idx]->set_default_info(1, ((HumeProtocol::pd_1_gain *)recieve_gain_set)->kd);
        break;
        
    case 2:
        for (int i(0); i< num_gain; ++i){
            win_vec_[win_idx]->set_default_info(i, ((HumeProtocol::pd_2_gain*)recieve_gain_set)->kp[i]);
            win_vec_[win_idx]->set_default_info(i + num_gain, ((HumeProtocol::pd_2_gain*)recieve_gain_set)->kd[i]);  
        }
        break;
    case 3:
        for (int i(0); i< num_gain; ++i){
            win_vec_[win_idx]->set_default_info(i, ((HumeProtocol::pd_3_gain*)recieve_gain_set)->kp[i]);
            win_vec_[win_idx]->set_default_info(i + num_gain, ((HumeProtocol::pd_3_gain*)recieve_gain_set)->kd[i]);  
        }
        break;
    case 4:
        for (int i(0); i< num_gain; ++i){
            win_vec_[win_idx]->set_default_info(i, ((HumeProtocol::pd_4_gain*)recieve_gain_set)->kp[i]);
            win_vec_[win_idx]->set_default_info(i + num_gain, ((HumeProtocol::pd_4_gain*)recieve_gain_set)->kd[i]);  
        }
        break;
    case 5:
        for (int i(0); i< num_gain; ++i){
            win_vec_[win_idx]->set_default_info(i, ((HumeProtocol::pd_5_gain*)recieve_gain_set)->kp[i]);
            win_vec_[win_idx]->set_default_info(i + num_gain, ((HumeProtocol::pd_5_gain*)recieve_gain_set)->kd[i]);  
        }
        break;
    }
}

void Gain_Manager::set_gain_set(int win_idx, int num_gain, void * send_gain_set){

    switch (num_gain){
    case 1:
        ((HumeProtocol::pd_1_gain*)send_gain_set)->kp = win_vec_[win_idx]->set_value_[0];
        ((HumeProtocol::pd_1_gain*)send_gain_set)->kd = win_vec_[win_idx]->set_value_[1];
        break;
    case 2:
        for (int i(0); i< num_gain; ++i){
            ((HumeProtocol::pd_2_gain*)send_gain_set)->kp[i] = win_vec_[win_idx]->set_value_[i];
            ((HumeProtocol::pd_2_gain*)send_gain_set)->kd[i] = win_vec_[win_idx]->set_value_[i + num_gain];
        }
        break;
    case 3:
        for (int i(0); i< num_gain; ++i){
            ((HumeProtocol::pd_3_gain*)send_gain_set)->kp[i] = win_vec_[win_idx]->set_value_[i];
            ((HumeProtocol::pd_3_gain*)send_gain_set)->kd[i] = win_vec_[win_idx]->set_value_[i + num_gain];
        }
        break;
    case 4:
        for (int i(0); i< num_gain; ++i){
            ((HumeProtocol::pd_4_gain*)send_gain_set)->kp[i] = win_vec_[win_idx]->set_value_[i];
            ((HumeProtocol::pd_4_gain*)send_gain_set)->kd[i] = win_vec_[win_idx]->set_value_[i + num_gain];
        }
        break;
    case 5:
        for (int i(0); i< num_gain; ++i){
            ((HumeProtocol::pd_5_gain*)send_gain_set)->kp[i] = win_vec_[win_idx]->set_value_[i];
            ((HumeProtocol::pd_5_gain*)send_gain_set)->kd[i] = win_vec_[win_idx]->set_value_[i + num_gain];
        }
        break;

    }
}

Gain_Manager_Height_Tilting_Left_Foot_XZ:: Gain_Manager_Height_Tilting_Left_Foot_XZ():
    Gain_Manager(),
    socket_height_tilting_left_foot_xz_recieve_(0),
    socket_height_tilting_left_foot_xz_send_(0){
    
    char * slot_name [ 8] {
        (char*)"[Kp] Height",
            (char*)"[Kp] Tilting",
            (char*)"[Kp] Foot X",
            (char*)"[Kp] Foot Z",
            (char*)"[Kd] Height",
            (char*)"[Kd] Tilting",
            (char*)"[Kd] Foot X",
            (char*)"[Kd] Foot Z"};
    add_manager("Height & Tilting & Left Foot XZ", 8, slot_name);
}

void Gain_Manager_Height_Tilting_Left_Foot_XZ::run(){
    while(true){
        recieve_send_pd_gain(0, 4,
                             socket_height_tilting_left_foot_xz_recieve_,
                             socket_height_tilting_left_foot_xz_send_,
                             PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN_FROM_TASK,
                             PORT_HEIGHT_TILTING_LEFT_FOOT_XZ_GAIN);
    }
}

void Gain_Manager_Height_Tilting_Right_Foot_XZ::run(){
    while(true){
        recieve_send_pd_gain(0, 4,
                             socket_height_tilting_right_foot_xz_recieve_,
                             socket_height_tilting_right_foot_xz_send_,
                             PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN_FROM_TASK,
                             PORT_HEIGHT_TILTING_RIGHT_FOOT_XZ_GAIN);
    }
}

Gain_Manager_Height_Tilting_Right_Foot_XZ:: Gain_Manager_Height_Tilting_Right_Foot_XZ():
    Gain_Manager(),
    socket_height_tilting_right_foot_xz_recieve_(0),
    socket_height_tilting_right_foot_xz_send_(0){
    
    char * slot_name [ 8] {
        (char*)"[Kp] Height",
            (char*)"[Kp] Tilting",
            (char*)"[Kp] Foot X",
            (char*)"[Kp] Foot Z",
            (char*)"[Kd] Height",
            (char*)"[Kd] Tilting",
            (char*)"[Kd] Foot X",
            (char*)"[Kd] Foot Z"};
    add_manager("Height & Tilting & Right Foot XZ", 8, slot_name);
}
