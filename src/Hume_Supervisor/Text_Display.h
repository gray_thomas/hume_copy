#ifndef TEXT_DISPLAY
#define TEXT_DISPLAY

#include "util/Hume_Thread.h"

class Text_Display : public Hume_Thread{
public:    
    Text_Display();
    virtual ~Text_Display(){}
    virtual void run(void );

protected:
    int socket_;
};



#endif
