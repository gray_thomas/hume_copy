#ifndef ORIENTATION_CALCULATOR
#define ORIENTATION_CALCULATOR

#include "util/wrap_eigen.hpp"
#include "AttitudeEstimator/noEigenAttitudeEstimatorInterface.hpp"

#include <vector>

using namespace std;
using namespace jspace;

class filter;
// Body orientation: Rz, Ry, Rx
// IMU orientation: Rx, Ry, Rz

class Orientation_Calculator{
public:
    Orientation_Calculator();
    ~Orientation_Calculator(){}

    bool mocap_ori_imu_vel(int curr_count, int preparing_count,
                           const vector<double> & imu_ang,
                           const vector<double> & imu_ang_vel,
                           Vector & Body_ori,
                           Vector & Body_ori_vel);

    bool mocap_imu_vel_fusion(int curr_count, int preparing_count,
                              double time_step,
                              const vector<double> & imu_ang,
                              const vector<double> & imu_ang_vel,
                              Vector & Body_ori,
                              Vector & Body_ori_vel);
    
    bool imu_vel_int(int curr_count, int preparing_count, double time_step,
                     const vector<double> & imu_ang,
                     const vector<double> & imu_ang_vel,
                     Vector & Body_ori,
                     Vector & Body_ori_vel);
    bool imu_ori_filter_vel(int curr_count, int preparing_count,
                            const vector<double> & imu_ang,
                            const vector<double> & imu_ang_vel,
                            Vector & Body_ori,
                            Vector & Body_ori_vel);

    bool mocap_default_pose_estimator(int curr_count, int preparing_count,
                                      const vector<double> & imu_ang,
                                      const vector<double> &  imu_ang_vel,
                                      Vector & Body_ori, Vector & Body_ori_vel);
protected:
    uta::attitudeEstimator::noEigenInterface::HumePoseEstimator* poseEstimator_;
    Vector Body_ori_vel_pre_;
    std::vector<filter*> filter_ori_vel_;
};

#endif
