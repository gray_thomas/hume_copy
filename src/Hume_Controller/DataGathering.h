#ifndef DATA_GATHERING_
#define DATA_GATHERING_

#include "util/Hume_Thread.h"

class HUME_System;

class Data_Gathering: public Hume_Thread{
public:
    Data_Gathering(HUME_System* hume_system);
    virtual ~Data_Gathering(void){}

    virtual void run(void);

    int Data_Gathering_Holding_;
    
protected:
    int socket_;
    int socket_actuator_;
    int socket_display_;
    HUME_System* hume_system_;
    void _LoadingHumeSystemData();
    void _LoadingTaskData();
    void _LoadingActuatorData();
};

#endif
