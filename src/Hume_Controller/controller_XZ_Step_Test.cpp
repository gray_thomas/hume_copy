#include "controller_XZ_Step_Test.h"

Ctrl_XZ_Step_Test::Ctrl_XZ_Step_Test(HUME_System* _hume_system): Virtual_Ctrl_XZ_Step(_hume_system){
    lifting_time_ = 0.3;
    landing_time_ = 0.3;
    supp_time_    = 0.005;
    supp_lift_transition_time_ = 0.1;
    land_supp_transition_time_ = 0.1;
    lifting_height_ = 0.2;

    constraint_ = new Hume_Contact_Both_Abduction();
    constraint_left_ = new Hume_Point_Contact_Abduction(LFOOT);
    constraint_right_ = new Hume_Point_Contact_Abduction(RFOOT);

    dual_contact_task_ = new Height_Tilting_Task(hume_system_);
    single_contact_lfoot_ = new Height_Tilting_Foot_XZ_Task(hume_system_, LFOOT);
    single_contact_rfoot_ = new Height_Tilting_Foot_XZ_Task(hume_system_, RFOOT);
    
    printf("[3D Step Control] Start\n");
}

Ctrl_XZ_Step_Test::~Ctrl_XZ_Step_Test(){
    delete constraint_;
    delete constraint_left_;
    delete constraint_right_;
}


void Ctrl_XZ_Step_Test::_set_land_location(){
    landing_loc_ = foot_pre_;
    landing_loc_[2] = -0.05;
}


bool Ctrl_XZ_Step_Test::_Do_Supp_Lift(){
    for (int i(0); i< 3; ++i){
        foot_des_[i] = _smooth_changing(foot_pre_[i], apex_foot_loc_[i],
                                        lifting_time_, state_machine_time_);
        
        foot_vel_des_[i] = _smooth_changing_vel(foot_pre_[i], apex_foot_loc_[i],
                                                lifting_time_, state_machine_time_);
    }
    _set_single_contact_task();
    
    switch (swing_foot_){
    case RFOOT:
        task_array_.push_back(single_contact_rfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_left_, gamma_);
        break;
    case LFOOT:
        task_array_.push_back(single_contact_lfoot_);
        wbc_->MakeTorque(curr_conf_, task_array_, constraint_right_, gamma_);
        break;
    }

    if(state_machine_time_ > lifting_time_){
        _end_action();
        _set_land_location();
        return false;
    }
    state_machine_time_ += SERVO_RATE;        
    return true;
}
