#ifndef CONSTRAINT_H
#define CONSTRAINT_H


#include "HumeSystem.h"
#include "util/wrap_eigen.hpp"

using namespace std;

namespace jspace {
  
  class Model;

  class WBC_Constraint {
  public:
      WBC_Constraint();
      virtual ~WBC_Constraint(){}
      virtual bool getU(Matrix & U);
      virtual bool updateJcU(const vector<double> & conf, HUME_System * model) =0;
      virtual bool getJc(Matrix & Jc);
      virtual bool getJcBar(Matrix const Ainv, Matrix & JcBar);
      virtual bool getNc(Matrix const Ainv,
                           Matrix & Nc);
      virtual bool getUNc(Matrix const Ainv,
                            Matrix & UNc);
      virtual bool getUNcBar(Matrix const Ainv,
                             Matrix & UNcBar);
      virtual bool getInternal_Matrix(Matrix & W_int);
      virtual bool getSelection(Vector & Fr, Matrix& S);
//      virtual void getFullState(State const & state, State & fullState) = 0;
      bool remove_internal(){ return b_internal_; }
      bool reaction_force_control(){ return b_re_force_ctrl_; }

      virtual void set_constraint_weight(const Vector & weigth){}
      virtual void set_internal_force_weight(const Vector & int_weight){}
      virtual void getDesInternalForce(Vector & des_int_force);
      bool b_constraint_weight_;
      bool b_internal_weight_;
      bool b_change_internal_force_;

      Vector internal_weight_;
      Vector constraint_weight_;
      Matrix W_int_;
      int num_constraint(){ return num_constraint_; }
      Matrix Jc_;
      bool b_re_force_ctrl_;
  protected:
      int num_constraint_;
      Matrix U_;

      double sigmaThreshold_;
      bool b_internal_;

  };
}

#endif
