#include "task_library.h"

///////////////////////////////////////////////////////
//////////         COM Height Tilting Roll
///////////////////////////////////////////////////////
COM_Height_Tilting_Roll_Task::COM_Height_Tilting_Roll_Task(HUME_System* hume_system):
    // COM height, Body Tilting (Ry), Body Roll (Rx)
    error_sum_(Vector::Zero(3)),
    Task(hume_system){
    
    force_ = Vector::Zero(3);
    Ki_ = Vector::Zero(3);
    Kp_ = Vector(3);
    Kd_ = Vector(3);

    Kp_[0] = 450.0;
    Kp_[1] = 400.0;
    Kp_[2] = 80.0;

    Ki_[0] = 55.0;
    Ki_[1] = 55.0;
    Ki_[2] = 10.0;

    Kd_[0] = 5.0;
    Kd_(1) = 5.0;
    Kd_[2] = 5.0;
    
}

Matrix COM_Height_Tilting_Roll_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(3, NUM_JOINT);
    Matrix Jcom;
    hume_system_->getCOMJacobian(conf, Jcom);
    J.block(0,0, 1, NUM_JOINT) = Jcom.block(2, 0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;
    return J;
}

Vector COM_Height_Tilting_Roll_Task::getCommand(){
    Vector input(3);
    // COM
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[2])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->COM_vel_[2]);
    // Pitch
    input[1] = Kp_(1)*(des_[1] - hume_system_->getOri()[1])
        + Kd_(1)*(vel_des_[1] - hume_system_ ->getOri_vel()[1]);
    // Roll
    input[2] = Kp_(2)*(des_[2] - hume_system_->getOri()[2])
        + Kd_(2)*(vel_des_[2] - hume_system_ ->getOri_vel()[2]);
    
    error_sum_[0] += (des_[0] - hume_system_->COM_pos_[2])*SERVO_RATE;
    error_sum_[1] += (des_[1] - hume_system_->getOri()[1])*SERVO_RATE;
    error_sum_[2] += (des_[2] - hume_system_->getOri()[2])*SERVO_RATE;
    
    for(int i(0); i< 3; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -10.0, 10.0, "[(COM) Height & Tilting & Roll] error sum");
        input[i] += error_sum_[i] * Ki_[i];
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    data_body_.body_pos[2] = hume_system_->COM_pos_[2];
    data_body_.body_vel[2] = hume_system_->COM_vel_[2];
    
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    
    data_ori_.ori[1] = hume_system_->getOri()[1];
    data_ori_.ori_vel[1] = hume_system_->getOri_vel()[1];


    // Save Data -- Ori (Roll)
    data_ori_.ori_des[2] = des_[2];
    data_ori_.ori_vel_des[2] = vel_des_[2];
    
    data_ori_.ori[2] = hume_system_->getOri()[2];
    data_ori_.ori_vel[2] = hume_system_->getOri_vel()[2];

    
    double limit = 1000.0;
    for (int i(0); i< 2; ++i){
        input[i] = crop_value(input[i], -limit, limit, "(COM) Height & Tilting & Roll");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

void COM_Height_Tilting_Roll_Task::send(){
    _SaveBodyData();
    _SaveOrientationData();
}

void COM_Height_Tilting_Roll_Task::set_Gain(){
}


///////////////////////////////////////////////////////
//////////         COM XZ Ry Rx 
///////////////////////////////////////////////////////
COM_XZ_RyRx_Task::COM_XZ_RyRx_Task(HUME_System* hume_system):Task(hume_system){
    // COM XZ, Body Tilting (Ry), Body Roll (Rx)
    num_control_DOF_ = 4;
    error_sum_ = Vector::Zero(num_control_DOF_);

    force_ = Vector::Zero(num_control_DOF_);
    Ki_ = Vector::Zero(num_control_DOF_);
    Kp_ = Vector(num_control_DOF_);
    Kd_ = Vector(num_control_DOF_);

    Kp_[0] = 15.0;
    Kp_[1] = 200.0;
    Kp_[2] = 150.0;
    Kp_[3] = 250.0;

    Ki_[0] = 0.0;
    Ki_[1] = 30.0;
    Ki_[2] = 15.0;
    Ki_[3] = 15.0;

    Kd_[0] = 0.0;
    Kd_[1] = 3.0;
    Kd_[2] = 7.0;
    Kd_[3] = 1.0;
}


Matrix COM_XZ_RyRx_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(num_control_DOF_, NUM_JOINT);
    Matrix Jcom;
    hume_system_->getCOMJacobian(conf, Jcom);
    J.block(0,0, 1, NUM_JOINT) = Jcom.block(0, 0, 1, NUM_JOINT);
    J.block(1,0, 1, NUM_JOINT) = Jcom.block(2, 0, 1, NUM_JOINT);
    J(2, Ry) = 1.0;
    J(3, Rx) = 1.0;
    return J;
}

Vector COM_XZ_RyRx_Task::getCommand(){
    Vector input(num_control_DOF_);
    // XZ
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[0])
        + Kd_[0]*(vel_des_[0] - hume_system_ ->COM_vel_[0]);

    input(1) = Kp_(1)*(des_[1] - hume_system_->COM_pos_[2])
        + Kd_[1]*(vel_des_[1] - hume_system_ ->COM_vel_[2]);
    
    // Pitch
    input[2] = Kp_(2)*(des_[2] - hume_system_->getOri()[1])
        + Kd_(2)*(vel_des_[2] - hume_system_ ->getOri_vel()[1]);
    // Roll
    input[3] = Kp_(3)*(des_[3] - hume_system_->getOri()[2])
        + Kd_(3)*(vel_des_[3] - hume_system_ ->getOri_vel()[2]);

    error_sum_[0] += (des_[0] - hume_system_->COM_pos_[0])*SERVO_RATE;
    error_sum_[1] += (des_[1] - hume_system_->COM_pos_[2])*SERVO_RATE;
    error_sum_[2] += (des_[2] - hume_system_->getOri()[1])*SERVO_RATE;
    error_sum_[3] += (des_[3] - hume_system_->getOri()[2])*SERVO_RATE;
    
    for(int i(0); i< num_control_DOF_; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -0.3, 0.3, "[(COM) XZ RyRx] error sum");
        input[i] += error_sum_[i] * Ki_[i];
    }
    double limit = 1000.0;
    for (int i(0); i< num_control_DOF_; ++i){
        input[i] = crop_value(input[i], -limit, limit, "(COM) Height & Tilting & Roll");
    }
    
    //Save Data -- Body (XZ)
    data_body_.body_des[0] = des_[0];
    data_body_.body_vel_des[0] = vel_des_[0];
    data_body_.body_des[2] = des_[1];
    data_body_.body_vel_des[2] = vel_des_[1];
    
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[2];
    data_ori_.ori_vel_des[1] = vel_des_[2];
    
    // Save Data -- Ori (Roll)
    data_ori_.ori_des[2] = des_[3];
    data_ori_.ori_vel_des[2] = vel_des_[3];

    // Save Current Data
    for(int i(0); i<3; ++i){
        data_body_.body_pos[i] = hume_system_->COM_pos_[i];
        data_body_.body_vel[i] = hume_system_->COM_vel_[i];
        data_ori_.ori[i] = hume_system_->getOri()[i];
        data_ori_.ori_vel[i] = hume_system_->getOri_vel()[i];
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

void COM_XZ_RyRx_Task::send(){
    _SaveBodyData();
    _SaveOrientationData();
}

void COM_XZ_RyRx_Task::set_Gain(){
}


///////////////////////////////////////////////////////
//////////     COM HEIGHT TILTING Roll FOOT TASK
///////////////////////////////////////////////////////
COM_Height_Tilting_Roll_Foot_Task::COM_Height_Tilting_Roll_Foot_Task( HUME_System* hume_system, LinkID link_id):
    Task(hume_system), link_id_(link_id)
{
    // Integral Gain
    num_control_DOF_ = 6;
    error_sum_ = Vector::Zero(num_control_DOF_);
    Ki_ = Vector::Zero(num_control_DOF_);
    // PD Gain
    Kp_ = Vector(num_control_DOF_);
    Kd_ = Vector(num_control_DOF_);

    force_ = Vector::Zero(num_control_DOF_);
}

void COM_Height_Tilting_Roll_Foot_Task::SetTask(const Vector & des, const Vector & vel_des){

    des_ = des;
    vel_des_ = vel_des;
    
    //Height Tilting Roll
    Kp_[0] = 350.0;
    Kp_[1] = 300.0;
    Kp_[2] = 300.0;

    Ki_[0] = 60.0;
    Ki_[1] = 40.0;
    Ki_[2] = 30.0;

    Kd_[0] = 10.0;
    Kd_[1] = 5.0;
    Kd_[2] = 5.0;

    //Foot (X, Y, Z)
    Kp_[3] = 500.0;
    Kp_[4] = 300.0;
    Kp_[5] = 505.0;

    Ki_[3] = 50.0;
    Ki_[4] = 30.0;
    Ki_[5] = 50.0;

    Kd_[3] = 20.0;
    Kd_[4] = 10.0;
    Kd_[5] = 20.0;

    
    // Full Position X, Y, Z
    hume_system_->CopyFoot(curr_foot_pos_, link_id_);
    hume_system_->CopyFootVel(curr_foot_vel_, link_id_);
    
    b_settask_ = true;
}

Matrix COM_Height_Tilting_Roll_Foot_Task::getJacobian(const vector<double> & conf){
    Matrix J = Matrix::Zero(num_control_DOF_, NUM_JOINT);
    Matrix J_com, J_foot;
    hume_system_->getCOMJacobian(conf, J_com);
    J.block(0,0, 1, NUM_JOINT) = J_com.block(2,0, 1, NUM_JOINT);
    J(1, Ry) = 1.0;
    J(2, Rx) = 1.0;

    hume_system_->GetFullJacobian(conf, link_id_, J_foot);
    J.block(3, 0, 3, NUM_JOINT) = J_foot.block(0,0, 3, NUM_JOINT);
    return J;
}


void COM_Height_Tilting_Roll_Foot_Task::setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel){
    curr_foot_pos_ = curr_foot_pos;
    curr_foot_vel_ = curr_foot_vel;
}

Vector COM_Height_Tilting_Roll_Foot_Task::getCommand(){
    Vector input(num_control_DOF_);
    // Body Height
    input(0) = Kp_(0)*(des_[0] - hume_system_->COM_pos_[2])
        + Kd_(0)*(vel_des_[0] - hume_system_ ->COM_vel_[2]);

    // Pitch
    input[1] = Kp_[1] * (des_[1] - hume_system_->getOri()[1])
        + Kd_[1] * (vel_des_[1] - hume_system_ ->getOri()[1]);

    // Roll
    input[2] = Kp_[2] * (des_[2] - hume_system_->getOri()[2])
        + Kd_[2] * (vel_des_[2] - hume_system_ ->getOri()[2]);
    
    // Foot 
    for (int i(0); i< 3; ++i){
        input[i+3] = Kp_[i+3] * (des_[i+3] - curr_foot_pos_[i]) +
            Kd_[i+3] * (vel_des_[i+3] - curr_foot_vel_[i]);
    }

    //
    error_sum_[0] += (des_[0] - hume_system_->COM_pos_[2])*SERVO_RATE;
    error_sum_[1] += (des_[1] - hume_system_->getOri()[1])*SERVO_RATE;
    error_sum_[2] += (des_[2] - hume_system_->getOri()[2])*SERVO_RATE;

    // Foot
    for (int i(0); i< 3; ++i){
        error_sum_[i+3] += (des_[i+3] - curr_foot_pos_[i])*SERVO_RATE;
    }
  
    // Integral Control
    for(int i(0); i<6; ++i){
        error_sum_[i] = crop_value(error_sum_[i], -5.0, 5.0, "[COM height tilting foot]error sum");
        input[i] += Ki_[i] * error_sum_[i];
    }
    
    //Save Data -- Body (Height)
    data_body_.body_des[2] = des_[0];
    data_body_.body_vel_des[2] = vel_des_[0];

    for (int i(0); i<3; ++i){
        data_body_.body_pos[i] = hume_system_->COM_pos_[i];
        data_body_.body_vel[i] = hume_system_->COM_vel_[i];
    }
    
    // Save Data -- Ori (pitch)
    data_ori_.ori_des[1] = des_[1];
    data_ori_.ori_vel_des[1] = vel_des_[1];
    data_ori_.ori_des[2] = des_[2];
    data_ori_.ori_vel_des[2] = vel_des_[2];

    for (int i(0); i< 3; ++i){
        data_ori_.ori[i] = hume_system_->getOri()[i];
        data_ori_.ori_vel[i] = hume_system_->getOri_vel()[i];
    }
    
    //Save Data -- Foot
    for (int i(0); i<3; ++i){
        data_foot_.foot_des[i] = des_[i + 3];
        data_foot_.foot_vel_des[i] = vel_des_[i + 3];
    }
 
    for (int i(0); i<3; ++i){
        data_foot_.foot_pos[i] = curr_foot_pos_[i];
        data_foot_.foot_vel[i] = curr_foot_vel_[i];
    }

    double limit = 1050.0;
    for (int i(0); i< num_control_DOF_; ++i){
        input[i] = crop_value(input[i], -limit, limit, "COM Pos & Foot XZ");
    }
    // Post Process
    _PostProcess_Task();

    return input;
}

void COM_Height_Tilting_Roll_Foot_Task::send(){
    _SaveBodyData();
    _SaveOrientationData();
    _SaveFootData(link_id_);
}

void COM_Height_Tilting_Roll_Foot_Task::set_Gain(){
}

