#include "Planner.h"
#include <math.h>
#include "util/wrap_eigen.hpp"

using namespace jspace;
using namespace std;

Planner::Planner() :Hume_Thread(),
                    g_(9.8),
                    velocity_limit_(2),
                    phase_goal_position(2),
                    x_foot_place_limit_(2),
                    y_foot_place_limit_(2),
                    next_foot_placement_(3),
                    calculating_start_(false),
                    end_of_calculation_(false),
                    IsRunning_(false)
{
    // 0: positive velocity
    // 1: negative velocity
    x_foot_place_limit_(0) =  -0.27; //0.11
    x_foot_place_limit_(1) = 0.27; 
    y_foot_place_limit_(0) =  0.1;
    y_foot_place_limit_(1) = 0.27;
    phase_goal_position(0) = 0.2; // meters in the desired direction of motion
    phase_goal_position(1) = 0.05; // meters/second in the direction of desired motion

    // x_foot_place_limit_(0) =  -0.527; //0.11
    // x_foot_place_limit_(1) =   0.527; 
    // y_foot_place_limit_(0) =  0.0;
    // y_foot_place_limit_(1) = 0.537;

    boom_system_ratio_ = 1.0;
    
    velocity_limit_(0) = 1.05;
    velocity_limit_min_ = 0.25;
    velocity_limit_(1) = -1.05;
}

void Planner::_SavePlannerData(const std::vector<Vector> & x_phase_path,
                               const std::vector<Vector> & y_phase_path)
{
	static int num_plan(1);

	vector<Vector> next_y_path;
	vector<Vector> integrated_path_y;
	int inter_frame_y;
	vector<Vector> next_x_path;
	vector<Vector> integrated_path_x;
	int inter_frame_x;

	_MakeHalfPhasePath(*x_phase_path.rbegin(), next_foot_placement_[0] - com_pos_[0], next_x_path);

	_MakeHalfPhasePath(*y_phase_path.rbegin(), next_foot_placement_[1] - com_pos_[1], next_y_path);

	_save_planned_path(y_phase_path, "y_phase_path", num_plan);
	_save_planned_path(next_x_path, "next_x_path", num_plan);
	_save_planned_path(x_phase_path, "x_phase_path", num_plan);
	_save_planned_path(next_y_path, "next_y_path", num_plan);

	saveVector(com_pos_, "com_pos_pl");
	saveVector(foot_pos_, "local_stance_foot");
	saveVector(next_foot_placement_, "foot_placement");

	// ///  Save & Print    //////////////////////////////////////////////////////
	// /// planner info
	saveValue(inter_frame_x, "inter_frame_x");
	saveValue(inter_frame_y, "inter_frame_y");
	saveValue(switching_time_, "switching_time");

	++num_plan;
}
void Planner::_save_planned_path(const std::vector<jspace::Vector> & path, string name, int num_plan)
{
	string filename;
	stringstream ss;
	ss << num_plan;
	filename += ss.str();
	filename += "_";
	filename += name;
//    sprintf(filename, "%i_%s", num_plan, name);
//    sprintf(filename, "%s", name);
	saveVectorSequence(path, filename);
}

void Planner::_IntegrateTwoPath(const std::vector<Vector> & path_1, const std::vector<Vector> & path_2,
										std::vector<Vector> & int_path, int & cross_frame)
{
	int_path = path_1;
	for (int i(0); i < path_2.size(); ++i)
	{
		int_path.push_back(path_2[i]);
	}
	cross_frame = path_1.size();
}

void Planner::_MakeHalfPhasePath(const Vector& ini, double foot_place, std::vector<Vector> & path)
{
	int max_iter(2000);
	int iter(0);
	vector<Vector> pre_path;
	vector<Vector> nx_path;
	Vector tmp_state(2);
	path.clear();

	double x_ddot, x_dot, x, xp;

	xp = foot_place;
	x = ini(0);
	x_dot = ini(1);

	while (true)
	{
		x_ddot = boom_system_ratio_ * g_ / h_ * (x - xp);
		x_dot = x_dot + x_ddot * SERVO_RATE;
		x = x + x_dot * SERVO_RATE + 0.5 * x_ddot * pow(SERVO_RATE, 2);

		tmp_state(0) = x;
		tmp_state(1) = x_dot;

		path.push_back(tmp_state);

		iter++;
		if (iter > max_iter)
		{
			break;
		}
	}
}

void Planner::_MakePhasePath(const Vector & x_ini, double foot_pos, std::vector<Vector> & path, bool forcing)
{
	int max_iter(1500);
	int iter(0);

	Vector tmp_state(2);
	path.clear();

	double x_ddot, x_dot, x, xp;
	double x_dot_nx, x_nx;
	xp = foot_pos;
	x = x_ini(0);
	x_dot = x_ini(1);
	double limit_upp, limit_low;

	while ((fabs(x_dot) < velocity_limit_min_) || (SERVO_RATE * iter < switching_time_))
	{
		x_ddot = boom_system_ratio_ * g_ / h_ * (x - xp);
		x_dot_nx = x_dot + x_ddot * SERVO_RATE;
		x_nx = x + x_dot * SERVO_RATE + 0.5 * x_ddot * pow(SERVO_RATE, 2);

		tmp_state(0) = x;
		tmp_state(1) = x_dot;

		path.push_back(tmp_state);

		x_dot = x_dot_nx;
		x = x_nx;
		x_dot = x_dot_nx;

		iter++;

		if (x_dot > velocity_limit_(0) || x_dot < velocity_limit_(1))
		{
			printf("Exceed velocity limit\n");
			switching_time_ = iter * SERVO_RATE;
			break;
		}
		if (forcing)
		{
			if (iter * SERVO_RATE > switching_time_)
			{
                            printf("switching time: %f, \n switching velocity: %f\n", SERVO_RATE * iter, x_dot);
				break;
			}
		}
		if (iter > max_iter)
		{
			printf("Phase Path: Hit the maximum iteration, Switching Velocity: %f\n", x_dot);
			break;
		}
	}
	switching_time_ = SERVO_RATE * iter;
}


///////////////////////////
void Planner::run()
{
	while (true)
	{
		if (calculating_start_)
		{
			printf("[Planner] Calculating Start\n");
			IsRunning_ = true;
			// Calculation();
                        Calculation();
			end_of_calculation_ = true;
			calculating_start_ = false;
		}
		usleep(100);
	}
}
