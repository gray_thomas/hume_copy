#ifndef CONTROLLER_BODY_TEST
#define CONTROLLER_BODY_TEST

#include "Controller_Hume.h"

class Ctrl_Body_Test: public Controller_Hume{
public:
    Ctrl_Body_Test(HUME_System* _hume_system);
    virtual ~Ctrl_Body_Test();
    
public:
    virtual void getCurrentCommand(std::vector<double> & command);
    virtual void setCurrentConfiguration();
    
public:
    Task * com_task_;

    jspace::Vector pos_des_;
    jspace::Vector vel_des_;

protected:
    double omega_x_;
    double amp_x_;
    double omega_height_;
    double amp_height_;
    double amp_tilting_;
    double omega_tilting_;
    LinkID stance_foot_;
    LinkID moving_foot_;

    double des_height_;
    int count_command_;
    
    // Function
    void _SetDesPos();
};

#endif
