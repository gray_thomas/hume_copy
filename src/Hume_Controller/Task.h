#ifndef _TASK_
#define _TASK_

#include "HumeSystem.h"
#include <string>
#include "Hume_Supervisor/hume_protocol.h"
#include "util/comm_udp.h"
#include "GainTuner.h"

using namespace std;
using namespace jspace;

class Task{
public:
    Task(HUME_System* hume_system);
    virtual ~Task(){}

    virtual Matrix getJacobian(const vector<double> & conf) = 0 ;
    virtual Vector getCommand() = 0;
    virtual void SetTask(const Vector & , const Vector & );
    
    virtual Vector getForce() { return force_; }

    double crop_value(double value, double min, double max, char* source);

public:
    HUME_System* hume_system_;
    bool b_settask_;
    virtual void send() =0;
    virtual void set_Gain() = 0;
    bool b_set_weight_;
    virtual void set_weight(const Vector & weight){}
    Vector force_;
    
protected:
    Vector des_;
    Vector vel_des_;
    
    Vector weight_;
    int num_control_DOF_;
    int socket_;
    int socket_gain_send_;
    int socket_recieve_;
    Gain_Tuner* gain_tuner_;

    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;
    
    virtual void _PostProcess_Task();
    void _SaveBodyData();
    void _SaveOrientationData();
    void _SaveFootData(LinkID foot);
};

class Body_Task: public Task{
public:
    Body_Task(HUME_System*);
    virtual ~Body_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}
public:
    Vector  Body_d_;
    Vector  Body_vel_d_;
    Vector  Kp_;
    Vector  Kd_;
};

class Body_XZ_Task: public Task{
public:
    Body_XZ_Task(HUME_System*);
    virtual ~Body_XZ_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();
    virtual void set_weight(const Vector & weight);
public:
    Vector  Body_d_;
    Vector  Body_vel_d_;
    Vector  Kp_;
    Vector  Kd_;
    HumeProtocol::body_task_data data_;
    HumeProtocol::pd_2_gain gain_data_;
};


class Height_Task: public Task{
public:
    Height_Task(HUME_System*);
    virtual ~Height_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();

public:
    Vector height_d_;
    Vector height_vel_d_;
    Vector Kp_;
    Vector Kd_;

    HumeProtocol::body_task_data data_;
    HumeProtocol::pd_1_gain gain_data_;
};

class Tilting_Task: public Task{
public:
    Tilting_Task(HUME_System*);
    virtual ~Tilting_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();
    virtual void set_weight(const Vector & weight);
public:
    Vector ang_d_;
    Vector ang_vel_d_;
    Vector Kp_;
    Vector Kd_;
    HumeProtocol::ori_task_data data_;
    HumeProtocol::pd_1_gain gain_data_;
};

class ORI_Task: public Task{
public:
    ORI_Task(HUME_System*);
    virtual ~ORI_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}

public:
    Vector ORI_d_;
    Vector ORI_vel_d_;
    Vector Kp_;
    Vector Kd_;
};

class ORI_YX_Task: public Task{
public:
    ORI_YX_Task(HUME_System*);
    virtual ~ORI_YX_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}

public:
    Vector ORI_yx_d_;
    Vector ORI_yx_vel_d_;
    Vector Kp_;
    Vector Kd_;
};


class FOOT_Task: public Task{
public:
    FOOT_Task(HUME_System*, LinkID);
    virtual ~FOOT_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );
    void setCurrentFoot(const Vector & curr_foot_pos, const Vector & curr_foot_vel);

    virtual void send();
    virtual void set_Gain();
    virtual void set_weight(const Vector & weight);
    
public:
    LinkID link_id_;    
    int send_gain_port_;
    int recieve_gain_port_;
    
    Vector FOOT_d_;
    Vector FOOT_vel_d_;
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;

    Vector error_sum_;

    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    bool b_set_curr_foot_;
    HumeProtocol::foot_task_data data_;
    HumeProtocol::pd_3_gain gain_data_;
};

class Joint_Task: public Task{
public:
    Joint_Task(HUME_System *);
    virtual ~Joint_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector& , const Vector &);


    virtual void send(){}
    virtual void set_Gain(){}

public:
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
    // From Right Abduction
    Vector jpos_des_;
    Vector jvel_des_;
};

class Height_Tilting_Task:public Task{
public:
    Height_Tilting_Task(HUME_System*);
    virtual ~Height_Tilting_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector &, const Vector& );

    virtual void send();
    virtual void set_Gain();

public:
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
    // Height & Pitch
    Vector des_;
    Vector vel_des_;
    int socket_2_;
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::pd_2_gain gain_data_;

};

class COM_Height_Tilting_Task:public Task{
public:
    COM_Height_Tilting_Task(HUME_System*);
    virtual ~COM_Height_Tilting_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector &, const Vector& );

    virtual void send();
    virtual void set_Gain();

public:
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
    // Height & Pitch
    Vector des_;
    Vector vel_des_;
    int socket_2_;
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::pd_2_gain gain_data_;

};


class XZ_Tilting_Task:public Task{
    // X, Z, Tilting of Virtual Joint
public:
    XZ_Tilting_Task(HUME_System*);
    virtual ~XZ_Tilting_Task(){}
    
    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    // useless
    virtual void set_Gain();

public:
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
    // X & Height & Pitch
    Vector des_;
    Vector vel_des_;
    int socket_2_;
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::pd_2_gain gain_data_;
};

class COM_Tilting_Task:public Task{
    // COM (XZ), Tilting
public:
    COM_Tilting_Task(HUME_System*);
    virtual ~COM_Tilting_Task(){}
    
    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    // useless
    virtual void set_Gain();

public:
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
    // X & Height & Pitch
    Vector des_;
    Vector vel_des_;
    int socket_2_;
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::pd_2_gain gain_data_;
};


class Height_Tilting_Roll_Task:public Task{
public:
    Height_Tilting_Roll_Task(HUME_System*);
    virtual ~Height_Tilting_Roll_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector &, const Vector& );

    virtual void send();
    virtual void set_Gain();

public:
    Vector Kp_;
    Vector Kd_;
    // Height & Pitch
    Vector des_;
    Vector vel_des_;
    int socket_2_;
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::pd_3_gain gain_data_;
};


class Height_Tilting_Foot_Task: public Task{
public:
    Height_Tilting_Foot_Task(HUME_System*, LinkID);
    virtual ~Height_Tilting_Foot_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector&, const Vector&);

    virtual void send();
    virtual void set_Gain();
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    LinkID link_id_;    

    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
    //Height & Pitch & Foot
    Vector des_;
    Vector vel_des_;
    // Socket for sending orientation and foot position data 
    int socket_2_;
    int socket_3_;

    int send_gain_port_;
    int recieve_gain_port_;
    
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;

    HumeProtocol::pd_5_gain gain_data_;
};
    
class Height_Tilting_Foot_XZ_Task: public Task{
public:
    Height_Tilting_Foot_XZ_Task(HUME_System*, LinkID);
    virtual ~Height_Tilting_Foot_XZ_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector&, const Vector&);

    virtual void send();
    virtual void set_Gain();
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    LinkID link_id_;    
    //
    Vector error_sum_;
    Vector Ki_;
    
    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
    //Height & Pitch & Foot
    Vector des_;
    Vector vel_des_;
    // Socket for sending orientation and foot position data 
    int socket_2_;
    int socket_3_;

    int send_gain_port_;
    int recieve_gain_port_;
    
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;

    HumeProtocol::pd_4_gain gain_data_;
};

class COM_Height_Tilting_Foot_XZ_Task: public Task{
public:
    COM_Height_Tilting_Foot_XZ_Task(HUME_System*, LinkID);
    virtual ~COM_Height_Tilting_Foot_XZ_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector&, const Vector&);

    virtual void send();
    virtual void set_Gain();
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    LinkID link_id_;    
    //
    Vector error_sum_;
    Vector Ki_;
    
    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
    //Height & Pitch & Foot
    Vector des_;
    Vector vel_des_;
    // Socket for sending orientation and foot position data 
    int socket_2_;
    int socket_3_;

    int send_gain_port_;
    int recieve_gain_port_;
    
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;

    HumeProtocol::pd_4_gain gain_data_;
};


class Height_Tilting_Roll_Foot_Task: public Task{
public:
    Height_Tilting_Roll_Foot_Task(HUME_System*, LinkID);
    virtual ~Height_Tilting_Roll_Foot_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector&, const Vector&);

    virtual void send();
    virtual void set_Gain();
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    LinkID link_id_;    

    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
    //Height & Pitch & Foot
    Vector des_;
    Vector vel_des_;
    // Socket for sending orientation and foot position data 
    int socket_2_;
    int socket_3_;

    int send_gain_port_;
    int recieve_gain_port_;
    
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;

    HumeProtocol::pd_6_gain gain_data_;
};


#endif
