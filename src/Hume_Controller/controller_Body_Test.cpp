#include "controller_Body_Test.h"
#include <math.h>
#include <stdio.h>
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"
#include "Process.h"
#include "task_library.h"

//#define XZ_Ry_TASK
#define XZ_RyRx_TASK

Ctrl_Body_Test::Ctrl_Body_Test(HUME_System * hume_system):
    Controller_Hume(hume_system),
    stance_foot_(RFOOT), moving_foot_(LFOOT), pos_des_(4), vel_des_(Vector::Zero(4)),
    omega_x_(1.0), amp_x_(0.0), //0.05
    omega_height_(1.0), amp_height_(0.0), count_command_(0), //0.09
    amp_tilting_(0.0),
    omega_tilting_(1.2)
{
    // constraint_ = new Hume_Contact_Both_Abduction();
    constraint_ = new Hume_Contact_Both();

#ifdef XZ_Ry_TASK    
    com_task_ = new COM_Tilting_Task(hume_system_);
#endif

#ifdef XZ_RyRx_TASK
    com_task_ = new COM_XZ_RyRx_Task(hume_system_);
#endif
    pro_stable_ = new stabilizing(hume_system_);

    printf("[Body Control] Start\n");
}

Ctrl_Body_Test::~Ctrl_Body_Test(){
    delete com_task_;
}

void Ctrl_Body_Test::getCurrentCommand(std::vector<double> & command){
    _PreProcessing_Command();
    ++count_command_;
    
    double curr_time = ((double)count_command_)*SERVO_RATE;

    if(pro_stable_->do_process(this)){
        _PostProcessing_Command(command);
        count_command_ = 0;
        return;
    }
    phase_ = 1;
    task_array_.push_back(com_task_);
    
    _SetDesPos();
    
    wbc_->MakeTorque(hume_system_->tot_configuration_, task_array_, constraint_, gamma_);
    
    _PostProcessing_Command(command);
}

void Ctrl_Body_Test::setCurrentConfiguration(){
    _SetLocalConfiguration2StanceFoot(stance_foot_);
}

void Ctrl_Body_Test::_SetDesPos(){
#ifdef XZ_Ry_TASK
    // X, Z
    pos_des_[0] = pos_ini_[0] + 0.5*amp_x_ * (1- cos(count_command_ * SERVO_RATE * omega_x_  + M_PI/2.0) );
    vel_des_[0] = 0.5*omega_x_ * amp_x_ * sin(count_command_ * SERVO_RATE * omega_x_ + M_PI*0.5);
    
    pos_des_[1] = pos_ini_[2] + 0.5*amp_height_*(1 - cos(count_command_*SERVO_RATE*omega_height_));
    vel_des_[1] = 0.5*amp_height_*omega_height_*sin(count_command_*SERVO_RATE*omega_height_);
    // Pitch
    pos_des_[2] = ori_ini_[1];
    vel_des_[2] = 0.0;
#endif

#ifdef XZ_RyRx_TASK
    // Position
    // XZ
    pos_des_[0] = pos_ini_[0] + 0.5*amp_x_ * (1- cos(count_command_ * SERVO_RATE * omega_x_) );
    pos_des_[1] = pos_ini_[2] + 0.5*amp_height_*(1 - cos(count_command_*SERVO_RATE*omega_height_  + M_PI/2.0));
    // Ry Rx
    pos_des_[2] = ori_ini_[1];
    pos_des_[3] = ori_ini_[2];

    // Velocity
    vel_des_[0] = 0.5*omega_x_ * amp_x_ * sin(count_command_ * SERVO_RATE * omega_x_);
    vel_des_[1] = 0.5*amp_height_*omega_height_*sin(count_command_*SERVO_RATE*omega_height_  + M_PI/2.0);

#endif
    com_task_->SetTask(pos_des_, vel_des_);
}
