#include "Gray_planner.h"
#include <math.h>
#include "util/wrap_eigen.hpp"

using namespace jspace;
using namespace std;

Gray_planner::Gray_planner() : Planner(),
			landing_time(), //relative to end of lifting time
			lifting_time(), //duration of total lift trajectory
			lifting_ratio(), //ratio of planning point to total lift
			transition_landing_time(),
			transition_lifting_time(),
			dual_support_time()
{
    // 0: positive velocity
    // 1: negative velocity
    x_foot_place_limit_(0) =  -0.27; //0.11
    x_foot_place_limit_(1) = 0.27; 
    y_foot_place_limit_(0) =  0.1;
    y_foot_place_limit_(1) = 0.27;
    phase_goal_position(0) = 0.2; // meters in the desired direction of motion
    phase_goal_position(1) = 0.05; // meters/second in the direction of desired motion

    // x_foot_place_limit_(0) =  -0.527; //0.11
    // x_foot_place_limit_(1) =   0.527; 
    // y_foot_place_limit_(0) =  0.0;
    // y_foot_place_limit_(1) = 0.537;

    boom_system_ratio_ = 1.0;
    
    velocity_limit_(0) = 1.05;
    velocity_limit_min_ = 0.25;
    velocity_limit_(1) = -1.05;
}

void Gray_planner::Calculation()
{
	printf("[Planner]Alternate Version Calculation Start\n");
	h_ = com_pos_[2];
	foot_pos_ -= com_pos_;
	// Draw The future Phase path before switching time
	Vector x_ini(2);
	x_ini(0) = 0.0;
	x_ini(1) = com_vel_[0];
	double timeToLand(
			(1 - this->lifting_ratio) * this->lifting_time + this->landing_time + this->transition_landing_time
					+ this->dual_support_time * 0.5);
	double timeFromPlanning(
			(this->lifting_ratio) * this->lifting_time + this->transition_lifting_time
					+ this->dual_support_time * 0.5);

	bool desireForwardMotion;
	if (x_ini(1) < 0)
		desireForwardMotion = true;
	else
		desireForwardMotion = false;

	vector<Vector> x_phase_path;
	vector<Vector> x_phase_path_backwards;
	Vector x_backwards_ini(2);
	Vector phaseGoal = this->phase_goal_position;
	if (!desireForwardMotion)
		phaseGoal = -phaseGoal;
	x_backwards_ini(0) = 0.0;
	x_backwards_ini(1) = phaseGoal(1);
	double x_backwards_foot = -phaseGoal(0);
	double x_forwards_foot = foot_pos_(0);

	// Initial Integration
	this->integrate(x_backwards_ini, x_backwards_foot, x_phase_path_backwards, false, timeFromPlanning);
	this->integrate(x_ini, x_forwards_foot, x_phase_path, true, timeToLand);

	Vector finalXForwards(*x_phase_path.rbegin());
	Vector finalXBackwards(*x_phase_path_backwards.rbegin());

	double finalForwardsAccel = boom_system_ratio_ * g_ / h_ * (finalXForwards(0) - foot_pos_(0));
	double finalBackwardsAccel = boom_system_ratio_ * g_ / h_ * (finalXBackwards(0) - x_backwards_foot);
	double rel_accel = finalForwardsAccel - finalBackwardsAccel;
	double excessBackwardsVel = finalXBackwards(1) - finalXForwards(1);
	double delayTime = excessBackwardsVel / rel_accel;
	double dist_coords = (finalXForwards[0] + finalXForwards[1] * delayTime
			+ 0.5 * finalForwardsAccel * delayTime * delayTime)
			- (finalXBackwards[0] + finalXBackwards[1] * delayTime
					+ 0.5 * finalBackwardsAccel * delayTime * delayTime);
	double timeToBeginTransition = (1 - this->lifting_ratio) * this->lifting_time + this->landing_time;
	double timeFromEndOfTransition = (this->lifting_ratio) * this->lifting_time;
	printf("[Planner]Iteration1: dist= %.2f, delayTime=%.3f\n", dist_coords, delayTime);
	for (int i = 0; i < 3; i++)
	{
		// Second Integration
		this->integrate(x_ini, x_forwards_foot, x_phase_path, true, timeToBeginTransition);
		double footMidpoint_forwards = (x_forwards_foot + dist_coords + x_backwards_foot) * 0.5;
		this->integrateTransition(x_forwards_foot, footMidpoint_forwards, x_phase_path, true,
				this->transition_landing_time + delayTime);
		this->integrateTransition(footMidpoint_forwards, footMidpoint_forwards, x_phase_path, true,
				this->dual_support_time * 0.5);

		this->integrate(x_backwards_ini, x_backwards_foot, x_phase_path_backwards, false, timeFromEndOfTransition);
		double footMidpoint_backwards = (x_forwards_foot - dist_coords + x_backwards_foot) * 0.5;
		this->integrateTransition(x_backwards_foot, footMidpoint_backwards, x_phase_path_backwards, false,
				this->transition_lifting_time - delayTime);
		this->integrateTransition(footMidpoint_backwards, footMidpoint_backwards, x_phase_path_backwards, false,
				this->dual_support_time * 0.5);

		Vector finalXForwards2(*x_phase_path.rbegin());
		Vector finalXBackwards2(*x_phase_path_backwards.rbegin());
		double finalForwardsAccel2 = boom_system_ratio_ * g_ / h_ * (finalXForwards2(0) - foot_pos_(0));
		double finalBackwardsAccel2 = boom_system_ratio_ * g_ / h_ * (finalXBackwards2(0) - x_backwards_foot);
		double partial_momentum_wrt_transitionTime1 = (finalForwardsAccel2 - finalBackwardsAccel2) / 2;
		double excessBackwardsVel2 = finalXBackwards2(1) - finalXForwards2(1);
		double delayTime2 = excessBackwardsVel2 / partial_momentum_wrt_transitionTime1;
		double dist_coords2 = (finalXForwards2[0] + finalXForwards2[1] * delayTime2
				+ 0.5 * finalForwardsAccel2 * delayTime2 * delayTime2)
				- (finalXBackwards2[0] + finalXBackwards2[1] * delayTime2
						+ 0.5 * finalBackwardsAccel2 * delayTime2 * delayTime2);
		delayTime += delayTime2;
		dist_coords = dist_coords2;
		printf("[Planner]Iteration%d: dist= %.2f, delayTime=%.3f\n", i + 2, dist_coords, delayTime);
	}
	next_foot_placement_[0] = dist_coords + x_backwards_foot;
	ret_transition_landing_time = transition_landing_time + delayTime;
	ret_transition_lifting_time = transition_lifting_time - delayTime;

	if (next_foot_placement_[0] < x_foot_place_limit_(0))
	{
		printf("x foot placement %.2f is behind the reachable limit!", next_foot_placement_[0]);
		next_foot_placement_[0] = x_foot_place_limit_(0); // min
	}
	if (next_foot_placement_[0] > x_foot_place_limit_(1))
	{
		printf("x foot placement %.2f is ahead of the reachable limit!", next_foot_placement_[0]);
		next_foot_placement_[0] = x_foot_place_limit_(1); // max
	}
	if (ret_transition_landing_time < 0.04)
	{
		printf("transition landing time %.3f is below the reachable limit!", ret_transition_landing_time);
		ret_transition_landing_time = 0.04;
	}
	if (ret_transition_lifting_time < 0.04)
	{
		printf("transition lifting time %.3f is below the reachable limit!", ret_transition_lifting_time);
		ret_transition_lifting_time = 0.04;
	}
	if (ret_transition_landing_time > 0.2)
	{
		printf("transition landing time %.3f is excessively large!", ret_transition_landing_time);
		ret_transition_landing_time = 0.2;
	}
	if (ret_transition_lifting_time > 0.2)
	{
		printf("transition lifting time %.3f is excessively large!", ret_transition_lifting_time);
		ret_transition_lifting_time = 0.2;
	}

	// Final Figure Iteration:
	double footMidpoint_forwards = (x_forwards_foot + dist_coords + x_backwards_foot) * 0.5;
	double nextFoot = dist_coords + x_backwards_foot;
	this->integrate(x_ini, x_forwards_foot, x_phase_path, true, timeToBeginTransition);
	this->integrateTransition(x_forwards_foot, footMidpoint_forwards, x_phase_path, true,
			this->ret_transition_landing_time );
	this->integrateTransition(footMidpoint_forwards, footMidpoint_forwards, x_phase_path, true,
			this->dual_support_time);
	this->integrateTransition(footMidpoint_forwards, nextFoot, x_phase_path, true,
			this->ret_transition_lifting_time);
	this->integrateTransition(nextFoot, nextFoot, x_phase_path, true,
			timeFromEndOfTransition);
	vector<double> x_phase_path_x;
	vector<double> x_phase_path_xd;
	for (vector<Vector,allocator<Vector> >::iterator it(x_phase_path.begin());it!=x_phase_path.end();++it)
	{
		x_phase_path_x.push_back((*it)[0]);
		x_phase_path_xd.push_back((*it)[1]);
	}
	saveVector(x_phase_path_x, "x_phase_path_x");
	saveVector(x_phase_path_xd, "x_phase_path_xd");
//	_SavePlannerData(x_phase_path, x_phase_path);
}

void Gray_planner::integrate(const Vector & x_ini, double foot_pos, std::vector<Vector> & path, bool forward,
								double duration)
{
	Vector tmp_state(2);
	path.clear();

	double dt = SERVO_RATE;
	if (!forward)
		dt = -dt;

	double x_ddot, x_dot, x, xp;
	double x_dot_nx, x_nx;
	xp = foot_pos;
	x = x_ini(0);
	x_dot = x_ini(1);
	double limit_upp, limit_low;

	for (int i = 0; i < duration / SERVO_RATE; i++)
	{
		x_ddot = boom_system_ratio_ * g_ / h_ * (x - xp);
		x_dot_nx = x_dot + x_ddot * dt;
		x_nx = x + x_dot * dt + 0.5 * x_ddot * pow(dt, 2);

		tmp_state(0) = x;
		tmp_state(1) = x_dot;

		path.push_back(tmp_state);

		x_dot = x_dot_nx;
		x = x_nx;
	}
}
void Gray_planner::integrateTransition(double foot_pos_ini, double foot_pos_fin, std::vector<Vector> & path,
										bool forward, double duration)
{
	Vector tmp_state(path.back());

	double dt = SERVO_RATE;
	if (!forward)
		dt = -dt;

	double x_ddot, x_dot, x, xp;
	double x_dot_nx, x_nx;
	x = tmp_state(0);
	x_dot = tmp_state(1);
	double limit_upp, limit_low;
	int max_iter = duration / SERVO_RATE;

	for (int i = 0; i < max_iter; i++)
	{
		xp = (foot_pos_ini * i + foot_pos_fin * (max_iter - i)) / ((double) max_iter);
		x_ddot = boom_system_ratio_ * g_ / h_ * (x - xp);
		x_dot_nx = x_dot + x_ddot * dt;
		x_nx = x + x_dot * dt + 0.5 * x_ddot * pow(dt, 2);

		tmp_state(0) = x;
		tmp_state(1) = x_dot;

		path.push_back(tmp_state);

		x_dot = x_dot_nx;
		x = x_nx;
	}
}
// called after ratio times lifting time from lifting period beginning. (80% of lifting)
bool Gray_planner::Calculate_SwitchTime(const Vector & com_pos, //
		const Vector & com_vel, //
		const Vector & foot_pos, //
		LinkID stance_foot, //
		double landing_time, //relative to end of lifting time
		double lifting_time, //duration of total lift trajectory
		double lifting_ratio, //ratio of planning point to total lift
		double transition_landing_time, //
		double transition_lifting_time, //
		double dual_support_time, //
		double middle_time_x, //
		double middle_time_y)
{ //define it in this function
	if (!IsRunning_)
	{
		com_pos_ = com_pos;
		com_vel_ = com_vel;
		foot_pos_ = foot_pos;
		stance_foot_ = stance_foot;
//		this->switching_time_ = switching_time;
		this->landing_time = landing_time;
		this->lifting_time = lifting_time;
		this->lifting_ratio = lifting_ratio;
		this->transition_landing_time = transition_landing_time;
		this->transition_lifting_time = transition_lifting_time;
		this->dual_support_time = dual_support_time;
		middle_time_x_ = middle_time_x;
		middle_time_y_ = middle_time_y;
		calculating_start_ = true;
		printf("\n[Gray Planner] Calculating_Switching Time Running Start\n");
	}

	if (end_of_calculation_)
	{
		printf("[Gray Planner] Calculation_End\n \n");

		end_of_calculation_ = false;
		IsRunning_ = false;
		return true;
	}
	return false;
}
