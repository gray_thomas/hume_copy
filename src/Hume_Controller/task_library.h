#ifndef TASK_LIBRARY
#define TASK_LIBRARY

#include "Task.h"


using namespace std;
using namespace jspace;

class COM_Height_Tilting_Roll_Task: public Task{
    public:
    COM_Height_Tilting_Roll_Task(HUME_System*);
    virtual ~COM_Height_Tilting_Roll_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();

    virtual void send();
    virtual void set_Gain();

public:
    // Height & Pitch & Roll
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
};

class COM_XZ_RyRx_Task: public Task{
    public:
    COM_XZ_RyRx_Task(HUME_System*);
    virtual ~COM_XZ_RyRx_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();

    virtual void send();
    virtual void set_Gain();

public:
    // X & Height & Pitch & Roll
    Vector Kp_;
    Vector Kd_;
    Vector Ki_;
    Vector error_sum_;
};



class COM_Height_Tilting_Roll_Foot_Task: public Task{
public:
    COM_Height_Tilting_Roll_Foot_Task(HUME_System*, LinkID);
    virtual ~COM_Height_Tilting_Roll_Foot_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();

    virtual void send();
    virtual void set_Gain();
    virtual void SetTask(const Vector & , const Vector & );
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    // Foot (LFOOT or RFOOT)
    LinkID link_id_;    
    //
    Vector error_sum_;
    Vector Ki_;
    
    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
};

#endif
