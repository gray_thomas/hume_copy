#ifndef PROCESS
#define PROCESS

#include "HumeSystem.h"
#include "Constraint.hpp"
#include "Controller_Hume.h"
#include "util/wrap_eigen.hpp"
#include "Task.h"

//namespace jspace;

class process{
public:
    process(HUME_System* hume_system);
    virtual ~process(){}
    
    virtual bool do_process(Controller_Hume* controller) = 0;
    void _set_current_as_initial(Controller_Hume* controller);
public:
    HUME_System* hume_system_;

    Vector init_lfoot_pos_;
    Vector init_rfoot_pos_;
};

class stabilizing : public process{
public:
    stabilizing(HUME_System* hume_system);
    virtual ~stabilizing(){}
    virtual bool do_process(Controller_Hume* controller);
    bool _zero_torque_preparing_phase(Controller_Hume* controller);
    void _set_current_foot_pos_vel();
    bool _move_to_desired_jpos(double curr_time, Controller_Hume * controller);
    bool _move_to_desired_foot_pos(double curr_time, Controller_Hume* controller);
    void _get_gamma_foot_control(Controller_Hume* controller, double curr_time, Vector & gamma_foot_control);
    bool _stable_lifting_two_task_boom_x(Controller_Hume* controller, double curr_time);
    bool _stable_lifting_one_task_boom_x(Controller_Hume* controller, double curr_time);
    bool _stable_lifting_XZ_tilting_task(Controller_Hume* controller, double curr_time);
    bool _stable_lifting_COM_tilting_task(Controller_Hume* controller, double curr_time);
    bool _stable_lifting_height_tilting_roll_task(Controller_Hume* controller, double curr_time);
    bool _stable_lifting_XZ_RyRx_task(Controller_Hume* controller, double curr_time);
    
public:    
    double stable_lifting_time_;
    double stable_move_time_;
    double lifting_height_;
    double start_height_;
    double start_x_;
    double initial_pitch_;
    double initial_roll_;
    
    bool b_stabilized_;
    bool b_move_stabilized_;
    bool b_foot_move_stabilized_;
    bool b_transit_; // Floating to Dual Contact
    int count_;
    Task* COM_tilting_task_;
    Task* COM_height_tilting_roll_task_;
    Task* COM_XZ_RyRx_task_;
    Task* XZ_tilting_task_;
    Task* height_task_;
    Task* body_xz_;
    Task* tilting_;
    Task* jpos_task_;
    Task* left_foot_task_;
    Task* right_foot_task_;
    Task* height_tilting_task_;

    
    WBC_Constraint* dual_contact_;
    WBC_Constraint* dual_contact_abduction_;
    WBC_Constraint* fixed_constraint_;
    WBC_Constraint* dual_contact_boom_x_;

    Vector height_des_;
    Vector pos_des_;
    Vector vel_des_;
    Vector ori_des_;
    Vector ori_vel_des_;

    Vector height_pitch_des_; // Height & Pitch
    Vector height_pitch_vel_des_;

    Vector jpos_stable_;
    Vector stable_rfoot_pos_;
    Vector stable_lfoot_pos_;
    Vector jpos_stable_2_;
    Vector jpos_des_;
    Vector jvel_des_;
    Vector jpos_init_;

    // From Primatic Joint
    Vector curr_lfoot_pos_;
    Vector curr_rfoot_pos_;
    Vector curr_lfoot_vel_;
    Vector curr_rfoot_vel_;

};

#endif
