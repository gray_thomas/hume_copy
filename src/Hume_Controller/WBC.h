#ifndef _WBC_
#define _WBC_


#include <vector>
#include "Task.h"
#include "constraint_library.hpp"
#include <fstream>

using namespace jspace;
using namespace std;


class HUME_System;

class Controller_WBC{
public:
    Controller_WBC(){}
    Controller_WBC(HUME_System *  hume_system, int idx);
    ~Controller_WBC();

    void MakeTorque(const vector<double> & conf, vector<Task*> task_array,
                    WBC_Constraint* constraint, Vector& gamma);

public:
    bool data_updated_;
    
    Matrix Js;
    Matrix jac;
    Matrix U;
    Matrix ainv;
    Matrix W_int;
    Vector grav;
    std::vector<double> config_;
    void SaveMatrix();
    HUME_System *hume_system_;
    std::ofstream Js_file;
    std::ofstream Jt_file;
    std::ofstream U_file;
    std::ofstream Ainv_file;
    std::ofstream Wint_file;
    std::ofstream grav_file;

    int count;
};


#endif
