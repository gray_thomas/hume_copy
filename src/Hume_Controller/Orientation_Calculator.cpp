#include "Orientation_Calculator.h"
#include <stdio.h>
#include "HumeSystem.h"
#include "filters.h"

Orientation_Calculator::Orientation_Calculator():Body_ori_vel_pre_(Vector::Zero(3)){
    // poseEstimator_ = uta::attitudeEstimator::noEigenInterface::getLowPassEstimator();
    poseEstimator_ = uta::attitudeEstimator::noEigenInterface::getDefaultPoseEstimator();

    for (int i(0); i< 3; ++i){
        filter* ori_vel = new deriv_lp_filter(50.0, SERVO_RATE);
        filter_ori_vel_.push_back(ori_vel);
    }
}

bool Orientation_Calculator::mocap_imu_vel_fusion(int curr_count, int preparing_count,
                                                  double time_step,
                                                  const vector<double> & imu_ang,
                                                  const vector<double> &  imu_ang_vel,
                                                  Vector & Body_ori,
                                                  Vector & Body_ori_vel)
{
    bool task_start;
    double ratio_mocap(0.6);

    Body_ori = Vector::Zero(3);
    Body_ori_vel = Vector::Zero(3);

    Vector Body_ori_mocap = Vector::Zero(3);
    Vector Body_ori_imu_int = Vector::Zero(3);
    
    static Vector offset_ori = Vector::Zero(3) ;
    static Vector pre_Body_ori = Vector::Zero(3);
    
    Body_ori_vel[0] = 0.0;
    Body_ori_vel[1] = imu_ang_vel[1];
    Body_ori_vel[2] = imu_ang_vel[0];
    
    if(curr_count < preparing_count){
        offset_ori[0] = poseEstimator_->getYaw() - 0.0;
        offset_ori[1] = poseEstimator_->getPitch() - imu_ang[1];
        offset_ori[2] = poseEstimator_->getRoll() -  imu_ang[0];

        Body_ori[0] = poseEstimator_->getYaw()  - offset_ori[0];
        Body_ori[1] = poseEstimator_->getPitch()- offset_ori[1];
        Body_ori[2] = poseEstimator_->getRoll() - offset_ori[2];
        pre_Body_ori = Body_ori;
        task_start = false;
        return task_start;
    }
    else{ task_start = true; }
    
    Body_ori_mocap[0] = poseEstimator_->getYaw()  - offset_ori[0];
    Body_ori_mocap[1] = poseEstimator_->getPitch()- offset_ori[1];
    Body_ori_mocap[2] = poseEstimator_->getRoll() - offset_ori[2];

    for(int i(0); i<3; ++i){
        pre_Body_ori[i] += Body_ori_vel[i]*time_step;
        Body_ori_imu_int[i] = pre_Body_ori[i];

        Body_ori[i] = ratio_mocap* Body_ori_mocap[i]
            + (1.0-ratio_mocap)*Body_ori_imu_int[i];
    }
    
    // Yaw = 0
    Body_ori[0] = 0.0;
    pre_Body_ori = Body_ori;
    
    return task_start;
}

bool Orientation_Calculator::imu_vel_int(int curr_count, int preparing_count, double time_step,
                 const vector<double> & imu_ang, const vector<double> &  imu_ang_vel,
                 Vector & Body_ori, Vector & Body_ori_vel){
    bool task_start;

    Body_ori = Vector::Zero(3);
    Body_ori_vel = Vector::Zero(3);
    
    static Vector pre_Body_ori = Vector::Zero(3);
    
    if(curr_count < preparing_count){
        for (int i(0); i< 3; ++i){
            Body_ori[i] = imu_ang[2-i];
        }
        // Yaw = 0
        Body_ori[0] = 0.0;
        pre_Body_ori = Body_ori;

        task_start = false;
    }
    else {
        for (int i(0); i< 3; ++i){
            Body_ori_vel[i] = imu_ang_vel[2-i];
        }
        for(int i(0); i<3; ++i){
            pre_Body_ori[i] += Body_ori_vel[i]*time_step;
            Body_ori[i] = pre_Body_ori[i];
        }
        // Yaw = 0
        Body_ori[0] = 0.0;
        pre_Body_ori = Body_ori;
        
        task_start = true;
    }
    return task_start;
}

bool Orientation_Calculator::imu_ori_filter_vel(int curr_count, int preparing_count,
                                                const vector<double> & imu_ang,
                                                const vector<double> & imu_ang_vel,
                                                Vector & Body_ori,
                                                Vector & Body_ori_vel){
    Body_ori = Vector::Zero(3);
    Body_ori_vel = Vector::Zero(3);

    for (int i(0); i< 3; ++i){
        Body_ori[i] = imu_ang[2-i];
        filter_ori_vel_[i]->input(Body_ori[i]);
    }
    // Yaw = 0
    Body_ori[0] = 0.0;

    for (int i(0); i< 3; ++i){
        Body_ori_vel[i] = filter_ori_vel_[i]->output();
    }
    return false;
}
                                                

bool Orientation_Calculator::mocap_ori_imu_vel(int curr_count, int preparing_count, const vector<double> & imu_ang, const vector<double> &  imu_ang_vel, Vector & Body_ori, Vector & Body_ori_vel){
    bool task_start;
    
    Body_ori = Vector::Zero(3);
    Body_ori_vel = Vector::Zero(3);
    static Vector offset_ori = Vector::Zero(3) ;

    
    if(curr_count < preparing_count){
        offset_ori[0] = poseEstimator_->getYaw() - 0.0;
        offset_ori[1] = poseEstimator_->getPitch() - imu_ang[1];
        offset_ori[2] = poseEstimator_->getRoll() -  imu_ang[0];
        task_start = false;
    }
    else{ task_start = true; }
    Body_ori[0] = poseEstimator_->getYaw()  - offset_ori[0];
    Body_ori[1] = poseEstimator_->getPitch()- offset_ori[1];
    Body_ori[2] = poseEstimator_->getRoll() - offset_ori[2];

    Body_ori_vel[0] = 0.0;
    Body_ori_vel[1] = imu_ang_vel[1];
    Body_ori_vel[2] = imu_ang_vel[0];

    return task_start;
}

bool Orientation_Calculator::mocap_default_pose_estimator(int curr_count, int preparing_count, const vector<double> & imu_ang, const vector<double> &  imu_ang_vel, Vector & Body_ori, Vector & Body_ori_vel){
    bool task_start;
    poseEstimator_->updateIMU(0.0, 0.0, 0.0, imu_ang_vel[0], imu_ang_vel[1], imu_ang_vel[2]);
    
    Body_ori = Vector::Zero(3);
    Body_ori_vel = Vector::Zero(3);
    static Vector offset_ori = Vector::Zero(3) ;

    
    if(curr_count < preparing_count){
        offset_ori[0] = poseEstimator_->getYaw() - 0.0;
        offset_ori[1] = poseEstimator_->getPitch() - imu_ang[1];
        offset_ori[2] = poseEstimator_->getRoll() -  imu_ang[0];
        task_start = false;
    }
    else{ task_start = true; }
    Body_ori[0] = poseEstimator_->getYaw()  - offset_ori[0];
    Body_ori[1] = poseEstimator_->getPitch()- offset_ori[1];
    Body_ori[2] = poseEstimator_->getRoll() - offset_ori[2];

    
    
    Body_ori_vel[0] = imu_ang_vel[2];
    Body_ori_vel[1] = imu_ang_vel[1];
    Body_ori_vel[2] = imu_ang_vel[0];

    for( int i(0); i<3; ++i){
        if(fabs(Body_ori_vel[i])< 1e-2){
            Body_ori_vel[i] = Body_ori_vel_pre_[i];
            // printf("[Orientation Estimator] IMU has 0 velocity\n");
        }
        else{
            Body_ori_vel_pre_[i] = Body_ori_vel[i];
        }
    }

    return task_start;
}

