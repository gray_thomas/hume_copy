#ifndef GAIN_TUNER
#define GAIN_TUNER

#include "util/Hume_Thread.h"

class Task;
class HUME_System;

class Gain_Tuner: public Hume_Thread{
public:
    Gain_Tuner(Task* task);
    virtual ~Gain_Tuner(void);

    virtual void run(void);

protected:
    int socket_;
    Task* task_;
};

class Act_gain_tuner: public Hume_Thread{
public:
    Act_gain_tuner(HUME_System* hume_system);
    virtual ~Act_gain_tuner(){}

    virtual void run(void);

public:
    int socket_;
    HUME_System* hume_system_;
};


#endif
