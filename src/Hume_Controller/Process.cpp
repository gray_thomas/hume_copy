#include "Process.h"
#include "constraint_library.hpp"
#include "task_library.h"

process::process(HUME_System* hume_system){
    hume_system_ = hume_system;
}

stabilizing::stabilizing(HUME_System* hume_system): process(hume_system),
                                                    start_height_(0.0),
                                                    stable_lifting_time_(7.0),
                                                    stable_move_time_(5.0),
                                                    lifting_height_(0.12),
                                                    b_stabilized_(false),
                                                    b_move_stabilized_(false),
                                                    b_foot_move_stabilized_(false),
                                                    b_transit_(false),
                                                    count_(0),
                                                    pos_des_(2),
                                                    ori_des_(1),
                                                    height_des_(1),
                                                    vel_des_(Vector::Zero(2)),
                                                    ori_vel_des_(Vector::Zero(1)),
                                                    jpos_init_(NUM_ACT_JOINT),
                                                    jpos_des_(NUM_ACT_JOINT),
                                                    jvel_des_(NUM_ACT_JOINT),
                                                    jpos_stable_(Vector::Zero(NUM_ACT_JOINT)),
                                                    jpos_stable_2_(Vector::Zero(NUM_ACT_JOINT)),
                                                    height_pitch_des_(Vector::Zero(2)),
                                                    height_pitch_vel_des_(Vector::Zero(2)),
                                                    initial_pitch_(0.0),
                                                    stable_rfoot_pos_(Vector::Zero(3)),
                                                    stable_lfoot_pos_(Vector::Zero(3))
{
    jpos_task_ = new Joint_Task(hume_system_);
    right_foot_task_ = new FOOT_Task(hume_system_, RFOOT);
    left_foot_task_ = new FOOT_Task(hume_system_, LFOOT);

    height_tilting_task_ = new Height_Tilting_Task(hume_system_);
    XZ_tilting_task_ = new XZ_Tilting_Task(hume_system_);
    
    fixed_constraint_ = new Hume_Fixed_Constraint();
    dual_contact_ = new Hume_Contact_Both();
    dual_contact_abduction_ = new Hume_Contact_Both_Abduction();

    COM_tilting_task_ = new COM_Tilting_Task(hume_system_);
    COM_height_tilting_roll_task_ = new COM_Height_Tilting_Roll_Task(hume_system_);
    COM_XZ_RyRx_task_ = new COM_XZ_RyRx_Task(hume_system_);
// Narrow
    // Left Leg Front
    // jpos_stable_[0] = 0.0;
    // jpos_stable_[1] = -0.76;
    // jpos_stable_[2] = 1.60;
    // jpos_stable_[3] = 0.0;
    // jpos_stable_[4] = -1.17;
    // jpos_stable_[5] = 1.55;

    // stable_rfoot_pos_[0] = -0.3;
    // stable_lfoot_pos_[0] = 0.4;
    
    //Right Leg Front
    jpos_stable_[0] = 0.0;
    jpos_stable_[1] = -0.75;
    jpos_stable_[2] = 1.55;
    jpos_stable_[3] = 0.0;
    jpos_stable_[4] = -1.05;
    jpos_stable_[5] = 1.55;

    // jpos_stable_[0] = 0.0;
    // jpos_stable_[4] = -1.60;
    // jpos_stable_[5] = 1.6;
    // jpos_stable_[3] = 0.0;
    // jpos_stable_[1] = -0.85;
    // jpos_stable_[2] = 1.70;

    // stable_lfoot_pos_[0] = -0.15;
    // stable_rfoot_pos_[0] = 0.2;

    stable_rfoot_pos_[0] = -0.1;
    stable_lfoot_pos_[0] = 0.15;
}

bool stabilizing::do_process(Controller_Hume* controller){
    double curr_time = (double)SERVO_RATE * (double)count_;
    ++count_;

    if(b_stabilized_) { return false; }
    // Zero Torque
    if(!b_stabilized_ && !(hume_system_->task_start_)){
        return _zero_torque_preparing_phase(controller);
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Joint Position Setting
    ///////////////////////////////////////////////////////////////////////////////
    else if(!b_stabilized_ && !(b_move_stabilized_)){
        if(curr_time>stable_move_time_){
            b_move_stabilized_ = true;
            count_ = 0;
            pos_des_[0] = controller->pos_ini_[0];
            pos_des_[1] = controller->pos_ini_[2];
            ori_des_[0] = controller->ori_ini_[1];
            start_x_ = controller->pos_ini_[0];
            start_height_ = controller->pos_ini_[2];
            initial_pitch_ = controller->ori_ini_[1];
        }
        return _move_to_desired_jpos(curr_time, controller);
    }
    else if(!b_stabilized_ && !(b_foot_move_stabilized_)){
        if(curr_time>stable_move_time_){
            b_foot_move_stabilized_ = true;
            count_ = 0;
            pos_des_[0] = controller->pos_ini_[0];
            pos_des_[1] = controller->pos_ini_[2];
            ori_des_[0] = controller->ori_ini_[1];
            start_x_ = controller->pos_ini_[0];
            start_height_ = controller->pos_ini_[2];
            initial_pitch_ = controller->ori_ini_[1];
        }
        return _move_to_desired_foot_pos(curr_time, controller);
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Lifting the Body a little
    ///////////////////////////////////////////////////////////////////////////////
    else if (b_move_stabilized_ && !b_stabilized_ && curr_time < stable_lifting_time_){
        // return _stable_lifting_two_task_boom_x(controller, curr_time);
        // return _stable_lifting_one_task_boom_x(controller, curr_time);
        // return _stable_lifting_XZ_tilting_task(controller, curr_time);
#ifdef WALKING_2D        
        return _stable_lifting_COM_tilting_task(controller, curr_time);
#elif WALKING_3D
        // return _stable_lifting_height_tilting_roll_task(controller, curr_time);
        return _stable_lifting_XZ_RyRx_task(controller, curr_time);
#endif
    }
    else{
        b_stabilized_ = true;
        b_transit_ = false;
        count_ = 0;
        printf("[End of Stabilization]\n");
        return false;
    }
}
bool stabilizing::_move_to_desired_foot_pos(double curr_time, Controller_Hume* controller){
    _set_current_foot_pos_vel();
    static bool first(true);
    static Vector start_lfoot_pos(Vector::Zero(3));
    static Vector start_rfoot_pos(Vector::Zero(3));

    if(first){
        start_lfoot_pos = curr_lfoot_pos_;
        start_rfoot_pos = curr_rfoot_pos_;
        first = false;
    }
    Vector right_foot_vel_des(Vector::Zero(3));
    Vector left_foot_vel_des(Vector::Zero(3));

    Vector right_foot_des = start_rfoot_pos;
    Vector left_foot_des = start_lfoot_pos;
    
    // Right
    right_foot_des[0] = controller->_smooth_changing(start_rfoot_pos[0], stable_rfoot_pos_[0], stable_move_time_, curr_time);
    right_foot_vel_des[0] = controller->_smooth_changing_vel(start_rfoot_pos[0], stable_rfoot_pos_[0], stable_move_time_, curr_time);
    // Left
    left_foot_des[0] = controller->_smooth_changing(start_lfoot_pos[0], stable_lfoot_pos_[0], stable_move_time_, curr_time);
    left_foot_vel_des[0] = controller->_smooth_changing_vel(start_lfoot_pos[0], stable_lfoot_pos_[0], stable_move_time_, curr_time);

    right_foot_task_->SetTask(right_foot_des, right_foot_vel_des);
    left_foot_task_->SetTask(left_foot_des, left_foot_vel_des);

    (controller->task_array_).push_back(right_foot_task_);
    (controller->task_array_).push_back(left_foot_task_);
    _set_current_as_initial(controller);
    controller->wbc_ -> MakeTorque(controller->curr_conf_,
                                   controller->task_array_,
                                   fixed_constraint_,
                                   controller->gamma_);
    return true;

}
bool stabilizing::_move_to_desired_jpos(double curr_time, Controller_Hume* controller){
    for (int i(0); i<NUM_ACT_JOINT; ++i){
            jpos_des_[i] = controller->_smooth_changing(jpos_init_[i], jpos_stable_[i], stable_move_time_, curr_time);
            jvel_des_[i] = controller->_smooth_changing_vel(jpos_init_[i], jpos_stable_[i], stable_move_time_, curr_time);
            
    }
    jpos_task_->SetTask(jpos_des_, jvel_des_);
    controller->task_array_.push_back(jpos_task_);
    //Save Current Pose
    _set_current_as_initial(controller);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                 controller->task_array_,
                                 fixed_constraint_,
                                 controller->gamma_);
    

    return true;
}

void stabilizing::_get_gamma_foot_control(Controller_Hume* controller, double curr_time, Vector & gamma_foot_control){
    _set_current_foot_pos_vel();
    
    std::vector<Task*> task_array_foot;
    double push_down_speed(-0.15);
    double push_forward_speed(0.0);
    // Left Foot
    Vector left_foot_des = init_lfoot_pos_;
    Vector left_foot_vel_des(Vector::Zero(3));
    left_foot_des[2] = init_lfoot_pos_[2] + push_down_speed * curr_time ;
    left_foot_des[0] = init_lfoot_pos_[0] + push_forward_speed * curr_time;
    
    left_foot_vel_des[2] = push_down_speed;
    left_foot_task_->SetTask(left_foot_des, left_foot_vel_des);
    
    // Right Foot
    Vector right_foot_des = init_rfoot_pos_;
    Vector right_foot_vel_des(Vector::Zero(3));
    right_foot_des[2] = init_rfoot_pos_[2] + push_down_speed * curr_time;
    right_foot_des[0] = init_rfoot_pos_[0] + push_forward_speed * curr_time;
    
    right_foot_vel_des[2] = push_down_speed;
    right_foot_task_->SetTask(right_foot_des, right_foot_vel_des);

    // Controller Set
    task_array_foot.push_back(right_foot_task_);
    task_array_foot.push_back(left_foot_task_);
    
    controller->wbc_2_->MakeTorque(controller->curr_conf_,
                                  task_array_foot,
                                  fixed_constraint_,
                                  gamma_foot_control);
}

bool stabilizing::_stable_lifting_one_task_boom_x(Controller_Hume* controller, double curr_time){
    // Compare two gamma
    Vector gamma_foot_control, gamma_dual_contact;
    Vector re_force_left, re_force_right;
    Vector re_curr_force_left, re_curr_force_right;
    Vector re_force_left_dual, re_force_right_dual;
    
    height_pitch_des_[0] = controller->_smooth_changing(start_height_,
                                                        start_height_ + lifting_height_, stable_lifting_time_, curr_time);
    height_pitch_vel_des_[0] = controller->_smooth_changing_vel(start_height_,
                                                                start_height_ + lifting_height_, stable_lifting_time_, curr_time);

    height_pitch_des_[1] = controller->_smooth_changing(initial_pitch_, 0.04,stable_lifting_time_, curr_time) ;
    height_pitch_vel_des_[1] = controller ->_smooth_changing_vel(initial_pitch_, 0.04, stable_lifting_time_, curr_time);
    
    //Task for Both Leg
    if(!b_transit_){
        // Foot Control
        _get_gamma_foot_control(controller, curr_time, gamma_foot_control);
        
        controller->Cal_FootForce(RFOOT, gamma_foot_control, re_force_right);
        controller->Cal_FootForce(LFOOT, gamma_foot_control, re_force_left);
        controller->Cal_curr_FootForce(RFOOT, re_curr_force_right);
        controller->Cal_curr_FootForce(LFOOT, re_curr_force_left);
        
    }
    height_tilting_task_->SetTask(height_pitch_des_, height_pitch_vel_des_);
    height_tilting_task_->force_[1] = 30.0;
    controller->task_array_.push_back(height_tilting_task_);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                controller->task_array_,
                                dual_contact_,
                                gamma_dual_contact);

    controller->Cal_FootForce(RFOOT, gamma_dual_contact, re_force_right_dual);
    controller->Cal_FootForce(LFOOT, gamma_dual_contact, re_force_left_dual);

    bool both_contact(false);
    if(hume_system_->RFoot_Contact() && hume_system_->LFoot_Contact()){ both_contact = true; }
    
    if(!b_transit_ && both_contact && re_force_right[2] < -15.0 && re_force_left[2] < -15.0){
        printf("[Stabilization] Change to Dual Contact Constraint\n");
        b_transit_ = true;
    }
    
    if(b_transit_){
        controller->b_int_gain_right_ = false;
        controller->b_int_gain_left_ = false;
        controller->gamma_ = gamma_dual_contact;
    }
    else{
        controller->gamma_ = gamma_foot_control;
    }
    controller->pos_ini_[2] = height_pitch_des_[0];
    controller->ori_ini_[1] = height_pitch_des_[1];
    return true;
}

bool stabilizing::_stable_lifting_XZ_tilting_task(Controller_Hume* controller, double curr_time){
    // Compare two gamma
    Vector gamma_foot_control, gamma_dual_contact;
    Vector re_force_left, re_force_right;
    Vector re_curr_force_left, re_curr_force_right;
    Vector re_force_left_dual, re_force_right_dual;
    Vector des(3);
    Vector vel_des(3);
    static double changing_time(0.0);

    double push_back_vel(-0.1);
    double height_offset(0.0);

    
    //Task for Both Leg
    if(!b_transit_){
        // Foot Control
        _get_gamma_foot_control(controller, curr_time, gamma_foot_control);
        
        controller->Cal_FootForce(RFOOT, gamma_foot_control, re_force_right);
        controller->Cal_FootForce(LFOOT, gamma_foot_control, re_force_left);
        controller->Cal_curr_FootForce(RFOOT, re_curr_force_right);
        controller->Cal_curr_FootForce(LFOOT, re_curr_force_left);
        // X
        des[0] = hume_system_->tot_configuration_[Px];
        vel_des[0] = 0.0;
        start_x_ = des[0];
        //Height
        des[1] = hume_system_->tot_configuration_[Pz];
        vel_des[1] = 0.0;
        start_height_ = des[1];
        // Pitch
        des[2] = hume_system_->getOri()[1];
        vel_des[2] = 0.0;
        initial_pitch_ = des[2];
    }
    else{
        // Height
        des[1] = controller->_smooth_changing(start_height_ + height_offset,
                                              start_height_ + lifting_height_ + height_offset,
                                              stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[1] = controller->_smooth_changing_vel(start_height_+ height_offset,
                                                      start_height_ + lifting_height_ + height_offset,
                                                      stable_lifting_time_ - changing_time, curr_time - changing_time);
        // Pitch
        des[2] =  controller->_smooth_changing(initial_pitch_, 0.02, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[2] = controller->_smooth_changing_vel(initial_pitch_, 0.02, stable_lifting_time_ - changing_time, curr_time - changing_time);
        // X
        des[0] = start_x_;
        vel_des[0] = 0.0;

        // des[0] = start_x_ + push_back_vel*(curr_time - changing_time) ;
        // vel_des[0] = push_back;
    }
    controller->task_array_.clear();
    XZ_tilting_task_->SetTask(des, vel_des);
    XZ_tilting_task_->force_[0] = 0.0;
    XZ_tilting_task_->force_[1] = 0.0;// * (  1- curr_time/ stable_lifting_time_);
    XZ_tilting_task_->force_[1] = 0.0;// * (  1- curr_time/ stable_lifting_time_);
    // Constraint
    // Vector weight(8);
    // for(int i(0); i<8; ++i){
    //     weight[i] = 1.0;
    // }
    // weight[3] = 1.0;
    // weight[4] = 1.0;
    // weight[5] = 5.0;
    // dual_contact_->set_constraint_weight(weight);
    
    controller->task_array_.push_back(XZ_tilting_task_);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                 controller->task_array_,
                                 dual_contact_abduction_,
                                 gamma_dual_contact);

    controller->Cal_FootForce(RFOOT, gamma_dual_contact, re_force_right_dual);
    controller->Cal_FootForce(LFOOT, gamma_dual_contact, re_force_left_dual);


    
    bool both_contact(false);
    if(hume_system_->RFoot_Contact() && hume_system_->LFoot_Contact()){ both_contact = true; }
    
    if(!b_transit_  && re_force_right[2] < -5.0 && re_force_left[2] < -5.0){
        printf("[XZ Tilting Stabilization] Change to Dual Contact Constraint\n");
        // dual_contact_ -> b_re_force_ctrl_ = true;
        b_transit_ = true;
        changing_time = curr_time;
    }
    
    if(b_transit_){
        controller->b_int_gain_right_ = false;
        controller->b_int_gain_left_ = false;
        controller->gamma_ = gamma_dual_contact;
    }
    else{
        controller->gamma_ = gamma_foot_control;
    }
    controller->pos_ini_[0] = des[0];
    controller->pos_ini_[2] = des[1];
    controller->ori_ini_[1] = des[2];
    return true;
}


bool stabilizing::_stable_lifting_COM_tilting_task(Controller_Hume* controller, double curr_time){
    // Compare two gamma
    Vector gamma_foot_control, gamma_dual_contact;
    Vector re_force_left, re_force_right;
    Vector re_curr_force_left, re_curr_force_right;
    Vector re_force_left_dual, re_force_right_dual;
    Vector des(3);
    Vector vel_des(3);
    static double changing_time(0.0);

    double push_back_vel(-0.1);
    double height_offset(0.0);
    //Task for Both Leg
    if(!b_transit_){
        // Foot Control
        _get_gamma_foot_control(controller, curr_time, gamma_foot_control);
        
        controller->Cal_FootForce(RFOOT, gamma_foot_control, re_force_right);
        controller->Cal_FootForce(LFOOT, gamma_foot_control, re_force_left);
        controller->Cal_curr_FootForce(RFOOT, re_curr_force_right);
        controller->Cal_curr_FootForce(LFOOT, re_curr_force_left);
        // X
        des[0] = hume_system_->COM_pos_[0];
        vel_des[0] = 0.0;
        start_x_ = des[0];
        //Height
        des[1] = hume_system_->COM_pos_[2];
        vel_des[1] = 0.0;
        start_height_ = des[1];
        // Pitch
        des[2] = hume_system_->getOri()[1];
        vel_des[2] = 0.0;
        initial_pitch_ = des[2];
    }
    else{
        // Height
        des[1] = controller->_smooth_changing(start_height_ + height_offset,
                                              start_height_ + lifting_height_ + height_offset,
                                              stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[1] = controller->_smooth_changing_vel(start_height_+ height_offset,
                                                      start_height_ + lifting_height_ + height_offset,
                                                      stable_lifting_time_ - changing_time, curr_time - changing_time);
        // Pitch
        des[2] =  controller->_smooth_changing(initial_pitch_, 0.05, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[2] = controller->_smooth_changing_vel(initial_pitch_, 0.05, stable_lifting_time_ - changing_time, curr_time - changing_time);

        // des[2] = initial_pitch_;
        // vel_des[2] = 0.0;
        // X
        des[0] = start_x_;
        vel_des[0] = 0.0;
    }
    controller->task_array_.clear();
    COM_tilting_task_->SetTask(des, vel_des);
    // ((COM_Tilting_Task*)COM_tilting_task_)->Kp_[0] = 10.0 + 40.0* (curr_time/stable_lifting_time_);
    // ((COM_Tilting_Task*)COM_tilting_task_)->Kp_[1] = 20.0 + 100.0* (curr_time/stable_lifting_time_);

    COM_tilting_task_->force_[0] = 0.0;
    COM_tilting_task_->force_[1] = 0.0;
    COM_tilting_task_->force_[2] = 0.0;

    controller->task_array_.push_back(COM_tilting_task_);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                 controller->task_array_,
                                 dual_contact_abduction_,
                                 gamma_dual_contact);

    controller->Cal_FootForce(RFOOT, gamma_dual_contact, re_force_right_dual);
    controller->Cal_FootForce(LFOOT, gamma_dual_contact, re_force_left_dual);

    if(!b_transit_  && re_force_right[2] < -5.0 && re_force_left[2] < -5.0){
        printf("[XZ Tilting Stabilization] Change to Dual Contact Constraint\n");
        b_transit_ = true;
        changing_time = curr_time;
    }
    
    if(b_transit_){
        controller->b_int_gain_right_ = false;
        controller->b_int_gain_left_ = false;
        controller->gamma_ = gamma_dual_contact;
    }
    else{
        controller->gamma_ = gamma_foot_control;
    }
    controller->pos_ini_[0] = des[0];
    controller->pos_ini_[2] = des[1];
    controller->ori_ini_[1] = des[2];
    return true;
}


bool stabilizing::_stable_lifting_height_tilting_roll_task(Controller_Hume* controller, double curr_time){
    // Compare two gamma
    Vector gamma_foot_control, gamma_dual_contact;
    Vector re_force_left, re_force_right;
    Vector re_curr_force_left, re_curr_force_right;
    Vector re_force_left_dual, re_force_right_dual;
    Vector des(3);
    Vector vel_des(3);
    static double changing_time(0.0);

    double push_back_vel(-0.1);
    double height_offset(0.0);

    //Task for Both Leg
    if(!b_transit_){
        // Foot Control
        _get_gamma_foot_control(controller, curr_time, gamma_foot_control);
        
        controller->Cal_FootForce(RFOOT, gamma_foot_control, re_force_right);
        controller->Cal_FootForce(LFOOT, gamma_foot_control, re_force_left);
        controller->Cal_curr_FootForce(RFOOT, re_curr_force_right);
        controller->Cal_curr_FootForce(LFOOT, re_curr_force_left);
        //Height
        des[0] = hume_system_->COM_pos_[2];
        vel_des[0] = 0.0;
        start_height_ = des[0];
        // Pitch
        des[1] = hume_system_->getOri()[1];
        vel_des[1] = 0.0;
        initial_pitch_ = des[1];
        // Roll
        des[2] = hume_system_->getOri()[2];
        vel_des[2] = 0.0;
        initial_roll_ = des[2];

    }
    else{
        // Height
        des[0] = controller->_smooth_changing(start_height_ + height_offset,
                                              start_height_ + lifting_height_ + height_offset,
                                              stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[0] = controller->_smooth_changing_vel(start_height_+ height_offset,
                                                      start_height_ + lifting_height_ + height_offset,
                                                      stable_lifting_time_ - changing_time, curr_time - changing_time);
        // Pitch
        des[1] =  controller->_smooth_changing(initial_pitch_, 0.05, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[1] = controller->_smooth_changing_vel(initial_pitch_, 0.05, stable_lifting_time_ - changing_time, curr_time - changing_time);

        // Roll
        des[2] =  controller->_smooth_changing(initial_roll_, 0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[2] = controller->_smooth_changing_vel(initial_roll_, 0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);
        
    }
    controller->task_array_.clear();
    COM_height_tilting_roll_task_->SetTask(des, vel_des);

    controller->task_array_.push_back(COM_height_tilting_roll_task_);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                 controller->task_array_,
                                 dual_contact_,
                                 gamma_dual_contact);

    controller->Cal_FootForce(RFOOT, gamma_dual_contact, re_force_right_dual);
    controller->Cal_FootForce(LFOOT, gamma_dual_contact, re_force_left_dual);

    if(!b_transit_  && re_force_right[2] < -5.0 && re_force_left[2] < -5.0){
        printf("[XZ Tilting Stabilization] Change to Dual Contact Constraint\n");
        b_transit_ = true;
        changing_time = curr_time;
    }
    
    if(b_transit_){
        controller->b_int_gain_right_ = false;
        controller->b_int_gain_left_ = false;
        controller->gamma_ = gamma_dual_contact;
    }
    else{
        controller->gamma_ = gamma_foot_control;
    }
    controller->pos_ini_[2] = des[0];
    controller->ori_ini_[1] = des[1];
    controller->ori_ini_[2] = des[2];
    return true;
}


bool stabilizing::_stable_lifting_XZ_RyRx_task(Controller_Hume* controller, double curr_time){
    // Compare two gamma
    Vector gamma_foot_control, gamma_dual_contact;
    Vector re_force_left, re_force_right;
    Vector re_curr_force_left, re_curr_force_right;
    Vector re_force_left_dual, re_force_right_dual;
    Vector des(4);
    Vector vel_des(4);
    static double changing_time(0.0);

    double push_back_vel(-0.1);
    double height_offset(0.0);

    //Task for Both Leg
    if(!b_transit_){
        // Foot Control
        _get_gamma_foot_control(controller, curr_time, gamma_foot_control);
        
        controller->Cal_FootForce(RFOOT, gamma_foot_control, re_force_right);
        controller->Cal_FootForce(LFOOT, gamma_foot_control, re_force_left);
        controller->Cal_curr_FootForce(RFOOT, re_curr_force_right);
        controller->Cal_curr_FootForce(LFOOT, re_curr_force_left);
        // X
        des[0] = hume_system_->COM_pos_[0];
        vel_des[0] = 0.0;
        start_x_ = des[0];
        //Height
        des[1] = hume_system_->COM_pos_[2];
        vel_des[1] = 0.0;
        start_height_ = des[1];
        // Pitch
        des[2] = hume_system_->getOri()[1];
        vel_des[2] = 0.0;
        initial_pitch_ = des[2];
        // Roll
        des[3] = hume_system_->getOri()[2];
        vel_des[3] = 0.0;
        initial_roll_ = des[3];

    }
    else{
        des[0] = start_x_;
        vel_des[0] = 0.0;
        // Height
        des[1] = controller->_smooth_changing(start_height_ + height_offset,
                                              start_height_ + lifting_height_ + height_offset,
                                              stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[1] = controller->_smooth_changing_vel(start_height_+ height_offset,
                                                      start_height_ + lifting_height_ + height_offset,
                                                      stable_lifting_time_ - changing_time, curr_time - changing_time);
        // Pitch
        des[2] =  controller->_smooth_changing(initial_pitch_, -0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[2] = controller->_smooth_changing_vel(initial_pitch_, -0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);

        // Roll
        des[3] =  controller->_smooth_changing(initial_roll_, 0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);
        vel_des[3] = controller->_smooth_changing_vel(initial_roll_, 0.0, stable_lifting_time_ - changing_time, curr_time - changing_time);
        
    }
    controller->task_array_.clear();
    COM_XZ_RyRx_task_->SetTask(des, vel_des);

    controller->task_array_.push_back(COM_XZ_RyRx_task_);
    
    controller->wbc_->MakeTorque(controller->curr_conf_,
                                 controller->task_array_,
                                 dual_contact_,
                                 gamma_dual_contact);

    controller->Cal_FootForce(RFOOT, gamma_dual_contact, re_force_right_dual);
    controller->Cal_FootForce(LFOOT, gamma_dual_contact, re_force_left_dual);

    if(!b_transit_  && re_force_right[2] < -5.0 && re_force_left[2] < -5.0){
        printf("[XZ RyRx] Change to Dual Contact Constraint\n");
        b_transit_ = true;
        changing_time = curr_time;
    }
    
    if(b_transit_){
        controller->b_int_gain_right_ = false;
        controller->b_int_gain_left_ = false;
        controller->gamma_ = gamma_dual_contact;
    }
    else{
        controller->gamma_ = gamma_foot_control;
    }
    controller->pos_ini_[0] = des[0];
    controller->pos_ini_[2] = des[1];
    controller->ori_ini_[1] = des[2];
    controller->ori_ini_[2] = des[3];
    return true;
}





void process::_set_current_as_initial(Controller_Hume* controller){
    for (int i(0); i<3; ++i){
        controller->pos_ini_[i] = hume_system_->tot_configuration_[i];
    }
    controller->com_ini_ = hume_system_->COM_pos_;
    hume_system_->CopyOri (controller->ori_ini_);
    
    hume_system_->CopyFoot(controller->left_foot_ini_, LFOOT);
    hume_system_->CopyFoot(controller->right_foot_ini_, RFOOT);

    init_lfoot_pos_ = controller->left_foot_ini_;
    init_rfoot_pos_ = controller->right_foot_ini_;
    for (int i(0); i<3 ; ++i){
        init_lfoot_pos_[i] -= hume_system_->tot_configuration_[i];
        init_rfoot_pos_[i] -= hume_system_->tot_configuration_[i];
    }
}

void stabilizing::_set_current_foot_pos_vel(){
    hume_system_->CopyFoot(curr_lfoot_pos_, LFOOT);
    hume_system_->CopyFoot(curr_rfoot_pos_, RFOOT);

    hume_system_->CopyFootVel(curr_lfoot_vel_, LFOOT);
    hume_system_->CopyFootVel(curr_rfoot_vel_, RFOOT);

    for(int i(0); i<3 ; ++i){
        curr_lfoot_pos_[i] -= hume_system_->tot_configuration_[i];
        curr_rfoot_pos_[i] -= hume_system_->tot_configuration_[i];
        curr_lfoot_vel_[i] -= hume_system_->tot_vel_[i];
        curr_rfoot_vel_[i] -= hume_system_->tot_vel_[i];
    }
    ((FOOT_Task*)left_foot_task_)->setCurrentFoot(curr_lfoot_pos_, curr_lfoot_vel_);
    ((FOOT_Task*)right_foot_task_)->setCurrentFoot(curr_rfoot_pos_, curr_rfoot_vel_);
}

bool stabilizing::_zero_torque_preparing_phase(Controller_Hume* controller){
    controller->gamma_ = Vector::Zero(NUM_ACT_JOINT);
    _set_current_as_initial(controller);
    
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        jpos_init_[i] = hume_system_->tot_configuration_[i + NUM_PASSIVE];
    }
    count_ = 0;
    
    pos_des_[0] = controller->pos_ini_[0];
    pos_des_[1] = controller->pos_ini_[2];
    ori_des_[0] = controller->ori_ini_[1];
    return true;
}
