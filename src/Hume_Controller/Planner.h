#ifndef _PLANNER_
#define _PLANNER_

#include "FootPlacement.h"
#include "util/Hume_Thread.h"
#include <vector>
#include "HumeSystem.h"
#include <string>

using namespace jspace;
using namespace std;

class Planner: public Hume_Thread
{
public:
    Planner();
    virtual ~Planner(){	}
    
    virtual void run();
    
    virtual bool Calculate_SwitchTime(const Vector & com_pos, //
                                      const Vector & com_vel, //
                              const Vector & foot_pos, //
                              LinkID stance_foot, //
                              double landing_time, //relative to end of lifting time
                              double lifting_time, //duration of total lift trajectory
                              double lifting_ratio, //ratio of planning point to total lift
                              double transition_landing_time, //
                              double transition_lifting_time, //
                              double dual_support_time, //
                              double middle_time_x, //
                              double middle_time_y) = 0;
    
    virtual void Calculation() = 0;

    // Get Funtions
    double GetSwitchingTime(){
        return switching_time_;
    }
    double GetLandingTime(){
        return landing_time_;
    }
    void CopyNextFootLoc(Vector & land_loc) {
        land_loc = next_foot_placement_;
    }
    double GetLandingTransitionTime()	{
        return ret_transition_landing_time;
    }
    double GetLiftingTransitionTime()	{
        return ret_transition_lifting_time;
    }
    
protected:
    void _save_planned_path(const std::vector<Vector> & path, string name, int num_plan);

    void _SavePlannerData(const std::vector<Vector> & x_phase_path, const std::vector<Vector> & y_phase_path);
    void _MakePhasePath(const Vector & x_ini, double foot_pos, std::vector<Vector> & path, bool forcing);
    //Define initial Condition
    void _MakeHalfPhasePath(const Vector & ini, double foot_place, std::vector<Vector> & path);
    void _IntegrateTwoPath(const std::vector<Vector> & path_1,
                           const std::vector<Vector> & path_2,
                           std::vector<Vector> & int_path, int & cross_frame);
    void _find_next_foot_placement(const Vector & com_vel,
                                   const Vector & com_pos,
                                   Vector & nx_foot_placement,
                                   Vector & nx_stable_position);
    void _MakeCOMtrj(const std::vector<Vector> & phase_path_x,
                     const std::vector<Vector> & phase_path_y,
                     std::vector<Vector> pos_trj, std::vector<Vector> vel_trj);
    void _change_foot_placement(const Vector & sign_movement,
                                int inter_frame_x, int inter_frame_y,
                                Vector & nx_foot_placement, Vector & nx_stable_position);
    Vector _check_step_length(const Vector & nx_foot_pl);

    foot_placement foot_placement_planner_;
    Vector Footposition_;
    Vector x_foot_place_limit_;
    Vector y_foot_place_limit_;
    Vector phase_goal_position;
    Vector velocity_limit_;
    double velocity_limit_min_;

    double h_;
    double g_;

    bool calculating_start_;
    bool end_of_calculation_;
    bool IsRunning_;
    Vector next_foot_placement_;

    double boom_system_ratio_;
    Vector com_pos_;
    Vector com_vel_;
    Vector foot_pos_;
    double middle_time_x_;
    double middle_time_y_;
    LinkID stance_foot_;

    double switching_time_;
    double landing_time_;
    double ret_transition_landing_time;
    double ret_transition_lifting_time;
    double dual_support_time;
};

static int find_near_index(const vector<Vector> & vec, int compare, double value, double & trj_error)
{
    int curr_idx(0);
    int min_idx(0);
    double min_error(100.0);
    double curr_error;
    for (size_t ii(0); ii < vec.size(); ++ii)
    {
        curr_error = fabs(vec[ii][compare] - value);

        if (curr_error < min_error)
        {
            min_error = curr_error;
            min_idx = ii;
        }
    }
    trj_error = min_error;

    return min_idx;
}

#endif
