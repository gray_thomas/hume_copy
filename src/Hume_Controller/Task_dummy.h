class Body_Task: public Task{
public:
    Body_Task(HUME_System*);
    virtual ~Body_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}
public:
    Vector  Body_d_;
    Vector  Body_vel_d_;
    Vector  Kp_;
    Vector  Kd_;
};

class Body_XZ_Task: public Task{
public:
    Body_XZ_Task(HUME_System*);
    virtual ~Body_XZ_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();
    virtual void set_weight(const Vector & weight);
public:
    Vector  Body_d_;
    Vector  Body_vel_d_;
    Vector  Kp_;
    Vector  Kd_;
    HumeProtocol::body_task_data data_;
    HumeProtocol::pd_2_gain gain_data_;
};


class Height_Task: public Task{
public:
    Height_Task(HUME_System*);
    virtual ~Height_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();

public:
    Vector height_d_;
    Vector height_vel_d_;
    Vector Kp_;
    Vector Kd_;

    HumeProtocol::body_task_data data_;
    HumeProtocol::pd_1_gain gain_data_;
};

class Tilting_Task: public Task{
public:
    Tilting_Task(HUME_System*);
    virtual ~Tilting_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send();
    virtual void set_Gain();
    virtual void set_weight(const Vector & weight);
public:
    Vector ang_d_;
    Vector ang_vel_d_;
    Vector Kp_;
    Vector Kd_;
    HumeProtocol::ori_task_data data_;
    HumeProtocol::pd_1_gain gain_data_;
};

class ORI_Task: public Task{
public:
    ORI_Task(HUME_System*);
    virtual ~ORI_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}

public:
    Vector ORI_d_;
    Vector ORI_vel_d_;
    Vector Kp_;
    Vector Kd_;
};

class ORI_YX_Task: public Task{
public:
    ORI_YX_Task(HUME_System*);
    virtual ~ORI_YX_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector & , const Vector & );

    virtual void send(){}
    virtual void set_Gain(){}

public:
    Vector ORI_yx_d_;
    Vector ORI_yx_vel_d_;
    Vector Kp_;
    Vector Kd_;
};


class Height_Tilting_Foot_Task: public Task{
public:
    Height_Tilting_Foot_Task(HUME_System*, LinkID);
    virtual ~Height_Tilting_Foot_Task(){}

    virtual Matrix getJacobian(const vector<double> & conf);
    virtual Vector getCommand();
    virtual Vector getForce();
    virtual void SetTask(const Vector&, const Vector&);

    virtual void send();
    virtual void set_Gain();
    
    void setCurrentFoot(const Vector & , const Vector &);
public:
    LinkID link_id_;    

    Vector curr_foot_pos_;
    Vector curr_foot_vel_;
    Vector Kp_;
    Vector Kd_;
    //Height & Pitch & Foot
    Vector des_;
    Vector vel_des_;
    // Socket for sending orientation and foot position data 
    int socket_2_;
    int socket_3_;

    int send_gain_port_;
    int recieve_gain_port_;
    
    HumeProtocol::body_task_data data_body_;
    HumeProtocol::ori_task_data data_ori_;
    HumeProtocol::foot_task_data data_foot_;

    HumeProtocol::pd_5_gain gain_data_;
};
