#ifndef _FOOT_PLACEMENT_
#define _FOOT_PLACEMENT_

#include <stdio.h>
#include "util/wrap_eigen.hpp"

using namespace jspace;

class foot_placement{
public:
    foot_placement(){}
    ~foot_placement(){}
public:
    double find_foot_placement(const Vector & chainging_state, const Vector &range, double middle_time, double height, bool forcing);
    

protected:
    double _stop(const Vector & changing_state, double middle_time, double height);
    double _stop_numeric(const Vector & changing_state, double middle_time, double height);

    void _integrating_time(const Vector & st_state, double foot_placement,  double time, double height, Vector & end_state);
    void _integrating_pos(const Vector & st_state, double foot_placement, double pos, double height, Vector & end_state);
    
    double _step(const Vector & chainging_state, double min_vel, double height);
    double _step_numeric(const Vector & changing_state, double min_vel, double height);
    bool _check_limit(const Vector & changing_state, const Vector & range, double & foot_placement);
    double _increment_foot_placement(const Vector & chainging_state, double switching_time, double height, double foot_placement);
    double x_t(double x0, double x0_dot, double t, double height, double foot_place);
    double xdot_t(double x0, double x0_dot, double t, double height, double foot_place);
    
};

#endif
