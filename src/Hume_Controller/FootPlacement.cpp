#include "FootPlacement.h"
#include <math.h>

double foot_placement::find_foot_placement(const Vector & chainging_state, const Vector & range, double middle_time, double height, bool forcing){

    double foot_placement;
    bool is_hit_limit;
    // if(chainging_state[1] <0){
    //     foot_placement = _stop_numeric(chainging_state, middle_time, height);
    // }
    // else{
    //     foot_placement = _step_numeric(chainging_state, chainging_state(1)*0.25, height);
    // }
    foot_placement = _stop_numeric(chainging_state, middle_time, height);
    is_hit_limit = _check_limit(chainging_state, range, foot_placement);
    
    if(!forcing){
        if(chainging_state(1)>0.8){
            printf("*****Hit the maximum velocity: %f\n", chainging_state(1));
        }
        if(is_hit_limit){
            foot_placement = _step_numeric(chainging_state, chainging_state(1)*0.25, height);
            _check_limit(chainging_state, range, foot_placement);
        }
    }

    return foot_placement;

}
double foot_placement::_stop(const Vector & changing_state, double middle_time, double height)
{

	double g(9.8);
	double h(height);
	double x0(changing_state(0));
	double x0_dot(changing_state(1));

	double alpha(exp(2 * sqrt(g / h) * middle_time));
	//Test
	double xs(x0 + 0.3);
	if (changing_state(1) < 0)
	{
		xs = x0 - 0.3;
	}
	double foot_placement;
	//foot_placement = (0.5*((x0 + xs) - 1/(x0 - xs)*h/g*x0*x0));
	foot_placement = ((alpha + 1) / (alpha - 1) * x0_dot * sqrt(h / g) + x0);
	return foot_placement;
}
double foot_placement::_stop_numeric(const Vector & changing_state, double middle_time, double height)
{

	double foot_placement;
	foot_placement = _stop(changing_state, middle_time, height);

	double a = foot_placement - 0.15;
	double b = foot_placement + 0.12;
	double c;

	Vector end_state;

	double error(10);
	int iter(0);
	while (error > 0.0001)
	{
		c = (a + b) / 2;

		_integrating_time(changing_state, c, middle_time, height, end_state);

		if (end_state(1) > 0)
		{
			a = c;
		}
		else
		{
			b = c;
		}

		error = fabs(end_state(1));
//        printf("%ith error: %f \n", iter, error);
		if (iter > 100)
		{
			break;
		}
		++iter;
	}
	foot_placement = c;
	printf("STOP: Numeric Solution: %f \n", foot_placement);
	return foot_placement;
}

void foot_placement::_integrating_time(const Vector & st_state, double foot_placement, double time, double height,
										Vector & end_state)
{
	//Test
	double z_x = height;

	double inc_t(0.0005);
	double x_p(foot_placement);
	double g(9.8);
	double x_ddot, x_dot_nx, x_nx;
	double x(st_state(0));
	double x_dot(st_state(1));

	double t(0.0);

	while (t < time)
	{
		x_ddot = g / z_x * (x - x_p);
		x_dot_nx = x_dot + x_ddot * inc_t;
		x_nx = x + x_dot * inc_t + 0.5 * x_ddot * inc_t * inc_t;

		x_dot = x_dot_nx;
		x = x_nx;
		t += inc_t;
	}

	end_state = Vector::Zero(2);
	end_state(0) = x;
	end_state(1) = x_dot;
}

void foot_placement::_integrating_pos(const Vector & st_state, double foot_placement, double pos, double height,
										Vector & end_state)
{
	//Test
	double z_x = height;

	double inc_t(0.0005);
	double x_p(foot_placement);
	double g(9.8);
	double x_ddot, x_dot_nx, x_nx;
	double x(st_state(0));
	double x_dot(st_state(1));

	double t(0.0);
	int iter(0);
	while (fabs(x - pos) > 0.0001)
	{
		x_ddot = g / z_x * (x - x_p);
		x_dot_nx = x_dot + x_ddot * inc_t;
		x_nx = x + x_dot * inc_t + 0.5 * x_ddot * inc_t * inc_t;

		x_dot = x_dot_nx;
		x = x_nx;
		t += inc_t;

		if (iter > 10000)
		{
			printf("Maximum iteration in integrating_pos!!\n\n");
			break;
		}
		++iter;
	}

	end_state = Vector::Zero(2);
	end_state(0) = x;
	end_state(1) = x_dot;
}

double foot_placement::_step(const Vector & chainging_state, double min_vel, double height)
{
	printf("step\n");
	double g(9.8);
	double h(height);
	double x0(chainging_state(0));
	double x0_dot(chainging_state(1));
	double sign(-1.0);
	if (min_vel > 0)
	{
		sign = 1.0;
	}
	double foot_placement(x0 + sign * sqrt(h / g) * sqrt(x0_dot * x0_dot - min_vel * min_vel));
	return foot_placement;
}

double foot_placement::_step_numeric(const Vector & changing_state, double min_vel, double height)
{

	double foot_placement;
	foot_placement = _step(changing_state, min_vel, height);

	double a = foot_placement - 0.12;
	double b = foot_placement + 0.08;
	double c;

	Vector end_state;

	double error(10);

	int iter(0);

	while (error > 0.0001)
	{
		c = (a + b) / 2.0;
		_integrating_pos(changing_state, c, c, height, end_state);

		if (end_state(1) - min_vel > 0)
		{
			a = c;
		}
		else
		{
			b = c;
		}

		error = fabs(end_state(1) - min_vel);

		if (iter > 100)
		{
			break;
		}
		++iter;
	}
	foot_placement = c;
	printf("STEP: Numeric solution: %f\n", foot_placement);
	return foot_placement;
}

bool foot_placement::_check_limit(const Vector & chainging_state, const Vector & range, double & foot_placement)
{
	bool ret(false);
	if (range(0) < foot_placement && foot_placement < range(1))
	{
		return false;
	}
	else
	{

		if (fabs(foot_placement - range(0)) > fabs(foot_placement - range(1)))
		{
			foot_placement = range(1);
		}
		else
		{
			foot_placement = range(0);
		}
		printf("Foot Placment: Hit the limit, changed value: %f\n", foot_placement);
		return true;
	}
}

// bool foot_placement::_check_limit(const Vector & chainging_state,  const Vector & range, double & foot_placement){
//     bool ret(false);
//     if(chainging_state(0) - foot_placement>0){
//         if(foot_placement<-range(1)){
//             foot_placement = -range(1);
//             ret = true;
//             printf("Foot Placement: Hit the lower limit \n");
//         }
//         if(foot_placement>-range(0)){
//             foot_placement = -range(0);
//             ret = true;
//             printf("Foot Placment: Hit the upper limit\n");
//         }
//     }
//     else{
//         if(foot_placement<range(0)){
//             foot_placement = range(0);
//             ret = true;
//             printf("Foot Placement: Hit the lower limit \n");
//         }
//         if(foot_placement>range(1)){
//             foot_placement = range(1);
//             ret = true;
//             printf("Foot Placement: Hit the upper limit \n");
//         }
//     }
//     return ret;
// }

double foot_placement::_increment_foot_placement(const Vector & chainging_state, double switching_time,
													double height, double foot_placement)
{
	double x_dot_chainge;
	while (fabs(x_dot_chainge) > 0.1)
	{
		x_dot_chainge = xdot_t(chainging_state(0), chainging_state(1), switching_time, height, foot_placement);
		foot_placement += 0.005;
	}
	return foot_placement;
}

double foot_placement::x_t(double x0, double x0_dot, double t, double height, double foot_place)
{
	double h(height);
	double xp(foot_place);
	double g(9.8);

	double x_t(
			-0.5 * (sqrt(h / g) * x0_dot - (x0 - xp)) * exp(-sqrt(g / h) * t)
					+ 0.5 * (sqrt(h / g) * x0_dot + (x0 - xp)) * exp(sqrt(g / h) * t) + xp);

	return x_t;
}
double foot_placement::xdot_t(double x0, double x0_dot, double t, double height, double foot_place)
{
	double h(height);
	double xp(foot_place);
	double g(9.8);

	double xdot_t(
			0.5 * (x0_dot - sqrt(g / h) * (x0 - xp)) * exp(-sqrt(g / h) * t)
					+ 0.5 * (x0_dot + sqrt(g / h) * (x0 - xp)) * exp(sqrt(g / h) * t));
	return xdot_t;
}
