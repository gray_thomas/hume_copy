#include "controller_Body_Test.h"

#include <math.h>
#include <stdio.h>
#include "analytic_solution/analytic_sol.h"
#include "analytic_solution/analytic_jacobian.h"
#include "analytic_solution/analytic_forward_kin.h"
#include "Hume_Supervisor/hume_protocol.h"
#include "util/comm_udp.h"

Ctrl_Body_Test::Ctrl_Body_Test(HUME_System * hume_system):
    Controller_Hume(hume_system),
    stance_foot_(RFOOT), moving_foot_(LFOOT), pos_des_(3), vel_des_(3), ori_des_(3), ori_vel_des_(3),
    omega_height_(1.3), amp_height_(0.04), pos_offset_(3), pos_ini_(3), count_command_(0),
    constraint_changed_(false),
    curr_rfoot_pos_(Vector::Zero(3)),
    curr_rfoot_vel_(Vector::Zero(3)), 
    curr_lfoot_pos_(Vector::Zero(3)), 
    curr_lfoot_vel_(Vector::Zero(3)),
    rfoot_pos_des_(Vector::Zero(3)),
    rfoot_vel_des_(Vector::Zero(3)),
    lfoot_pos_des_(Vector::Zero(3)),
    lfoot_vel_des_(Vector::Zero(3)),
    amp_tilting_(0.0),
    omega_tilting_(1.2)

{
    initial_jpos_ = Vector::Zero(NUM_ACT_JOINT);
    start_jpos_ = Vector(NUM_ACT_JOINT);
    /// JPos (Saved)//////////
    start_jpos_[0] = 0.0;
    start_jpos_[1] = -0.37;
    start_jpos_[2] = 1.2;
    start_jpos_[3] = 0.0;
    start_jpos_[4] = -1.0;
    start_jpos_[5] = 1.25;
    ///////////////////////////

    stabilizing_time_ = 1.02;
    stable_lifting_time_ = 11.5;
    stabilizing_height_ = 0.14;

    fixed_constraint_ = new Hume_Fixed_Constraint();
    lfoot_task_ = new FOOT_Task(hume_system_, LFOOT);
    rfoot_task_ = new FOOT_Task(hume_system_, RFOOT);
    joint_task_ = new Joint_Task(hume_system_);
    pos_offset_[2] = 0.93;
#ifndef BOOM_SYSTEM    
    constraint_ = new Hume_Contact_Both();
    com_task_ = new Body_Task(hume_system_);
//    ori_task_ = new ORI_Task(hume_system_);
    ori_task_ = new ORI_YX_Task(hume_system_);    
#else
    constraint_ = new Hume_Contact_Both_Boom(); 
//    constraint_ = new Hume_Contact_Both_Y_Yaw();
//    constraint_ = new Hume_Contact_Both();
    com_task_ = new Body_XZ_Task(hume_system_);
    ori_task_ = new Tilting_Task(hume_system_);
//   ori_task_ = new ORI_YX_Task(hume_system_);
#endif
    printf("[Body Control] Start\n");
}

Ctrl_Body_Test::~Ctrl_Body_Test(){
    delete com_task_;
    delete ori_task_;
}

void Ctrl_Body_Test::getCurrentCommand(std::vector<double> & command){
    
    _PreProcessing_Command();
    ++count_command_;
    
    double curr_time = ((double)count_command_)*SERVO_RATE;

    if(_stabilizing(curr_time)){
        _PostProcessing_Command(command);
        return;
    }
    task_array_.push_back(com_task_);
    task_array_.push_back(ori_task_);
    
    _SetDesBody();
    _SetDesOri();
    
    wbc_.MakeTorque(hume_system_->tot_configuration_, task_array_, constraint_, gamma_);
    
    _PostProcessing_Command(command);
    _LoadingBodyControlData();
}

void Ctrl_Body_Test::_LoadingBodyControlData(){
    HumeProtocol::body_ctrl_data data;

    for (int i(0); i< 3; ++i){
        data.body_pos[i] = 0.0;
    }
    
//    COMM::send_data(socket_, PORT_BODY_CONTROL, & data, sizeof(HumeProtocol::body_ctrl_data));
}

bool Ctrl_Body_Test::_stabilizing(double time){
    static bool start_move(false);

    if(start_move){ return false; }
    
    if(!start_move &&  !(hume_system_->task_start_)){
        gamma_ = Vector(NUM_ACT_JOINT);
        for(int i(0); i<NUM_ACT_JOINT; ++i){
            gamma_(i) = 0.0;
        }
        //Before Touching ground
        hume_system_-> CopyBody(pos_ini_);        
        hume_system_-> CopyOri(ori_ini_);

        hume_system_->CopyFoot(rfoot_pos_ini_, RFOOT);
        hume_system_->CopyFoot(lfoot_pos_ini_, LFOOT);
        for (int i(0); i< 3; ++i){
            rfoot_pos_ini_[i] -= hume_system_->tot_configuration_[i];
            lfoot_pos_ini_[i] -= hume_system_->tot_configuration_[i];
        }
        for (int i(0); i<NUM_ACT_JOINT; ++i){
            initial_jpos_[i] = hume_system_->tot_configuration_[i + NUM_PASSIVE];
        }        
        _SetDesOri();
        des_height_ = pos_ini_[2];
        stabilizing_time_ = time;
        task_array_.push_back(com_task_);
        task_array_.push_back(ori_task_);

        return true;
    }
    else if(!start_move && !constraint_changed_){
        pos_des_ = pos_ini_;        
#ifndef BOOM_SYSTEM
        pos_des_[2] = _smooth_changing(pos_ini_[2], pos_ini_[2] + stabilizing_height_,
                                       stable_lifting_time_,
                                       time-stabilizing_time_);
        vel_des_[2] = _smooth_changing_vel(pos_ini_[2], pos_ini_[2] + stabilizing_height_,
                                           stable_lifting_time_,
                                           time-stabilizing_time_);
        des_height_ = pos_des_[2];
#else
        pos_des_[1] = _smooth_changing(pos_ini_[2], pos_ini_[2] + stabilizing_height_,
                                       stable_lifting_time_,
                                       time-stabilizing_time_);
        vel_des_[1] = _smooth_changing_vel(pos_ini_[2], pos_ini_[2] + stabilizing_height_,
                                           stable_lifting_time_,
                                           time-stabilizing_time_);
        des_height_ = pos_des_[1]; 
#endif
        com_task_->SetTask(pos_des_, vel_des_);
        ori_des_[0] = ori_ini_[1];
        ori_vel_des_[0] = 0.0;
        ori_task_->SetTask(ori_des_, ori_vel_des_);

        task_array_.push_back(com_task_);
        task_array_.push_back(ori_task_);
        Vector contact_gamma = Vector::Zero(NUM_ACT_JOINT);
        Vector floating_gamma = Vector::Zero(NUM_ACT_JOINT);

        Vector constraint_weight(9);
        double changing_time(2.0);
        for (int i(0); i< 9 ; ++i){
            constraint_weight[i] = 1.0;
        }
        // constraint_weight[3] = 0.5;
        // constraint_weight[4] = 0.5;
        // constraint_weight[6] = 0.5;
        // constraint_weight[7]  = 0.5;
        // constraint_weight[3] = _smooth_changing(0.0, 1.0, changing_time, time-stabilizing_time_);
        // constraint_weight[4] = _smooth_changing(0.0, 1.0, changing_time, time-stabilizing_time_);
        // constraint_weight[6] = _smooth_changing(0.0, 1.0, changing_time, time-stabilizing_time_);
        // constraint_weight[7] = _smooth_changing(0.0, 1.0, changing_time, time-stabilizing_time_);
                
        constraint_->set_constraint_weight(constraint_weight);

        wbc_.MakeTorque(curr_conf_, task_array_, constraint_, contact_gamma);
//        SetFootTask_simple( time - stabilizing_time_, floating_gamma);
        SetFootTask( time , floating_gamma);
//        SetJointTask(time, floating_gamma);
//        constraint_changed_ = check_gamma(contact_gamma, floating_gamma, gamma_);
        if(time - stabilizing_time_ > stable_lifting_time_){
            constraint_changed_ = true;
        }
        //TEST
        gamma_ = contact_gamma;
        return true;
    }
    else{
        pos_ini_[2] = des_height_;
        start_move = true;
        count_command_ = 0;
        return false;
    }
}

void Ctrl_Body_Test::SetJointTask(double time, Vector & gamma){
    vector<Task*> task_array_joint;
    double changing_time(4.0);
    Vector jpos_des(NUM_ACT_JOINT);
    Vector jvel_des(NUM_ACT_JOINT);
    
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        jpos_des[i] = _smooth_changing(initial_jpos_[i], start_jpos_[i], changing_time, time);
        jvel_des[i] = _smooth_changing_vel(initial_jpos_[i], start_jpos_[i], changing_time, time);
    }
    joint_task_->SetTask(jpos_des, jvel_des);
    task_array_joint.push_back(joint_task_);
    
    wbc_.MakeTorque(curr_conf_, task_array_joint, fixed_constraint_, gamma);
}

void Ctrl_Body_Test::SetFootTask_simple(double time, Vector & gamma){
    Matrix J_right, J_left;
    hume_system_->GetFullJacobian(hume_system_->tot_configuration_, RFOOT, J_right);
    hume_system_->GetFullJacobian(hume_system_->tot_configuration_, LFOOT, J_left);

    Vector f(Vector::Zero(3));
    double moving_duration(3.0);
    f[2] = _smooth_changing(0.0, -2.0, moving_duration, time);
    Vector input;
    input = J_right.transpose()*f;
    input += J_left.transpose()*f;
    gamma = Vector::Zero(NUM_ACT_JOINT);
    for (int i(0); i< NUM_ACT_JOINT; ++i){
        gamma[i] = input[i + NUM_PASSIVE];
    }
}

void Ctrl_Body_Test::SetFootTask(double time, Vector & gamma){


    lfoot_pos_des_ = lfoot_pos_ini_;
    rfoot_pos_des_ = rfoot_pos_ini_;

    rfoot_vel_des_ = Vector::Zero(3);
    lfoot_vel_des_ = Vector::Zero(3);
    double landing_time(1.0);
    double mag_ratio(1.0);
    double mag_ratio_tan( 0.0);
    
    lfoot_pos_des_[2] = _smooth_changing(lfoot_pos_ini_[2], lfoot_pos_ini_[2] - mag_ratio* stabilizing_height_,
                                         landing_time,
                                         time-stabilizing_time_);
    lfoot_vel_des_[2] = _smooth_changing_vel(lfoot_pos_ini_[2], lfoot_pos_ini_[2] - mag_ratio*stabilizing_height_,                                            landing_time,
                                             time-stabilizing_time_);
    
    rfoot_pos_des_[2] = _smooth_changing(rfoot_pos_ini_[2], rfoot_pos_ini_[2] - mag_ratio * stabilizing_height_,
                                         landing_time,
                                         time-stabilizing_time_);
    rfoot_vel_des_[2] = _smooth_changing_vel(rfoot_pos_ini_[2], rfoot_pos_ini_[2] - mag_ratio * stabilizing_height_,
                                             landing_time,
                                             time-stabilizing_time_);

        
    /////////////////////////
    // lfoot_pos_des_[0] = _smooth_changing    (lfoot_pos_ini_[0], lfoot_pos_ini_[0] - mag_ratio_tan* stabilizing_height_, landing_time, time-stabilizing_time_);
    // lfoot_vel_des_[0] = _smooth_changing_vel(lfoot_pos_ini_[0], lfoot_pos_ini_[0] - mag_ratio_tan*stabilizing_height_,  landing_time, time-stabilizing_time_);
    // rfoot_pos_des_[0] = _smooth_changing    (rfoot_pos_ini_[0], rfoot_pos_ini_[0] - mag_ratio_tan * stabilizing_height_,                                        landing_time,                                         time-stabilizing_time_);
    // rfoot_vel_des_[0] = _smooth_changing_vel(rfoot_pos_ini_[0], rfoot_pos_ini_[0] - mag_ratio_tan * stabilizing_height_,                                             landing_time,                                             time-stabilizing_time_);

    
    lfoot_task_->SetTask(lfoot_pos_des_, lfoot_vel_des_);
    rfoot_task_->SetTask(rfoot_pos_des_, rfoot_vel_des_);


    for (int i(0); i< 3; ++i){
        curr_rfoot_pos_[i] = hume_system_-> getFoot(RFOOT)[i] - hume_system_->tot_configuration_[i];
        curr_rfoot_vel_[i] = hume_system_-> getFoot_vel(RFOOT)[i] - hume_system_->tot_vel_[i];

        curr_lfoot_pos_[i] = hume_system_-> getFoot(LFOOT)[i] - hume_system_->tot_configuration_[i];
        curr_lfoot_vel_[i] = hume_system_-> getFoot_vel(LFOOT)[i] - hume_system_->tot_vel_[i];
    }
    
    ((FOOT_Task*)rfoot_task_) -> setCurrentFoot(curr_rfoot_pos_, curr_rfoot_vel_);
    ((FOOT_Task*)lfoot_task_) -> setCurrentFoot(curr_lfoot_pos_, curr_lfoot_vel_);

    vector<Task*> task_array_floating;
    task_array_floating.push_back(rfoot_task_);
    task_array_floating.push_back(lfoot_task_);

    wbc_.MakeTorque(curr_conf_, task_array_floating, fixed_constraint_, gamma);
}

bool Ctrl_Body_Test::check_gamma(const Vector & contact_gamma, const Vector & floating_gamma, Vector & ret_gamma){
    double error = 0.0;
    static bool b_start_mix(false);
    static int count(0.0);
    double Thr(-1.0);
    double mixing_time(2.0);
    double ratio;
    Vector l_re_force,r_re_force;
    static Vector set_floating_gamma;

    Cal_FootForce(RFOOT, floating_gamma, r_re_force);
    Cal_FootForce(LFOOT, floating_gamma, l_re_force);
    
    ret_gamma = floating_gamma;

    if(!b_start_mix && (r_re_force[2] < Thr || l_re_force[2] < Thr)){
        b_start_mix = true;
        set_floating_gamma = floating_gamma;
        hume_system_->CopyOri(ori_ini_);
        hume_system_->CopyBody(pos_ini_);
        count = 0;
        return false;
    }
        
    if(b_start_mix){
        ratio = _smooth_changing(0.0, 1.0, mixing_time, SERVO_RATE*((double)count));
        ret_gamma = ratio * contact_gamma +  (1-ratio)*set_floating_gamma;
    }
    if(b_start_mix && SERVO_RATE*((double)count) > mixing_time){
        b_start_mix = false;
        count = 0;
        return true;
    }
    Cal_FootForce(RFOOT, ret_gamma, r_re_force);
    Cal_FootForce(LFOOT, ret_gamma, l_re_force);

    // pretty_print(r_re_force, std::cout, "R re force", "");
    // pretty_print(l_re_force, std::cout, "L re force", "");
    
    ++count;
    return false;
}

void Ctrl_Body_Test::_save_initial_pos(){
    static bool init(false);
    
    if(!init){
        hume_system_->CopyBody(pos_ini_);
        hume_system_->CopyOri(ori_ini_);
        init = true;
    }
}
void Ctrl_Body_Test::setCurrentConfiguration(){
    curr_conf_ = hume_system_->tot_configuration_;
    jspace::Vector curr_vel (hume_system_->tot_vel_ );
    
    Vector Foot_pos;
    Vector Foot_vel;
    Matrix J_foot;
    if(stance_foot_ == RFOOT){
        getForwardKinematics(curr_conf_, RFOOT, Foot_pos);
        getAnalyticJacobian(curr_conf_, RFOOT, J_foot);
        Foot_vel = J_foot*curr_vel;
    }
    else if(stance_foot_ == LFOOT){
        getForwardKinematics(curr_conf_, LFOOT, Foot_pos);
        getAnalyticJacobian(curr_conf_, LFOOT, J_foot);
        Foot_vel = J_foot*curr_vel;
    }

    for (int i(0); i< 3; ++i){
        curr_conf_[i] = -Foot_pos[i];
    }

    for (int i(0); i<3; ++i){
        hume_system_->tot_vel_[i] = -Foot_vel[i];
    }
    hume_system_->tot_configuration_ = curr_conf_;
    hume_system_->UpdateStatus();
//    jspace::pretty_print(curr_conf_, "tot_configuration (control body):");
}
void Ctrl_Body_Test::_SetDesBody(){

#ifndef BOOM_SYSTEM
    pos_des_ = pos_ini_; //pos_offset_(2) + amp_height_*sin(hume_system_->getCurrentTime()* omega_height_);
    pos_des_[2] = pos_ini_[2] + 0.5*amp_height_*(1 - cos(count_command_*SERVO_RATE*omega_height_));
    vel_des_[2] = 0.5*amp_height_*omega_height_*sin(count_command_*SERVO_RATE*omega_height_);
#else
    pos_des_[0] = pos_ini_[0]; //+1.1;//+0.1;
    pos_des_[1] = pos_ini_[2] + 0.5*amp_height_*(1 - cos(count_command_*SERVO_RATE*omega_height_));
    vel_des_[1] = 0.5*amp_height_*omega_height_*sin(count_command_*SERVO_RATE*omega_height_);
    // pos_des_[1] = pos_ini_[2];
    // vel_des_[1] = 0.0;
#endif
    com_task_->SetTask(pos_des_, vel_des_);
}

void Ctrl_Body_Test::_SetDesOri(){

#ifndef BOOM_SYSTEM
//    ori_des_ = ori_ini_; 
    ori_des_[0] = ori_ini_[1];
    ori_des_[1] = 0.0;
    ori_vel_des_ = Vector::Zero(3);
#else
    // ori_des_[0] = ori_ini_[1];
    // ori_des_[1] = 0.0;
    // ori_vel_des_ = Vector::Zero(3);
    ori_des_[0] = ori_ini_[1] + 0.5*amp_tilting_*(1 - cos(count_command_*SERVO_RATE*omega_tilting_));
    ori_vel_des_[0] = 0.5*amp_tilting_*omega_tilting_*sin(count_command_*SERVO_RATE*omega_tilting_);
#endif
    ori_task_->SetTask(ori_des_, ori_vel_des_);
}
