#ifndef CONTROLLER_FOOT_TEST
#define CONTROLLER_FOOT_TEST

#include "Controller_Hume.h"

class Ctrl_Foot_Test: public Controller_Hume{
public:
    Ctrl_Foot_Test(HUME_System* _hume_system, LinkID target_foot);
    virtual ~Ctrl_Foot_Test();

public:
    virtual void getCurrentCommand(std::vector<double> & command);
    virtual void setCurrentConfiguration();

    void _save_initial_pos();
public:
    Task* foot_task_;
    LinkID target_foot_;

    jspace::Vector Foot_des_;
    jspace::Vector Foot_vel_des_;
    double omega_;
    double amp_;
    double phase_shift_;
    jspace::Vector center_pt_;
    
protected:
    jspace::Vector Foot_ini_;
    void _MakeTrajectory();
};

#endif





