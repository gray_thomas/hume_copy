#include "Constraint.hpp"
#include <fstream>
#include <iostream>
#include "util/pseudo_inverse.hpp"

std::ofstream sf_coeff;

namespace jspace {
    
    WBC_Constraint::WBC_Constraint():sigmaThreshold_(0.001),
                                     b_internal_(false), b_re_force_ctrl_(false),
                                     b_constraint_weight_(false),
                                     b_internal_weight_(false),
                                     b_change_internal_force_(false)
    {  }
    
    
    bool WBC_Constraint::getU(Matrix & U) {
        U = U_;
        return true;
    }
    
    bool WBC_Constraint::getJc(Matrix & Jc){
        Jc = Jc_;
        return true;
    }
    
    bool WBC_Constraint::getJcBar(Matrix const Ainv,
				 Matrix & JcBar) {
        Matrix lambda;
        pseudoInverse(Jc_ * Ainv * Jc_.transpose(),
                      sigmaThreshold_,
                      lambda, 0);
        
        JcBar = Ainv * Jc_.transpose() * lambda;
        return true;
    }

    bool WBC_Constraint::getNc(Matrix const Ainv, Matrix & Nc) {
        Matrix JcBar;
        if (!getJcBar(Ainv,JcBar)) {
            return false;
        }
        Nc = Matrix::Identity(Ainv.rows(),Ainv.cols())-JcBar*Jc_;
        return true;
    }

    bool WBC_Constraint::getUNc(Matrix const Ainv, Matrix & UNc) {
        Matrix Nc;
        if (!getNc(Ainv,Nc)) {
            return false;
        }
        UNc = U_*Nc;
        return true;
    }
  
    bool WBC_Constraint::getUNcBar(Matrix const Ainv, Matrix & UNcBar) {
        Matrix UNc;
        if (!getUNc(Ainv,UNc)) {
            return false;
        }
        
        Matrix lambda;
        pseudoInverse(UNc * Ainv * UNc.transpose(),
                      sigmaThreshold_,
                      lambda, 0);
        UNcBar = Ainv * UNc.transpose() * lambda;
        return true;
    }
    bool WBC_Constraint::getInternal_Matrix(Matrix & W_int){
        W_int = W_int_;
        return true;
    }
    bool WBC_Constraint::getSelection(Vector & Fr, Matrix &S){
        S = Matrix::Zero(1,1);
    }
    void WBC_Constraint::getDesInternalForce(Vector & des_int_force){
        des_int_force = Vector::Zero(1);
    }
}
