#ifndef GRAY_PLANNER
#define GRAY_PLANNER

#include "Planner.h"

using namespace jspace;
using namespace std;

class Gray_planner: public Planner
{
public:
    Gray_planner();
    virtual ~Gray_planner()
	{
	}
    
    virtual bool Calculate_SwitchTime(const Vector & com_pos, //
                              const Vector & com_vel, //
                              const Vector & foot_pos, //
                              LinkID stance_foot, //
                              double landing_time, //relative to end of lifting time
                              double lifting_time, //duration of total lift trajectory
                              double lifting_ratio, //ratio of planning point to total lift
                              double transition_landing_time, //
                              double transition_lifting_time, //
                              double dual_support_time, //
                              double middle_time_x, //
                              double middle_time_y) ;
    
    virtual void Calculation() ;
    
    void integrate(const Vector & x_ini, double foot_pos, std::vector<Vector> & path, bool forward, double duration);
    void integrateTransition(double foot_pos_ini, double foot_pos_fin, std::vector<Vector> & path, bool forward,
                             double duration);
public:
    double landing_time; //relative to end of lifting time
    double lifting_time; //duration of total lift trajectory
    double lifting_ratio; //ratio of planning point to total lift
    double transition_landing_time;
    double transition_lifting_time;
    double dual_support_time;
};
#endif
