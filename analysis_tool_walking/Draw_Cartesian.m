function ret = Draw_Cartesian(x, des, actual, phase_changing_frame, fig_title)
min_length = length(x);
subplot(311)
hold on
plot(x, des(1,1:min_length), '--', 'Linewidth', 3);
plot(x, actual(1,1:min_length),'r-','linewidth', 3);
hold off
ylabel('X','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(x, phase_changing_frame);
axis tight
grid on
title(fig_title);
%
subplot(312)
hold on
plot(x, des(2,1:min_length), '--', 'Linewidth', 3);
plot(x, actual(2,1:min_length),'r-','linewidth', 3);
hold off
ylabel('Y','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(x, phase_changing_frame);
axis tight
grid on
%
subplot(313)
hold on
plot(x, des(3,1:min_length), '--', 'Linewidth', 3);
plot(x, actual(3,1:min_length),'r-','linewidth', 3);
hold off
ylabel('Z','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(x, phase_changing_frame);
axis tight
grid on
%
xlabel('Time (sec)','fontsize', 12);
end