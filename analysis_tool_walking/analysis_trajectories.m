clc
clear all
close all

font_size = 16;

%% 
path = '~/new_control_pc_mekabot/m3bip/experiment_data';
% path = '~/mekabot/m3bip/experiment_data';

read_trj_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 6;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height;
    position_x = original_x;
  end
end

% shortening the length
cut_end =2000;
min_length = length(x) - cut_end;
x = x(1:min_length);
phase = phase(1:min_length);
task_time = x(min_length - length(des_com)+1:end);
task_length = length(task_time);
%% Phase Changing Frame
j = 1;
phase_changing_frame = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end
phase_task = phase(min_length - length(des_com) + 1:end);
j =1;
for i = 1:length(phase_task) -1
    if(phase_task(i + 1) ~= phase_task(i))
        phase_changing_frame_task(j) = i+1;
        j = j+1;
    end
end
%phase_changing_frame = 1;

time_start = 26;
time_end = 28;
%% Draw Figure
%%%%%%%%%% Torque %%%%%%%%%%%%%%
figure(fig(1))
for i = 1:3
  subplot(3,1,i);
  hold on
  plot(x, gamma(i,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  fn_draw_phase_block(x, phase_changing_frame, phase);
  axis tight
  xlim([time_start, time_end])
  fn_latex_font_label('y', fn_joint_name(i), font_size);
  if(i~=3) set(gca, 'XTick',[]); end
end
fn_latex_font_label('x', 'Time (sec)', font_size);

figure(fig(2))
for i = 1:3
  subplot(3,1,i);
  hold on
  plot(x, gamma(i+3,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i+3,1:min_length) ,'r-', 'linewidth',1.2);
  hold off
  fn_draw_phase_block(x, phase_changing_frame, phase);
  axis tight
  xlim([time_start, time_end])
  fn_latex_font_label('y', fn_joint_name(i), font_size);
  if(i~=3) set(gca, 'XTick',[]); end
end
fn_latex_font_label('x', 'Time (sec)', font_size);

%%%%%%%%% Height & Tilting %%%%%%%%%%%%%
figure(fig(3))
Draw_body_ori_traj

figure(fig(4))
Draw_body_ori_vel_traj


%%%%%%%% Foot Task %%%%%%%
figure(fig(5))
Draw_foot_traj
figure(fig(6))
Draw_foot_vel_traj
