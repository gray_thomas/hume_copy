%%%%%% Phase 
%% X
plan_start = 1;
num_plan = 1;
path = '/home/meka/experiment_data';

x_phase_path_cell = cell (num_plan,3); %phase_path, nx_phase_path, int_phase_path
y_phase_path_cell  = cell (num_plan, 3);



for i = plan_start:num_plan
fname_x_phase_path = sprintf('%s/%i_x_phase_path.txt', path, i);
fname_nx_phase_path = sprintf('%s/%i_next_x_path.txt',path, i);
fname_int_phase_path_x = sprintf('%s/%i_integrated_path_x.txt', path, i);

[x_phase_path_f, msg22] = fopen(fname_x_phase_path, 'rt');
[nx_phase_path_f, msg23] = fopen(fname_nx_phase_path, 'rt');
[int_phase_path_x_f, msg24] = fopen(fname_int_phase_path_x, 'rt');

x_phase_path_cell{i,1} = fscanf(x_phase_path_f, '%f %f', [2, inf]);
x_phase_path_cell{i,2} = fscanf(nx_phase_path_f, '%f %f ', [2, inf]);
x_phase_path_cell{i,3} = fscanf(int_phase_path_x_f, '%f %f', [2, inf]);

%% Y
fname_y_phase_path = sprintf('%s/%i_y_phase_path.txt',path, i);
fname_ny_phase_path = sprintf('%s/%i_next_y_path.txt',path, i);
fname_int_phase_path_y = sprintf('%s/%i_integrated_path_y.txt',path, i);

[y_phase_path_f, msg22] = fopen(fname_y_phase_path, 'rt');
[ny_phase_path_f, msg23] = fopen(fname_ny_phase_path, 'rt');
[int_phase_path_y_f, msg24] = fopen(fname_int_phase_path_y, 'rt');

y_phase_path_cell{i,1} = fscanf(y_phase_path_f, '%f %f', [2, inf]);
y_phase_path_cell{i,2} = fscanf(ny_phase_path_f, '%f %f ', [2, inf]);
y_phase_path_cell{i,3} = fscanf(int_phase_path_y_f, '%f %f', [2, inf]);
end
fname_phase = sprintf('%s/phase.txt', path);

fname_x_frame = sprintf('%s/inter_frame_x.txt', path);
fname_y_frame = sprintf('%s/inter_frame_y.txt', path);
fname_foot_placement = sprintf('%s/foot_placement.txt', path);
fname_foot_st = sprintf('%s/local_stance_foot.txt', path);
fname_left_land = sprintf('%s/landing_time_left.txt',path);
fname_right_land = sprintf('%s/landing_time_right.txt', path);
fname_theta = sprintf('%s/theta.txt', path);
fname_com_pos_pl = sprintf('%s/com_pos_pl.txt', path);
%COM
fname_cpos = sprintf('%s/Body_pos.txt', path);
fname_cvel = sprintf('%s/Body_vel.txt', path);

[phase_f, msg10] = fopen(fname_phase,'rt');

[frame_x_f, msg24] = fopen(fname_x_frame, 'rt');
[frame_y_f, msg25] = fopen(fname_y_frame, 'rt');
[foot_pl_f, msg22] = fopen(fname_foot_placement, 'rt');
[foot_st_f, msg23] = fopen(fname_foot_st, 'rt');
[com_pos_pl_f, msg24] = fopen(fname_com_pos_pl, 'rt');

[theta_f, msg4] = fopen(fname_theta, 'rt');
[land_left_f, msg24] = fopen(fname_left_land, 'rt');
[land_right_f, msg25] = fopen(fname_right_land, 'rt');

[vel_fcom, msg9] = fopen(fname_cvel, 'rt');
[pos_fcom, msg1] = fopen  (fname_cpos,'rt');

%COM
pos_com =   fscanf(pos_fcom, '%f %f %f', [3, inf]);
vel_com = fscanf(vel_fcom, '%f %f %f', [3, inf]);

com_pos_pl = fscanf(com_pos_pl_f, '%f %f %f', [3, inf]);
frame_x = fscanf(frame_x_f, '%f', [1,inf]);
frame_y = fscanf(frame_y_f, '%f', [1,inf]);
%theat = fscanf(theta_f, '%f', [1, inf]);
theta = zeros(1,length(pos_com));
foot_pl = fscanf(foot_pl_f, '%f %f %f', [3, inf]);
foot_st = fscanf(foot_st_f, '%f %f %f', [3, inf]);
%land_left = fscanf(land_left_f, '%f %f %f', [3, inf]);
%land_right = fscanf(land_right_f, '%f %f %f', [3, inf]);

land_left = zeros(3, num_plan);
land_right = zeros(3, num_plan);


phase = fscanf(phase_f, '%f',[1,inf]);
