function ret = plot_foot_place(axis, num_step, theta, com_pos, foot_pl, foot_st, land)
    % axis - x:0, y:1
    
    local_land = land - com_pos;
    foot_pl = foot_pl - com_pos;
%     foot_stance = offset_foot_st(1)* cos(theta +axis*0.5*pi) ...
%                   + offset_foot_st(2)* sin(theta + axis*0.5 *pi);
%     local_land_left = offset_land_left(1)*cos(theta + axis*0.5*pi)...
%                       + offset_land_left(2)*sin(theta +  axis*0.5*pi);
%     local_land_right = offset_land_right(1)*cos(theta + axis*0.5*pi) ...
%                         + offset_land_right(2)*sin(theta + axis*0.5*pi);
%     
    hold on
    %Planned
    plot(foot_pl(axis + 1), 0.0, 'color', 'black', 'marker','o', 'markersize', 31,'linewidth',4);
    %Foot
    plot(local_land(axis+1), 0.0, 'color', 'blue','marker', '+', 'markersize', 32,'linewidth',4);
    plot(foot_st(axis +1), 0.0, 'color', 'red','marker', '*', 'markersize', 32,'linewidth',4.5);
    hold off

end