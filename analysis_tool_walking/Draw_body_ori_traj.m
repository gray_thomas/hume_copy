subplot(311)
hold on
plot(task_time, des_com(3,:), '--', 'Linewidth', 3);
plot(task_time, pos_com(3,:),'r-','linewidth', 3);
hold off
fn_latex_label('y', 'Height ($m$)', font_size);
set(gca, 'XTick',[]);
%fn_draw_phase(task_time, phase_changing_frame_task);
axis tight
xlim([time_start, time_end])

%
subplot(312)
hold on
plot(task_time, orientation_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'Pitch ($rad$)', font_size);
set(gca, 'XTick',[]);
%fn_draw_phase(task_time, phase_changing_frame_task);
axis tight
xlim([time_start, time_end])


subplot(313)
hold on
plot(task_time, orientation_d(3, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(3, 1:task_length),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'Roll ($rad$)', font_size);
set(gca, 'fontsize',font_size);
%fn_draw_phase(task_time, phase_changing_frame_task);
fn_latex_label('x', 'Time (sec)', font_size);
axis tight
xlim([time_start, time_end])
