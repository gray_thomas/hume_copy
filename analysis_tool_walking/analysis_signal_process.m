% signal analysis
close all
clear all
clc
fclose all
%%
path = '~/mekabot/m3bip/experiment_data';

configure_read_file
% 
% Fs = length(x)/x(end);
% L = length(jvel(1,:));
% y = jvel(1,:);
% figure 
% plot(x,jvel(1,:));
% NFFT = 2^nextpow2(L); % Next power of 2 from length of y
% Y = fft(y,NFFT)/L;
% f = Fs/2*linspace(0,1,NFFT/2+1);
% 
% % Plot single-sided amplitude spectrum.
% plot(f,2*abs(Y(1:NFFT/2+1))) 
% title('Single-Sided Amplitude Spectrum of y(t)')
% xlabel('Frequency (Hz)')
% ylabel('|Y(f)|')
% 
% d=fdesign.lowpass('N,Fc',10, 5, Fs);
% designmethods(d)
% % only valid design method is FIR window method
% Hd = design(d);
% % Display filter magnitude response
% %fvtool(Hd);
% f_jvel = filter(Hd, jvel(5,:));
% figure
% plot(x, f_jvel, x, jvel(5,:));
% 
% 

t_s = x(end)/length(x);
w_c = 25;

Lpf_in_prev = zeros(1,2);
Lpf_out_prev = zeros(1,2);

den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;

Lpf_in1 = 2500*t_s*t_s*w_c*w_c / den;
Lpf_in2 = 5000*t_s*t_s*w_c*w_c / den;
Lpf_in3 = 2500*t_s*t_s*w_c*w_c / den;
Lpf_out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
Lpf_out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;

f_jvel = zeros(size(jvel(1:3,:)));
for j = 1:3
for i = 1: length(x)
    lpf_in = jvel(j,i);
    lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev(1) + Lpf_in3*Lpf_in_prev(2) + ... %input component
              Lpf_out1*Lpf_out_prev(1) + Lpf_out2*Lpf_out_prev(2); %output component
    Lpf_in_prev(2) = Lpf_in_prev(1);
    Lpf_in_prev(1) = lpf_in;
    Lpf_out_prev(2) = Lpf_out_prev(1);
    Lpf_out_prev(1) = lpf_out;
    
    f_jvel(j,i) = lpf_out;
end
end
figure
plot(x, jvel(1,:), x, f_jvel(1,:), 'r');
