subplot(311)
  hold on
plot(task_time, des_vel(3,:), '--', 'Linewidth', 3);
plot(task_time, vel_com(3,:),'r-','linewidth', 3);
hold off
ylabel('Height Vel ($m/s$)','fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(task_time, phase_changing_frame_task);
axis tight
%
subplot(312)
hold on
plot(task_time, ori_vel_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, ori_vel(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
ylabel('Pitch Vel ($rad/s$)', 'fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12);
fn_draw_phase(task_time, phase_changing_frame_task);
xlabel('Time (sec)','fontsize', 12);
axis tight

subplot(313)
  hold on
plot(task_time, des_vel(1,:), '--', 'Linewidth', 3);
plot(task_time, vel_com(1,:),'r-','linewidth', 3);
hold off
ylabel('XVel ($m/s$)','fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12, 'XTick', []);
fn_draw_phase(task_time, phase_changing_frame_task);
axis tight