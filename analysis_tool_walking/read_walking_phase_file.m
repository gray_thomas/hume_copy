%%%%%% Phase 
%% X
x_phase_path_cell = cell (num_plan,3); %phase_path, nx_phase_path, int_phase_path
y_phase_path_cell  = cell (num_plan, 3);

for i = plan_start:num_plan
fname_x_phase_path = sprintf('%s/%i_x_phase_path.txt', path, i);
fname_nx_phase_path = sprintf('%s/%i_next_x_path.txt',path, i);
fname_int_phase_path_x = sprintf('%s/%i_integrated_path_x.txt', path, i);

[x_phase_path_f, msg22] = fopen(fname_x_phase_path, 'rt');
[nx_phase_path_f, msg23] = fopen(fname_nx_phase_path, 'rt');


x_phase_path_cell{i,1} = fscanf(x_phase_path_f, '%f %f', [2, inf]);
x_phase_path_cell{i,2} = fscanf(nx_phase_path_f, '%f %f ', [2, inf]);


%% Y
fname_y_phase_path = sprintf('%s/%i_y_phase_path.txt',path, i);
fname_ny_phase_path = sprintf('%s/%i_next_y_path.txt',path, i);
fname_int_phase_path_y = sprintf('%s/%i_integrated_path_y.txt',path, i);

[y_phase_path_f, msg22] = fopen(fname_y_phase_path, 'rt');
[ny_phase_path_f, msg23] = fopen(fname_ny_phase_path, 'rt');


y_phase_path_cell{i,1} = fscanf(y_phase_path_f, '%f %f', [2, inf]);
y_phase_path_cell{i,2} = fscanf(ny_phase_path_f, '%f %f ', [2, inf]);

end

%COM
com_pos_pl = fn_read_file(path, 'com_pos_pl', 3);
frame_x = fn_read_file(path, 'inter_frame_x', 1); 
frame_y = fn_read_file(path, 'inter_frame_y', 1);

foot_pl = fn_read_file(path, 'foot_placement',3);
foot_st = fn_read_file(path, 'local_stance_foot', 3);

landing_loc = fn_read_file(path, 'landing_loc', 3);
filtered_vel = fn_read_file(path, 'filter_body_vel_x', 1);
filtered_pos = fn_read_file(path, 'filter_body_pos_x', 1);