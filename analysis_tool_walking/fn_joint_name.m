function name = fn_joint_name(i)
if i == 1
    name = 'Abduction';
elseif i==2
    name = 'Hip';
elseif i==3
    name = 'Knee';
else
    name = 'Unknown';
end

end