st_foot = 1;
end_foot = length(des_foot_vel);
time = [1: 1: end_foot-st_foot+1];
des_foot_cut = des_foot_vel(:, st_foot:end_foot);
foot_pos_cut = foot_vel(:, st_foot:end_foot);
subplot(211)
  hold on
plot(time, des_foot_cut(1,:), '--', 'Linewidth', 3);
plot(time, foot_pos_cut(1,:),'r-','linewidth', 3);
hold off
ylabel('Foot Vel( $x$)','fontsize',12, 'Interpreter', 'Latex')
set(gca, 'fontsize',12, 'XTick', []);
axis tight
%
subplot(212)
hold on
plot(time, des_foot_cut(3, :), '--', 'linewidth', 3);
plot(time, foot_pos_cut(3, :),  'r-', 'linewidth', 3);
hold off
ylabel('Foot Vel($z$)', 'fontsize',12, 'interpreter', 'latex')
set(gca, 'fontsize',12);
axis tight