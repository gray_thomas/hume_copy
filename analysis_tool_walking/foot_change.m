left_land = find(pos_left(3,:) < 0.0253);
right_land = find(pos_right(3,:) < 0.0253);

change_frame_left = 1;
change_frame_right = 1;

k = 1;
for i =2:length(left_land)
  if(left_land(i) - left_land(i-1) ~= 1)
    change_frame_left(k) = left_land(i-1);
    change_frame_left(k+1) = left_land(i);
    k = k+2;
  end
end

k = 1;
for i =2:length(right_land)
  if(right_land(i) - right_land(i-1) ~= 1)
    change_frame_right(k) = right_land(i-1);
    change_frame_right(k+1) = right_land(i);
    k = k+2;
  end
end

