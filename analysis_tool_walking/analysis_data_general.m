clear all
clc 
close all
fclose all;
%% 
% path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot/m3bip/experiment_data';

read_walking_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 9;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height;
    position_x = original_x;
  end
end

% shortening the length
min_length = min(length(x), length(lcontact));
x = x(1:min_length);
rcontact = rcontact(1:min_length);
lcontact = lcontact(1:min_length);
task_time = linspace(0,1, length(pos_com));
task_length = length(task_time);
phase_task = phase(length(phase) - task_length + 1: end);
%% Phase Changing Frame
j = 1;
phase_changing_frame = 1;
phase_changing_frame_task = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end

j = 1;
for i = 1:length(phase_task)-1
  if(phase_task(i+1) ~= phase_task(i))
    phase_changing_frame_task(j) = i+1;
    j = j+1;
  end
end

%% Draw Figure
% Torque
figure(fig(1))

for i = 1:3
  tmp(i) = subplot(3,1,i)
  hold on
  plot(x, gamma(i,1:min_length) ,'b--','linewidth',5);
  plot(x, torque(i,1:min_length) ,'r-', 'linewidth',1.2);
    plot(x, rcontact*10);   
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  %ylim([-70, 10])  
  set(gca, 'fontsize',12);
  fn_draw_phase(x, phase_changing_frame);

end
title(tmp(1), 'Torque (right)');
xlabel('Time (sec)','fontsize', 12);

figure(fig(2))
for i = 1:3
  tmp(i) = subplot(3,1,i)
  hold on
  plot(x, gamma(i+3,1:min_length) ,'b--','linewidth',3);
  plot(x, torque(i+3,1:min_length) ,'r-', 'linewidth',1.2);
      plot(x, lcontact*10);
  hold off
  if(i~=3) set(gca, 'XTick',[]); end
  %ylim([-70, 10])    
  set(gca, 'fontsize',12);
    fn_draw_phase(x, phase_changing_frame);    

end
xlabel('Time (sec)','fontsize', 12);
title(tmp(1), 'Torque (left)');
%%%%%%%%%%%%%%%%%%%   END of Torque %%%%%%%%%%%%

%% Foot Pos

figure(fig(7))
% Draw_Cartesian(x, zeros(size(force_left)), force_left, phase_changing_frame, 'Left Foot Force');
Draw_Cartesian(linspace(0,1, length(des_foot)), des_foot, pos_foot, [1], 'Foot Pos');

%%  foot force

figure(fig(8))
Draw_Cartesian(linspace(0,1, length(des_foot_vel)), des_foot_vel, vel_foot, [1], 'foot vel');

figure(fig(9))
Draw_Cartesian(x, zeros(size(force_left)), force_left, phase_changing_frame, 'left foot force');


%% %%%%%%%%%%%%%%%% Joint Position
% figure(fig(3))
% for i = 1:3
%   subplot(3,1,i)
%   plot(x, conf(i+6,1:min_length), 'LineWidth', 3)
%   if(i~=3) set(gca, 'XTick',[]); end
%   set(gca, 'fontsize',12);
% end
% xlabel('Time (sec)','fontsize', 12);
% title('Joint Position (right)', 'fontsize', 15);
% 
% figure(fig(4))
% for i = 1:3
%   subplot(3,1,i)
%   hold on 
%   plot(x, conf(i+9,1:min_length), 'LineWidth', 3);
% 
%   hold off
%     fn_draw_phase(x, phase_changing_frame);
% end
% xlabel('Time (sec)','fontsize', 12);
% title('Joint Position (left)', 'fontsize', 15);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%% Joint Velocity
figure(fig(3))
for i = 1:3
  subplot(3,1,i)
  plot(x, jvel(i+6,1:min_length), 'LineWidth', 3)
  if(i~=3) set(gca, 'XTick',[]); end
  set(gca, 'fontsize',12);
end
xlabel('Time (sec)','fontsize', 12);
title('Joint vel (right)', 'fontsize', 15);

figure(fig(4))
for i = 1:3
  subplot(3,1,i)
  hold on 
  plot(x, jvel(i+9,1:min_length), 'LineWidth', 3);

  hold off
    fn_draw_phase(x, phase_changing_frame);
end
xlabel('Time (sec)','fontsize', 12);
title('Joint vel (left)', 'fontsize', 15);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Cartesian Space
figure(fig(5))

subplot(4,1, 1)
  hold on
plot(task_time, des_com(3,:), '--', 'Linewidth', 3);
plot(task_time, pos_com(3,:),'r-','linewidth', 3);
hold off
%  draw_phase(task_time, phase_changing_frame_task);
ylabel('Height','fontsize',12)
xlabel('Time (sec)','fontsize', 12);
%set(gca, 'fontsize',12, 'XTick', []);
axis tight

subplot(412)
hold on
plot(task_time, orientation_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(2, 1:task_length),  'r-', 'linewidth', 3);
hold off
%  draw_phase(task_time, phase_changing_frame_task);
ylabel('Pitch', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);
xlabel('Time (sec)','fontsize', 12);
ylim([-0.2, 0.2])

subplot(413)
hold on
plot(task_time, orientation_d(3, 1:task_length), '--', 'linewidth', 3);
plot(task_time, orientation(3, 1:task_length),  'r-', 'linewidth', 3);
hold off
% draw_phase(task_time, phase_changing_frame_task);
ylabel('Roll', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);

xlabel('Time (sec)','fontsize', 12);

subplot(414)
hold on
plot(task_time, des_com(1, :), '--', 'linewidth', 3);
plot(task_time, pos_com(1, :),  'r-', 'linewidth', 3);
hold off
% draw_phase(task_time, phase_changing_frame_task);
ylabel('X', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);
ylim([-.2, 0.2])

xlabel('Time (sec)','fontsize', 12);


% Cartesian Space
figure(fig(6))

subplot(3,1, 1)
  hold on
plot(task_time, des_vel(1,:), '--', 'Linewidth', 3);
plot(task_time, jvel(1,length(jvel)-task_length + 1: end),'r-','linewidth', 3);
hold off
ylabel('Vel X','fontsize',12)
set(gca, 'fontsize',12, 'XTick', []);
axis tight
  fn_draw_phase(task_time, phase_changing_frame_task)
ylim([-1.0, 1.0])
  
subplot(312)
hold on
plot(task_time, des_vel(1, 1:task_length), '--', 'linewidth', 3);
plot(task_time, vel_com(1, 1:task_length), 'r-', 'linewidth',3);
hold off
ylabel('\dot{X}', 'fontsize',12)
  fn_draw_phase(task_time, phase_changing_frame_task);
  
axis tight
set(gca, 'fontsize',12, 'XTick', []);
%
subplot(313)
hold on
%plot(x, ori_vel_d(2, 1:task_length), '--', 'linewidth', 3);
plot(task_time, des_vel(2,:), '--', 'Linewidth', 3);
plot(x, jvel(5, :),  'r-', 'linewidth', 3);
hold off
ylabel('Vel Pitch', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);
  fn_draw_phase(x, phase_changing_frame);
xlabel('Time (sec)','fontsize', 12);
ylim([-.2, 0.2])

figure
hold on
plot(des_com(1,:), des_com(3,:))
plot(pos_com(1,:), des_com(3,:), 'r')
hold off
axis equal


