skip = false;

if(~skip)
clear all
clc 
close all
%%
plan_start = 1;
num_plan =10;
% path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot/m3bip/experiment_data';
read_configure_file;
read_walking_phase_file;
rebuild_com;
end

global com_x_offset
com_x_offset = -0.0;
%filter = 'butterworth';
%filter = 'low_pass';
filter = 'c++';

%%%%%%%%%% Array Window %%%%%%%%%%%%%%%%%%%%%%%%%
original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 460;
increase_x = 560;
increase_y = 820;

x_limit = 1800;
y_plot = 0;
for i = 1:(num_plan - plan_start+1)*(1 + y_plot)
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y - increase_y;
    position_x = original_x;
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
parcing_phase;

j = 1;
phase_changing_frame = 1;
phase_changing_frame_task = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end

axis_font_size = 30;
font_size = 35;
line_width = 6;
%foot_change
kk = 1;
%%%%%%% Phase space 
next_stance_l = mod(plan_start,2);
next_stance_r = -mod(plan_start,2)+1;
vel_filter = false;

%%%%%%   Low Pass Filter  %%%%%%%%%%%%%%%%%%%%%%
t_s = x(end)/length(x);
w_c = 25;

for jj = plan_start: num_plan
  %% X
  ori = 0.0;
  x_phase_path = x_phase_path_cell{jj,1};
  nx_phase_path = x_phase_path_cell{jj,2};
  figure(fig(jj - plan_start + 1))
  
  % Frame
  start_fr = stance_frame(jj) +2 ;
  middle_fr_end = stance_frame(jj+1) -2;
  middle_fr_start = stance_frame(jj+1) +2;
  end_fr = stance_frame(jj+2) -2;
  %
  start_supp_lift_trans = min(find(phase_changing_frame > start_fr));
  start_supp_lift_trans = phase_changing_frame(start_supp_lift_trans);
  %
  end_supp_lift_trans = min(find(phase_changing_frame>start_supp_lift_trans));
  end_supp_lift_trans = phase_changing_frame(end_supp_lift_trans);
  end_supp_lift_trans  = end_supp_lift_trans -1;
  %
  start_land = min(find(phase_changing_frame > end_supp_lift_trans + 1));
  start_land = phase_changing_frame(start_land);
  %
  start_land_supp_trans = min(find(phase_changing_frame> start_land));
  start_land_supp_trans = phase_changing_frame(start_land_supp_trans);
  %Foot
  plot_foot_place(0, jj, ori, com_pos_pl(:,jj), foot_pl(:,jj), ...
                  foot_st(:,jj), landing_loc(:, jj));
              
              
  % Planned Phase Path
  idx_zero_x = 1;
  for kkk = 1:length(nx_phase_path)
      if(abs(nx_phase_path(2,kkk)) < 1.5)
          idx_zero_x = kkk;
      end

  end
     if(idx_zero_x>500)
          idx_zero_x = 500;
      end
  hold on
  plot(x_phase_path(1,:), x_phase_path(2,:), 'color','black','linewidth', line_width);
  plot(nx_phase_path(1,1:idx_zero_x), nx_phase_path(2,1:idx_zero_x), '--', 'linewidth', line_width);
  hold off 
  
  % filter
  switch filter
      case 'nothing'
          pos_com_1 = pos_com(:,start_fr:middle_fr_end);
          pos_com_2 = pos_com(:,middle_fr_start:end_fr);
      case 'low_pass'
        vel_com_1 = fn_low_pass_filter(vel_com(1:3, start_fr:middle_fr_end), w_c, t_s);
        vel_com_2 = fn_low_pass_filter(vel_com(1:3, middle_fr_start:end_fr), w_c, t_s);
      case 'butterworth'
        num_sample = 150;
        w_c = 50;
        vel_com_1 = fn_butterworth_filter(vel_com(1:3, start_fr:middle_fr_end), num_sample, w_c, t_s);
        vel_com_2 = fn_butterworth_filter(vel_com(1:3, middle_fr_start:end_fr), num_sample, w_c, t_s);
      case 'c++'
        vel_com_1 = zeros(3, middle_fr_end - start_fr +1);
        vel_com_1(1,:) = filtered_vel(start_fr:middle_fr_end);
        vel_com_2 = zeros(3, end_fr - middle_fr_start +1);
        vel_com_2(1,:) = filtered_vel(middle_fr_start:end_fr);
        
        
        pos_com_1 = pos_com(:, start_fr:middle_fr_end);
        pos_com_2 = pos_com(:, middle_fr_start:end_fr);
  end
  % phase portrait  
    plot_phase_portrait(0,ori, com_pos_pl(:,jj), ...
                        pos_com_1, ...
                        vel_com_1, ...
                        start_supp_lift_trans - start_fr, ...
                        end_supp_lift_trans - start_fr, ...
                        start_land_supp_trans - start_fr, 'r', 2.5);
    plot_phase_portrait_2(0,ori, pos_com_1(:,end) - com_pos_pl(:,jj), ... 
                           pos_com_2, ...
                           vel_com_2, 'g', 2.5);
                       
    plot_phase_portrait(0,ori, com_pos_pl(:,jj), ...
                        pos_com(:,start_fr:middle_fr_end), ...
                        vel_com(:,start_fr:middle_fr_end), ...
                        start_supp_lift_trans - start_fr, ...
                        end_supp_lift_trans - start_fr, ...
                        start_land_supp_trans - start_fr,'c', 1.5);
    plot_phase_portrait_2(0,ori, pos_com(:,middle_fr_end) - com_pos_pl(:,jj), ... 
                           pos_com(:,middle_fr_start:end_fr), ...
                           vel_com(:, middle_fr_start:end_fr), 'm', 1.5);
  set(gca, 'fontsize', axis_font_size);
  xlabel('$x$','interpreter', 'latex','fontsize',font_size);
  ylabel('$\dot{x}$','interpreter', 'latex', 'fontsize', font_size);
end

fclose all;

