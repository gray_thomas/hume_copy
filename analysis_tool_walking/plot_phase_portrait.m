function ret = plot_phase_portrait(axis, theta, com_pos_pl, ...
                                   pos_com, vel_com, ...
                                   start_supp_lift_trans, ...
                                   end_supp_lift_trans, ...
                                   start_land_supp_trans, ...
                                   color, ...
                                   linesize)
                                   

global com_x_offset

offset_com_pos = pos_com - com_pos_pl*ones(1, length(pos_com(1,:)));

local_com_pos = offset_com_pos(1,:)*cos(theta + axis*0.5*pi) + ...
                offset_com_pos(2,:)*sin(theta + axis*0.5*pi) + ...
                ones(1,length(offset_com_pos))*com_x_offset;
local_vel_com = vel_com(1,:)*cos(theta + axis*0.5*pi) + ...
                vel_com(2,:)*sin(theta + axis*0.5*pi);
hold on
plot(local_com_pos, local_vel_com, color, 'linewidth', linesize);
% plot(local_com_pos(start_supp_lift_trans), ...
%      local_vel_com(start_supp_lift_trans), 'b*', 'markers',12);
% plot(local_com_pos(end_supp_lift_trans), ...
%      local_vel_com(end_supp_lift_trans), 'b*','markers',14);
% plot(local_com_pos(start_land_supp_trans), ...
%      local_vel_com(start_land_supp_trans), 'b*', 'markers', 10);
hold off
j = 1;            
for i = 1:length(local_com_pos)
    if(i == 50*(j-1) + 1)
    sparce_pos(j) = local_com_pos(i);
    sparce_vel(j) = local_vel_com(i);
    j = j+1;
    end
end
hold on
% plot(local_com_pos, local_vel_com, 'color','red','maker','+'); %, 'linewidth', 1.5);

hold off
end