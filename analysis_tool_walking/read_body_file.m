x = fn_read_file(path, 'time', 1);
% Desired
des_com  = fn_read_file(path, 'Body_des', 3);
des_vel = fn_read_file(path, 'Body_vel_des', 3);
orientation_d  = fn_read_file(path, 'Ori_des', 3);
ori_vel_d = fn_read_file(path, 'Ori_vel_des',3);
gamma    = fn_read_file(path, 'gamma',6);

pos_com   = fn_read_file(path, 'Body_pos', 3);
vel_com   = fn_read_file(path, 'Body_vel',3);
orientation = fn_read_file(path, 'Body_ori',3);
ori_vel = fn_read_file(path, 'Body_ori_vel',3);

torque = fn_read_file(path, 'torque', 6);
conf   = fn_read_file(path, 'curr_conf', 12);
jvel = fn_read_file(path, 'curr_vel', 12);


force_left = fn_read_file(path, 'Left_foot_force', 3);
force_right = fn_read_file(path, 'Right_foot_force', 3);    

phase = fn_read_file(path, 'phase', 1);
