function ret = fn_draw_phase_block(x, phase_changing_frame, phase)
    hold on
    ylimit = get(gca, 'ylim');
    for j = 1:length(phase_changing_frame)-1
      line([x(phase_changing_frame(j)), x(phase_changing_frame(j))], ...
           [ ylimit(1), ylimit(2)], 'color','k');
      % Fill
      if((phase(phase_changing_frame(j)) == 2) || ...
         (phase(phase_changing_frame(j)) == 3) )
         % Fill Right Swing
         h = fill([x(phase_changing_frame(j)), x(phase_changing_frame(j)),...
                   x(phase_changing_frame(j+1)), x(phase_changing_frame(j+1))], ...
                  [ylimit(1), ylimit(2), ylimit(2), ylimit(1)],'y');
         set(h, 'facealpha', 0.2);

      elseif( (phase(phase_changing_frame(j)) == 7) || ...
              (phase(phase_changing_frame(j)) == 8) )
          % Fill Left Swing
         h = fill([x(phase_changing_frame(j)), x(phase_changing_frame(j)),...
               x(phase_changing_frame(j+1)), x(phase_changing_frame(j+1))], ...
              [ ylimit(1), ylimit(2), ylimit(2), ylimit(1)],'g'); %, 'facealpha',0.2);
          set(h, 'facealpha', 0.2);
      end
    end
    hold off
    
end