previous_num = 0;
phase_cropped_1 = phase;
phase_cropped_6 = phase;

cropping_num = 0;
acc_cropping_num = 0;
for j = 1: num_plan+1
    stance_frame_1 = min(find(phase_cropped_1 == 0));
    stance_frame_6 = min(find(phase_cropped_6 == 5));
    if( stance_frame_6 & stance_frame_1)
        stance_frame(j) = min( stance_frame_1 + acc_cropping_num, ... 
                               stance_frame_6 + acc_cropping_num);
        cropping_num = max(stance_frame_1, stance_frame_6);
        phase_cropped_1 = phase_cropped_1(cropping_num:end);
        phase_cropped_6 = phase_cropped_6(cropping_num:end);
        acc_cropping_num = acc_cropping_num + cropping_num -1;
        
    else
        if(stance_frame_1)
            stance_frame(j) = stance_frame_1 + acc_cropping_num;
        else
            stance_frame(j) = stance_frame_6 + acc_cropping_num;
        end
    end
    
end
stance_frame(j+1) = min(stance_frame(j) + 20, length(phase));