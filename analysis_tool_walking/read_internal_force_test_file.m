x = fn_read_file(path, 'time', 1);

torque = fn_read_file(path, 'torque', 6);
command = fn_read_file(path, 'gamma', 6);
% Body
des_com  = fn_read_file(path, 'Body_des', 3);
pos_com   = fn_read_file(path, 'Body_pos', 3);
orientation_d  = fn_read_file(path, 'Ori_des', 3);
orientation = fn_read_file(path, 'Body_ori',3);


force_left = fn_read_file(path, 'Left_foot_force', 3);
force_right = fn_read_file(path, 'Right_foot_force', 3);    
curr_force_left = fn_read_file(path, 'Curr_Left_foot_force', 3);
curr_force_right = fn_read_file(path, 'Curr_Right_foot_force', 3);

des_int_force = fn_read_file(path, 'Des_internal_force', 2);
ori_int_force = fn_read_file(path, 'Ori_internal_force', 2);

Wint = fn_read_matrix_file(path, 'Wint_1', 6);
Ainv = fn_read_matrix_file(path, 'Ainv_1', 12);
Js = fn_read_matrix_file(path, 'Js_1', 12);
U = fn_read_matrix_file(path, 'U_1', 12);
grav = fn_read_matrix_file(path, 'grav_1', 12);
phase = fn_read_file(path, 'phase', 1);