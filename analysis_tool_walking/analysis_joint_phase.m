clear all
clc 
close all
fclose all;
%% 
%path = '~/new_control_pc_mekabot/m3bip/experiment_data';
path = '~/mekabot/m3bip/experiment_data';

read_configure_file;
colour = ['b'; 'c'; 'r'; 'g'; 'y'; 
          'b'; 'c'; 'r'; 'g'; 'y'; ];
%%%%%%%%%% Array Window %%%%%%%%%%%%%%%%%%%%%%%%%
original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 460;
increase_x = 560;
increase_y = 660;
num_figure = 7;

x_limit = 1800;
y_plot = 0;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y - increase_y;
    position_x = original_x;
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

%% Phase Changing Frame
num_plan = 15;
parcing_phase;

%% Draw Figure

start_step = 5;
end_step = 10;

for kk = 7:12

figure(fig(kk - 6))
hold on
for i = start_step:end_step
start_fr = stance_frame(i) ;
end_fr = stance_frame(i+1) ;

plot(conf(kk, start_fr:end_fr), jvel(kk,start_fr:end_fr), colour(i));
end
hold off

end


figure(fig(7))
hold on
for i = start_step:end_step
start_fr = stance_frame(i) ;
end_fr = stance_frame(i+1) ;

plot(conf(5, start_fr:end_fr), jvel(5,start_fr:end_fr), colour(i));
end
hold off
