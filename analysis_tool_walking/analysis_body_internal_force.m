clear all
clc 
close all
fclose all;

font_size = 16;
%% 
path = '~/new_control_pc_mekabot/m3bip/experiment_data';
% path = '~/mekabot/m3bip/experiment_data';

% Read File
read_internal_force_test_file;

% Create Window
fig = fn_create_window(5);


%% Phase Changing Frame
j = 1;
phase_changing_frame = 1;
phase_changing_frame_task = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end
task_length = length(x) - phase_changing_frame(1);
cut_end_part = 2000;

des_com = des_com(:, end - task_length:end - cut_end_part);
pos_com = pos_com(:, end - task_length:end - cut_end_part);
orientation = orientation(:, end - task_length:end - cut_end_part);
orientation_d = orientation_d(:, end - task_length:end - cut_end_part);
task_time = x(end - task_length:end - cut_end_part);


%% Draw Figure
% Cartesian Circle
figure(fig(1))
hold on
plot(des_com(1,:), des_com(3,:), 'b--', 'linewidth',3);
plot(pos_com(1,:), pos_com(3,:), 'r-');
axis equal
hold off
set(gca, 'fontsize',font_size);
xlabel('X ($m$)', 'interpreter', 'latex','fontsize', font_size);
ylabel('Z ($m$)', 'interpreter', 'latex','fontsize', font_size);
grid on
axis tight
%%
figure(fig(2))
subplot(211)
hold on
plot(task_time, orientation_d(2, :), '--', 'linewidth', 3);
plot(task_time, orientation(2, :),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'Pitch ($rad$)', font_size);
axis tight

subplot(212)
hold on
plot(task_time, orientation_d(3, :), '--', 'linewidth', 3);
plot(task_time, orientation(3, :),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'Roll ($rad$)', font_size);
axis tight

xlabel('Time (sec)', 'interpreter', 'latex', 'fontsize', font_size);

%%
figure(fig(3))
subplot(211)
hold on
plot(task_time, des_com(1, :), '--', 'linewidth', 3);
plot(task_time, pos_com(1, :),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'X ($m$)', font_size);
axis tight

subplot(212)
hold on
plot(task_time, des_com(3, :), '--', 'linewidth', 3);
plot(task_time, pos_com(3, :),  'r-', 'linewidth', 3);
hold off
fn_latex_label('y', 'Z ($m$)', font_size);
axis tight

xlabel('Time (sec)', 'interpreter', 'latex', 'fontsize', font_size);


%%

figure(fig(4))
% calculate internal force

Wint_cut = Wint(end - task_length:end - cut_end_part);
force_left = force_left(:, end- task_length:end - cut_end_part);
force_right = force_right(:, end- task_length:end - cut_end_part);

Js = Js(end - task_length:end - cut_end_part);
Ainv = Ainv(end - task_length: end - cut_end_part);
torque = torque(:, end - task_length: end - cut_end_part);
command = command(:, end - task_length: end - cut_end_part);

U = U(end - task_length: end - cut_end_part);
grav = grav(end - task_length: end - cut_end_part);

curr_force_left = curr_force_left(:, end- task_length:end - cut_end_part);
curr_force_right = curr_force_right(:, end- task_length:end - cut_end_part);
des_int_force = des_int_force(:, end - task_length:end - cut_end_part);

Fr = zeros(6,1);

for i = 1: length(Js)
    Js_right = Js{i}(1:3, :);
    Js_left = Js{i}(4:6, :);
    Js_bar_right = Ainv{i} * Js_right.' * pinv(Js_right * Ainv{i} * Js_right.');
    Js_bar_left = Ainv{i} * Js_left.' * pinv(Js_left * Ainv{i} * Js_left.');
    Fr(:,i) = [Js_bar_right.' * U{i}.' *torque(:,i) - Js_bar_right.' * grav{i}.';
        Js_bar_left.' * U{i}.' *torque(:,i) - Js_bar_left.' * grav{i}.';];
end

Fr = zeros(6,1);

for i = 1: length(Js)
    Js_bar = Ainv{i} * Js{i}.' * pinv(Js{i} * Ainv{i} * Js{i}.');
    Fr(:,i) = Js_bar.' * U{i}.' *torque(:,i) - Js_bar.' * grav{i}.';
end


% Reaction Force
figure
for i =1:3
    subplot(3,1,i);
    plot(task_time, curr_force_right(i,:), task_time, Fr(i,:));
end

figure
for i =1:3
    subplot(3,1,i);
    plot(task_time, curr_force_left(i,:), task_time, Fr(i+3,:));
end

% Force
figure
for i =1:3
    subplot(3,1,i);
    plot(task_time, torque(i,:), task_time, command(i,:));
end

figure
for i =1:3
    subplot(3,1,i);
    plot(task_time, torque(i+3,:), task_time, command(i+3,:));
end

int_force = zeros(2,1);
act_int_force = zeros(2,1);
for i = 1: length(force_right)
    int_force(:,i) = Wint_cut{i}*([force_right(:,i);force_left(:,i)]);
    act_int_force(:,i) = Wint_cut{i} * ([curr_force_right(:,i); curr_force_left(:,i)]);
end


act_int_force_dyn_consistent = zeros(2,1);
for i = 1: length(force_right)
    act_int_force_dyn_consistent(:,i) = Wint_cut{i} * Fr(:,i);
end

figure
for i = 1:2
    subplot(2,1,i)
    hold on
    plot(task_time, des_int_force(i,:),'c', 'linewidth',4);
    plot(task_time, int_force(i,:), 'b--', 'linewidth',3);
    plot(task_time, act_int_force_dyn_consistent(i,:), 'r-');
    switch(i)
        case 1
            ylabel('X ($N$)', 'interpreter', 'latex', 'fontsize',font_size)
        case 2
            ylabel('Y ($N$)', 'interpreter', 'latex', 'fontsize',font_size);
    end
    % axis setting
    axis tight
    set(gca, 'fontsize',font_size);
    hold off
end
xlabel('$\mbox{Time(sec)}$', 'interpreter','latex', 'fontsize', font_size);

for i = 1:2
    subplot(2,1,i)
    hold on
    plot(task_time, des_int_force(i,:),'c', 'linewidth',4);
    plot(task_time, int_force(i,:), 'b--', 'linewidth',3);
    plot(task_time, act_int_force(i,:), 'r-');
    switch(i)
        case 1
            ylabel('X ($N$)', 'interpreter', 'latex', 'fontsize',font_size)
        case 2
            ylabel('Y ($N$)', 'interpreter', 'latex', 'fontsize',font_size);
    end
    % axis setting
    axis tight
    set(gca, 'fontsize',font_size);
    hold off
end
xlabel('$\mbox{Time(sec)}$', 'interpreter','latex', 'fontsize', font_size);




%% 
figure(fig(5))
subplot(4,1, 1)
hold on
plot(task_time, des_com(3,:), '--', 'Linewidth', 3);
plot(task_time, pos_com(3,:),'r-','linewidth', 3);
hold off
fn_latex_label('y', 'Height', font_size);
axis tight

subplot(412)
hold on
plot(task_time, orientation_d(2, :), '--', 'linewidth', 3);
plot(task_time, orientation(2, :),  'r-', 'linewidth', 3);
hold off
ylabel('Pitch', 'fontsize',12)
axis tight

subplot(413)
hold on
plot(task_time, orientation_d(3, :), '--', 'linewidth', 3);
plot(task_time, orientation(3, :),  'r-', 'linewidth', 3);
hold off
% draw_phase(task_time, phase_changing_frame_task);
ylabel('Roll', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);

subplot(414)
hold on
plot(task_time, des_com(1, :), '--', 'linewidth', 3);
plot(task_time, pos_com(1, :),  'r-', 'linewidth', 3);
hold off
% draw_phase(task_time, phase_changing_frame_task);
ylabel('X', 'fontsize',12)
axis tight
  set(gca, 'fontsize',12);

xlabel('Time (sec)','fontsize', 12);
