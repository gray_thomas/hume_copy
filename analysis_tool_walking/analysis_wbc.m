
% clear all
% clc
% path = '~/mekabot/m3bip/experiment_data';
% read_matrix_file
%%
path = '~/new_control_pc_mekabot/m3bip/experiment_data';




len_matrix = length(Ainv_1);
   
idx = 210

Ainv = Ainv_1{idx};

Js = Js_1{idx}
Jt = Jt_1{idx};

%  Jt = [Jt(1:2, :); Jt(4:6,:)];
 add_row = zeros(3, 12);
add_row(1, 6) = 1.0;
 add_row(2, 2) = 1.0;
% add_row(3, 4) = 1.0;
 Js = [add_row; Js_1{idx}];

grav = grav_1{idx};
U = U_1{idx};

JsBar = Ainv*Js.' * pinv(Js * Ainv * Js.');
Ns = eye(size(Ainv)) - JsBar*Js;
UNs = U*Ns;
phi = UNs*Ainv*UNs.';

UNsBar = Ainv*UNs.' * pinv(UNs * Ainv * UNs.');
Jts = Jt*Ns;
JtsBar = Ainv*Jts.'*inv(Jts * Ainv * Jts.');
Jstar = Jts * UNsBar;
Lambda_ts = inv(Jts*Ainv*Jts.');
% Jstar.'*Lambda_ts * [10, 0, 0, 0, 0, 0].' + Jstar.'*JtsBar.'*grav.'
Jstar.'*Lambda_ts * [10, 10, 10, 0, 50].' + Jstar.'*JtsBar.'*grav.'

% tau_input = Jstar.'*Lambda_ts * [1, 1, 0].' + Jstar.'*JtsBar.'*grav.'

% Internal Force
Lstar = eye(6,6) - UNs*UNsBar


% tmp = Jts*Ainv*UNs.'
% pinv(Jts*Ainv*UNs.')
% pinv(Lambda_ts*Jts*Ainv*UNs.')*Lambda_ts
% pinv(Jts.'*Lambda_ts*Jts*Ainv*UNs.')*Jts.'*Lambda_ts
% pinv(UNsBar.'*Jts.'*Lambda_ts*Jts*Ainv*UNs.')*UNsBar.'*Jts.'*Lambda_ts
% Jstar.'*Lambda_ts
% UNsBar.'*Jts.'*Lambda_ts