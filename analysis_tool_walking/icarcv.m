clear all
clc 
close all
fclose all;
%% 

read_file;
original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 2;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height-100;
    position_x = original_x;
  end
end

% shortening the length
min_length = 2000;
x = x(1:min_length);
phase = phase(1:min_length);

%% Phase Changing Frame
j = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end

%% Draw Figure
% Torque

line_width = 2.5;
figure(fig(1))
subplot(2,1,1)
hold on
plot(x, torque(4,1:min_length) ,'r-', 'linewidth',line_width);
plot(x, torque(5,1:min_length) ,'g--', 'linewidth',line_width);
plot(x, torque(6,1:min_length) ,'b-.', 'linewidth',line_width);
hold off
draw_phase;
legend('Abduction','Hip','Knee');
subplot(2,1,2)
hold on
plot(x, torque(1,1:min_length) ,'r-', 'linewidth',line_width);
plot(x, torque(2,1:min_length) ,'g--', 'linewidth',line_width);
plot(x, torque(3,1:min_length) ,'b-.', 'linewidth',line_width);
hold off
draw_phase;
xlabel('Time (sec)','fontsize', 12);

% Joint Velocity
figure(fig(2))
subplot(211)
hold on
plot(x, jvel(4,1:min_length), 'r-', 'LineWidth', 3)
plot(x, jvel(5,1:min_length) ,'g--', 'linewidth', 3);
plot(x, jvel(6,1:min_length) ,'b-.', 'linewidth', 3);
hold off
draw_phase;
legend('Abduction', 'Hip', 'Knee')
subplot(212)
hold on
plot(x, jvel(1,1:min_length), 'r-', 'LineWidth', 3)
plot(x, jvel(2,1:min_length) ,'g--', 'linewidth', 3);
plot(x, jvel(3,1:min_length) ,'b-.', 'linewidth', 3);
hold off
draw_phase;
xlabel('Time (sec)','fontsize', 12);

close_file;
