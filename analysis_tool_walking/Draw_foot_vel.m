figure(fig(6))

offset_phase = 15;
offset_foot = 0;
des_foot_cut = des_foot_vel(:, 1:end - offset_foot);
foot_pos_cut = foot_vel(:, 1:end - offset_foot);

x = fn_read_file(path, 'time', 1);
phase = fn_read_file(path, 'phase', 1);

phase = phase(1:end-offset_phase);
x = x(1:end- offset_phase);
j = 1;
phase_changing_frame = 1;
for i = 1:length(phase)-1
  if(phase(i+1) ~= phase(i))
    phase_changing_frame(j) = i+1;
    j = j+1;
  end
end

% Build Foot Trajectory
len_phase = length(phase);
full_des_foot = zeros(3, len_phase);
full_pos_foot = zeros(3, len_phase);
j = length(des_foot_cut);
stuck = false;
for k = 1 : len_phase
   i = len_phase - k +1; 
   if ((phase(i) == 0) || ...
       (phase(i) == 5) || ...
       (phase(i) == 10))
       full_des_foot(:,i) = zeros(3,1);
       full_pos_foot(:,i) = zeros(3,1);
   else
       full_des_foot(:,i) = des_foot_cut(:,j);
       full_pos_foot(:,i) = foot_pos_cut(:,j);
       j = j-1;
   end   
end

subplot(311)
hold on
plot(x, full_des_foot(1,:), '-.', 'Linewidth', 3);
plot(x, full_pos_foot(1,:),'r--','linewidth', 3);
fn_draw_phase_block(x, phase_changing_frame, phase);
hold off
fn_latex_font_label('y', 'X (m/s)', font_size);
set(gca, 'XTick',[]);
axis tight
xlim([time_start, time_end])
%
subplot(312)
hold on
plot(x, full_des_foot(2,:), '-.', 'Linewidth', 3);
plot(x, full_pos_foot(2,:),'r--','linewidth', 3);
hold off
fn_draw_phase_block(x, phase_changing_frame, phase);
fn_latex_font_label('y', 'Y (m/s)', font_size);
set(gca, 'XTick',[]);
axis tight
xlim([time_start, time_end])
%
subplot(313)
hold on
plot(x, full_des_foot(3,:), '-.', 'Linewidth', 3);
plot(x, full_pos_foot(3,:),'r--','linewidth', 3);
hold off
fn_draw_phase_block(x, phase_changing_frame, phase);
fn_latex_font_label('y', 'Z (m/s)', font_size);
axis tight
xlim([time_start, time_end])


fn_latex_font_label('x', 'Time (sec)', font_size);

