clear all
clc 
close all

%% 

read_file;
read_phase_file;

original_x = 100;
position_x = original_x;
position_y = 700;
width = 560;
height = 420;
increase_x = 560;
increase_y = 420;

x_limit = 1800;

num_figure = 8;
for i = 1:num_figure
  fig(i) = figure('position', [position_x, position_y, width, height]);
  position_x = position_x + width;
  if( position_x + width > x_limit)
    position_y = position_y -height-100;
    position_x = original_x;
  end
end

figure(fig(1))
plot(x_phase_path(1,:), x_phase_path(2,:), '*');

figure(fig(2))
plot(nx_phase_path(1,:), nx_phase_path(2,:), '*');

figure(fig(3))
hold on 
plot(x_phase_path(1,:), x_phase_path(2,:), '*');
plot(nx_phase_path(1,:), nx_phase_path(2,:), 'r*');
hold off

figure(fig(4));
plot(int_phase_path_x(1,:), int_phase_path_x(2,:));


figure(fig(5))
plot(y_phase_path(1,:), y_phase_path(2,:), '*');

figure(fig(6))
plot(ny_phase_path(1,:), ny_phase_path(2,:), '*');

figure(fig(7))
hold on 
plot(y_phase_path(1,:), y_phase_path(2,:), '*');
plot(ny_phase_path(1,:), ny_phase_path(2,:), 'r*');
hold off

figure(fig(8));
plot(int_phase_path_y(1,:), int_phase_path_y(2,:));


%% Close File
close_file;